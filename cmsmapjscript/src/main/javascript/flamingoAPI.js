var metodosFlamingo={
	addEvent: function(elm, evType, fn, useCapture){
		if (elm.addEventListener){
			elm.addEventListener(evType, fn, useCapture);
			return true;
		} else if (elm.attachEvent) {
			var r = elm.attachEvent('on' + evType, fn);
			return r;
		} else {
			elm['on' + evType] = fn;
		}
	},
	FireEvent: function(element,event){
	    if (document.createEventObject){
	        // dispatch for IE
	        var evt = document.createEventObject();
	        return element.fireEvent('on'+event,evt)
	    }
	    else{
	        // dispatch for firefox + others
	        var evt = document.createEvent("HTMLEvents");
	        evt.initEvent(event, true, true ); // event type,bubbling,cancelable
	        return !element.dispatchEvent(evt);
	    }
	},
	getMovie: function (movieName) {
	    if (navigator.appName.indexOf("Microsoft") != -1) {
	        return window[movieName];
	    }else {
	        return document[movieName];
	    }
	}
}

function flamingo_onConfigComplete() {
	var app = metodosFlamingo.getMovie("flamingo");
	metodosFlamingo.FireEvent(app, "click");
}

