/*
 * Libreria metodosCartociudad para acceder a los servicios de cartociudad. 
 * Mario Pérez. Agosto 2008
 */
var metodosCartociudad = function(){};

metodosCartociudad.prototype = {
	ajaxRequest:function(url, type, postParams){
		// Ruta al proxy (necesario para hacer peticiones entre dominios)
		
		var scripts = document.getElementsByTagName('script');
	
		for (var i = 0; i < scripts.length; i++) {
			var match = scripts[i].src.replace(/%20/g , '').match(/^(.*?)cmsmapjscript\.js(\?\(\[?(.*?)\]?\))?$/);
			if (match != null) {
				scriptBase = match[1];
				break;
		   }
		}
		var proxy_url = scriptBase+'/proxy.php?c_path=' + encodeURIComponent(url);
		var xmlDoc ="";
		// Creamos el objeto XMLHttpRequest primero segun estandar, luego para IE
		var xmlhttp = null;
		if (window.XMLHttpRequest) {
			xmlhttp = new XMLHttpRequest();
			
			if (typeof xmlhttp.overrideMimeType != 'undefined') {
				xmlhttp.overrideMimeType('text/xml');
			}
		}
		else 
			if (window.ActiveXObject) {
				xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
			else {
				alert('Navegador incompatible');
			}
		
		// Hacemos una petición asincrona/sincrona (ultimo parametro a true/false)
		xmlhttp.open(type.method, proxy_url, type.async);
		
		xmlhttp.setRequestHeader("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
		xmlhttp.setRequestHeader("Cache-Control", "no-cache");
		
		xmlhttp.send(postParams);
		
		//Recogemos la respuesta xml (es importante fijar el parametro correspondiente del proxy adecuadamente)
		if (!type.async) {
			xmlDoc = xmlhttp.responseXML.documentElement;
			return xmlDoc;
		}
	
	},
	
	findCoordenadas:function(xmlDoc, address){
		if (OpenLayers.Ajax.getElementsByTagNameNS(xmlDoc,'http://www.opengis.net/gml', 'gml','Envelope')[0]) {
			var bbox = OpenLayers.Ajax.getElementsByTagNameNS(xmlDoc,'http://www.opengis.net/gml', 'gml','Envelope')[0].firstChild;
			while (bbox.nodeType != 1) {
				bbox = bbox.nextSibling; //Para quedarnos con el nodo de texto
			}
			var bounds = new Array();
			bounds[0]=bbox.firstChild.nodeValue;

			bbox = bbox.nextSibling;
			while (bbox.nodeType != 1) {
				bbox = bbox.nextSibling; //Para quedarnos con el nodo de texto
			}
			bounds[1]=bbox.firstChild.nodeValue;
			address.bounds = new OpenLayers.Bounds(bounds[0],bounds[1]);
		}
		if (OpenLayers.Ajax.getElementsByTagNameNS(xmlDoc,'http://www.opengis.net/gml', 'gml','Point')[0]) {
			var pos = OpenLayers.Ajax.getElementsByTagNameNS(xmlDoc,'http://www.opengis.net/gml', 'gml','Point')[0].firstChild;
			while (pos.nodeType != 1) {
				pos = pos.nextSibling; //Para quedarnos con el nodo de texto
			}
			pos = pos.firstChild;
			//Sustitucion debida a que el wfs-municipio separa coordenadas con espacio y wfs-portal con coma
			pos.nodeValue = pos.nodeValue.replace(/\s/g, ",");
			var coordenadas = pos.nodeValue.split(",");
			address.point = new OpenLayers.LonLat(coordenadas[0],coordenadas[1]);
			//Si no hay error del servidor ponemos el error a cero 
			address.error=0;
		}
		
		return address;
		
	},			
	
	sanitizeInput:function(input){
		input = input.replace(/\s/g, "%20"); //sustituimos caracteres especiales por hexadecimales
		input = input.replace(/á/g, "%E1"); 
		input = input.replace(/é/g, "%E9");
		input = input.replace(/í/g, "%ED");
		input = input.replace(/ó/g, "%F3");
		input = input.replace(/ú/g, "%FA");
		input = input.replace(/ñ/g, "%F1");
		input = input.replace(/,/g, "");
	
		return input;
	},
	
	normalizeStreetType:function(input){
		input = input.replace(/(c\/|c\.|c\/\.|calle)/i, "CALLE"); 	//El parámetro con el tipo de vial debe estar normalizado
		input = input.replace(/(av.*|avenida)/i, "AVDA");
		input = input.replace(/(pl.*|pz.*|plaza)/i, "PLAZA");
		input = input.replace(/(gta.*|glorieta)/i, "GTA");
		input = input.replace(/(ps.*|p\.º|paseo)/i, "PASEO");
		input = input.replace(/(ctra.*|carretera)/i, "CTRA");
		
		return input;
	},
	
	limpiarSalida:function(input){
		input = input.replace(/%20/g, " "); //sustituimos caracteres especiales por hexadecimales
		input = input.replace(/%E1/g, "á"); 
		input = input.replace(/%E9/g, "é");
		input = input.replace(/%ED/g, "í");
		input = input.replace(/%F3/g, "ó");
		input = input.replace(/%FA/g, "ú");
		input = input.replace(/%F1/g, "ñ");
		input = input.replace("CALLE", "C/");
		input = input.replace("AVDA", "Avda.");
		input = input.replace("PLAZA", "Pza.");
		input = input.replace("GTA", "Gta.");
		input = input.replace("PASEO", "Ps.");
		input = input.replace("CTRA", "Ctra.");
		
		return input;
	},
	
	addressToMapstraction:function(address){
			//limpiar popups y devolver puntos en formato mapstraction
			if (address.point && address.point !== '') {
				address.point = new LatLonPoint(address.point.lat, address.point.lon);
			}
			if (address.bounds && address.bounds !== '') {
				address.bounds = new BoundingBox(address.bounds.bottom, address.bounds.left, address.bounds.top, address.bounds.right);
			}
			if (address.region && address.region !== '') {
				address.region = this.limpiarSalida(address.region);
			}
			else {
				address.region = '';
			}
			if (address.locality && address.locality !== '') {
				address.locality = this.limpiarSalida(address.locality);
			}
			else {
				address.locality = '';
			}
			if (address.street && address.street !== '') {
				address.street = this.limpiarSalida(address.street);
				if (address.streetType && address.streetType !== '') {
					address.streetType = this.limpiarSalida(address.streetType);
				}
				else{
					address.streetType = '';
				}
			}
			else {
				address.street = '';
			}
			if (address.portal && address.portal !== '') {
				address.portal = this.limpiarSalida(address.portal);
			}
			else {
				address.portal = '';
			}
			if (address.CP && address.CP !== '') {
				address.CP = 'CP. ' + address.CP;
			}
			else {
				address.CP = '';
			}
	},
	
	capitalizar:function(entrada){
		entrada = entrada.toLowerCase();
		var letras = entrada.split('');
		letras[0] = letras[0].toUpperCase();
		entrada = letras.join('');

		return entrada;
	},
	
	separarDireccion:function(address){
		//expresiones regulares de troceo de direcciones postales
		var myRexp01 = /\s*(calle|c\/|c\.|c\/\.|av\w*\.*|pl\w*\.*|pz\w*\.*|gta\.*|glorieta|ps\.*|p\.*º|paseo|ctra\.*|carretera)*\s*([^,]*)?\s*,?\s*(?:c\.*p\.*|cod\.*pos\.*)*\s*(\d{5})?\s*,?\s*(?:municipio:)*\s*([^,]*)?\s*,?\s*([^,]*)?\s*/i;
		var myRexp02 = /\s*([^,]*)\s+(?:n\.*|nº|num\.*)*\s*(\d{1,4})*\s*$/; //si no se introduce portal al menos hay que introducir un espacio en blanco
		var myRexp03 = /(?:c\.*p\.*|cod\.*pos\.*)*\s*(\d{5})/;		//para busquedas solo por cp
		var myRexp04 = /(?:municipio:)\s*([^,]*)\s*,?\s*([^,]*)/; 	//para busquedas solo por municipio
		
		var campos = address.address.match(myRexp01);
		
		address.streetType = campos[1];
		
		//El campo con el nombre de calle es el que habra que contrastar dado que toma tres tipos de 
		//valores segun el tipo de busqueda que efectuemos
		if (campos[2] && campos[2] !== '') {
			var aux;
			if (campos[2].match(myRexp04)) {
				aux = campos[2].match(myRexp04);
				campos[5] = aux[2];
				campos[4] = aux[1];
			}
			else {
				if (campos[2].match(myRexp03)) {
					aux = campos[2].match(myRexp03);
					campos[3] = aux[1];
				}
				else {
					if (campos[2].match(myRexp02)) {
						aux = campos[2].match(myRexp02);
						address.portal = aux[2];
						address.street = aux[1];
					}
				}
			}
					
		}
		
		address.region = campos[5];
		if (address.region && address.region !== '') {
			address.region = this.capitalizar(address.region);
		}

		address.locality = campos[4];
		if (address.locality && address.locality !== '') {
			address.locality = this.capitalizar(address.locality);
		}
		address.CP = campos[3];
		
	},
	
	queryNomenclator:function(address){
		if (address.address && address.address !== '') {
			this.separarDireccion(address);
		}
		if (address.region && address.region !== ''){
			address.region = this.sanitizeInput(address.region);
		}
		if (address.locality && address.locality !== ''){
			address.locality = this.sanitizeInput(address.locality);
		}	
		if (address.street && address.street !== ''){
			address.street = this.sanitizeInput(address.street);
		}
		if (address.portal && address.portal !== ''){
			address.portal = this.sanitizeInput(address.portal);
		}	
		if (address.streetType && address.streetType !== ''){
			address.streetType = this.normalizeStreetType(address.streetType);
		}
		
		//Rutas a los servicios
		var wfsCP = "wfs-codigo/services?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&NAMESPACE=xmlns(app=http://www.deegree.org/app)&TYPENAME=app:Entidad&FILTER=";
		var wfsMunicipio = "wfs-municipio/services?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&NAMESPACE=xmlns(app=http://www.deegree.org/app)&TYPENAME=app:Entidad&FILTER=";
		var wfsVial = "wfs-vial/services?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&NAMESPACE=xmlns(app=http://www.deegree.org/app)&TYPENAME=app:Entidad&FILTER=";
		var wfsPortal = "wfs-portal/services?SERVICE=WFS&VERSION=1.1.0&REQUEST=GetFeature&NAMESPACE=xmlns(app=http://www.deegree.org/app)&TYPENAME=app:Entidad&FILTER=";
		
		var xml,url,filtro,errorCode;
		var requestType = new Object();
		
		//Si tenemos localidad buscamos por localidad (hay localidades que comparten cp)
		if (address.locality && address.locality !== '') {
			filtro = "%3CFilter%3E";
			if (address.region && address.region !== '') {
				filtro += "%3CAnd%3E%3CPropertyIsEqualTo%3E%3CPropertyName%3EnombreEntidad/nombre%3C/PropertyName%3E%3CLiteral%3E" + address.locality + "%3C/Literal%3E%3C/PropertyIsEqualTo%3E%3CPropertyIsEqualTo%3E%3CPropertyName%3EentidadLocal/provincia%3C/PropertyName%3E%3CLiteral%3E" + address.region + "%3C/Literal%3E%3C/PropertyIsEqualTo%3E%3C/And%3E%3C/Filter%3E";
			}
			else {
				filtro += "%3CPropertyIsEqualTo%3E%3CPropertyName%3EnombreEntidad/nombre%3C/PropertyName%3E%3CLiteral%3E" + address.locality + "%3C/Literal%3E%3C/PropertyIsEqualTo%3E%3C/Filter%3E";
			}
			url = wfsMunicipio + filtro;
			address.error=3; //inicializamos la variable de error a 3:error de WFS de municipio
		}
		//si no hay localidad buscamos por cp
		else if(address.CP && address.CP !== ''){
			filtro = "%3CFilter%3E%3CPropertyIsLike%20wildCard=%22*%22%20singleChar=%22_%22%20escapeChar=%22!%22%3E%3CPropertyName%3EnombreEntidad/nombre%3C/PropertyName%3E%3CLiteral%3E" + address.CP + "%3C/Literal%3E%3C/PropertyIsLike%3E%3C/Filter%3E";
			url = wfsCP + filtro;
			address.error=4; //inicializamos la variable de error a 4:error de WFS de CP		
		}
		else{
			address.error=1; //Error 1: Necesitamos localidad o cp siempre
			return address;
		}
		
		requestType.method = 'GET';
		requestType.async=false;
		xml = this.ajaxRequest(url, requestType);
		
		
		if (xml.getElementsByTagName('provincia')[0]) {
			if (!address.locality || address.locality === '') {
				address.locality = xml.getElementsByTagName('municipio')[0].firstChild.nodeValue;
				address.locality = this.sanitizeInput(address.locality);
			}
			if (!address.region || address.region === '') {
			
				address.region = xml.getElementsByTagName('provincia')[0].firstChild.nodeValue;
				address.region = this.sanitizeInput(address.region);
			}
			address.error = 0;
		}
		
		//Si no tenemos vial, localizamos sólo el municipio o codigo postal, si no seguimos con el vial
		if (!address.street || address.street === '') {
			this.findCoordenadas(xml, address);
		}
		else {
			if (address.error === 0) {
				filtro = "%3CFilter%3E%3CAnd%3E%3CPropertyIsLike%20wildCard=%22*%22%20singleChar=%22_%22%20escapeChar=%22!%22%3E%3CPropertyName%3EnombreEntidad/nombre%3C/PropertyName%3E%3CLiteral%3E*" + address.street + "*%3C/Literal%3E%3C/PropertyIsLike%3E%3CPropertyIsEqualTo%3E%3CPropertyName%3EentidadLocal/provincia%3C/PropertyName%3E%3CLiteral%3E" + address.region + "%3C/Literal%3E%3C/PropertyIsEqualTo%3E%3CPropertyIsEqualTo%3E%3CPropertyName%3EentidadLocal/municipio%3C/PropertyName%3E%3CLiteral%3E" + address.locality + "%3C/Literal%3E%3C/PropertyIsEqualTo%3E";
				
				if (address.streetType) {
					filtro += "%3CPropertyIsEqualTo%3E%3CPropertyName%3EtipoEntidad/tipo%3C/PropertyName%3E%3CLiteral%3E" + address.streetType + "%3C/Literal%3E%3C/PropertyIsEqualTo%3E%3C/And%3E%3C/Filter%3E";
				}
				else {
					filtro += "%3C/And%3E%3C/Filter%3E";
				}
				
				url = wfsVial + filtro;
				xml = this.ajaxRequest(url, requestType);
				
				
				if (address.portal && address.portal !== '') {
					if(xml.getElementsByTagName('Entidad')[0]){
						address.entidad = xml.getElementsByTagName('Entidad')[0].getAttribute("fid");
						filtro = "%3CFilter%3E%3CAnd%3E%3CPropertyIsEqualTo%3E%3CPropertyName%3EnombreEntidad/nombre%3C/PropertyName%3E%3CLiteral%3E" + address.portal + "%3C/Literal%3E%3C/PropertyIsEqualTo%3E%3CPropertyIsEqualTo%3E%3CPropertyName%3EentidadRelacionada/idEntidad%3C/PropertyName%3E%3CLiteral%3E" + address.entidad + "%3C/Literal%3E%3C/PropertyIsEqualTo%3E%3C/And%3E%3C/Filter%3E";
						url = wfsPortal + filtro;
						
						xml = this.ajaxRequest(url, requestType);
						address.error = 6; //error WFS portal
					
						this.findCoordenadas(xml, address);
					}
					else{
						address.error = 5; //error WFS vial
					}
				}
				else {
					address.error = 2; //Error 2: Necesitamos un valor para el portal si tenemos vial
					return address;
				}
			}
			else{
				return address;
			}
		}
	},
	queryRoute:function(addresses){
		var postParams="request=<?xml version='1.0' encoding='UTF-8' standalone='yes'?><Execute service='WPS' version='0.4.0' store='false' status='false' xmlns='http://www.opengeospatial.net/wps' xmlns:pak='http://www.opengis.net/examples/packet' xmlns:ows='http://www.opengeospatial.net/ows' xmlns:xlink='http://www.w3.org/1999/xlink' xmlns:xsi='http://www.w3.org/2001/XMLSchema-instance' xsi:schemaLocation='http://www.opengeospatial.net/wps ..%5CwpsExecute.xsd' xmlns:om='http://www.opengis.net/om' xmlns:gml='http://www.opengis.net/gml'> <ows:Identifier>com.ign.process.geometry.RouteFinder</ows:Identifier><DataInputs><Input><ows:Identifier>details</ows:Identifier><ows:Title>Distancia</ows:Title>";
		postParams += "<LiteralValue dataType='xs:boolean'>true</LiteralValue></Input><Input><ows:Identifier>wayPointList</ows:Identifier><ows:Title>WayPoints</ows:Title>";
		postParams += "<ComplexValue><wayPointList>";
		
		for (var i = 0; i < addresses.length; i++) {
			postParams += "<wayPoint><gml:Point><gml:coord><gml:X>"+addresses[i].point.lon+"</gml:X><gml:Y>"+addresses[i].point.lat+"</gml:Y></gml:coord></gml:Point></wayPoint>";
		}
		postParams += "</wayPointList></ComplexValue></Input></DataInputs><ProcessOutputs><Output><ows:Identifier>result</ows:Identifier><ows:Title>LineString</ows:Title><ows:Abstract>GML describiendo una feature de Linestring.</ows:Abstract><ComplexOutput defaultFormat='text/XML' defaultSchema='http://geoserver.itc.nl:8080/wps/schemas/gml/2.1.2/gmlpacket.xsd'><SupportedComplexData><Schema>http://schemas.opengis.net/gml/2.1.2/feature.xsd</Schema></SupportedComplexData></ComplexOutput></Output><Output><ows:Identifier>route</ows:Identifier><ows:Title>Ruta</ows:Title><ows:Abstract>Ruta</ows:Abstract><ComplexOutput defaultFormat='text/XML' defaultSchema='http://www.idee.es/complexValues.xsd'><SupportedComplexData><Schema>http://www.idee.es/complexValues.xsd</Schema></SupportedComplexData></ComplexOutput></Output><Output><ows:Identifier>wayPoints</ows:Identifier><ows:Title>Puntos</ows:Title><ows:Abstract>Lista de puntos</ows:Abstract><ComplexOutput defaultFormat='text/XML' defaultSchema='http://www.idee.es/wayPointsValues.xsd'><SupportedComplexData><Schema>http://www.idee.es/wayPointsValues.xsd</Schema></SupportedComplexData></ComplexOutput></Output></ProcessOutputs></Execute>";
		
		//URL de WPS en servidor de desarrollo grupotecopy.es y definitiva en cartociudad.es
		//var url = "http://desarrollo.grupotecopy.es/visor/proxy.do?urlValor=http://192.168.47.153:8083/wps/WebProcessingService";
		var url = "visor/proxy.do?urlValor=http://localhost:8080/wps/WebProcessingService";

		//Las peticiones WPS seran POST y no GET
		var requestType = new Object();
		requestType.method = 'POST';
		requestType.async = false;
		
		var xmlDoc = this.ajaxRequest(url, requestType, postParams);
		
		var rutaWPS = new Object();
		if (OpenLayers.Ajax.getElementsByTagNameNS(xmlDoc, 'http://www.opengis.net/gml', 'gml', 'Coordinates')[0]) {
			var stringWPS = '';
			var aux = '';
			for (i = 0; i < addresses.length - 1; i++) {
				aux = OpenLayers.Ajax.getElementsByTagNameNS(xmlDoc, 'http://www.opengis.net/gml', 'gml', 'Coordinates')[i].firstChild.nodeValue;
				stringWPS += aux;
			}
			stringWPS = stringWPS.replace(/,\s/g, ',');
			stringWPS = stringWPS.replace(/\s(?=$)/, '');
			stringWPS = stringWPS.split(/\s/);
			
			rutaWPS.points = new Array();
			for (i = 0; i < stringWPS.length; i++) {
				aux = stringWPS[i].split(',');
				var point = new OpenLayers.Geometry.Point(aux[0], aux[1]);
				rutaWPS.points.push(point);
			}
			var linea = new OpenLayers.Geometry.LineString(rutaWPS.points);
			rutaWPS.bounds = linea.getBounds();
			
			var nodosCoste = xmlDoc.getElementsByTagName('coste');
			var coste = 0;
			for (i = 0; i < nodosCoste.length; i++) {
				coste += '+' + nodosCoste[i].firstChild.nodeValue;
			}
			coste = eval(coste);
			rutaWPS.distance = parseInt(coste, 10);

			rutaWPS.error=0;
			return rutaWPS;
		}
		else{
			rutaWPS.error=7;
			return rutaWPS;
		}
		
	}
};

var metodosFlamingo={
	addEvent: function(elm, evType, fn, useCapture){
		if (elm.addEventListener){
			elm.addEventListener(evType, fn, useCapture);
			return true;
		} else if (elm.attachEvent) {
			var r = elm.attachEvent('on' + evType, fn);
			return r;
		} else {
			elm['on' + evType] = fn;
		}
	},
	FireEvent: function(element,event){
	    if (document.createEventObject){
	        // dispatch for IE
	        var evt = document.createEventObject();
	        return element.fireEvent('on'+event,evt)
	    }
	    else{
	        // dispatch for firefox + others
	        var evt = document.createEvent("HTMLEvents");
	        evt.initEvent(event, true, true ); // event type,bubbling,cancelable
	        return !element.dispatchEvent(evt);
	    }
	},
	getMovie: function (movieName) {
	    if (navigator.appName.indexOf("Microsoft") != -1) {
	        return window[movieName];
	    }else {
	        return document[movieName];
	    }
	}
};

function flamingo_onConfigComplete() {
	var app = metodosFlamingo.getMovie("flamingo");
	metodosFlamingo.FireEvent(app, "click");
}


