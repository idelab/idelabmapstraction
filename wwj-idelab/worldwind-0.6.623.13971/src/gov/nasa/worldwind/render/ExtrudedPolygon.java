/*
Copyright (C) 2001, 2010 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/

package gov.nasa.worldwind.render;

import com.sun.opengl.util.BufferUtil;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.geom.*;
import gov.nasa.worldwind.ogc.kml.impl.KMLExportUtil;
import gov.nasa.worldwind.util.Logging;

import javax.media.opengl.GL;
import javax.xml.stream.*;
import java.awt.*;
import java.io.IOException;
import java.nio.*;
import java.util.*;
import java.util.List;

/**
 * A multi-sided 3D shell formed by a base polygon in latitude and longitude extruded from the terrain to either a
 * specified height or an independent height per location. The base polygon may be complex with multiple internal but
 * not intersecting contours. Extruded polygons may optionally be textured. Textures may be applied to both the faces of
 * the outer and inner boundaries or just the outer boundaries. Texture can not be applied to the cap. Standard lighting
 * is optionally applied. ExtrudedPolygon side faces and cap have independent attributes for both normal and highlighted
 * drawing.
 * <p/>
 * When specifying a single height, the height is relative to a reference location designated by one of the specified
 * polygon locations in the outer boundary. The default reference location is the first one in the polygon's outer
 * boundary. An alternative location may be specified by calling {@link #setReferenceLocation(LatLon)}. The extruded
 * polygon is capped with a plane at the specified height and tangent to the ellipsoid at the reference location. Since
 * location other than the reference position may resolve to points at elevations other than that at the reference
 * location, the distances from those points to the cap are adjusted so that the adjacent sides precisely meet the cap.
 * When specifying polygons using a single height, only the latitude and longitudes of polygon boundary positions must
 * be specified.
 * <p/>
 * Independent per-location heights may be specified via the <i>altitude</i> field of {@link Position}s defining the
 * polygon's inner and outer boundaries. Depending on the specified altitude mode, the position altitudes may be
 * interpreted as altitudes relative to mean sea level or altitudes above the ground at the associated latitude and
 * longitude locations.
 * <p/>
 * <p/>
 * Extruded polygons are safe to share among World Windows. They should not be shared among layers in the same World
 * Window.
 *
 * @author tag
 * @version $Id: ExtrudedPolygon.java 13827 2010-09-18 03:12:53Z pabercrombie $
 */
public class ExtrudedPolygon extends AbstractPolygon
{
    /** The default interior color for sides. */
    protected static final Material DEFAULT_SIDES_INTERIOR_MATERIAL = Material.LIGHT_GRAY;
    /** The default altitude mode. */
    protected static final int DEFAULT_ALTITUDE_MODE = WorldWind.CONSTANT;

    /** The attributes used if attributes are not specified. */
    protected static final ShapeAttributes defaultSideAttributes;

    static
    {
        defaultSideAttributes = new BasicShapeAttributes();
        defaultSideAttributes.setInteriorMaterial(DEFAULT_SIDES_INTERIOR_MATERIAL);
        defaultSideAttributes.setOutlineMaterial(DEFAULT_OUTLINE_MATERIAL);
    }

    /** Holds information for each countour of the polygon. The vertex values are updated every frame. */
    protected static class ExtrudedBoundaryInfo extends BoundaryInfo
    {
        protected int faceCount;
        protected Vec4[] baseVertices; // computed terrain vertices
        protected IntBuffer sideIndices; // indices identifying side faces in the vertex buffer
        protected IntBuffer sideEdgeIndices; // indices identifying edges in the vertex buffer
        protected FloatBuffer sideVertexBuffer; // vertices passed to OpenGL
        protected FloatBuffer sideNormalBuffer; // vertex normals if lighting is applied
        protected FloatBuffer textureCoords; // texture coords if texturing
        protected List<WWTexture> sideTextures;
    }

    protected BoundaryInfo createBoundaryInfo()
    {
        return new ExtrudedBoundaryInfo();
    }

    // This static hash map hold the vertex indices that define the shape geometry. Their contents depend only on the
    // number of locations in the source polygon, so they can be reused by all shapes with the same location count.
    protected static HashMap<Integer, IntBuffer> fillIndexBuffers = new HashMap<Integer, IntBuffer>();
    protected static HashMap<Integer, IntBuffer> sideEdgeIndexBuffers = new HashMap<Integer, IntBuffer>();

    protected double height = 1;
    protected int totalFaceCount;
    protected ShapeAttributes sideAttributes;
    protected ShapeAttributes sideHighlightAttributes;
    protected ShapeAttributes activeSideAttributes = new BasicShapeAttributes();
    protected boolean enableCap = true;
    protected boolean enableSides = true;
    protected FloatBuffer sideVertexBuffer;
    protected FloatBuffer sideNormalBuffer;

    /** Construct an extruded polygon with an empty position list and a default height of 1 meter. */
    public ExtrudedPolygon()
    {
    }

    /**
     * Construct an extruded polygon for a specified list of locations and a height.
     *
     * @param corners the list of locations defining the polygon.
     * @param height  the shape height, in meters. May be null, in which case a height of 1 is used when the altitude
     *                mode is {@link WorldWind#CONSTANT}.
     *
     * @throws IllegalArgumentException if the location list is null or the height is less than or equal to zero.
     */
    public ExtrudedPolygon(Iterable<? extends LatLon> corners, Double height)
    {
        if (corners == null)
        {
            String message = Logging.getMessage("nullValue.IterableIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        if (height != null && height <= 0)
        {
            String message = Logging.getMessage("generic.ArgumentOutOfRange", "height <= 0");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.setOuterBoundary(corners, height);
    }

    /**
     * Construct an extruded polygon for a specified list of locations and a height, and apply specified textures to its
     * outer faces.
     *
     * @param corners      the list of locations defining the polygon.
     * @param height       the shape height, in meters. May be null, in which case a height of 1 is used when the
     *                     altitude mode is {@link WorldWind#CONSTANT}.
     * @param imageSources textures to apply to the polygon's outer faces. One texture for each face must be included.
     *                     May also be null.
     *
     * @throws IllegalArgumentException if the location list is null or the height is less than or equal to zero.
     */
    public ExtrudedPolygon(Iterable<? extends LatLon> corners, double height, Iterable<?> imageSources)
    {
        this(corners, height);

        if (imageSources != null)
            this.addImageSourcesToBoundary((ExtrudedBoundaryInfo) this.outerBoundary(), imageSources);
    }

    /**
     * Construct an extruded polygon for a specified list of positions.
     *
     * @param corners the list of positions -- latitude longitude and altitude -- defining the polygon. The altitude
     *                mode determines whether the positions are considered relative to mean sea level (they are
     *                "absolute") or the ground elevation at the associated latitude and longitude.
     *
     * @throws IllegalArgumentException if the position list is null.
     */
    public ExtrudedPolygon(Iterable<? extends Position> corners)
    {
        this(corners, 1d); // height is ignored when positions are specified, so any value will do
    }

    /**
     * Construct an extruded polygon for a specified list of positions.
     *
     * @param corners the list of positions -- latitude longitude and altitude -- defining the polygon. The altitude
     *                mode determines whether the positions are considered relative to mean sea level (they are
     *                "absolute") or the ground elevation at the associated latitude and longitude.
     *
     * @throws IllegalArgumentException if the position list is null.
     */
    public ExtrudedPolygon(Position.PositionList corners)
    {
        if (corners == null)
        {
            String message = Logging.getMessage("nullValue.PositionsListIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.setOuterBoundary(corners.list, null);
    }

    /**
     * Construct an extruded polygon for a specified list of positions, and apply specified textures to its outer
     * faces.
     *
     * @param corners      the list of positions -- latitude longitude and altitude -- defining the polygon. The
     *                     altitude mode determines whether the positions are considered relative to mean sea level
     *                     (they are "absolute") or the ground elevation at the associated latitude and longitude.
     * @param imageSources textures to apply to the polygon's outer faces. One texture for each face must be included.
     *                     May also be null.
     *
     * @throws IllegalArgumentException if the position list is null.
     */
    public ExtrudedPolygon(Iterable<? extends Position> corners, Iterable<?> imageSources)
    {
        this(corners);

        if (imageSources != null)
            this.addImageSourcesToBoundary((ExtrudedBoundaryInfo) this.outerBoundary(), imageSources);
    }

    /** Void any computed data. Called when a factor affecting the computed data is changed. */
    protected void reinitialize()
    {
        super.reinitialize();

        for (BoundaryInfo boundary : this.boundaries)
        {
            ((ExtrudedBoundaryInfo) boundary).sideIndices = this.getSideIndices(boundary.locations.size());
            ((ExtrudedBoundaryInfo) boundary).sideEdgeIndices = this.getSideEdgeIndices(boundary.locations.size());
        }
    }

    /** {@inheritDoc} */
    public void dispose()
    {
        // Remove references to textures and NIO buffers. Not necessary, but prevents dangling references to large
        // chunks of memory.
        for (BoundaryInfo boundary : this.boundaries)
        {
            ExtrudedBoundaryInfo info = (ExtrudedBoundaryInfo) boundary;
            if (info.sideTextures != null)
                info.sideTextures.clear();
            info.sideTextures = null;
        }

        super.dispose();
    }

    @Override
    protected int getDefaultAltitudeMode()
    {
        return DEFAULT_ALTITUDE_MODE;
    }

    /**
     * Specifies the latitude and longitude and optional altitude of the locations defining the polygon. To specify
     * altitudes, pass {@link Position}s rather than {@link LatLon}s.
     *
     * @param corners the polygon locations.
     * @param height  the shape height, in meters.
     *
     * @throws IllegalArgumentException if the location list is null or contains fewer than three locations.
     */
    public void setOuterBoundary(Iterable<? extends LatLon> corners, Double height)
    {
        if (corners == null)
        {
            String message = Logging.getMessage("nullValue.IterableIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        BoundaryInfo outerBoundary = this.createBoundaryInfo();
        if (this.boundaries.size() > 0)
            this.boundaries.set(0, outerBoundary);
        else
            this.boundaries.add(outerBoundary);

        if (height != null)
            this.height = height;

        ArrayList<LatLon> list = new ArrayList<LatLon>();
        for (LatLon corner : corners)
        {
            if (corner != null)
                list.add(corner);
        }

        if (list.size() < 3)
        {
            String message = Logging.getMessage("nullValue.LocationInListIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        // Close the list if not already closed.
        if (list.size() > 0 && !list.get(0).equals(list.get(list.size() - 1)))
            list.add(list.get(0));

        list.trimToSize();
        outerBoundary.locations = list;

        this.reinitialize();
    }

    /**
     * Specifies the latitude, longitude and optional altitude of the locations defining the polygon, and specified
     * textures to apply the the polygon's outer faces. To specify altitudes, pass {@link Position}s rather than {@link
     * LatLon}s.
     *
     * @param corners      the polygon locations.
     * @param height       the shape height, in meters.
     * @param imageSources textures to apply to the outer faces. One texture must be specified for each face. May be
     *                     null.
     *
     * @throws IllegalArgumentException if the position list is null.
     */
    public void setOuterBoundary(Iterable<? extends LatLon> corners, Double height, Iterable<?> imageSources)
    {
        this.setOuterBoundary(corners, height);

        if (imageSources != null)
            this.addImageSourcesToBoundary((ExtrudedBoundaryInfo) this.outerBoundary(), imageSources);
    }

    /**
     * Add an inner boundary to the polygon. A duplicate of the first position is appended to the list if the list's
     * last position is not identical to the first.
     *
     * @param corners the polygon positions.
     *
     * @throws IllegalArgumentException if the location list is null or contains fewer than three locations.
     */
    public void addInnerBoundary(Iterable<? extends LatLon> corners)
    {
        if (corners == null)
        {
            String message = Logging.getMessage("nullValue.LocationInListIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        BoundaryInfo boundary = this.createBoundaryInfo();
        this.boundaries.add(boundary);

        ArrayList<LatLon> list = new ArrayList<LatLon>();
        for (LatLon corner : corners)
        {
            if (corner != null)
                list.add(corner);
        }

        if (list.size() < 3)
        {
            String message = Logging.getMessage("generic.InsufficientPositions");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        // Close the list if not already closed.
        if (list.size() > 0 && !list.get(0).equals(list.get(list.size() - 1)))
            list.add(list.get(0));

        list.trimToSize();
        boundary.locations = list;

        this.reinitialize();
    }

    /**
     * Add an inner boundary to the polygon and specify textures to apply to each of its faces. Specify {@link LatLon}s
     * to use the polygon's single height, or {@link Position}s, to include individual altitudes.
     *
     * @param corners      the polygon locations.
     * @param imageSources textures to apply to the boundary's faces. One texture must be specified for each face. May
     *                     be null.
     *
     * @throws IllegalArgumentException if the location list is null.
     */
    public void addInnerBoundary(Iterable<? extends LatLon> corners, Iterable<?> imageSources)
    {
        this.addInnerBoundary(corners);

        if (imageSources != null)
            this.addImageSourcesToBoundary((ExtrudedBoundaryInfo) this.boundaries.get(this.boundaries.size() - 1),
                imageSources);
    }

    protected void addImageSourcesToBoundary(ExtrudedBoundaryInfo boundary, Iterable<?> imageSources)
    {
        boundary.sideTextures = new ArrayList<WWTexture>(boundary.locations.size());

        for (Object source : imageSources)
        {
            if (source != null)
                boundary.sideTextures.add(new BasicWWTexture(source, true));
            else
                boundary.sideTextures.add(null);
        }
    }

    @Override
    protected void countPositions()
    {
        super.countPositions();

        this.totalFaceCount = this.totalNumVertices - this.boundaries.size();
    }

    /**
     * Returns the specified shape height.
     *
     * @return the shape height originally specified, in meters.
     */
    public double getHeight()
    {
        return height;
    }

    /**
     * Specifies the shape height.
     *
     * @param height the shape height, in meters.
     *
     * @throws IllegalArgumentException if the height is less than or equal to zero.
     */
    public void setHeight(double height)
    {
        if (height <= 0)
        {
            String message = Logging.getMessage("generic.ArgumentOutOfRange", "height <= 0");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.height = height;
        this.reinitialize();
    }

    /**
     * Indicates whether the cap should be drawn.
     *
     * @return true to draw the cap, otherwise false.
     */
    public boolean isEnableCap()
    {
        return enableCap;
    }

    /**
     * Specifies whether the cap should be drawn.
     *
     * @param enableCap true to draw the cap, otherwise false.
     */
    public void setEnableCap(boolean enableCap)
    {
        this.enableCap = enableCap;
    }

    /**
     * Inicates whether the sides should be drawn.
     *
     * @return true to draw the sides, othewise false.
     */
    public boolean isEnableSides()
    {
        return enableSides;
    }

    /**
     * Specifies whether to draw the sides.
     *
     * @param enableSides true to draw the sides, otherwise false.
     */
    public void setEnableSides(boolean enableSides)
    {
        this.enableSides = enableSides;
    }

    /**
     * Returns the attributes applied to the polygon's side faces.
     *
     * @return the polygon's side atributes.
     */
    public ShapeAttributes getSideAttributes()
    {
        return sideAttributes;
    }

    /**
     * Specifies the attributes applied to the polygon's side faces.
     *
     * @param attributes the polygon's side atributes.
     */
    public void setSideAttributes(ShapeAttributes attributes)
    {
        if (attributes == null)
        {
            String message = "nullValue.AttributesIsNull";
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.sideAttributes = attributes;
    }

    /**
     * Returns the attributes applied to the polygon's cap.
     *
     * @return the polygon's cap atributes.
     */
    public ShapeAttributes getCapAttributes()
    {
        return this.getAttributes();
    }

    /**
     * Specifies the attributes applied to the polygon's cap.
     *
     * @param attributes the polygon's cap atributes.
     *
     * @throws IllegalArgumentException if attributes is null.
     */
    public void setCapAttributes(ShapeAttributes attributes)
    {
        this.setAttributes(attributes);
    }

    /**
     * Returns the highlight attributes applied to the polygon's side faces.
     *
     * @return the polygon's side highlight atributes.
     */
    public ShapeAttributes getSideHighlightAttributes()
    {
        return sideHighlightAttributes;
    }

    /**
     * Specifies the highlight attributes applied to the polygon's side faces.
     *
     * @param attributes the polygon's side highlight atributes.
     *
     * @throws IllegalArgumentException if attributes is null.
     */
    public void setSideHighlightAttributes(ShapeAttributes attributes)
    {
        if (attributes == null)
        {
            String message = "nullValue.AttributesIsNull";
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.sideHighlightAttributes = attributes;
    }

    /**
     * Returns the highlight attributes applied to the polygon's cap.
     *
     * @return the polygon's cap highlight atributes.
     */
    public ShapeAttributes getCapHighlightAttributes()
    {
        return this.getHighlightAttributes();
    }

    /**
     * Specifies the highlight attributes applied to the polygon's cap.
     *
     * @param attributes the polygon's cap highlight atributes.
     *
     * @throws IllegalArgumentException if attributes is null.
     */
    public void setCapHighlightAttributes(ShapeAttributes attributes)
    {
        this.setHighlightAttributes(attributes);
    }

    /**
     * Each time the polygon is rendered the appropriate attributes for the current mode are determined. This method
     * returns the resolved attributes.
     *
     * @return the currently active attributes for the polygon's side faces.
     */
    protected ShapeAttributes getActiveSideAttributes()
    {
        return activeSideAttributes;
    }

    /**
     * Each time the polygon is rendered the appropriate attributes for the current mode are determined. This method
     * returns the resolved attributes.
     *
     * @return the currently active attributes for the polygon's cap.
     */
    protected ShapeAttributes getActiveCapAttributes()
    {
        return this.getActiveAttributes();
    }

    /**
     * Returns the polygon's altitude mode, one of {@link WorldWind#RELATIVE_TO_GROUND}, {@link WorldWind#ABSOLUTE}, or
     * {@link WorldWind#CONSTANT}. The altitude mode [@link WorldWind#CLAMP_TO_GROUND} is not supported. The default
     * altitude mode is {@link WorldWind#CONSTANT}.
     *
     * @return the polygon's altitude mode.
     */
    public int getAltitudeMode()
    {
        return super.getAltitudeMode();
    }

    /**
     * Specifies the polygon's altitude mode, one of {@link WorldWind#RELATIVE_TO_GROUND}, {@link WorldWind#ABSOLUTE},
     * or {@link WorldWind#CONSTANT}. The altitude mode [@link WorldWind#CLAMP_TO_GROUND} is not supported and {@link
     * WorldWind#ABSOLUTE} is used insted if {@link Position}s have been specified as the polygon's outer boundary.}
     * <p/>
     * The altitude mode is ignored if {@link LatLon}s are specified as the polygon's outer boundary.
     *
     * @param altitudeMode the polygon's altitude mode.
     */
    public void setAltitudeMode(int altitudeMode)
    {
        super.setAltitudeMode(altitudeMode);
    }

    /**
     * Rotation is not supported for extruded polygons.
     *
     * @return null.
     */
    @Override
    public Double getRotation()
    {
        return null;
    }

    /**
     * Rotation is not supported for extruded polygons.
     *
     * @param rotation not used.
     */
    @Override
    public void setRotation(Double rotation)
    {
        String message = Logging.getMessage("generic.UnsupportedOperation", "ExtrudedPolygon rotation");
        Logging.logger().warning(message);
        throw new UnsupportedOperationException(message);
    }

    /**
     * Indicates the position to use as a reference position for computed geometry.
     *
     * @return the reference location, or null if no reference location has been specified.
     */
    public LatLon getReferenceLocation()
    {
        return this.getReferencePosition();
    }

    /**
     * Specifies the location to use as a reference position for computed geometry. This value should typically left to
     * the default value of the first location in the polygon's outer boundary.
     *
     * @param referenceLocation the reference location. May be null, in which case the first location of the outer
     *                          boundary is the reference position.
     */
    public void setReferenceLocation(LatLon referenceLocation)
    {
        if (referenceLocation == null)
        {
            String message = Logging.getMessage("nullValue.LocationIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.referencePosition = new Position(referenceLocation, 0);
    }

    /**
     * Returns the polygon's texture image sources.
     *
     * @return a collection of lists each identifying the image sources for the associated outer or inner polygon
     *         boundary.
     */
    public List<List<Object>> getImageSources()
    {
        boolean hasTextures = false;
        for (BoundaryInfo boundary : this.boundaries)
        {
            if (((ExtrudedBoundaryInfo) boundary).sideTextures != null)
                hasTextures = true;
        }

        if (!hasTextures)
            return null;

        List<List<Object>> imageSources = new ArrayList<List<Object>>(this.boundaries.size());

        for (BoundaryInfo boundary : this.boundaries)
        {
            if (((ExtrudedBoundaryInfo) boundary).sideTextures == null)
            {
                imageSources.add(null);
            }
            else
            {
                ArrayList<Object> images = new ArrayList<Object>(((ExtrudedBoundaryInfo) boundary).sideTextures.size());
                imageSources.add(images);

                for (WWTexture image : ((ExtrudedBoundaryInfo) boundary).sideTextures)
                {
                    images.add(image.getImageSource());
                }
            }
        }

        return imageSources;
    }

    /** {@inheritDoc} */
    public boolean hasSideTextures()
    {
        for (BoundaryInfo info : this.boundaries)
        {
            ExtrudedBoundaryInfo boundary = (ExtrudedBoundaryInfo) info;
            if (boundary.sideTextures != null && boundary.sideTextures.size() == boundary.faceCount)
                return true;
        }

        return false;
    }

    /**
     * Returns the texture for a specified outer boundary.
     *
     * @param side the zero-origin position of the desired texture.
     *
     * @return the texture specified for the indicated side, or null if no outer boundary textures are specified.
     */
    protected WWTexture getTexture(int side)
    {
        ExtrudedBoundaryInfo ob = (ExtrudedBoundaryInfo) this.outerBoundary();

        if (ob == null)
            return null;

        if (ob.sideTextures == null || ob.sideTextures.size() == 0)
            return null;

        return ob.sideTextures.size() > side ? ob.sideTextures.get(side) : null;
    }

    protected boolean mustApplySideTextures(DrawContext dc)
    {
        return !dc.isPickingMode() && this.hasSideTextures();
    }

    /**
     * Indicates whether the interior of either the sides or cap must be drawn.
     *
     * @return true if an interior must be drawn, otherwise false.
     */
    protected boolean mustDrawInterior()
    {
        return super.mustDrawInterior() || this.getActiveSideAttributes().isDrawInterior();
    }

    /**
     * Indicates whether the polygon's outline should be drawn.
     *
     * @return true if the outline should be drawn, otherwise false.
     */
    protected boolean mustDrawOutline()
    {
        return super.mustDrawOutline() || this.getActiveSideAttributes().isDrawOutline();
    }

    protected Extent computeExtent()
    {
        if (this.getOuterBoundary() == null)
            return null;

        Vec4[] topVerts = this.outerBoundary().vertices;
        Vec4[] botVerts = ((ExtrudedBoundaryInfo) this.outerBoundary()).baseVertices;
        ArrayList<Vec4> allVerts = new ArrayList<Vec4>(2 * topVerts.length);

        allVerts.addAll(Arrays.asList(topVerts));
        allVerts.addAll(Arrays.asList(botVerts));

        Box boundingBox = Box.computeBoundingBox(allVerts);

        // The bounding box is computed relative to the polygon's reference point, so it needs to be translated to
        // model coordinates in order to indicate its model-coordinate extent.
        return boundingBox != null ? boundingBox.translate(this.referencePoint) : null;
    }

    /** Determines which attributes -- normal, highlight or default -- to use each frame. */
    protected void determineActiveAttributes()
    {
        super.determineActiveAttributes();

        if (this.isHighlighted())
        {
            if (this.getSideHighlightAttributes() != null)
                this.activeSideAttributes.copy(this.getSideHighlightAttributes());
            else
            {
                // If no highlight attributes have been specified we need to use the normal attributes but adjust them
                // to cause highlighting.
                if (this.getSideAttributes() != null)
                    this.activeSideAttributes.copy(this.getSideAttributes());
                else
                    this.activeSideAttributes.copy(defaultSideAttributes);

                this.activeSideAttributes.setOutlineMaterial(DEFAULT_HIGHLIGHT_MATERIAL);
                this.activeSideAttributes.setInteriorMaterial(DEFAULT_HIGHLIGHT_MATERIAL);
            }
        }
        else
        {
            if (this.getSideAttributes() != null)
                this.activeSideAttributes.copy(this.getSideAttributes());
            else
                this.activeSideAttributes.copy(defaultSideAttributes);
        }
    }

    public void drawOutline(DrawContext dc)
    {
        if (this.isEnableSides() && getActiveSideAttributes().isDrawOutline())
            this.drawEdges(dc);
        if (this.isEnableCap() && getActiveCapAttributes().isDrawOutline())
            super.drawOutline(dc);
    }

    public void drawInterior(DrawContext dc)
    {
        if (this.isEnableSides() && getActiveSideAttributes().isDrawInterior())
            this.drawSides(dc);

        if (this.isEnableCap() && getActiveCapAttributes().isDrawInterior())
            super.drawInterior(dc);
    }

    /**
     * Draws the shape's sides.
     *
     * @param dc the draw context.
     */
    protected void drawSides(DrawContext dc)
    {
        GL gl = dc.getGL();

        if (!dc.isPickingMode())
        {
            Material material = this.getActiveSideAttributes().getInteriorMaterial();
            if (material == null)
                material = defaultSideAttributes.getInteriorMaterial();

            if (this.activeSideAttributes.isEnableLighting())
            {
                material.apply(gl, GL.GL_FRONT_AND_BACK, (float) this.getActiveSideAttributes().getInteriorOpacity());
            }
            else
            {
                Color sc = material.getDiffuse();
                double opacity = this.getActiveSideAttributes().getInteriorOpacity();
                gl.glColor4ub((byte) sc.getRed(), (byte) sc.getGreen(), (byte) sc.getBlue(),
                    (byte) (opacity < 1 ? (int) (opacity * 255 + 0.5) : 255));
            }
        }

        for (BoundaryInfo info : this.boundaries)
        {
            ExtrudedBoundaryInfo boundary = (ExtrudedBoundaryInfo) info;

            if (this.mustApplyLighting(dc))
                gl.glNormalPointer(GL.GL_FLOAT, 0, boundary.sideNormalBuffer.rewind());

            if (!dc.isPickingMode() && boundary.textureCoords != null)
            {
                dc.getGL().glEnable(GL.GL_TEXTURE_2D);
                gl.glEnableClientState(GL.GL_TEXTURE_COORD_ARRAY);
                gl.glTexCoordPointer(2, GL.GL_FLOAT, 0, boundary.textureCoords.rewind());
            }
            else
            {
                dc.getGL().glDisable(GL.GL_TEXTURE_2D);
                gl.glDisableClientState(GL.GL_TEXTURE_COORD_ARRAY);
            }

            gl.glVertexPointer(3, GL.GL_FLOAT, 0, boundary.sideVertexBuffer.rewind());

            boundary.sideIndices.rewind();
            for (int j = 0; j < boundary.faceCount; j++)
            {
                if (boundary.textureCoords != null)
                {
                    boundary.sideTextures.get(j).bind(dc);
                    boundary.sideTextures.get(j).applyInternalTransform(dc);
                }

                boundary.sideIndices.position(4 * j);
                boundary.sideIndices.limit(4 * (j + 1));
                gl.glDrawElements(GL.GL_TRIANGLE_STRIP, 4, GL.GL_UNSIGNED_INT, boundary.sideIndices);
            }
        }
    }

    /**
     * Draws the shape's edges.
     *
     * @param dc the draw context.
     */
    protected void drawEdges(DrawContext dc)
    {
        GL gl = dc.getGL();

        ShapeAttributes activeAttrs = this.getActiveSideAttributes();

        if (!dc.isPickingMode())
        {
            Material material = activeAttrs.getOutlineMaterial();
            if (material == null)
                material = defaultSideAttributes.getOutlineMaterial();

            if (this.activeSideAttributes.isEnableLighting())
            {
                material.apply(gl, GL.GL_FRONT_AND_BACK, (float) this.getActiveSideAttributes().getOutlineOpacity());
            }
            else
            {
                Color sc = material.getDiffuse();
                double opacity = activeAttrs.getOutlineOpacity();
                gl.glColor4ub((byte) sc.getRed(), (byte) sc.getGreen(), (byte) sc.getBlue(),
                    (byte) (opacity < 1 ? (int) (opacity * 255 + 0.5) : 255));
            }

            gl.glHint(GL.GL_LINE_SMOOTH_HINT, activeAttrs.isEnableAntialiasing() ? GL.GL_NICEST : GL.GL_DONT_CARE);
        }

        if (dc.isPickingMode() && activeAttrs.getOutlineWidth() < this.getOutlinePickWidth())
            gl.glLineWidth(this.getOutlinePickWidth());
        else
            gl.glLineWidth((float) activeAttrs.getOutlineWidth());

        if (activeAttrs.getOutlineStippleFactor() > 0)
        {
            gl.glEnable(GL.GL_LINE_STIPPLE);
            gl.glLineStipple(activeAttrs.getOutlineStippleFactor(), activeAttrs.getOutlineStipplePattern());
        }

        dc.getGL().glDisable(GL.GL_TEXTURE_2D);

        for (BoundaryInfo info : this.boundaries)
        {
            ExtrudedBoundaryInfo boundary = (ExtrudedBoundaryInfo) info;

            if (this.mustApplyLighting(dc))
                gl.glNormalPointer(GL.GL_FLOAT, 0, boundary.sideNormalBuffer.rewind());

            IntBuffer indices = boundary.sideEdgeIndices;
            indices.rewind();

            // Don't draw the top outline if the cap will draw it.
            if (this.isEnableCap() && this.getActiveCapAttributes().isDrawOutline())
            {
                indices = indices.slice();
                indices.position(2 * boundary.faceCount);
            }

            gl.glVertexPointer(3, GL.GL_FLOAT, 0, boundary.sideVertexBuffer.rewind());
            gl.glDrawElements(GL.GL_LINES, indices.remaining(), GL.GL_UNSIGNED_INT, indices);
        }
    }

    /**
     * Computes the information necessary to determine the extruded polygon's extent.
     *
     * @param dc the current draw context.
     */
    protected void createMinimalGeometry(DrawContext dc)
    {
        this.computeReferencePoint(dc);

        this.computeBoundaryVertices(dc, (ExtrudedBoundaryInfo) this.outerBoundary(), this.referencePoint);

        this.extent = this.computeExtent();
        this.eyeDistance = this.computeEyeDistance(dc);
    }

    protected void computeReferencePoint(DrawContext dc)
    {
        LatLon refPos = this.getReferencePosition();
        if (refPos == null)
            refPos = this.outerBoundary().locations.get(0);

        this.referencePoint = dc.computeTerrainPoint(refPos.getLatitude(), refPos.getLongitude(), 0);
    }

    /**
     * Computes the full geometry to render.
     *
     * @param dc                the current draw context.
     * @param skipOuterBoundary true if outer boundaries vertices do not need to be calculated, otherwise false.
     */
    protected void createVisualGeometry(DrawContext dc, boolean skipOuterBoundary)
    {
        if (this.isEnableCap())
            super.createVisualGeometry(dc, skipOuterBoundary);

        if (!this.isEnableSides())
            return;

        // The vertex buffer requires 4 vertices of x,y,z for each polygon face.
        int vertexCount = this.totalFaceCount * 4 * 3; // 4 vertices of x,y,z per face
        if (this.sideVertexBuffer != null && this.sideVertexBuffer.capacity() >= vertexCount)
            this.sideVertexBuffer.clear();
        else
            this.sideVertexBuffer = BufferUtil.newFloatBuffer(vertexCount);

        if (this.mustApplyLighting(dc))
        {
            if (this.sideNormalBuffer != null && this.sideNormalBuffer.capacity() >= vertexCount)
                this.sideNormalBuffer.clear();
            else
                this.sideNormalBuffer = BufferUtil.newFloatBuffer(vertexCount);
        }

        // Create individual buffer slices for each boundary.
        for (int i = 0; i < this.boundaries.size(); i++)
        {
            ExtrudedBoundaryInfo boundary = (ExtrudedBoundaryInfo) this.boundaries.get(i);

            if (i > 0 || !skipOuterBoundary)
                this.computeBoundaryVertices(dc, boundary, this.referencePoint);

            boundary.sideVertexBuffer = this.fillVertexBuffer(boundary.vertices, boundary.baseVertices,
                this.sideVertexBuffer.slice());
            this.sideVertexBuffer.position(this.sideVertexBuffer.position() + boundary.sideVertexBuffer.limit());

            if (this.mustApplyLighting(dc))
            {
                boundary.sideNormalBuffer = this.fillNormalBuffer(boundary.vertices, boundary.baseVertices,
                    this.sideNormalBuffer.slice());
                this.sideNormalBuffer.position(this.sideNormalBuffer.position() + boundary.sideNormalBuffer.limit());
            }

            boolean applyTextureToThisBoundary = this.hasSideTextures()
                && boundary.sideTextures != null && boundary.sideTextures.size() == boundary.faceCount;
            if (applyTextureToThisBoundary)
            {
                int texCoordSize = boundary.faceCount * 4 * 2; // n sides of 4 verts w/s,t
                if (boundary.textureCoords != null && boundary.textureCoords.capacity() >= texCoordSize)
                    boundary.textureCoords.clear();
                else
                    boundary.textureCoords = BufferUtil.newFloatBuffer(texCoordSize);

                this.fillSideTexCoordBuffer(boundary.vertices, boundary.baseVertices, boundary.textureCoords);
            }
        }
    }

    protected void computeBoundaryVertices(DrawContext dc, ExtrudedBoundaryInfo boundary, Vec4 refPoint)
    {
        Vec4[] topVertices = boundary.vertices;
        if (topVertices == null || topVertices.length < boundary.locations.size())
            topVertices = new Vec4[boundary.locations.size()];

        Vec4[] bottomVertices = boundary.baseVertices;
        if (bottomVertices == null || bottomVertices.length < boundary.locations.size())
            bottomVertices = new Vec4[boundary.locations.size()];

        Vec4 vaa = null;
        double vaaLength = 0; // used to compute independent length of each cap vertex
        double vaLength = 0;

        boundary.faceCount = boundary.locations.size() - 1;
        for (int i = 0; i < boundary.faceCount; i++)
        {
            // The order for both top and bottom is CCW as one looks down from space onto the base polygon. For a
            // 4-sided polygon (defined by 5 lat/lon locations) the vertex order is 0-1-2-3-4.

            LatLon location = boundary.locations.get(i);

            // Compute the bottom point, which is on the terrain.
            Vec4 vert = dc.computeTerrainPoint(location.getLatitude(), location.getLongitude(), 0);
            bottomVertices[i] = vert.subtract3(refPoint);

            // Compute the top/cap point.
            if (this.getAltitudeMode() == WorldWind.CONSTANT || !(location instanceof Position))
            {
                if (vaa == null)
                {
                    // Compute the vector lengths of the top and bottom points at the reference position.
                    vaa = refPoint.multiply3(this.getHeight() / refPoint.getLength3());
                    vaaLength = vaa.getLength3();
                    vaLength = refPoint.getLength3();
                }

                double delta = vaLength - vert.dot3(refPoint) / vaLength;
                vert = vert.add3(vaa.multiply3(1d + delta / vaaLength));
            }
            else if (this.getAltitudeMode() == WorldWind.RELATIVE_TO_GROUND)
            {
                vert = vert.add3(vert.normalize3().multiply3(((Position) location).getAltitude()));
            }
            else // WorldWind.ABSOLUTE
            {
                vert = dc.getGlobe().computePointFromPosition(location.getLatitude(), location.getLongitude(),
                    ((Position) location).getAltitude() * dc.getVerticalExaggeration());
            }

            topVertices[i] = vert.subtract3(refPoint);
        }

        topVertices[boundary.locations.size() - 1] = topVertices[0];
        bottomVertices[boundary.locations.size() - 1] = bottomVertices[0];

        boundary.vertices = topVertices;
        boundary.baseVertices = bottomVertices;
    }

    protected FloatBuffer fillVertexBuffer(Vec4[] topVerts, Vec4[] bottomVerts, FloatBuffer vBuf)
    {
        // Forms the polygon's faces from its vertices, simultaneously copying the Cartesian coordinates from a Vec4
        // array to a FloatBuffer.

        int faceCount = topVerts.length - 1;
        int size = faceCount * 4 * 3; // n sides of 4 verts of x,y,z
        vBuf.limit(size);

        // Fill the vertex buffer with coordinates for each independent face -- 4 vertices per face. Vertices need to be
        // independent in order to have different texture coordinates and normals per face.
        // For an n-sided polygon the vertex order is b0-b1-t1-t0, b1-b2-t2-t1, ... Note the counter-clockwise ordering.
        for (int i = 0; i < faceCount; i++)
        {
            int v = i;
            vBuf.put((float) bottomVerts[v].x).put((float) bottomVerts[v].y).put((float) bottomVerts[v].z);
            v = i + 1;
            vBuf.put((float) bottomVerts[v].x).put((float) bottomVerts[v].y).put((float) bottomVerts[v].z);
            v = i + 1;
            vBuf.put((float) topVerts[v].x).put((float) topVerts[v].y).put((float) topVerts[v].z);
            v = i;
            vBuf.put((float) topVerts[v].x).put((float) topVerts[v].y).put((float) topVerts[v].z);
        }

        vBuf.flip();

        return vBuf;
    }

    protected FloatBuffer fillNormalBuffer(Vec4[] topVerts, Vec4[] bottomVerts, FloatBuffer nBuf)
    {
        // This method parallels fillVertexBuffer. The normals are stored in exactly the same order.

        int faceCount = topVerts.length - 1;
        int size = faceCount * 4 * 3; // n sides of 4 verts of x,y,z
        nBuf.limit(size);

        for (int i = 0; i < faceCount; i++)
        {
            Vec4 va = topVerts[i + 1].subtract3(bottomVerts[i]);
            Vec4 vb = topVerts[i].subtract3(bottomVerts[i + 1]);
            Vec4 normal = va.cross3(vb).normalize3();

            nBuf.put((float) normal.x).put((float) normal.y).put((float) normal.z);
            nBuf.put((float) normal.x).put((float) normal.y).put((float) normal.z);
            nBuf.put((float) normal.x).put((float) normal.y).put((float) normal.z);
            nBuf.put((float) normal.x).put((float) normal.y).put((float) normal.z);
        }

        nBuf.flip();

        return nBuf;
    }

    protected FloatBuffer fillSideTexCoordBuffer(Vec4[] topVerts, Vec4[] bottomVerts, FloatBuffer tBuf)
    {
        int faceCount = topVerts.length - 1;
        double lengths[] = new double[faceCount + 1];

        // Find the top-to-bottom lengths of the corners in order to determine their relative lengths.
        for (int i = 0; i < faceCount; i++)
        {
            lengths[i] = bottomVerts[i].distanceTo3(topVerts[i]);
        }
        lengths[faceCount] = lengths[0]; // duplicate the first length to ease iteration below

        // Fill the vertex buffer with texture coordinates for each independent face in the same order as the vertices
        // in the vertex buffer.
        int b = 0;
        for (int i = 0; i < faceCount; i++)
        {
            // Set the base texture coord to 0 for the longer side and a proportional value for the shorter side.
            if (lengths[i] > lengths[i + 1])
            {
                tBuf.put(b++, 0).put(b++, 0);
                tBuf.put(b++, 1).put(b++, (float) (1d - lengths[i + 1] / lengths[i]));
            }
            else
            {
                tBuf.put(b++, 0).put(b++, (float) (1d - lengths[i] / lengths[i + 1]));
                tBuf.put(b++, 1).put(b++, 0);
            }
            tBuf.put(b++, 1).put(b++, 1);
            tBuf.put(b++, 0).put(b++, 1);
        }

        return tBuf;
    }

    /**
     * Returns the indices defining the vertices of each side of the shape.
     *
     * @param n the number of positions in the polygon.
     *
     * @return a buffer of indices that can be passed to OpenGL to draw all sides of the shape.
     */
    protected IntBuffer getSideIndices(int n)
    {
        IntBuffer ib = fillIndexBuffers.get(n);
        if (ib != null)
            return ib;

        // Compute them if not already computed. Each side is two triangles defined by one triangle strip. All edges
        // can't be combined into one tri-strip because each side may have its own texture and therefore different
        // texture coordinates.
        ib = BufferUtil.newIntBuffer(n * 4);
        for (int i = 0; i < n; i++)
        {
            ib.put(4 * i + 3).put(4 * i).put(4 * i + 2).put(4 * i + 1);
        }

        fillIndexBuffers.put(n, ib);

        return ib;
    }

    /**
     * Returns the indices defining the vertices of each edge of a boundary.
     *
     * @param n the number of positions in the boundary.
     *
     * @return a buffer of indices that can be passed to OpenGL to draw all the boundary's edges.
     */
    protected IntBuffer getSideEdgeIndices(int n)
    {
        IntBuffer ib = sideEdgeIndexBuffers.get(n);
        if (ib != null)
            return ib;

        int nn = n - 1; // the boundary is closed so don't add an edge for the redundant position.

        // The edges are two-point lines connecting vertex pairs.

        ib = BufferUtil.newIntBuffer((2 * nn) * 3); // 2n each for top, bottom and corners

        // Top. Keep this first so that the top edge can be turned off independently.
        for (int i = 0; i < nn; i++)
        {
            ib.put(4 * i + 2).put(4 * i + 3);
        }

        // Bottom
        for (int i = 0; i < nn; i++)
        {
            ib.put(4 * i).put(4 * i + 1);
        }

        // Corners
        for (int i = 0; i < nn; i++)
        {
            ib.put(4 * i).put(4 * i + 3);
        }

        sideEdgeIndexBuffers.put(n, ib);

        return ib;
    }

    protected String getKMLExtrudeValue()
    {
        return "1";
    }

    /** {@inheritDoc} */
    protected void writeKMLBoundaries(XMLStreamWriter xmlWriter) throws IOException, XMLStreamException
    {
        // Outer boundary
        Iterable<? extends LatLon> outerBoundary = this.getOuterBoundary();
        if (outerBoundary != null)
        {
            xmlWriter.writeStartElement("outerBoundaryIs");
            if (outerBoundary.iterator().hasNext() && outerBoundary.iterator().next() instanceof Position)
                this.exportBoundaryAsLinearRing(xmlWriter, outerBoundary);
            else
                KMLExportUtil.exportBoundaryAsLinearRing(xmlWriter, outerBoundary, getHeight());
            xmlWriter.writeEndElement(); // outerBoundaryIs
        }

        // Inner boundaries
        Iterator<BoundaryInfo> boundaryIterator = boundaries.iterator();
        if (boundaryIterator.hasNext())
            boundaryIterator.next(); // Skip outer boundary, we already dealt with it above

        while (boundaryIterator.hasNext())
        {
            BoundaryInfo boundary = boundaryIterator.next();

            xmlWriter.writeStartElement("innerBoundaryIs");
            if (boundary.locations.iterator().hasNext() && boundary.locations.iterator().next() instanceof Position)
                this.exportBoundaryAsLinearRing(xmlWriter, outerBoundary);
            else
                KMLExportUtil.exportBoundaryAsLinearRing(xmlWriter, boundary.locations, getHeight());
            xmlWriter.writeEndElement(); // innerBoundaryIs
        }
    }
}
