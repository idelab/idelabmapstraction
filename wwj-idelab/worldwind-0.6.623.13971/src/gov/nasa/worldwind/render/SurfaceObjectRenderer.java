/*
Copyright (C) 2001, 2010 United States Government as represented by
the Administrator of the National Aeronautics and Space Administration.
All Rights Reserved.
*/
package gov.nasa.worldwind.render;

import com.sun.opengl.util.texture.*;
import gov.nasa.worldwind.avlist.*;
import gov.nasa.worldwind.geom.*;
import gov.nasa.worldwind.layers.TextureTile;
import gov.nasa.worldwind.util.*;

import javax.media.opengl.GL;
import java.awt.*;
import java.util.*;
import java.util.List;

/**
 * Builds and renders a set of surface tiles who's content is defined by a specified set of {@link
 * gov.nasa.worldwind.render.SurfaceObject} references. It's typically not necessary to use SurfaceObjectRenderer
 * directly. World Wind's default {@link gov.nasa.worldwind.SceneController} automatically batches instances of {@link
 * gov.nasa.worldwind.render.SurfaceObject} in a single SurfaceObjectRenderer. Applications that need to draw basic
 * surface shapes should use or extend {@link gov.nasa.worldwind.render.SurfaceShape} instead of using
 * SurfaceObjectRenderer directly.
 * <p/>
 * Surface tiles are built by calling {@link #buildSurfaceTiles(DrawContext, Iterable)} with an Iterable of
 * SurfaceObjects. This assembles a set of surface tiles that meet the resolution requirements for the specified draw
 * context, then draws the SurfaceObjects into those offscreen surface tiles. This process may temporarily use the
 * framebuffer to perform offscreen rendering, and therefore should be called during the preRender method of a World
 * Wind {@link gov.nasa.worldwind.layers.Layer}. See {@link gov.nasa.worldwind.render.PreRenderable} for details. Once
 * built, the surface tiles can be rendered by calling {@link #renderSurfaceTiles(DrawContext)}.
 * <code>renderSurfaceTiles</code> draws the set of surface tiles on the draw context; the tiles do not change until the
 * next time <code>buildSurfaceTiles</code> is called.
 * <p/>
 * By default, SurfaceObjectRenderer creates texture tiles with a width and height of 512 pixels, and with internal
 * format <code>GL.GL_RGBA</code>. These parameters are configurable by calling {@link
 * #setTileDimension(java.awt.Dimension)} or {@link #setTileTextureFormat(int)}.
 * <p/>
 * The most common usage pattern for SurfaceObjectRenderer is to build the surface tiles from a set of SurfaceObjects
 * during the preRender phase, then draw those surface tiles during the render phase. For example, a {@link
 * gov.nasa.worldwind.render.Renderable} can use SurfaceObjectRenderer to draw a set of SurfaceObjects as follows:
 * <p/>
 * <code>
 * <pre>
 * class MyRenderable implements Renderable, PreRenderable
 * {
 *     protected SurfaceObjectRenderer renderer = new SurfaceObjectRenderer();
 * <p/>
 *     public void preRender(DrawContext dc)
 *     {
 *         List<?> surfaceObjects = Arrays.asList(
 *             new SurfaceCircle(LatLon.fromDegrees(0, 100), 10000),
 *             new SurfaceSquare(LatLon.fromDegrees(0, 101), 10000));
 *         this.renderer.buildSurfaceTiles(dc, surfaceObjects);
 *     }
 * <p/>
 *     public void render(DrawContext dc)
 *     {
 *         this.renderer.renderSurfaceTiles(dc);
 *     }
 * }
 * </pre>
 * </code>
 *
 * @author dcollins
 * @version $Id: SurfaceObjectRenderer.java 13894 2010-09-28 14:43:34Z dcollins $
 */
public class SurfaceObjectRenderer
{
    /** The default surface tile texture dimension, in pixels. */
    protected static final int DEFAULT_TEXTURE_DIMENSION = 512;
    /** The default OpenGL internal format used to create surface tile textures. */
    protected static final int DEFAULT_TEXTURE_INTERNAL_FORMAT = GL.GL_RGBA8;
    /** The default OpenGL pixel format used to create surface tile textures. */
    protected static final int DEFAULT_TEXTURE_PIXEL_FORMAT = GL.GL_RGBA;
    /**
     * The default split scale. The split scale 2.9 has been empirically determined to render sharp lines and edges with
     * the SurfaceShapes such as SurfacePolyline and SurfacePolygon.
     */
    protected static final double DEFAULT_SPLIT_SCALE = 2.9;
    /** The default level zero tile delta used to construct a LevelSet. */
    protected static final LatLon DEFAULT_LEVEL_ZERO_TILE_DELTA = LatLon.fromDegrees(36, 36);
    /**
     * The default number of levels used to construct a LevelSet. Approximately 0.1 meters per pixel at the Earth's
     * equator.
     */
    protected static final int DEFAULT_NUM_LEVELS = 17;
    /**
     * The next unique ID used to construct a LevelSet's unique cache name. This property is shared by all instances of
     * SurfaceObjectRenderer.
     */
    protected static long nextUniqueLevelSetId = 1;
    /** The name used to log the number of tiles rendered as a per-frame statistic. */
    protected static final String TILE_COUNT_NAME = "Surface Object Tiles";

    /** The surface tile texture width, in pixels. */
    protected int tileWidth = DEFAULT_TEXTURE_DIMENSION;
    /** The surafce tile texture height, in pixels. */
    protected int tileHeight = DEFAULT_TEXTURE_DIMENSION;
    /** The surface tile OpenGL texture format. 0 indicates the default format is used. */
    protected int tileTextureFormat;
    /** Controls if surface tiles are rendred using a linear filter or a nearest-neighbor filter. */
    protected boolean useLinearFilter = true;
    /** Controls if mip-maps are generated for surface tile textures. */
    protected boolean useMipmaps;
    /** Controls the renderer's displayed resolution as distance changes between the globe's surface and the eye point. */
    protected double splitScale = DEFAULT_SPLIT_SCALE;

    /** List of currently assebmled surface objects. Used during tile assembly and updating. */
    protected List<SurfaceObject> currentSurfaceObjects = new ArrayList<SurfaceObject>();
    /** List of currently assembled surface tiles. */
    protected List<SurfaceObjectTile> currentTiles = new ArrayList<SurfaceObjectTile>();
    /** Map associating a surface tile texture dimension to its corresponding LevelSet. */
    protected Map<Dimension, LevelSet> levelSetMap = new HashMap<Dimension, LevelSet>();
    /** Support class used to render to an offscreen surface tile. */
    protected OGLRenderToTextureSupport rttSupport = new OGLRenderToTextureSupport();

    /**
     * Constructs a new SurfaceObjectRenderer with a tile width and height of <code>512</code>, with the default tile
     * texture format, with linear filtering enabled, and with mip-mapping disabled.
     */
    public SurfaceObjectRenderer()
    {
    }

    /**
     * Constructs a new SurfaceObjectRenderer width the specified tile dimension, tile texture format, and flags
     * specifying if linear filtering and mip-mapping are enabled.
     *
     * @param tileTextureDimension the surface tile texture dimension, in pixels.
     * @param tileTextureFormat    the surface tile OpenGL texture format, or 0 to use the default format.
     * @param useLinearFilter      true to use linear filtering while rendering surface tiles; false to use
     *                             nearest-neighbor filtering.
     * @param useMipmaps           true to generate mip-maps for surface tile textures; false otherwise.
     *
     * @throws IllegalArgumentException if the tile dimension is null.
     */
    public SurfaceObjectRenderer(Dimension tileTextureDimension, int tileTextureFormat, boolean useLinearFilter,
        boolean useMipmaps)
    {
        if (tileTextureDimension == null)
        {
            String message = Logging.getMessage("nullValue.DimensionIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.setTileDimension(tileTextureDimension);
        this.setTileTextureFormat(tileTextureFormat);
        this.setUseLinearFilter(useLinearFilter);
        this.setUseMipmaps(useMipmaps);
    }

    /**
     * Returns the surface tile dimension.
     *
     * @return the surface tile dimension, in pixels.
     */
    public Dimension getTileDimension()
    {
        return new Dimension(this.tileWidth, this.tileHeight);
    }

    /**
     * Specifies the preferred surface tile texture dimension. If the dimension is larger than the viewport dimension,
     * this uses a dimension with width and height set to the largest power of two that is less than or equal to the
     * specified dimension and the viewport dimension.
     *
     * @param dimension the surface tile dimension, in pixels.
     *
     * @throws IllegalArgumentException if the dimension is null.
     */
    public void setTileDimension(Dimension dimension)
    {
        if (dimension == null)
        {
            String message = Logging.getMessage("nullValue.DimensionIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.tileWidth = dimension.width;
        this.tileHeight = dimension.height;
    }

    /**
     * Returns the surface tile's OpenGL texture format, or 0 to indicate that the default format is used.
     *
     * @return the OpenGL texture format, or 0 if the default format is used.
     *
     * @see {@link #setTileTextureFormat(int)}
     */
    public int getTileTextureFormat()
    {
        return tileTextureFormat;
    }

    /**
     * Specifies the surface tile's OpenGL texture format. A value of 0 indicates that the default format should be
     * used. Otherwise, the texture format may be one of the following: <code> <ul> <li>GL.GL_ALPHA</li>
     * <li>GL.GL_ALPHA4</li> <li>GL.GL_ALPHA8</li> <li>GL.GL_ALPHA12</li> <li>GL.GL_ALPHA16</li>
     * <li>GL.GL_COMPRESSED_ALPHA</li> <li>GL.GL_COMPRESSED_LUMINANCE</li> <li>GL.GL_COMPRESSED_LUMINANCE_ALPHA</li>
     * <li>GL.GL_COMPRESSED_INTENSITY</li> <li>GL.GL_COMPRESSED_RGB</li> <li>GL.GL_COMPRESSED_RGBA</li>
     * <li>GL.GL_DEPTH_COMPONENT</li> <li>GL.GL_DEPTH_COMPONENT16</li> <li>GL.GL_DEPTH_COMPONENT24</li>
     * <li>GL.GL_DEPTH_COMPONENT32</li> <li>GL.GL_LUMINANCE</li> <li>GL.GL_LUMINANCE4</li> <li>GL.GL_LUMINANCE8</li>
     * <li>GL.GL_LUMINANCE12</li> <li>GL.GL_LUMINANCE16</li> <li>GL.GL_LUMINANCE_ALPHA</li>
     * <li>GL.GL_LUMINANCE4_ALPHA4</li> <li>GL.GL_LUMINANCE6_ALPHA2</li> <li>GL.GL_LUMINANCE8_ALPHA8</li>
     * <li>GL.GL_LUMINANCE12_ALPHA4</li> <li>GL.GL_LUMINANCE12_ALPHA12</li> <li>GL.GL_LUMINANCE16_ALPHA16</li>
     * <li>GL.GL_INTENSITY</li> <li>GL.GL_INTENSITY4</li> <li>GL.GL_INTENSITY8</li> <li>GL.GL_INTENSITY12</li>
     * <li>GL.GL_INTENSITY16</li> <li>GL.GL_R3_G3_B2</li> <li>GL.GL_RGB</li> <li>GL.GL_RGB4</li> <li>GL.GL_RGB5</li>
     * <li>GL.GL_RGB8</li> <li>GL.GL_RGB10</li> <li>GL.GL_RGB12</li> <li>GL.GL_RGB16</li> <li>GL.GL_RGBA</li>
     * <li>GL.GL_RGBA2</li> <li>GL.GL_RGBA4</li> <li>GL.GL_RGB5_A1</li> <li>GL.GL_RGBA8</li> <li>GL.GL_RGB10_A2</li>
     * <li>GL.GL_RGBA12</li> <li>GL.GL_RGBA16</li> <li>GL.GL_SLUMINANCE</li> <li>GL.GL_SLUMINANCE8</li>
     * <li>GL.GL_SLUMINANCE_ALPHA</li> <li>GL.GL_SLUMINANCE8_ALPHA8</li> <li>GL.GL_SRGB</li> <li>GL.GL_SRGB8</li>
     * <li>GL.GL_SRGB_ALPHA</li> <li>GL.GL_SRGB8_ALPHA8</li> </ul> </code>
     * <p/>
     * If the texture format is any of <code>GL.GL_RGB, GL.GL_RGB8, GL.GL_RGBA, or GL.GL_RGBA8</code>, the renderer
     * attempts to use OpenGL framebuffer objects to render shapes to the texture tiles. Otherwise, this renders shapes
     * to the framebuffer and copies the framebuffer contents to the texture tiles.
     *
     * @param textureFormat the OpenGL texture format, or 0 to use the default format.
     */
    public void setTileTextureFormat(int textureFormat)
    {
        this.tileTextureFormat = textureFormat;
    }

    /**
     * Returns if linear filtering is used when rendering surface tiles.
     *
     * @return true if linear filtering is used; false if nearest-neighbor filtering is used.
     */
    public boolean isUseLinearFilter()
    {
        return useLinearFilter;
    }

    /**
     * Specifies if linear filtering should be used when rendering surface tiles.
     *
     * @param useLinearFilter true to use linear filtering; false to use nearest-neighbor filtering.
     */
    public void setUseLinearFilter(boolean useLinearFilter)
    {
        this.useLinearFilter = useLinearFilter;
    }

    /**
     * Returns if mip-maps are generated for surface tile textures.
     *
     * @return true if mip-maps are generated; false otherwise.
     */
    public boolean isUseMipmaps()
    {
        return this.useMipmaps;
    }

    /**
     * Specifies if mip-maps should be generated for surface tile textures.
     *
     * @param useMipmaps true to generate mip-maps; false otherwise.
     */
    public void setUseMipmaps(boolean useMipmaps)
    {
        this.useMipmaps = useMipmaps;
    }

    /**
     * Sets the parameter controlling the renderer's displayed resolution as distance changes between the globe's
     * surface and the eye point. Higher resolution is displayed as the split scale increases from 1.0. Lower resolution
     * is displayed as the split scale decreases from 1.0. The default value is 2.9.
     *
     * @param splitScale a value near 1.0 that controls the renderer's surface texel resolution as the distance between
     *                   the globe's surface and the eye point change. Increasing values select higher resolution,
     *                   decreasing values select lower resolution. The default value is 2.9.
     */
    public void setSplitScale(double splitScale)
    {
        this.splitScale = splitScale;
    }

    /**
     * Returns the split scale value controlling the renderer's surface texel resolution relative to the distance
     * between the globe's surface at the image position and the eye point.
     *
     * @return the current split scale.
     *
     * @see #setSplitScale(double)
     */
    public double getSplitScale()
    {
        return this.splitScale;
    }

    /**
     * Assembles the surface tiles and draws any SurfaceObjects in the iterable into those offscreen tiles. The surface
     * tiles are assembled to meet the necessary resolution of to the draw context's {@link gov.nasa.worldwind.View}.
     * This may temporarily use the framebuffer to perform offscreen rendering, and therefore should be called during
     * the preRender method of a World Wind {@link gov.nasa.worldwind.layers.Layer}. Upon returning, the surface tiles
     * can be rendered by calling {@link #renderSurfaceTiles(DrawContext)}.
     * <p/>
     * This method is benign if the specified iterable is null, empty or contains no SurfaceObjects.
     *
     * @param dc       the draw context to build tiles for.
     * @param iterable the iterable to gather SurfaceObjects from.
     *
     * @throws IllegalArgumentException if the draw context is null.
     */
    public void buildSurfaceTiles(DrawContext dc, Iterable<?> iterable)
    {
        if (dc == null)
        {
            String message = Logging.getMessage("nullValue.DrawContextIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.currentSurfaceObjects.clear();
        this.currentTiles.clear();

        if (iterable == null)
            return;

        // Assemble the list of current surface objects from the specified iterable.
        this.assembleSurfaceObjects(iterable);

        // We've cleared any tile assembly state from the last rendering pass. Determine if we can assemble and update
        // the tiles. If not, we're done.
        if (this.currentSurfaceObjects.isEmpty() || !this.canAssembleTiles(dc))
            return;

        // Assemble the current visible tiles and update their associated textures if necessary.
        this.assembleTiles(dc);
        this.updateTiles(dc);

        // Objects in the current surface object list are no longer needed. Clear the lists to ensure we don't retain
        // dangling references to the surface objects.
        this.currentSurfaceObjects.clear();

        // Tiles in the current tile list contain references to SurfaceObjects. These references are used during tile
        // update, and are longer needed. Clear these lists to ensure we don't retain any dangling references to the
        // surface objects.
        for (SurfaceObjectTile tile : this.currentTiles)
        {
            tile.clearObjectList();
        }
    }

    /**
     * Draws the current set of surface tiles on the specified draw context. The tiles do not change until the next time
     * {@link #buildSurfaceTiles(DrawContext, Iterable)} is called. This draws the surface tiles using the draw
     * context's {@link gov.nasa.worldwind.render.SurfaceTileRenderer}. During rendering mode, this draws the surface
     * tiles by multiplying the surface tile colors with the current OpenGL color. During picking mode, this replaces
     * all non-transparent pixels in the surface tiles with the current OpenGL color.
     * <p/>
     * This method is benign if {@link #buildSurfaceTiles(DrawContext, Iterable)} has not been called, or if the
     * Iterable passed to <code>buildSurfaceTiles</code> was null, empty or contained no SurfaceObjects.
     *
     * @param dc the <code>DrawContext</code> to be used.
     *
     * @throws IllegalArgumentException if <code>dc</code> is null.
     */
    public void renderSurfaceTiles(DrawContext dc)
    {
        if (dc == null)
        {
            String message = Logging.getMessage("nullValue.DrawContextIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        if (this.currentTiles.isEmpty())
            return;

        GL gl = dc.getGL();

        int attributeMask =
            GL.GL_COLOR_BUFFER_BIT   // For alpha test func and ref, blend func.
                | GL.GL_POLYGON_BIT; // For polygon mode, cull enable and cull face.

        OGLStackHandler ogsh = new OGLStackHandler();
        ogsh.pushAttrib(gl, attributeMask);
        try
        {
            gl.glEnable(GL.GL_CULL_FACE);
            gl.glCullFace(GL.GL_BACK);
            gl.glPolygonMode(GL.GL_FRONT, GL.GL_FILL);

            if (!dc.isPickingMode())
            {
                // Enable blending in premultiplied color mode.
                gl.glEnable(GL.GL_BLEND);
                OGLUtil.applyBlending(gl, true);
            }

            dc.getGeographicSurfaceTileRenderer().renderTiles(dc, this.currentTiles);
            dc.setPerFrameStatistic(PerformanceStatistic.IMAGE_TILE_COUNT, TILE_COUNT_NAME, this.currentTiles.size());
        }
        finally
        {
            ogsh.pop(dc.getGL());
        }
    }

    //**************************************************************//
    //********************  Tile Updating  *************************//
    //**************************************************************//

    /**
     * Updates each {@link gov.nasa.worldwind.render.SurfaceObjectRenderer.SurfaceObjectTile} in the {@link
     * #currentTiles} list. This is typically called after {@link #assembleTiles(DrawContext)} to update the assembled
     * tiles.
     * <p/>
     * This method is benign if <code>currentTiles</code> is empty.
     *
     * @param dc the draw context the tiles relate to.
     */
    protected void updateTiles(DrawContext dc)
    {
        if (this.currentTiles.isEmpty())
            return;

        // The tile drawing rectangle has the same dimension as the current tile viewport, but it's lower left corner
        // is placed at the origin. This is because the orthographic projection setup by OGLRenderToTextureSupport
        // maps (0, 0) to the lower left corner of the drawing region, therefore we can drop the (x, y) offset when
        // drawing pixels to the texture, as (0, 0) is automatically mapped to (x, y). Since we've created the tiles
        // from a LevelSet where each level has equivalent dimension, we assume that tiles in the current tile list
        // have equivalent dimension.
        int tileWidth = this.currentTiles.get(0).getWidth();
        int tileHeight = this.currentTiles.get(0).getHeight();

        // The OpenGL framebuffer object extension used by RenderToTextureSupport works only for texture formats
        // GL_RGB and GL_RGBA. Disable framebuffer objects if the renderer has been configured with a different format.
        this.rttSupport.setEnableFramebufferObject(
            this.tileTextureFormat == 0 || // Default format is GL_RGB8.
                this.tileTextureFormat == GL.GL_RGB ||
                this.tileTextureFormat == GL.GL_RGB8 ||
                this.tileTextureFormat == GL.GL_RGBA ||
                this.tileTextureFormat == GL.GL_RGBA8);

        this.rttSupport.beginRendering(dc, 0, 0, tileWidth, tileHeight);
        try
        {
            for (SurfaceObjectTile tile : this.currentTiles)
            {
                this.updateTile(dc, tile);
            }
        }
        finally
        {
            this.rttSupport.endRendering(dc);
        }
    }

    /**
     * Draws the current list of SurfaceObjects into the specififed surface tile. The surface tiles is updated only when
     * necessary. The tile keeps track of the list of SurfaceObjects rendered into it, and the state keys those objects.
     * The tile is updated if the list changes, if any of the state keys change, or if the tile has no texture.
     * Otherwise the tile is left unchanged and the update is skipped.
     *
     * @param dc   the draw context the tile relates to.
     * @param tile the tile to update.
     */
    protected void updateTile(DrawContext dc, SurfaceObjectTile tile)
    {
        // Get the tile's texture from the draw context's texture cache. If null we create a new texture and update the
        // texture cache below.
        Texture texture = tile.getTexture(dc.getTextureCache());

        // Compare the previous tile state against the currently computed state to determine if the tile needs to be
        // updated. The tile needs to be updated if any the following conditions are true:
        // * The tile has no texture.
        // * The tile has no state.
        // * The list of intersecting objects has changed.
        // * An intersecting object's state key different than one stored in the tile's previous state key.
        Object tileStateKey = tile.getStateKey(dc);

        if (texture != null && tileStateKey.equals(tile.lastUpdateStateKey))
            return;

        if (texture == null) // Create the tile's texture if it doesn't already have one.
        {
            texture = this.createTileTexture(dc, tile.getWidth(), tile.getHeight());
            tile.setTexture(dc.getTextureCache(), texture);
        }

        if (texture == null) // This should never happen, but we check anyway.
        {
            Logging.logger().warning(Logging.getMessage("nullValue.TextureIsNull"));
            return;
        }

        // Create a SurfaceTileDrawContext with the tile's Sector and viewport. The Sector defines the context's
        // geographic extent, and the viewport defines the context's corresponding viewport in pixels.
        SurfaceTileDrawContext sdc = new SurfaceTileDrawContext(tile.getSector(), tile.getWidth(), tile.getHeight());
        try
        {
            this.rttSupport.setColorTarget(dc, texture);
            this.rttSupport.clear(dc, new Color(0, 0, 0, 0)); // Set all texture pixels to transparent black.
            // SurfaceObjects expect the SurfaceTileDrawContext to be attached to the draw context's AVList.
            dc.setValue(AVKey.SURFACE_TILE_DRAW_CONTEXT, sdc);

            for (SurfaceObject so : tile.getObjectList())
            {
                so.render(dc);
            }
        }
        finally
        {
            dc.removeKey(AVKey.SURFACE_TILE_DRAW_CONTEXT);
            this.rttSupport.setColorTarget(dc, null);
        }

        tile.lastUpdateStateKey = tileStateKey;
    }

    /**
     * Returns a new surface tile texture for use on the specified draw context with the specified width and height.
     * <p/>
     * The returned texture's internal format is specified by <code>tilePixelFormat</code>. If
     * <code>tilePixelFormat</code> is zero, this returns a texture with internal format <code>GL.GL_RGBA8</code>.
     * <p/>
     * The returned texture's parameters are configured as follows: <table> <tr><th>Parameter
     * Name</th><th>Value</th></tr> <tr><td><code>GL.GL_TEXTURE_MIN_FILTER</code></td><td><code>GL_LINEAR_MIPMAP_LINEAR</code>
     * if <code>useLinearFilter</code> and <code>useMipmaps</code> are both true, <code>GL_LINEAR</code> if
     * <code>useLinearFilter</code> is true and <code>useMipmaps</code> is false, and <code>GL_NEAREST</code> if
     * <code>useLinearFilter</code> is false.</td></tr> <tr><td><code>GL.GL_TEXTURE_MAG_FILTER</code></td><td><code>GL_LINEAR</code>
     * if <code>useLinearFilter</code> is true, <code>GL_NEAREST</code> if <code>useLinearFilter</code> is
     * false.</td></tr> <tr><td><code>GL.GL_TEXTURE_WRAP_S</code></td><td><code>GL_CLAMP_TO_EDGE</code></td></tr>
     * <tr><td><code>GL.GL_TEXTURE_WRAP_T</code></td><td><code>GL_CLAMP_TO_EDGE</code></td></tr>
     *
     * @param dc     the draw context to create a texture for.
     * @param width  the texture's width, in pixels.
     * @param height the texture's height, in pixels.
     *
     * @return a new texture with the specified width and height.
     */
    protected Texture createTileTexture(DrawContext dc, int width, int height)
    {
        int internalFormat = this.tileTextureFormat;
        if (internalFormat == 0)
            internalFormat = DEFAULT_TEXTURE_INTERNAL_FORMAT;

        int pixelFormat = OGLUtil.computeTexturePixelFormat(internalFormat);
        if (pixelFormat == 0)
            pixelFormat = DEFAULT_TEXTURE_PIXEL_FORMAT;

        Texture t;

        GL gl = dc.getGL();
        OGLStackHandler ogsh = new OGLStackHandler();
        ogsh.pushAttrib(gl, GL.GL_TEXTURE_BIT);
        try
        {
            TextureData td = new TextureData(
                internalFormat,       // internal format
                width, height,        // dimension
                0,                    // border
                pixelFormat,          // pixel format
                GL.GL_UNSIGNED_BYTE,  // pixel type
                this.isUseMipmaps(),  // mipmap
                false, false,         // dataIsCompressed, mustFlipVertically
                null, null)           // buffer, flusher
            {
                /**
                 * Overridden to return a non-zero size. TextureData does not compute an estimated memory size if the buffer
                 * is null. Therefore we override getEstimatedMemorySize() to return the appropriate size in bytes of a
                 * texture with the common pixel formats.
                 */
                @Override
                public int getEstimatedMemorySize()
                {
                    int sizeInBytes = OGLUtil.estimateTextureMemorySize(this.getInternalFormat(), this.getWidth(),
                        this.getHeight(), this.getMipmap());
                    if (sizeInBytes > 0)
                        return sizeInBytes;

                    return super.getEstimatedMemorySize();
                }
            };

            t = TextureIO.newTexture(td);
            t.bind();

            gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MIN_FILTER, this.isUseLinearFilter() ?
                (this.isUseMipmaps() ? GL.GL_LINEAR_MIPMAP_LINEAR : GL.GL_LINEAR) : GL.GL_NEAREST);
            gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_MAG_FILTER, this.isUseLinearFilter() ?
                GL.GL_LINEAR : GL.GL_NEAREST);
            gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_S, GL.GL_CLAMP_TO_EDGE);
            gl.glTexParameteri(GL.GL_TEXTURE_2D, GL.GL_TEXTURE_WRAP_T, GL.GL_CLAMP_TO_EDGE);
        }
        finally
        {
            ogsh.pop(gl);
        }

        return t;
    }

    //**************************************************************//
    //********************  Surface Object Assembly  ***************//
    //**************************************************************//

    /**
     * Adds any SurfaceObjects in the specified Iterable to the renderer's {@link #currentSurfaceObjects} list.
     *
     * @param iterable the Iterable to gather SurfaceObjects from.
     */
    protected void assembleSurfaceObjects(Iterable<?> iterable)
    {
        // Gather up all the SurfaceObjects, ignoring null references and non SurfaceObjects.
        for (Object o : iterable)
        {
            if (o instanceof SurfaceObject)
                this.currentSurfaceObjects.add((SurfaceObject) o);
        }
    }

    //**************************************************************//
    //********************  LevelSet Assembly  *********************//
    //**************************************************************//

    /**
     * Returns a {@link gov.nasa.worldwind.util.LevelSet} who's tile dimension fits in the specified DrawContext's
     * viewport. The returned LevelSet's tile dimension is always square and a power of two. If the viewport is larger
     * than the renderer's {@link #tileWidth} and {@link #tileHeight}, this returns a LevelSet with tile dimension set
     * to the maximum of <code>tileWidth</code> and <code>tileHeight</code>. Otherwise, this returns a LevelSet with
     * tile dimension set to the smallest power of two that fits in the viewport.
     * <p/>
     * Note that subsequent calls are guaranteed to return the same LevelSet for the same viewport dimensions.
     *
     * @param dc the DrawContext to return a LevelSet for.
     *
     * @return a LevelSet who's tile dimension fits in the DrawContext's viewport.
     */
    protected LevelSet getLevelSet(DrawContext dc)
    {
        // Determine the maximum dimension we can render into, depending on the viewport dimension. Create a LevelSet
        // for each unique dimension and keep the LevelSets in a map. In practice, there are at most 10 dimensions we
        // use: 512, 256, 128, 64, 32, 16, 8, 4, 2, 1. Therefore keeping the LevelSets in a map requires little memory
        // overhead, and ensures each LevelSet is retained once constructed. Retaining references to the LevelSets
        // means we're able to re-use the texture resources associated with each LevelSet in the DrawContext's texture
        // cache.

        // Force a square dimension by using the maximum of the renderer's tileWidth and tileHeight.
        int maxSize = Math.max(this.tileWidth, this.tileHeight);

        // The viewport may be smaller than the desired dimension. For that reason, we constrain the desired tile
        // dimension by the viewport width and height.
        Rectangle viewport = dc.getView().getViewport();
        if (maxSize > viewport.width)
            maxSize = viewport.width;
        if (maxSize > viewport.height)
            maxSize = viewport.height;

        // The final dimension used to render all surface tiles will be the power of two which is less than or equal to
        // the preferred dimension, and which fits into the viewport.
        int potSize = WWMath.powerOfTwoFloor(maxSize);
        Dimension dimension = new Dimension(potSize, potSize);

        // If we already have a LevelSet for the dimension, just return it. Otherwise create it and put it in a map for
        // use during subesequent calls.
        LevelSet levelSet = this.levelSetMap.get(dimension);
        if (levelSet == null)
        {
            levelSet = this.createLevelSet(dimension.width, dimension.height);
            this.levelSetMap.put(dimension, levelSet);
        }

        return levelSet;
    }

    /**
     * Returns a new LevelSet with the specified tile width and height. The LevelSet overs the full sphere, has a level
     * zero tile delta of {@link #DEFAULT_LEVEL_ZERO_TILE_DELTA}, has number of levels equal to {@link
     * #DEFAULT_NUM_LEVELS} (with no empty levels). Additionally, the LevelSets' cache name and dataset name are
     * configured by calling {@link #makeCacheName()}, and a dummy format suffix of <code>".xyz"</code>. The dummy
     * format suffix is never used.
     *
     * @param tileWidth  the LevelSet's tile width, in pixels.
     * @param tileHeight the LevelSet's tile height, in pixels.
     *
     * @return a new LevelSet configured to with
     */
    protected LevelSet createLevelSet(int tileWidth, int tileHeight)
    {
        AVList params = new AVListImpl();
        params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, DEFAULT_LEVEL_ZERO_TILE_DELTA);
        params.setValue(AVKey.SECTOR, Sector.FULL_SPHERE);
        params.setValue(AVKey.NUM_LEVELS, DEFAULT_NUM_LEVELS);
        params.setValue(AVKey.NUM_EMPTY_LEVELS, 0);
        params.setValue(AVKey.TILE_WIDTH, tileWidth);
        params.setValue(AVKey.TILE_HEIGHT, tileHeight);
        // Create a unique string representing this instance's cache name and dataset name.
        String cacheName = this.makeCacheName();
        params.setValue(AVKey.DATA_CACHE_NAME, cacheName);
        params.setValue(AVKey.DATASET_NAME, cacheName);
        // We won't use any tile resource paths, so just supply a dummy format suffix.
        params.setValue(AVKey.FORMAT_SUFFIX, ".xyz");

        return new LevelSet(params);
    }

    /**
     * Returns a unique name appropriate for use as part of a cache name. This is called by {@link #createLevelSet(int,
     * int)} to construct a unique cache name for the renderer's LevelSets. The returned string is constructed as
     * follows: <code>this.getClass().getName() + "/" + nextUniqueLevelSetId()</code>.
     *
     * @return a unique cache name.
     */
    protected String makeCacheName()
    {
        StringBuilder sb = new StringBuilder();
        sb.append(this.getClass().getName());
        sb.append("/");
        sb.append(nextUniqueLevelSetId());

        return sb.toString();
    }

    /**
     * Returns the next unique ID used to construct a LevelSet's unique cache name.
     *
     * @return the next unique LevelSet ID.
     */
    protected static long nextUniqueLevelSetId()
    {
        return nextUniqueLevelSetId++;
    }

    //**************************************************************//
    //********************  Tile Assembly  *************************//
    //**************************************************************//

    /**
     * Returns true if the draw context's viewport width and height are greater than zero.
     *
     * @param dc the DrawContext to test.
     *
     * @return true if the DrawContext's has a non-zero viewport; false otherwise.
     */
    protected boolean canAssembleTiles(DrawContext dc)
    {
        Rectangle viewport = dc.getView().getViewport();
        return viewport.getWidth() > 0 && viewport.getHeight() > 0;
    }

    /**
     * Assembles a set of surface tiles that are visible in the specified DrawContext and meet the renderer's resolution
     * criteria. The tiles belong to the LevelSet returned by {@link #getLevelSet(DrawContext)}, and are added to the
     * renderer's {@link #currentTiles} list. Tiles are culled against the current SurfaceObject list, against the
     * DrawContext's view frustum during rendering mode, and against the DrawContext's pick frustums during picking
     * mode. If a tile does not meet the renderer's resolution criteria, it's split into four sub-tiles and the process
     * recursively repeated on the sub-tiles.
     * <p/>
     * During assembly each SurfaceObject in {@link #currentSurfaceObjects} is sorted into the tiles they intersect. The
     * top level tiles are used as an index to quickly determine which tiles each SurfaceObjects intersects.
     * SurfaceObjects are sorted into sub-tiles by simple intersection tests. SurfaceObjects are added to each tile's
     * surface object list at most once. See {@link gov.nasa.worldwind.render.SurfaceObjectRenderer.SurfaceObjectTile#addSurfaceObject(SurfaceObject,
     * gov.nasa.worldwind.geom.Sector)}. Tiles that don't intersect any SurfaceObjects are discarded.
     *
     * @param dc the DrawContext to assemble tiles for.
     */
    protected void assembleTiles(DrawContext dc)
    {
        LevelSet levelSet = this.getLevelSet(dc);

        Level level = levelSet.getFirstLevel();
        Angle dLat = level.getTileDelta().getLatitude();
        Angle dLon = level.getTileDelta().getLongitude();
        Angle latOrigin = levelSet.getTileOrigin().getLatitude();
        Angle lonOrigin = levelSet.getTileOrigin().getLongitude();

        // Store the top level tiles in a set to ensure there are no duplicates.
        Set<SurfaceObjectTile> topLevelTiles = new HashSet<SurfaceObjectTile>();

        // Iterate over the current surface objects, adding each surface object to the top level tiles that it
        // intersects. This produces a set of top level tiles containing the surface objects that intersect each tile.
        // We use the tile structure as an index to quickly determine the tiles a surface object intersects, and add
        // object to those tiles. This has the effect of quickly sorting the objects into the top level tiles.
        // When the objects cover multiple top level tiles, this 
        for (SurfaceObject so : this.currentSurfaceObjects)
        {
            List<Sector> sectors = so.getSectors(dc);
            if (sectors == null)
                continue;

            for (Sector s : sectors)
            {
                // Use the LevelSets tiling scheme to index the surface object's sector into the top level tiles. This
                // index operation is faster than computing an intersection test between each tile and the list of
                // surface objects.
                int firstRow = Tile.computeRow(dLat, s.getMinLatitude(), latOrigin);
                int firstCol = Tile.computeColumn(dLon, s.getMinLongitude(), lonOrigin);
                int lastRow = Tile.computeRow(dLat, s.getMaxLatitude(), latOrigin);
                int lastCol = Tile.computeColumn(dLon, s.getMaxLongitude(), lonOrigin);

                Angle p1 = Tile.computeRowLatitude(firstRow, dLat, latOrigin);
                for (int row = firstRow; row <= lastRow; row++)
                {
                    Angle p2;
                    p2 = p1.add(dLat);

                    Angle t1 = Tile.computeColumnLongitude(firstCol, dLon, lonOrigin);
                    for (int col = firstCol; col <= lastCol; col++)
                    {
                        Angle t2;
                        t2 = t1.add(dLon);

                        TileKey tileKey = new TileKey(level.getLevelNumber(), row, col, level.getCacheName());
                        SurfaceObjectTile tile = (SurfaceObjectTile) TextureTile.getMemoryCache().getObject(tileKey);
                        if (tile == null)
                        {
                            tile = new SurfaceObjectTile(new Sector(p1, p2, t1, t2), level, row, col);
                            TextureTile.getMemoryCache().add(tileKey, tile);
                        }

                        topLevelTiles.add(tile); // Set of top level tiles ensures no duplicates tiles.
                        tile.addSurfaceObject(so, s); // Set of surface objects ensures no duplicate objects.

                        t1 = t2;
                    }
                    p1 = p2;
                }
            }
        }

        // Add each top level tile or its descendants to the current tile list.
        for (SurfaceObjectTile tile : topLevelTiles)
        {
            this.addTileOrDescendants(dc, levelSet, null, tile);
        }
    }

    /**
     * Potentially adds the specified tile or its descendants to the renderer's {@link #currentTiles} list. The tile and
     * its descendants are discarded if the tile is not visible or does not intersect any SurfaceObjects in the parent's
     * surface object list. See {@link gov.nasa.worldwind.render.SurfaceObjectRenderer.SurfaceObjectTile#getObjectList()}.
     * <p/>
     * If the tile meet the renderer's resolution criteria it's added to the renderer's <code>currentTiles</code> list.
     * Otherwise, it's split into four sub-tiles and each tile is recursively processed. See {@link
     * #meetsRenderCriteria(DrawContext, gov.nasa.worldwind.util.LevelSet, gov.nasa.worldwind.util.Tile)}.
     *
     * @param dc       the current DrawContext.
     * @param levelSet the tile's LevelSet.
     * @param parent   the tile's parent, or null if the tile is a top level tile.
     * @param tile     the tile to add.
     */
    protected void addTileOrDescendants(DrawContext dc, LevelSet levelSet, SurfaceObjectTile parent,
        SurfaceObjectTile tile)
    {
        // Ignore this tile if it falls completely outside the DrawContext's visible sector.
        if (!this.intersectsVisibleSector(dc, tile))
            return;

        // Ignore this tile if it falls completely outside the frustum. This may be the viewing frustum or the pick
        // frustum, depending on the implementation.
        if (!this.intersectsFrustum(dc, tile))
            return;

        // If the parent tile is not null, add any parent surface objects that intersect this tile.
        if (parent != null)
            this.addIntersectingObjects(dc, parent, tile);

        // Ignore tiles that do not intersect any surface objects.
        if (tile.getObjectList().isEmpty())
            return;

        // If this tile meets the current rendering criteria, add it to the current tile list. This tile's object list
        // is cleared after the tile update operation.
        if (this.meetsRenderCriteria(dc, levelSet, tile))
        {
            this.addTile(tile);
            return;
        }

        Level nextLevel = levelSet.getLevel(tile.getLevelNumber() + 1);
        for (TextureTile subTile : tile.createSubTiles(nextLevel))
        {
            this.addTileOrDescendants(dc, levelSet, tile, (SurfaceObjectTile) subTile);
        }

        // This tile is not added to the current tile list, so we clear it's object list to prepare it for use during
        // the next frame.
        tile.clearObjectList();
    }

    /**
     * Adds SurfaceObjects from the parent's object list to the specified tile's object list. If the tile's sector does
     * not intersect the sector bounding the parent's object list, this does nothing. Otherwise, this adds any of the
     * parent's SurfaceObjects that intersect the tile's sector to the tile's object list.
     *
     * @param dc     the current DrawContext.
     * @param parent the tile's parent.
     * @param tile   the tile to add intersecting SurfaceObject to.
     */
    protected void addIntersectingObjects(DrawContext dc, SurfaceObjectTile parent, SurfaceObjectTile tile)
    {
        // If this tile does not intersect the parent's object bounding sector, then none of the parent's objects
        // intersect this tile. Therefore we exit immediately, and do not add any objects to this tile.
        if (!tile.getSector().intersects(parent.getObjectSector()))
            return;

        // If this tile contains the parent's object bounding sector, then all of the parent's objects intersect this
        // tile. Therefore we just add all of the parent's objects to this tile. Additionally, the parent's object
        // bounding sector becomes this tile's object bounding sector.
        if (tile.getSector().contains(parent.getObjectSector()))
        {
            tile.addAllSurfaceObjects(parent.getObjectList(), parent.getObjectSector());
        }
        // Otherwise, the tile may intersect some of the parent's object list. Compute which objects intersect this
        // tile, and compute this tile's bounding sector as the union of those object's sectors.
        else
        {
            for (SurfaceObject so : parent.getObjectList())
            {
                List<Sector> sectors = so.getSectors(dc);
                if (sectors == null)
                    continue;

                // Test intersection against each of the SurfaceObject's sectors.
                for (Sector s : sectors)
                {
                    if (tile.getSector().intersects(s))
                        tile.addSurfaceObject(so, s); // Set of surface objects ensures no duplicate objects.
                }
            }
        }
    }

    /**
     * Adds the specified tile to the renderer's {@link #currentTiles} list.
     *
     * @param tile the tile to add.
     */
    protected void addTile(SurfaceObjectTile tile)
    {
        this.currentTiles.add(tile);
        TextureTile.getMemoryCache().add(tile.getTileKey(), tile);
    }

    /**
     * Test if the tile intersects the specified draw context's frustum. During picking mode, this tests intersection
     * against all of the draw context's pick frustums. During rendering mode, this tests intersection against the draw
     * context's viewing frustum.
     *
     * @param dc   the draw context the SurfaceObject is related to.
     * @param tile the tile to test for intersection.
     *
     * @return true if the tile intersects the draw context's frustum; false otherwise.
     */
    protected boolean intersectsFrustum(DrawContext dc, TextureTile tile)
    {
        Extent extent = tile.getExtent(dc);
        if (extent == null)
            return false;

        if (dc.isPickingMode())
            return dc.getPickFrustums().intersectsAny(extent);

        return dc.getView().getFrustumInModelCoordinates().intersects(extent);
    }

    /**
     * Test if the specified tile intersects the draw context's visible sector. This returns false if the draw context's
     * visible sector is null.
     *
     * @param dc   the current draw context.
     * @param tile the tile to test for intersection.
     *
     * @return true if the tile intersects the draw context's visible sector; false otherwise.
     */
    protected boolean intersectsVisibleSector(DrawContext dc, TextureTile tile)
    {
        return dc.getVisibleSector() != null && dc.getVisibleSector().intersects(tile.getSector());
    }

    /**
     * Tests if the specified tile meets the rendering criteria on the specified draw context. This returns true if the
     * tile is from the level set's final level, or if the tile achieves the desired resolution on the draw context.
     *
     * @param dc       the current draw context.
     * @param levelSet the level set the tile belongs to.
     * @param tile     the tile to test.
     *
     * @return true if the tile meets the rendering criteria; false otherwise.
     */
    protected boolean meetsRenderCriteria(DrawContext dc, LevelSet levelSet, Tile tile)
    {
        return levelSet.isFinalLevel(tile.getLevel().getLevelNumber()) || !this.needToSplit(dc, tile);
    }

    /**
     * Tests if the specified tile must be split to meets the desired resolution on the specified draw context. This
     * compares the distance form the eye point to the tile to determine if the tile meets the desired resolution for
     * the {@link gov.nasa.worldwind.View} attached to the draw context.
     *
     * @param dc   the current draw context.
     * @param tile the tile to test.
     *
     * @return true if the tile must be split; false otherwise.
     */
    protected boolean needToSplit(DrawContext dc, Tile tile)
    {
        Vec4[] corners = tile.getSector().computeCornerPoints(dc.getGlobe(), dc.getVerticalExaggeration());
        Vec4 centerPoint = tile.getSector().computeCenterPoint(dc.getGlobe(), dc.getVerticalExaggeration());

        Vec4 eyePoint = dc.getView().getEyePoint();
        double d1 = eyePoint.distanceTo3(corners[0]);
        double d2 = eyePoint.distanceTo3(corners[1]);
        double d3 = eyePoint.distanceTo3(corners[2]);
        double d4 = eyePoint.distanceTo3(corners[3]);
        double d5 = eyePoint.distanceTo3(centerPoint);

        double minDistance = d1;
        if (d2 < minDistance)
            minDistance = d2;
        if (d3 < minDistance)
            minDistance = d3;
        if (d4 < minDistance)
            minDistance = d4;
        if (d5 < minDistance)
            minDistance = d5;

        // Compute the cell size as a function of the tile's latitude delta in meters and the renderer's target tile
        // density in texels. We use the target density instead of the tile's actual density to ensure that tiles
        // smaller than the target density are split according to the same metric. Smaller tiles are used when the
        // viewport dimension is smaller than the target density. In this case, using the same metric to split tiles
        // avoids creating a large number of tiles for a small viewport, and produces sharp lines and edges in smaller
        // viewports.
        double cellSize = tile.getSector().getDeltaLatRadians() * dc.getGlobe().getRadius() / (double) this.tileHeight;

        return Math.log10(minDistance) < (this.getSplitScale() + Math.log10(cellSize));
    }

    //**************************************************************//
    //********************  Surface Object Tile  *******************//
    //**************************************************************//

    /**
     * Represents a {@link gov.nasa.worldwind.layers.TextureTile} who's contents is constructed by a set of surface
     * objects. The tile maintains a collection of surface objects that intersect the tile, and provides methods for to
     * modify and retrieve that collection. Additionally, the method {@link #getStateKey(DrawContext)} provides a
     * mechanism to uniquely identify the tile's current state, including the state of each intersecting surface
     * object.
     */
    protected static class SurfaceObjectTile extends TextureTile
    {
        /** The sector that bounds the surface objects intersecting the tile. */
        protected Sector objectSector;
        /**
         * Set of surface objects intersecting the tile. The set ensures no duplicate surface objects. Use a linked hash
         * to guarantee predictable iteration order.
         */
        protected Set<SurfaceObject> intersectingObjects = new LinkedHashSet<SurfaceObject>();
        /** The state key that was valid when the tile was last updated. */
        protected Object lastUpdateStateKey;

        /**
         * Constructs a tile for a given sector with a default level, row and column.
         *
         * @param sector the sector to create the tile for.
         *
         * @throws IllegalArgumentException if the sector is null.
         */
        public SurfaceObjectTile(Sector sector)
        {
            super(sector);
        }

        /**
         * Constructs a tile for a given sector, level, row and column of the tile's containing tile set.
         *
         * @param sector the sector corresponding with the tile.
         * @param level  the tile's level within a containing level set.
         * @param row    the row index (0 origin) of the tile within the indicated level.
         * @param column the column index (0 origin) of the tile within the indicated level.
         *
         * @throws IllegalArgumentException if either the sector or level is null.
         */
        public SurfaceObjectTile(Sector sector, Level level, int row, int column)
        {
            super(sector, level, row, column);
        }

        /**
         * Returns an object that uniquely identifies the tile's state on the specified draw context. This object is
         * guaranteed to be globally unique; an equality test with a state key from another always returns false.
         *
         * @param dc the draw context the state key relates to.
         *
         * @return an object representing surface object's current state.
         */
        public Object getStateKey(DrawContext dc)
        {
            return new SurfaceObjectTileStateKey(dc, this);
        }

        /**
         * Returns a sector that bounds the surface objects intersecting the tile. This returns null if no surface
         * objects intersect the tile.
         *
         * @return a sector bounding the tile's intersecting objects.
         */
        public Sector getObjectSector()
        {
            return this.objectSector;
        }

        /**
         * Returns a list of surface objects intersecting the tile.
         *
         * @return a tile's intersecting objects.
         */
        public Collection<SurfaceObject> getObjectList()
        {
            return this.intersectingObjects;
        }

        /**
         * Clears the tile's list of intersecting objects. {@link #getObjectSector()} returns null after calling this
         * method.
         */
        public void clearObjectList()
        {
            this.intersectingObjects.clear();
            this.objectSector = null;
        }

        /**
         * Adds the specified surface object to the tile's list of intersecting objects.
         *
         * @param so     the surface object to add.
         * @param sector the sector bounding the specified surface object.
         */
        public void addSurfaceObject(SurfaceObject so, Sector sector)
        {
            this.intersectingObjects.add(so);
            this.objectSector = (this.objectSector != null) ? this.objectSector.union(sector) : sector;
        }

        /**
         * Adds the specified collection of surface objects to the tile's list of intersecting objects.
         *
         * @param c      the collection of surface objects to add.
         * @param sector the sector bounding the specified surface object collection.
         */
        public void addAllSurfaceObjects(Collection<SurfaceObject> c, Sector sector)
        {
            this.intersectingObjects.addAll(c);
            this.objectSector = (this.objectSector != null) ? this.objectSector.union(sector) : sector;
        }

        /** {@inheritDoc} */
        @Override
        protected TextureTile createSubTile(Sector sector, Level level, int row, int col)
        {
            return new SurfaceObjectTile(sector, level, row, col);
        }
    }

    /**
     * Represents a surface object tile's current state. TileStateKey distinguishes the tile's state by comparing the
     * individual state keys of the surface objects intersecting the tile. This does not retain any references to the
     * surface objects themselves. Should the tile state key live longer than the surface objects, the state key does
     * not prevent those objects from being reclaimed by the garbage collector.
     */
    protected static class SurfaceObjectTileStateKey
    {
        protected final TileKey tileKey;
        protected final Object[] intersectingObjectKeys;

        /**
         * Construsts a tile state key for the specified surface object tile.
         *
         * @param dc   the draw context the state key is related to.
         * @param tile the tile to construct a state key for.
         */
        public SurfaceObjectTileStateKey(DrawContext dc, SurfaceObjectTile tile)
        {
            if (tile != null)
            {
                this.tileKey = tile.getTileKey();
                this.intersectingObjectKeys = new Object[tile.getObjectList().size()];

                int index = 0;
                for (SurfaceObject so : tile.getObjectList())
                {
                    this.intersectingObjectKeys[index++] = so.getStateKey(dc);
                }
            }
            else
            {
                this.tileKey = null;
                this.intersectingObjectKeys = null;
            }
        }

        @Override
        public boolean equals(Object o)
        {
            if (this == o)
                return true;
            if (o == null || this.getClass() != o.getClass())
                return false;

            // Compare the tile keys and each state key in the array. The state keys are equal if the tile keys are
            // equal, the arrays equivalent length, and each array element is equivalent. Arrays.equals() correctly
            // handles null references.
            SurfaceObjectTileStateKey that = (SurfaceObjectTileStateKey) o;
            return (this.tileKey != null ? this.tileKey.equals(that.tileKey) : that.tileKey == null)
                && Arrays.equals(this.intersectingObjectKeys, that.intersectingObjectKeys);
        }

        @Override
        public int hashCode()
        {
            int result = this.tileKey != null ? this.tileKey.hashCode() : 0;
            result = 31 * result + Arrays.hashCode(this.intersectingObjectKeys); // Correctly handles a null reference.
            return result;
        }
    }
}
