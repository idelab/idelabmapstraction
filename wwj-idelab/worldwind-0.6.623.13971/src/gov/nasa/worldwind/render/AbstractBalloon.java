/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.render;

import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.util.*;

import java.awt.*;

/**
 * Abstract implementation of {@link Balloon}.
 *
 * @author pabercrombie
 * @version $Id: AbstractBalloon.java 13930 2010-10-04 00:37:53Z pabercrombie $
 */
public abstract class AbstractBalloon extends AVListImpl implements Balloon
{
    protected boolean alwaysOnTop = false;
    protected int altitudeMode;
    protected boolean pickEnabled = true;
    protected BalloonAttributes attributes;
    protected BalloonAttributes highlightAttributes;
    protected Object delegateOwner;

    protected Position globePosition;
    protected Point screenPoint;

    protected double minActiveAltitude = -Double.MAX_VALUE;
    protected double maxActiveAltitude = Double.MAX_VALUE;

    protected String text;
    protected TextDecoder textDecoder = new BasicTextDecoder();

    protected boolean visible = true;
    protected boolean highlighted;

    /**
     * Create a balloon attached to the globe.
     *
     * @param text The balloon text.
     * @param position The balloon's initial position.
     */
    protected AbstractBalloon(String text, Position position)
    {
        this.setText(text);
        this.setPosition(position);
    }

    /**
     * Create a balloon attached to the screen.
     *
     * @param text The ballon text.
     * @param point The balloon's initial position. 0, 0 indicates the top left corner of the screen.
     */
    protected AbstractBalloon(String text, Point point)
    {
        this.setText(text);
        this.setPosition(point);
    }

    /** {@inheritDoc} */
    public boolean isAlwaysOnTop()
    {
        return this.alwaysOnTop;
    }

    /** {@inheritDoc} */
    public void setAlwaysOnTop(boolean alwaysOnTop)
    {
        this.alwaysOnTop = alwaysOnTop;
    }

    /** {@inheritDoc} */
    public boolean isPickEnabled()
    {
        return this.pickEnabled;
    }

    /** {@inheritDoc} */
    public void setPickEnabled(boolean enable)
    {
        this.pickEnabled = enable;
    }

    /** {@inheritDoc} */
    public String getText()
    {
        return this.text;
    }

    /**
     * Get text after it has been processed by the text decoder. Returns the original text if there is no {@link
     * TextDecoder}.
     *
     * @return Decoded text.
     *
     * @see TextDecoder
     * @see #setTextDecoder(gov.nasa.worldwind.util.TextDecoder)
     * @see #getTextDecoder()
     */
    protected String getDecodedText()
    {
        return this.getTextDecoder().getDecodedText();
    }

    /** {@inheritDoc} */
    public void setText(String text)
    {
        if (text == null)
        {
            String message = Logging.getMessage("nullValue.StringIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.text = text;
        this.getTextDecoder().setText(text);
    }

    /** {@inheritDoc} */
    public Object getDelegateOwner()
    {
        return this.delegateOwner;
    }

    /** {@inheritDoc} */
    public void setDelegateOwner(Object delegateOwner)
    {
        this.delegateOwner = delegateOwner;
    }

    /** {@inheritDoc} */
    public BalloonAttributes getAttributes()
    {
        return this.attributes;
    }

    /** {@inheritDoc} */
    public void setAttributes(BalloonAttributes attributes)
    {
        if (attributes == null)
        {
            String message = Logging.getMessage("nullValue.BalloonAttributesIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.attributes = attributes;
    }

    /** {@inheritDoc} */
    public BalloonAttributes getHighlightAttributes()
    {
        return this.highlightAttributes;
    }

    /** {@inheritDoc} */
    public void setHighlightAttributes(BalloonAttributes highlightAttributes)
    {
        if (highlightAttributes == null)
        {
            String message = Logging.getMessage("nullValue.BalloonAttributesIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.highlightAttributes = highlightAttributes;
    }

    /** {@inheritDoc} */
    protected BalloonAttributes getActiveAttributes()
    {
        if (this.isHighlighted() && this.getHighlightAttributes() != null)
            return this.getHighlightAttributes();
        else
            return this.getAttributes();
    }

    /** {@inheritDoc} */
    public double getMinActiveAltitude()
    {
        return this.minActiveAltitude;
    }

    /** {@inheritDoc} */
    public void setMinActiveAltitude(double minActiveAltitude)
    {
        this.minActiveAltitude = minActiveAltitude;
    }

    /** {@inheritDoc} */
    public double getMaxActiveAltitude()
    {
        return this.maxActiveAltitude;
    }

    /** {@inheritDoc} */
    public void setMaxActiveAltitude(double maxActiveAltitude)
    {
        this.maxActiveAltitude = maxActiveAltitude;
    }

    /** {@inheritDoc} */
    public TextDecoder getTextDecoder()
    {
        return this.textDecoder;
    }

    /** {@inheritDoc} */
    public void setTextDecoder(TextDecoder decoder)
    {
        if (decoder == null)
        {
            String message = Logging.getMessage("nullValue.TextDecoderIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.textDecoder = decoder;
        this.textDecoder.setText(this.getText());
    }

    /** {@inheritDoc} */
    public void setPosition(Point point)
    {
        if (point == null)
        {
            String message = Logging.getMessage("nullValue.PointIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        if (this.screenPoint == null && this.globePosition != null)
        {
            String message = Logging.getMessage("Balloon.CannotChangeBalloonMode");
            Logging.logger().severe(message);
            throw new IllegalStateException(message);
        }

        this.screenPoint = point;
        this.globePosition = null;
    }

    /** {@inheritDoc} */
    public void setPosition(Position position)
    {
        if (position == null)
        {
            String message = Logging.getMessage("nullValue.PositionIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        if (this.globePosition == null && this.screenPoint != null)
        {
            String message = Logging.getMessage("Balloon.CannotChangeBalloonMode");
            Logging.logger().severe(message);
            throw new IllegalStateException(message);
        }

        this.globePosition = position;
        this.screenPoint = null;
    }

    /** {@inheritDoc} */
    public Object getPosition()
    {
        if (this.globePosition != null)
            return this.globePosition;
        else
            return this.screenPoint;
    }

    /** {@inheritDoc} */
    public String getAttachmentMode()
    {
        if (this.globePosition != null)
            return GLOBE_MODE;
        else
            return SCREEN_MODE;
    }

    /** {@inheritDoc} */
    public boolean isHighlighted()
    {
        return highlighted;
    }

    /** {@inheritDoc} */
    public void setHighlighted(boolean highlighted)
    {
        this.highlighted = highlighted;
    }

    /** {@inheritDoc} */
    public boolean isVisible()
    {
        return visible;
    }

    /** {@inheritDoc} */
    public void setVisible(boolean visible)
    {
        this.visible = visible;
    }

    /** {@inheritDoc} */
    public int getAltitudeMode()
    {
        return altitudeMode;
    }

    /** {@inheritDoc} */
    public void setAltitudeMode(int altitudeMode)
    {
        this.altitudeMode = altitudeMode;
    }
}
