/*
Copyright (C) 2001, 2009 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/

package gov.nasa.worldwind.render;

import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.geom.*;
import gov.nasa.worldwind.util.Logging;

import javax.xml.stream.*;
import java.io.IOException;
import java.util.*;

/**
 * /** A 3D polygon. The polygon may be complex with multiple internal but not intersecting contours. Standard lighting
 * is optionally applied.
 * <p/>
 * Polygons are safe to share among World Windows. They should not be shared among layers in the same World Window.
 *
 * @author tag
 * @version $Id: Polygon.java 13724 2010-09-08 07:17:09Z tgaskins $
 */
public class Polygon extends AbstractPolygon
{
    /** The default altitude mode. */
    protected static final int DEFAULT_ALTITUDE_MODE = WorldWind.ABSOLUTE;

    /** Construct polygon with empty boundaries. */
    public Polygon()
    {
    }

    /**
     * Construct a polygon for a specified outer boundary.
     *
     * @param corners the list of locations defining the polygon.
     *
     * @throws IllegalArgumentException if the location list is null.
     */
    public Polygon(Iterable<? extends Position> corners)
    {
        if (corners == null)
        {
            String message = Logging.getMessage("nullValue.PositionsListIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.setOuterBoundary(corners);
    }

    /**
     * Construct a polygon for a specified list of outer-boundary positions.
     *
     * @param corners the list of positions -- latitude longitude and altitude -- defining the polygon. The current
     *                altitude mode determines whether the positions are considered relative to mean sea level (they are
     *                "absolute") or the ground elevation at the associated latitude and longitude.
     *
     * @throws IllegalArgumentException if the position list is null.
     */
    public Polygon(Position.PositionList corners)
    {
        if (corners == null)
        {
            String message = Logging.getMessage("nullValue.PositionsListIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        this.setOuterBoundary(corners.list);
    }

    /**
     * Specifies the latitude, longitude and altitude of the positions defining the polygon.
     *
     * @param corners the polygon positions. A duplicate of the first position is appended to the list if the list's
     *                last position is not identical to the first.
     *
     * @throws IllegalArgumentException if the location list is null or contains fewer than three locations.
     */
    public void setOuterBoundary(Iterable<? extends Position> corners)
    {
        if (corners == null)
        {
            String message = Logging.getMessage("nullValue.PositionsListIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        BoundaryInfo outerBoundary = this.createBoundaryInfo();
        if (this.boundaries.size() > 0)
            this.boundaries.set(0, outerBoundary);
        else
            this.boundaries.add(outerBoundary);

        ArrayList<LatLon> list = new ArrayList<LatLon>();
        for (LatLon corner : corners)
        {
            if (corner != null)
                list.add(corner);
        }

        if (list.size() < 3)
        {
            String message = Logging.getMessage("generic.InsufficientPositions");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        // Close the list if not already closed.
        if (list.size() > 0 && !list.get(0).equals(list.get(list.size() - 1)))
            list.add(list.get(0));

        list.trimToSize();
        outerBoundary.locations = list;

        this.reinitialize();
    }

    /**
     * Add an inner boundary to the polygon. A duplicate of the first position is appended to the list if the list's
     * last position is not identical to the first.
     *
     * @param corners the polygon positions.
     *
     * @throws IllegalArgumentException if the location list is null or contains fewer than three locations.
     */
    public void addInnerBoundary(Iterable<? extends Position> corners)
    {
        if (corners == null)
        {
            String message = Logging.getMessage("nullValue.PositionsListIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        BoundaryInfo boundary = this.createBoundaryInfo();
        this.boundaries.add(boundary);

        ArrayList<LatLon> list = new ArrayList<LatLon>();
        for (LatLon corner : corners)
        {
            if (corner != null)
                list.add(corner);
        }

        if (list.size() < 3)
        {
            String message = Logging.getMessage("generic.InsufficientPositions");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        // Close the list if not already closed.
        if (list.size() > 0 && !list.get(0).equals(list.get(list.size() - 1)))
            list.add(list.get(0));

        list.trimToSize();
        boundary.locations = list;

        this.reinitialize();
    }

    /** {@inheritDoc} */
    protected int getDefaultAltitudeMode()
    {
        return DEFAULT_ALTITUDE_MODE;
    }

    /** {@inheritDoc} */
    protected void makeOrderedRenderable(DrawContext dc)
    {
        // See if what's been computed can be reused.
        if (this.vertexBuffer != null)
        {
            if (this.getAltitudeMode() != WorldWind.RELATIVE_TO_GROUND)
            {
                if (dc.getFrameTimeStamp() != this.frameID)
                    this.determineActiveAttributes();

                dc.addOrderedRenderable(this);
                return;
            }
        }

        super.makeOrderedRenderable(dc);
    }

    protected String getKMLExtrudeValue()
    {
        return "0";
    }

    /** {@inheritDoc} */
    protected void writeKMLBoundaries(XMLStreamWriter xmlWriter) throws IOException, XMLStreamException
    {
        // Outer boundary
        Iterable<? extends LatLon> outerBoundary = this.getOuterBoundary();
        if (outerBoundary != null)
        {
            xmlWriter.writeStartElement("outerBoundaryIs");
            exportBoundaryAsLinearRing(xmlWriter, outerBoundary);
            xmlWriter.writeEndElement(); // outerBoundaryIs
        }

        // Inner boundaries
        Iterator<Polygon.BoundaryInfo> boundaryIterator = boundaries.iterator();
        if (boundaryIterator.hasNext())
            boundaryIterator.next(); // Skip outer boundary, we already dealt with it above

        while (boundaryIterator.hasNext())
        {
            BoundaryInfo boundary = boundaryIterator.next();

            xmlWriter.writeStartElement("innerBoundaryIs");
            exportBoundaryAsLinearRing(xmlWriter, boundary.locations);
            xmlWriter.writeEndElement(); // innerBoundaryIs
        }
    }
}
