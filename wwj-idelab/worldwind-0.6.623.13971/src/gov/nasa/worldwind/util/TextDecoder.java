/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.util;

/**
 * A general purpose text decoder.
 *
 * @author pabercrombie
 * @version $Id: TextDecoder.java 13921 2010-10-02 02:10:57Z pabercrombie $
 */
public interface TextDecoder
{
    /**
     * Set the input text which the decoder will process.
     *
     * @param input Text to decode.
     */
    public void setText(String input);

    /**
     * Get the decoded text. This method may be called many times. Implementations should cache decoding results that do
     * not need to be recomputed.
     *
     * @return Text after decoding.
     */
    public String getDecodedText();
}
