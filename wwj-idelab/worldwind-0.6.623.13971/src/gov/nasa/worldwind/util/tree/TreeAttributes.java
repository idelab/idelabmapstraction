/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.util.tree;

import java.awt.*;

/**
 * @author pabercrombie
 * @version $Id $
 */
public interface TreeAttributes
{
    boolean isDrawFrame();

    void setDrawFrame(boolean drawFrame);

    boolean isRootVisible();

    void setRootVisible(boolean visible);

    Color getTextColor();

    void setTextColor(Color textColor);

    Font getFont();

    void setFont(Font font);

    int getRowSpacing();

    void setRowSpacing(int spacing);

    int getIndent();

    void setIndent(int indent);

    int getIconWidth();

    void setIconWidth(int iconWidth);

    int getIconHeight();

    void setIconHeight(int iconHeight);

    int getIconSpace();

    void setIconSpace(int iconSpace);
}
