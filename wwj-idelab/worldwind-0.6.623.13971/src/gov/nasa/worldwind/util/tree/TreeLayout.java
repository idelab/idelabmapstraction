/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.util.tree;

import gov.nasa.worldwind.render.DrawContext;

/**
 * Handles rendering a {@link Tree}.
 *
 * @author pabercrombie
 * @version $Id: TreeLayout.java 13963 2010-10-09 00:31:24Z pabercrombie $
 * @see Tree
 */
public interface TreeLayout
{
    void render(Tree tree, DrawContext dc);

    void setAttributes(TreeAttributes attributes);

    TreeAttributes getAttributes();
}
