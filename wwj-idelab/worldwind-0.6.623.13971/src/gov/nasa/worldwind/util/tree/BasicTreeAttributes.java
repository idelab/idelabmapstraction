/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */
package gov.nasa.worldwind.util.tree;

import gov.nasa.worldwind.util.Logging;

import java.awt.*;

/**
 * @author pabercrombie
 * @version $Id $
 */
public class BasicTreeAttributes implements TreeAttributes
{
    protected boolean rootVisible = true;
    protected boolean drawFrame = false;
    protected Color textColor = Color.BLACK;
    protected Font font = Font.decode("Arial-BOLD-12");
    protected int indent = 10; // Indent applied to each new level of the tree
    protected int rowSpacing = 5; // Spacing between rows in the tree

    protected int iconWidth = 16;
    protected int iconHeight = 16;
    protected int iconSpace = 2;
    
    public boolean isDrawFrame()
    {
        return drawFrame;
    }

    public void setDrawFrame(boolean drawFrame)
    {
        this.drawFrame = drawFrame;
    }

    public boolean isRootVisible()
    {
        return this.rootVisible;
    }

    public void setRootVisible(boolean visible)
    {
        this.rootVisible = visible;
    }

    public Color getTextColor()
    {
        return textColor;
    }

    public void setTextColor(Color textColor)
    {
        if (textColor == null)
        {
            String msg = Logging.getMessage("nullValue.ColorIsNull");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }

        this.textColor = textColor;
    }

    public Font getFont()
    {
        return font;
    }

    public void setFont(Font font)
    {
        if (font == null)
        {
            String msg = Logging.getMessage("nullValue.FontIsNull");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }

        this.font = font;
    }

    public int getRowSpacing()
    {
        return this.rowSpacing;
    }

    public void setRowSpacing(int spacing)
    {
        this.rowSpacing = spacing;
    }

    public int getIndent()
    {
        return this.indent;
    }

    public void setIndent(int indent)
    {
        this.indent = indent;
    }

    public int getIconWidth()
    {
        return iconWidth;
    }

    public void setIconWidth(int iconWidth)
    {
        this.iconWidth = iconWidth;
    }

    public int getIconHeight()
    {
        return iconHeight;
    }

    public void setIconHeight(int iconHeight)
    {
        this.iconHeight = iconHeight;
    }

    public int getIconSpace()
    {
        return iconSpace;
    }

    public void setIconSpace(int iconSpace)
    {
        this.iconSpace = iconSpace;
    }
}
