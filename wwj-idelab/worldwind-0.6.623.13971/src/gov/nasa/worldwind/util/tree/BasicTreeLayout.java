/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.util.tree;

import gov.nasa.worldwind.render.*;
import gov.nasa.worldwind.util.*;

import javax.media.opengl.GL;
import java.awt.*;

/**
 * @author pabercrombie
 * @version $Id: BasicTreeLayout.java 13971 2010-10-12 00:44:06Z pabercrombie $
 */
public class BasicTreeLayout implements TreeLayout
{
    protected TreeNodeRenderer renderer = new BasicTreeNodeRenderer();

    protected Point screenLocation;
    protected TreeAttributes attributes = new BasicTreeAttributes();

    protected Insets insets = new Insets(10, 10, 15, 10);

    protected OGLStackHandler oglStackHandler = new OGLStackHandler();
    
    public BasicTreeLayout()
    {
    }

    public BasicTreeLayout(int x, int y)
    {
        this(new Point(x, y));
    }

    public BasicTreeLayout(Point screenLocation)
    {
        this.setScreenLocation(screenLocation);
    }

    public Rectangle getBounds(Tree tree, DrawContext dc)
    {
        TreeModel model = tree.getModel();
        TreeNode root = model.getRoot();

        Dimension size = new Dimension();
        computeSize(tree, root, dc, size, 1);

        size.width += this.insets.left + this.insets.right;
        size.height += this.insets.bottom + this.insets.top;        
        
        return new Rectangle(this.screenLocation.x, this.screenLocation.y, size.width, size.height);
    }

    public void computeSize(Tree tree, TreeNode root, DrawContext dc, Dimension size, int level)
    {
        TreeAttributes attributes = this.getAttributes();

        Dimension thisSize = this.renderer.getPreferredSize(dc, root, attributes);

        if (level > 1 || attributes.isRootVisible())
        {
            int indent = attributes.getIndent() * level;
            int thisWidth = thisSize.width + indent;
            if (thisWidth > size.width)
                size.width = thisWidth;
    
            size.height += thisSize.height;
        }

        if (tree.isNodeExpanded(root))
        {
            for (TreeNode child : root.getChildren())
            {
                this.computeSize(tree, child, dc, size, level + 1);
                size.height += attributes.getRowSpacing();
            }
        }
    }

    public void render(Tree tree, DrawContext dc)
    {
        TreeModel model = tree.getModel();
        TreeNode root = model.getRoot();
        
        if (this.getScreenLocation() == null)
            return;

        try
        {
            this.beginRendering(dc, root);

            this.drawFrame(tree, dc);

            Point location = new Point(this.getScreenLocation());

            // Adjust location so that the first line of text will be drawn below the screen point
            Dimension size = this.renderer.getPreferredSize(dc, root, this.getAttributes());
            location.y += size.height + insets.top;
            location.x += insets.left;

            renderSubtree(tree, root, dc, location, 1);
        }
        finally
        {
            this.endRendering(dc);
        }
    }

    protected void drawFrame(Tree tree, DrawContext dc)
    {
        GL gl = dc.getGL();

        java.awt.Rectangle viewport = dc.getView().getViewport();
        int y = (int)(viewport.getHeight() - screenLocation.y);

        OGLStackHandler oglStack = new OGLStackHandler();
        try
        {
            oglStack.pushModelviewIdentity(gl);
            Rectangle rect = this.getBounds(tree, dc);

            gl.glEnable(GL.GL_BLEND);
            OGLUtil.applyColor(gl, Color.WHITE, 0.8, false);

            gl.glTranslated(screenLocation.x, y - rect.height, 0.0);

            FrameFactory.drawShape(dc, FrameFactory.SHAPE_RECTANGLE, rect.width, rect.height, GL.GL_TRIANGLE_FAN, 10);

            OGLUtil.applyColor(gl, Color.WHITE, 1.0, false);
            FrameFactory.drawShape(dc, FrameFactory.SHAPE_RECTANGLE, rect.width, rect.height, GL.GL_LINE_STRIP, 10);

        }
        finally
        {
            oglStack.pop(gl);
        }
    }

    protected void renderSubtree(Tree tree, TreeNode root, DrawContext dc, Point location, int level)
    {
        TreeAttributes attributes = this.getAttributes();

        if (level > 1 || attributes.isRootVisible())
            this.renderer.render(dc, tree, root, attributes, location.x, location.y);

        int oldX = location.x;
        location.x += level * attributes.getIndent();

        if (tree.isNodeExpanded(root))
        {
            for (TreeNode child : root.getChildren())
            {
                Dimension size = this.renderer.getPreferredSize(dc, child, attributes);

                location.y += size.height + attributes.getRowSpacing();

                this.renderSubtree(tree, child, dc, location, level + 1);
            }
        }
        location.x  = oldX; // Restore previous indent level
    }

    protected void beginRendering(DrawContext dc, TreeNode node)
    {
        GL gl = dc.getGL();

        this.oglStackHandler.pushAttrib(gl, GL.GL_DEPTH_BUFFER_BIT
                | GL.GL_COLOR_BUFFER_BIT
                | GL.GL_ENABLE_BIT
                | GL.GL_TEXTURE_BIT
                | GL.GL_TRANSFORM_BIT
                | GL.GL_VIEWPORT_BIT
                | GL.GL_CURRENT_BIT);

        gl.glDisable(GL.GL_TEXTURE_2D);		// no textures

        gl.glEnable(GL.GL_BLEND);
        gl.glBlendFunc(GL.GL_SRC_ALPHA, GL.GL_ONE_MINUS_SRC_ALPHA);
        gl.glDisable(GL.GL_DEPTH_TEST);

        // Load a parallel projection with xy dimensions (viewportWidth, viewportHeight)
        // into the GL projection matrix.
        this.oglStackHandler.pushProjectionIdentity(gl);

        java.awt.Rectangle viewport = dc.getView().getViewport();

        Dimension size = this.renderer.getPreferredSize(dc, node, this.getAttributes());
        if (size.width < viewport.getWidth())
        {
            double maxwh = size.width > size.height ? size.width : size.height;
            gl.glOrtho(0d, viewport.width, 0d, viewport.height, -0.6 * maxwh, 0.6 * maxwh);

            this.oglStackHandler.pushModelviewIdentity(gl);
        }
    }

    public void endRendering(DrawContext dc)
    {
        GL gl = dc.getGL();
        this.oglStackHandler.pop(gl);
    }

    public Point getScreenLocation()
    {
        return this.screenLocation;
    }

    public void setScreenLocation(Point screenLocation)
    {
        this.screenLocation = screenLocation;
    }

    public TreeAttributes getAttributes()
    {
        return this.attributes;
    }

    public void setAttributes(TreeAttributes attributes)
    {
        if (attributes == null)
        {
            String msg = Logging.getMessage("nullValue.AttributesIsNull");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }

        this.attributes = attributes;
    }
}
