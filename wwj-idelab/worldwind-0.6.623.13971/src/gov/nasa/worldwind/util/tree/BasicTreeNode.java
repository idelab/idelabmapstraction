/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.util.tree;

import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.render.*;
import gov.nasa.worldwind.util.Logging;

import java.net.URL;
import java.util.*;

/**
 * @author pabercrombie
 * @version $Id: BasicTreeNode.java 13970 2010-10-11 23:29:33Z pabercrombie $
 */
public class BasicTreeNode extends AVListImpl implements TreeNode
{
    protected String text;
    protected Object imageSource;
    protected BasicWWTexture texture;

    protected String description;

    protected TreeNode parent;
    protected List<TreeNode> children;

    protected boolean enabled = true;
    protected boolean selected;
    protected boolean visible = true;

    public BasicTreeNode(String text)
    {
        this(text, null);
    }

    public BasicTreeNode(String text, Object imageSource)
    {
        this.text = text;
        if (imageSource != null)
            this.setImageSource(imageSource);
    }

    public String getText()
    {
        return this.text;
    }

    public TreeNode getParent()
    {
        return this.parent;
    }

    public void setParent(TreeNode node)
    {
        this.parent = node;
    }

    public Iterable<TreeNode> getChildren()
    {
        if (this.children != null)
            return Collections.unmodifiableList(this.children);
        else
            return Collections.emptyList();
    }

    public boolean isEnabled()
    {
        return this.enabled;
    }

    public void setEnabled(boolean enabled)
    {
        this.enabled = enabled;
    }

    public boolean isSelected()
    {
        return this.selected;
    }

    public void setSelected(boolean selected)
    {
        this.selected = selected;
    }

    /** {@inheritDoc} */
    public String isTreeSelected()
    {
        String selected = this.isSelected() ? SELECTED : NOT_SELECTED;

        for (TreeNode child : this.getChildren())
        {
            String childSelected = child.isTreeSelected();

            if (!selected.equals(childSelected))
            {
                selected = PARTIALLY_SELECTED;
                break; // No need to look at other nodes
            }
        }

        return selected;
    }

    public boolean isVisible()
    {
        return this.visible;
    }

    public boolean isLeaf()
    {
        return this.children == null;
    }

    public void setVisible(boolean visible)
    {
        this.visible = visible;
    }

    public String getDescription()
    {
        return description;
    }

    public void setDescription(String description)
    {
        this.description = description;
    }

    public Object getImageSource()
    {
        return imageSource;
    }

    public void setImageSource(Object imageSource)
    {
        this.imageSource = imageSource;
    }

    public boolean hasImage()
    {
        return this.getImageSource() != null;
    }

    /**
     * Get the texture for the node icon. The texture is loaded on a background thread. This method will return null
     * until the texture has been loaded.
     *
     * @return The texture or null if the texture is not yet available.
     */
    public BasicWWTexture getTexture()
    {
        if (this.texture != null)
            return this.texture;
        else
            return this.initializeTexture();
    }

    /**
     * Create and initialize the texture from the image source. If the image is not in memory this method will request
     * that it be loaded and return null.
     *
     * @return The texture, or null if the texture is not yet available.
     */
    protected BasicWWTexture initializeTexture()
    {
        Object imageSource = this.getImageSource();
        if (imageSource instanceof String || imageSource instanceof URL)
        {
            URL imageURL = WorldWind.getDataFileStore().requestFile(imageSource.toString());
            if (imageURL != null)
            {
                this.texture = new BasicWWTexture(imageURL, true);
            }
        }
        else if (imageSource != null)
        {
            this.texture = new BasicWWTexture(imageSource, true);
            return this.texture;
        }

        return null;
    }

    public void addChild(TreeNode child)
    {
        if (this.children == null)
            this.children = new ArrayList<TreeNode>();
        this.children.add(child);
        child.setParent(this);
    }

    public void removeChild(TreeNode child)
    {
        if (this.children != null)
            this.children.remove(child);
        if (child != null && child.getParent() == this)
            child.setParent(null);
    }

    public TreePath getPath()
    {
        TreePath path = new TreePath();

        TreeNode node = this;
        while (node != null)
        {
            path.add(0, node.getText());
            node = node.getParent();
        }

        return path;
    }
}
