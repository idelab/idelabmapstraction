/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.util.tree;

import com.sun.opengl.util.j2d.TextRenderer;
import com.sun.opengl.util.texture.TextureCoords;
import gov.nasa.worldwind.pick.PickSupport;
import gov.nasa.worldwind.render.*;
import gov.nasa.worldwind.util.*;

import javax.media.opengl.GL;
import java.awt.*;
import java.awt.geom.*;

/**
 * @author pabercrombie
 * @version $Id: BasicTreeNodeRenderer.java 13971 2010-10-12 00:44:06Z pabercrombie $
 */
public class BasicTreeNodeRenderer implements TreeNodeRenderer
{
    // TODO: replace these chars with icons
    protected char selectedChar = '\u25a0';
    protected char notSelectedChar = '\u25a1';

    protected PickSupport pickSupport = new PickSupport();

    public Dimension getPreferredSize(DrawContext dc, TreeNode node, TreeAttributes attributes)
    {
        Dimension dim = this.getTextRenderSize(dc, this.getText(node), attributes);

        if (node.hasImage())
        {
            if (attributes.getIconHeight() > dim.height)
                dim.height = attributes.getIconHeight();

            dim.width += attributes.getIconWidth() + attributes.getIconSpace();
        }

        if (!node.isLeaf())
        {
            dim.width += this.getNodeStateSymbolSize().width + attributes.getIconSpace();  // TODO: check expanded state
        }

        return dim;
    }

    public void render(DrawContext dc, Tree tree, TreeNode node, TreeAttributes attributes, int x, int y)
    {
        if (!node.isVisible())
            return;

        String text = this.getText(node);
        Dimension size = this.getPreferredSize(dc, node, attributes);
        Dimension textSize = this.getTextRenderSize(dc, text, attributes);

        java.awt.Rectangle viewport = dc.getView().getViewport();

        y = (int) (viewport.getHeight() - y);

        GL gl = dc.getGL();

        OGLStackHandler oglStack = new OGLStackHandler();

        oglStack.pushAttrib(gl,
            GL.GL_DEPTH_BUFFER_BIT
                | GL.GL_COLOR_BUFFER_BIT
                | GL.GL_ENABLE_BIT
                | GL.GL_TEXTURE_BIT
                | GL.GL_TRANSFORM_BIT
                | GL.GL_VIEWPORT_BIT
                | GL.GL_CURRENT_BIT);

        oglStack.pushModelviewIdentity(gl);
        try
        {
            if (!dc.isPickingMode())
            {
                if (!node.isLeaf())
                {
                    int vertAdjust = (size.height - 10) / 2;
                    if (tree.isNodeExpanded(node))
                        this.drawPathExpandedSymbol(dc, node, attributes, x, y + vertAdjust, size);
                    else
                        this.drawPathCollapsedSymbol(dc, node, attributes, x, y + vertAdjust, size);

                    x += this.getNodeStateSymbolSize().width + attributes.getIconSpace();
                }

                this.drawIcon(dc, node, attributes, x, y, size);
                x += attributes.getIconWidth() + attributes.getIconSpace();

                // Vertically center text with image
                // Multi-line text renderer wants the coordinate of the top left corner of the text
                y += textSize.height + (size.height - textSize.height) / 2;

                TextRenderer textRenderer = OGLTextRenderer.getOrCreateTextRenderer(dc.getTextRendererCache(),
                    attributes.getFont());
                MultiLineTextRenderer mltr = new MultiLineTextRenderer(textRenderer);
                textRenderer.begin3DRendering();
                try
                {
                    textRenderer.setColor(attributes.getTextColor());
                    mltr.drawHTML(text, x, y, dc.getTextRendererCache());
                }
                finally
                {
                    textRenderer.end3DRendering();
                }
            }
            else
            {
                this.pickSupport.clearPickList();
                this.pickSupport.beginPicking(dc);
                Color color = dc.getUniquePickColor();
                int colorCode = color.getRGB();
                this.pickSupport.addPickableObject(colorCode, node, null, false);
                gl.glColor3ub((byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue());
                gl.glTranslated(x, y, 1.0);
                gl.glScaled(size.width, size.height, 1d);
                dc.drawUnitQuad();
                this.pickSupport.endPicking(dc);
                this.pickSupport.resolvePick(dc, dc.getPickPoint(), dc.getCurrentLayer());
            }
        }
        finally
        {
            oglStack.pop(gl);
        }
    }

    protected void drawIcon(DrawContext dc, TreeNode node, TreeAttributes attributes, int x, int y, Dimension size)
    {
        GL gl = dc.getGL();

        WWTexture texture = node.getTexture();

        if (texture != null && texture.bind(dc))
        {
            gl.glEnable(GL.GL_TEXTURE_2D);

            // If the total node height is greater than the image height, vertically center the image
            int vertAdjustment = 0;
            if (attributes.getIconHeight() < size.height)
            {
                vertAdjustment = (size.height - attributes.getIconHeight()) / 2;
            }

            gl.glPushMatrix();
            TextureCoords texCoords = texture.getTexCoords();
            gl.glTranslated(x, y + vertAdjustment, 1.0);
            gl.glScaled((double) attributes.getIconWidth(), (double) attributes.getIconHeight(), 1d);
            dc.drawUnitQuad(texCoords);

            gl.glPopMatrix();
        }
    }

    protected void drawPathCollapsedSymbol(DrawContext dc, TreeNode node, TreeAttributes attributes, int x, int y,
        Dimension size)
    {
        this.drawTriangle(dc, 0, attributes, x, y);
    }

    protected void drawPathExpandedSymbol(DrawContext dc, TreeNode node, TreeAttributes attributes, int x, int y,
        Dimension size)
    {
        this.drawTriangle(dc, -90, attributes, x, y);
    }

    /**
     * Draw a small triangle as the node expanded or collapsed symbol.
     *
     * @param dc         Draw context to draw into.
     * @param rotation   Rotation to apply to triangle. Positive is counter clock-wise.
     * @param attributes Tree attributes.
     * @param x          X coordinate of symbol location.
     * @param y          Y coordinate of symbol location (from bottom of screen).
     */
    protected void drawTriangle(DrawContext dc, float rotation, TreeAttributes attributes, int x, int y)
    {
        GL gl = dc.getGL();

        OGLStackHandler oglStack = new OGLStackHandler();
        try
        {
            oglStack.pushModelviewIdentity(gl);

            oglStack.pushAttrib(gl,
                GL.GL_COLOR_BUFFER_BIT
                    | GL.GL_ENABLE_BIT
                    | GL.GL_TRANSFORM_BIT
                    | GL.GL_CURRENT_BIT);

            gl.glLineWidth(2f);
            Color color = attributes.getTextColor();
            gl.glColor3ub((byte) color.getRed(), (byte) color.getGreen(), (byte) color.getBlue());

            Dimension triSize = this.getNodeStateSymbolSize();
            int halfHeight = triSize.height / 2;

            gl.glTranslated(x, y + halfHeight, 1.0);
            gl.glRotatef(rotation, 0, 0, 1);

            gl.glBegin(GL.GL_TRIANGLES);
            gl.glVertex3d(0.0, halfHeight, 0);
            gl.glVertex3d(triSize.width, 0.0, 0);
            gl.glVertex3d(0.0, -halfHeight, 0);
            gl.glEnd();
        }
        finally
        {
            oglStack.pop(gl);
        }
    }

    protected Dimension getTextRenderSize(DrawContext dc, String text, TreeAttributes attributes)
    {
        TextRenderer textRenderer = OGLTextRenderer.getOrCreateTextRenderer(dc.getTextRendererCache(),
            attributes.getFont());
        MultiLineTextRenderer mltr = new MultiLineTextRenderer(textRenderer);
        Rectangle2D nameBound = mltr.getBoundsHTML(text, dc.getTextRendererCache());

        return nameBound.getBounds().getSize();
    }

    protected String getText(TreeNode node)
    {
        StringBuffer sb = new StringBuffer();

        if (node.isLeaf())
        {
            sb.append(node.isSelected() ? this.selectedChar : this.notSelectedChar);
            sb.append(" ");
        }

        sb.append(node.getText());

        if (node instanceof BasicTreeNode)
        {
            String description = ((BasicTreeNode) node).description;

            if (description != null)
            {
                sb.append("\n").append("<i>").append(description).append("</i>");
            }
        }

        return sb.toString();
    }

    protected Dimension getNodeStateSymbolSize()
    {
        return new Dimension(5, 10);
    }
}
