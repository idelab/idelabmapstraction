/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.util.tree;

/**
 * @author pabercrombie
 * @version $Id: BasicTreeModel.java 13957 2010-10-08 01:11:58Z pabercrombie $
 */
public class BasicTreeModel implements TreeModel
{
    protected TreeNode root;

    public BasicTreeModel()
    {

    }

    public BasicTreeModel(TreeNode root)
    {
        this.setRoot(root);
    }

    public TreeNode getRoot()
    {
        return this.root;
    }

    public void setRoot(TreeNode root)
    {
        this.root = root;
    }
}
