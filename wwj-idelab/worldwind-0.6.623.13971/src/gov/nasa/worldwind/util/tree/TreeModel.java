/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.util.tree;

/**
 * @author pabercrombie
 * @version $Id: TreeModel.java 13951 2010-10-07 22:11:42Z pabercrombie $
 */
public interface TreeModel
{
    TreeNode getRoot();

}
