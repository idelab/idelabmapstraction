/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.util.tree;

import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.render.*;

/**
 * @author pabercrombie
 * @version $Id: TreeNode.java 13970 2010-10-11 23:29:33Z pabercrombie $
 */
public interface TreeNode extends AVList
{
    final static String SELECTED = "util.tree.Selected";
    final static String NOT_SELECTED = "util.tree.NotSelected";
    final static String PARTIALLY_SELECTED = "util.tree.PartiallySelected";

    String getText();

    TreeNode getParent();

    void setParent(TreeNode node);

    Iterable<TreeNode> getChildren();

    boolean isEnabled();

    void setEnabled(boolean enabled);

    boolean isSelected();

    void setSelected(boolean selected);

    /**
     * Is any part of the sub-tree rooted at this node selected?
     *
     * @return {@link #SELECTED}, {@link #NOT_SELECTED}, {@link #PARTIALLY_SELECTED}.
     */
    String isTreeSelected();

    boolean isVisible();

    void setVisible(boolean visible);

    boolean isLeaf();

    void addChild(TreeNode child);

    void removeChild(TreeNode child);

    /**
     * Get the path from the root node to this node.
     *
     * @return Tree path to this node.
     */
    TreePath getPath();

    Object getImageSource();

    void setImageSource(Object imageSource);

    boolean hasImage();

    WWTexture getTexture();

}
