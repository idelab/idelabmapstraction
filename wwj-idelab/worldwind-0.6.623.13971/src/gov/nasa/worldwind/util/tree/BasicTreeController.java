/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.util.tree;

import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.event.*;
import gov.nasa.worldwindow.util.Util;

/**
 * @author pabercrombie
 * @version $Id: BasicTreeController.java 13960 2010-10-08 19:26:43Z pabercrombie $
 */
public class BasicTreeController implements SelectListener
{
    protected WorldWindow wwd;
    protected Tree tree;

    /**
     * Creates a controller for a specified World Window.
     *
     * @param wwd the World Window to monitor.
     */
    public BasicTreeController(WorldWindow wwd, Tree tree)
    {
        this.wwd = wwd;
        this.wwd.addSelectListener(this);
        this.tree = tree;
    }

    public void selected(SelectEvent event)
    {
        try
        {
            // TODO: the controller does not check to make sure that the selected node is in it's tree. This may result
            // TODO: in incorrect behavior if there are multiple trees that contain nodes identified by the same string
            // TODO: path 
            if (event.isLeftClick() && event.getTopObject() instanceof TreeNode)
            {
                TreeNode node = (TreeNode) event.getTopObject();

                if (node.isLeaf())
                    node.setSelected(!node.isSelected());
                else
                    this.tree.togglePath(node.getPath());
            }
        }
        catch (Exception e)
        {
            // Wrap the handler in a try/catch to keep exceptions from bubbling up
            Util.getLogger().warning(e.getMessage() != null ? e.getMessage() : e.toString());
        }
    }
}
