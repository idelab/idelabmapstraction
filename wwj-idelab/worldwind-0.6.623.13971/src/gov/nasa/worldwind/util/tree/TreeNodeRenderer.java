/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.util.tree;

import gov.nasa.worldwind.render.*;

import java.awt.*;

/**
 * @author pabercrombie
 * @version $Id: TreeNodeRenderer.java 13971 2010-10-12 00:44:06Z pabercrombie $
 */
public interface TreeNodeRenderer
{
    Dimension getPreferredSize(DrawContext dc, TreeNode node, TreeAttributes attributes);

    void render(DrawContext dc, Tree tree, TreeNode node, TreeAttributes attributes, int x, int y);
}
