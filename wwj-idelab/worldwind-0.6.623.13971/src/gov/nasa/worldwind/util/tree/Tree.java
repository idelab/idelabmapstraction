/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.util.tree;

import gov.nasa.worldwind.event.TreeListener;
import gov.nasa.worldwind.render.Renderable;

/**
 * A tree of objects, drawn in the World Window, that the user can interact with. How the tree is drawn is determined by
 * the {@link TreeLayout}.
 *
 * @author pabercrombie
 * @version $Id: Tree.java 13961 2010-10-08 21:31:17Z pabercrombie $
 * @see TreeModel
 * @see TreeLayout
 * @see TreeListener
 */
public interface Tree extends Renderable
{
    void setLayout(TreeLayout layout);

    TreeLayout getLayout();

    void setModel(TreeModel model);

    TreeModel getModel();

    TreeNode getNode(TreePath path);

    void scrollToPath(TreePath path);

    void expandPath(TreePath path);

    void collapsePath(TreePath path);

    public void togglePath(TreePath path);

    boolean isPathExpanded(TreePath path);

    boolean isNodeExpanded(TreeNode node);

    void addTreeListener(TreeListener listener);

    void removeTreeListener(TreeListener listener);
}
