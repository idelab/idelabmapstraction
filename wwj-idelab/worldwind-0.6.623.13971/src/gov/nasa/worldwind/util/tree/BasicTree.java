/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.util.tree;

import gov.nasa.worldwind.event.*;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.Logging;

import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * @author pabercrombie
 * @version $Id: BasicTree.java 13961 2010-10-08 21:31:17Z pabercrombie $
 */
public class BasicTree implements Tree
{
    protected TreeLayout layout;

    protected TreeModel model;

    protected List<TreeListener> listeners = new CopyOnWriteArrayList<TreeListener>();

    Set<TreePath> expandedNodes = new HashSet<TreePath>();

    public BasicTree()
    {
        this(null, null);
    }

    public BasicTree(TreeLayout layout)
    {
        this(null, layout);
    }

    public BasicTree(TreeModel model)
    {
        this(model, null);
    }

    public BasicTree(TreeModel model, TreeLayout layout)
    {
        this.setModel(model);
        this.setLayout(layout);
    }

    public void scrollToPath(TreePath path)
    {
        // TODO
    }

    public void expandPath(TreePath path)
    {
        this.expandedNodes.add(path);
        this.fireTreeEvent(new TreeEvent(this, this.getNode(path), TreeEvent.PATH_EXPANDED));
    }

    public void collapsePath(TreePath path)
    {
        this.expandedNodes.remove(path);
        this.fireTreeEvent(new TreeEvent(this, this.getNode(path), TreeEvent.PATH_COLLAPSED));
    }

    public TreeNode getNode(TreePath path)
    {
        TreeNode node = this.getModel().getRoot();
        if (!node.getText().equals(path.get(0))) // Test root node
            return null;    

        Iterator<String> iterator = path.iterator();
        iterator.next(); // Skip root node, we already tested it above
        while (iterator.hasNext())
        {
            String nodeText = iterator.next();
            boolean foundMatch = false;
            for (TreeNode child : node.getChildren())
            {
                if (node.getText().equals(nodeText))
                {
                    node = child;
                    foundMatch = true;
                    break;
                }
            }
            if (!foundMatch)
                return null;
        }
        return node;
    }

    public void togglePath(TreePath path)
    {
        if (this.isPathExpanded(path))
            this.collapsePath(path);
        else
            this.expandPath(path);
    }

    public boolean isPathExpanded(TreePath path)
    {
        return this.expandedNodes.contains(path);
    }

    public boolean isNodeExpanded(TreeNode node)
    {
        return this.expandedNodes.contains(node.getPath());
    }

    public void addTreeListener(TreeListener listener)
    {
        if (listener == null)
        {
            String msg = Logging.getMessage("nullValue.ListenerIsNull");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }

        listeners.add(listener);
    }

    public void removeTreeListener(TreeListener listener)
    {
        listeners.remove(listener);
    }

    public void render(DrawContext dc)
    {
        TreeLayout layout = this.getLayout();
        if (layout != null)
            layout.render(this, dc);
    }

    public TreeLayout getLayout()
    {
        return layout;
    }

    public void setLayout(TreeLayout layout)
    {
        this.layout = layout;
    }

    public TreeModel getModel()
    {
        return model;
    }

    public void setModel(TreeModel model)
    {
        this.model = model;
    }

    protected void fireTreeEvent(TreeEvent event)
    {
        for (TreeListener listener : listeners)
        {
            try
            {
                listener.onTreeEvent(event);
            }
            catch (Exception e)
            {
                Logging.logger().warning(e.getLocalizedMessage());
            }
        }
    }
}
