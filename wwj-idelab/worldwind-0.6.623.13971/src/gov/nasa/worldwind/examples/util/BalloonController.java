/*
Copyright (C) 2001, 2010 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/
package gov.nasa.worldwind.examples.util;

import gov.nasa.worldwind.*;
import gov.nasa.worldwind.avlist.*;
import gov.nasa.worldwind.event.*;
import gov.nasa.worldwind.examples.ApplicationTemplate;
import gov.nasa.worldwind.layers.*;
import gov.nasa.worldwind.render.*;
import gov.nasa.worldwind.util.Logging;

import java.awt.*;

/**
 * @author pabercrombie
 * @version $Id: BalloonController.java 13928 2010-10-03 19:36:55Z pabercrombie $
 */
public class BalloonController implements SelectListener
{
    protected WorldWindow wwd;

    protected RenderableLayer balloonLayer;
    protected AVList lastSelectedObject;
    protected Balloon balloon;

    public BalloonController(WorldWindow wwd)
    {
        this.wwd = wwd;
        this.wwd.addSelectListener(this);
    }

    public void selected(SelectEvent event)
    {
        try
        {
            if (event.isRollover())
            {
                final Object topObject = event.getTopObject();
                if (this.lastSelectedObject == topObject || this.balloon == topObject)
                {
                    return; // Same thing selected
                }

                if (this.lastSelectedObject != null)
                {
                    this.hideBalloonPanel();
                    this.lastSelectedObject = null;
                }

                if (topObject != null && topObject instanceof AVList)
                {
                    final AVList avList = (AVList) topObject;
                    Object balloonObj = avList.getValue(AVKey.BALLOON);
                    if (balloonObj instanceof Balloon)
                    {
                        this.balloon = (Balloon) balloonObj;

                        // Only display balloons attached to the screen
                        if (Balloon.SCREEN_MODE.equals(balloon.getAttachmentMode()))
                        {
                            this.lastSelectedObject = avList;
                            this.showBalloon(this.balloon, event.getPickPoint());
                        }
                    }
                }
            }
        }
        catch (Exception e)
        {
            // Wrap the handler in a try/catch to keep exceptions from bubbling up
            Logging.logger().warning(e.getMessage() != null ? e.getMessage() : e.toString());
        }
    }

    protected void showBalloon(Balloon balloon, Point point)
    {
        balloon.setPosition(point);

        if (this.balloonLayer == null)
        {
            this.balloonLayer = new RenderableLayer();
            this.balloonLayer.setPickEnabled(true);
            this.addLayer(balloonLayer);
        }

        this.balloonLayer.removeAllRenderables();
        this.balloonLayer.addRenderable(balloon);
    }

    protected void hideBalloonPanel()
    {
        if (this.balloonLayer != null)
        {
            this.balloonLayer.removeAllRenderables();
            this.removeLayer(this.balloonLayer);
            this.balloonLayer.dispose();
            this.balloonLayer = null;
        }
    }

    protected void addLayer(Layer layer)
    {
        if (!this.wwd.getModel().getLayers().contains(layer))
            ApplicationTemplate.insertBeforeCompass(this.wwd, layer);
    }

    protected void removeLayer(Layer layer)
    {
        this.wwd.getModel().getLayers().remove(layer);
    }
}
