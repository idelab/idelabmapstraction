/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.examples.util;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.layers.*;
import gov.nasa.worldwind.util.tree.*;

/**
 * @author pabercrombie
 * @version $Id $
 */
public class LayerSelector extends BasicTree
{
    private static final String ICON_PATH = "images/16x16-icon-earth.png";    
    private static final String LAYER_MANAGER_ICON_PATH = "gov/nasa/worldwindow/images/layer-manager-64x64.png";

    protected LayerList layers;

    public LayerSelector(LayerList layers)
    {
        this.layers = layers;

        TreeNode root = new BasicTreeNode("Layers", LAYER_MANAGER_ICON_PATH);
        this.setModel(new BasicTreeModel(root));
        this.setLayout(new BasicTreeLayout(20, 130));

        this.expandPath(root.getPath());

        for (Layer layer : this.layers)
        {
            root.addChild(new LayerTreeNode(layer));
        }
    }

    protected class LayerTreeNode extends BasicTreeNode
    {
        protected Layer layer;

        public LayerTreeNode(Layer layer)
        {
            super(layer.getName());
            this.layer = layer;

            Object icon = layer.getValue(AVKey.IMAGE);
            if (icon == null)
                icon = ICON_PATH;
            this.setImageSource(icon);
        }

        @Override
        public String getText()
        {
            return this.layer.getName();
        }

        @Override
        public boolean isSelected()
        {
            return this.layer.isEnabled();
        }

        @Override
        public void setSelected(boolean selected)
        {
            this.layer.setEnabled(selected);
        }
    }
}
