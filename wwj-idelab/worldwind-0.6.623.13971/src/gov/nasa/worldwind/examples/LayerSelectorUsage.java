/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.examples;

import gov.nasa.worldwind.examples.util.LayerSelector;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.util.tree.BasicTreeController;

/**
 * @author pabercrombie
 * @version $Id $
 */
public class LayerSelectorUsage extends ApplicationTemplate
{
    public static class AppFrame extends ApplicationTemplate.AppFrame
    {
        BasicTreeController controller;

        public AppFrame()
        {
            super(true, false, false);

            RenderableLayer layer = new RenderableLayer();

            LayerSelector selector = new LayerSelector(this.getWwd().getModel().getLayers());
            layer.addRenderable(selector);

            controller = new BasicTreeController(this.getWwd(), selector);

            // Add the layer to the model.
            insertBeforePlacenames(getWwd(), layer);
        }
    }

    public static void main(String[] args)
    {
        ApplicationTemplate.start("World Wind Layer Selector", AppFrame.class);
    }
}
