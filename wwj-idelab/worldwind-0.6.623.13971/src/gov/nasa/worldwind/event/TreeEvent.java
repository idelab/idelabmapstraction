/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.event;

import gov.nasa.worldwind.util.tree.*;

import java.util.EventObject;

/**
 * @author pabercrombie
 * @version $Id: TreeEvent.java 13961 2010-10-08 21:31:17Z pabercrombie $
 */
public class TreeEvent extends EventObject
{
    public static final String PATH_EXPANDED = "gov.nasa.worldwind.TreeEvent.PathExpanded";
    public static final String PATH_COLLAPSED = "gov.nasa.worldwind.TreeEvent.PathCollapsed";
    public static final String NODE_SELECTION_CHANGED = "gov.nasa.worldwind.TreeEvent.NodeSelectionChanged";

    protected TreeNode node;
    protected final String eventAction;

    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     *
     * @throws IllegalArgumentException if source is null.
     */
    public TreeEvent(Tree source, TreeNode node, String eventAction)
    {
        super(source);
        this.eventAction = eventAction;
        this.node = node;
    }

    @Override
    public Tree getSource()
    {
        return (Tree)super.getSource();
    }

    public TreeNode getNode()
    {
        return node;
    }

    public String getEventAction()
    {
        return eventAction;
    }
}
