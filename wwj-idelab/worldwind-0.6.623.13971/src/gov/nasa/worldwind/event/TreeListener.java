/*
 * Copyright (C) 2001, 2010 United States Government
 * as represented by the Administrator of the
 * National Aeronautics and Space Administration.
 * All Rights Reserved.
 */

package gov.nasa.worldwind.event;

import java.util.EventListener;

/**
 * @author pabercrombie
 * @version $Id: TreeListener.java 13961 2010-10-08 21:31:17Z pabercrombie $
 */
public interface TreeListener extends EventListener
{
    void onTreeEvent(TreeEvent event);
}
