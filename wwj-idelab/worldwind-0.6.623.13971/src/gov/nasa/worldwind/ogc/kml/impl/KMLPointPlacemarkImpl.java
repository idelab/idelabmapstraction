/*
Copyright (C) 2001, 2010 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/

package gov.nasa.worldwind.ogc.kml.impl;

import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.ogc.kml.*;
import gov.nasa.worldwind.render.*;
import gov.nasa.worldwind.util.*;

/**
 * Implements the <i>Point</i> case of a KML <i>Placemark</i> element.
 *
 * @author tag
 * @version $Id$
 */
public class KMLPointPlacemarkImpl extends PointPlacemark implements KMLRenderable
{
    protected final KMLPlacemark parent;
    protected boolean highlightAttributesResolved = false;
    protected boolean normalAttributesResolved = false;

    protected boolean drawBalloon;

    /**
     * Create an instance.
     *
     * @param tc        the current {@link KMLTraversalContext}.
     * @param placemark the <i>Placemark</i> element containing the <i>Point</i>.
     * @param geom      the {@link KMLPoint} geometry.
     *
     * @throws NullPointerException     if the geometry is null.
     * @throws IllegalArgumentException if the parent placemark or the traversal context is null.
     */
    public KMLPointPlacemarkImpl(KMLTraversalContext tc, KMLPlacemark placemark, KMLAbstractGeometry geom)
    {
        super(((KMLPoint) geom).getCoordinates());

        if (tc == null)
        {
            String msg = Logging.getMessage("nullValue.TraversalContextIsNull");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }

        if (placemark == null)
        {
            String msg = Logging.getMessage("nullValue.ParentIsNull");
            Logging.logger().severe(msg);
            throw new IllegalArgumentException(msg);
        }

        this.parent = placemark;

        KMLPoint point = (KMLPoint) geom;
        if (point.isExtrude())
            this.setLineEnabled(true);

        String altMode = point.getAltitudeMode();
        if (!WWUtil.isEmpty(altMode))
        {
            if ("clampToGround".equals(altMode))
                this.setAltitudeMode(WorldWind.CLAMP_TO_GROUND);
            else if ("relativeToGround".equals(altMode))
                this.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
            else if ("absolute".equals(altMode))
                this.setAltitudeMode(WorldWind.ABSOLUTE);
        }

        if (this.parent.getVisibility() != null)
            this.setVisible(this.parent.getVisibility());

        if (placemark.getName() != null)
        {
            this.setLabelText(placemark.getName());
            this.setValue(AVKey.DISPLAY_NAME, placemark.getName());
        }

        String description = placemark.getDescription();
        if (description != null)
            this.setValue(AVKey.DESCRIPTION, description);

        if (placemark.getSnippetText() != null)
            this.setValue(AVKey.SHORT_DESCRIPTION, placemark.getSnippetText());
    }

    public void preRender(KMLTraversalContext tc, DrawContext dc)
    {
        // Intentionally left blank; KML point placemark does nothing during the preRender phase.
    }

    public void render(KMLTraversalContext tc, DrawContext dc)
    {
        Balloon balloon = (Balloon) this.getValue(AVKey.BALLOON);

        // If the attributes are not inline or internal then they might not be resolved until the external KML
        // document is resolved. Therefore check to see if resolution has occurred.

        // If there are unresolved styles, we may need to create a balloon
        if (!normalAttributesResolved || !highlightAttributesResolved)
        {
            String state = highlighted ? KMLConstants.HIGHLIGHT : KMLConstants.NORMAL;
            KMLBalloonStyle balloonStyle = (KMLBalloonStyle) this.parent.getSubStyle(new KMLBalloonStyle(null), state);

            if (balloon == null && balloonStyle.hasFields())
            {
                balloon = this.createBalloon(balloonStyle);
                if (balloon != null)
                {
                    this.setValue(AVKey.BALLOON, balloon);
                    balloon.setDelegateOwner(this);    
                    this.drawBalloon = !"hide".equals(balloonStyle.getDisplayMode());
                }
            }
        }
        
        if (this.isHighlighted())
        {
            if (!this.highlightAttributesResolved)
            {
                PointPlacemarkAttributes a = this.getHighlightAttributes();
                if (a == null || a.isUnresolved())
                {
                    a = this.makeAttributesCurrent(KMLConstants.HIGHLIGHT);
                    if (a != null)
                    {
                        this.setHighlightAttributes(a);
                        if (!a.isUnresolved())
                            this.highlightAttributesResolved = true;
                    }
                }
            }
        }
        else
        {
            if (!this.normalAttributesResolved)
            {
                PointPlacemarkAttributes a = this.getAttributes();
                if (a == null || a.isUnresolved())
                {
                    a = this.makeAttributesCurrent(KMLConstants.NORMAL);
                    if (a != null)
                    {
                        this.setAttributes(a);
                        if (!a.isUnresolved())
                            this.normalAttributesResolved = true;
                    }
                }
            }
        }

        if (this.isVisible() && this.drawBalloon && balloon != null)
        {
            balloon.setPosition(this.position);
            balloon.render(dc);
        }

        this.render(dc);
    }

    /**
     * If there is a balloon attached to the placemark, highlight the balloon when the placemark is highlighted.
     *
     * @param highlighted true to highlight the shape, otherwise false.
     */
    @Override
    public void setHighlighted(boolean highlighted)
    {
        Balloon balloon = (Balloon) this.getValue(AVKey.BALLOON);
        if (balloon != null)
            balloon.setHighlighted(highlighted);

        super.setHighlighted(highlighted);
    }

    /**
     * Determine and set the {@link PointPlacemark} attributes from the KML <i>Feature</i> fields.
     *
     * @param attrType the type of attributes, either {@link KMLConstants#NORMAL} or {@link KMLConstants#HIGHLIGHT}.
     *
     * @return the new attributes.
     */
    protected PointPlacemarkAttributes makeAttributesCurrent(String attrType)
    {
        PointPlacemarkAttributes attrs = this.getInitialAttributes(
            this.isHighlighted() ? KMLConstants.HIGHLIGHT : KMLConstants.NORMAL);

        // Get the KML sub-style for Line attributes. Map them to Shape attributes.

        KMLAbstractSubStyle subStyle = this.parent.getSubStyle(new KMLLineStyle(null), attrType);
        if (!this.isHighlighted() || KMLUtil.isHighlightStyleState(subStyle))
        {
            this.assembleLineAttributes(attrs, (KMLLineStyle) subStyle);
            if (subStyle.hasField(AVKey.UNRESOLVED))
                attrs.setUnresolved(true);
        }

        subStyle = this.parent.getSubStyle(new KMLIconStyle(null), attrType);
        if (!this.isHighlighted() || KMLUtil.isHighlightStyleState(subStyle))
        {
            this.assemblePointAttributes(attrs, (KMLIconStyle) subStyle);
            if (subStyle.hasField(AVKey.UNRESOLVED))
                attrs.setUnresolved(true);
        }

        subStyle = this.parent.getSubStyle(new KMLLabelStyle(null), attrType);
        if (!this.isHighlighted() || KMLUtil.isHighlightStyleState(subStyle))
        {
            this.assembleLabelAttributes(attrs, (KMLLabelStyle) subStyle);
            if (subStyle.hasField(AVKey.UNRESOLVED))
                attrs.setUnresolved(true);
        }

        return attrs;
    }

    protected PointPlacemarkAttributes assemblePointAttributes(PointPlacemarkAttributes attrs, KMLIconStyle style)
    {
        if (style.getIcon() != null && style.getIcon().getHref() != null)
        {
            // The icon reference may be to a support file within a KMZ file, so check for that. If it's not, then just
            // let the normal PointPlacemark code resolve the reference.
            String href = style.getIcon().getHref();
            String localAddress = (String) this.parent.getRoot().resolveLocalReference(href, null);
            attrs.setImageAddress((localAddress != null ? localAddress : href));
        }

        // Assign the other attributes defined in the KML Feature element.

        if (style.getScale() != null)
            attrs.setScale(style.getScale());

        if (style.getHeading() != null)
        {
            attrs.setHeading(style.getHeading());
            attrs.setHeadingReference(AVKey.RELATIVE_TO_SCREEN); // KML spec is not clear about this
        }

        if (style.getHotSpot() != null)
        {
            KMLVec2 hs = style.getHotSpot();
            attrs.setImageOffset(new Offset(hs.getX(), hs.getY(), KMLUtil.kmlUnitsToWWUnits(hs.getXunits()),
                KMLUtil.kmlUnitsToWWUnits(hs.getYunits())));
        }

        return attrs;
    }

    protected PointPlacemarkAttributes assembleLineAttributes(PointPlacemarkAttributes attrs, KMLLineStyle style)
    {
        // Assign the attributes defined in the KML Feature element.

        if (style.getWidth() != null)
            attrs.setLineWidth(style.getWidth());

        if (style.getColor() != null)
            attrs.setLineColor(style.getColor());

        if (style.getColorMode() != null && "random".equals(style.getColorMode()))
            attrs.setLineMaterial(new Material(WWUtil.makeRandomColor(attrs.getLineColor())));

        return attrs;
    }

    protected PointPlacemarkAttributes assembleLabelAttributes(PointPlacemarkAttributes attrs, KMLLabelStyle style)
    {
        // Assign the attributes defined in the KML Feature element.

        if (style.getScale() != null)
            attrs.setLabelScale(style.getScale());

        if (style.getColor() != null)
            attrs.setLabelColor(style.getColor());

        if (style.getColorMode() != null && "random".equals(style.getColorMode()))
            attrs.setLabelMaterial(new Material(WWUtil.makeRandomColor(attrs.getLabelColor())));

        return attrs;
    }

    /**
     * Create a balloon for this feature.
     *
     * @param style Feature's balloon style.
     *
     * @return New balloon for the feature.
     */
    protected Balloon createBalloon(KMLBalloonStyle style)
    {
        return this.parent.getRoot().createBalloon(this.parent, style);
    }

    /**
     * Get the initial attributes for this feature. These attributes will be changed to reflect the feature's style.
     *
     * @param attrType {@link KMLConstants#NORMAL} or {@link KMLConstants#HIGHLIGHT}.
     *
     * @return New placemark attributes.
     */
    protected PointPlacemarkAttributes getInitialAttributes(String attrType)
    {
        return new PointPlacemarkAttributes();
    }
}
