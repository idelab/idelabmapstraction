/*
Copyright (C) 2001, 2010 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/

package gov.nasa.worldwind.ogc.kml;

import gov.nasa.worldwind.util.*;

import java.util.List;
import java.util.regex.*;

/**
 * Represents the KML <i>BalloonStyle</i> element and provides access to its contents.
 *
 * @author tag
 * @version $Id: KMLBalloonStyle.java 13876 2010-09-25 23:45:52Z pabercrombie $
 */
public class KMLBalloonStyle extends KMLAbstractSubStyle
{
    /**
     * Object returned by {@link KMLBalloonStyle#getBalloonText(KMLAbstractFeature)}. Consists of the balloon text,
     * and a boolean flag to indicate if there entities in the text which cannot be resolved. Some entities may refer
     * to schema in remote files, and it may not be possible to resolve them immediately. 
     */
    public class BalloonText
    {
        /**
         * The balloon text.
         */
        public String text;

        /**
         * True if there are entities in the balloon text which may refer to unresolved schema. False if all entities
         * have been resolved, or are known to be unresolvable (because the data they refer to does not exist).
         */
        public boolean isUnresolved;
    }

    /**
     * Construct an instance.
     *
     * @param namespaceURI the qualifying namespace URI. May be null to indicate no namespace qualification.
     */
    public KMLBalloonStyle(String namespaceURI)
    {
        super(namespaceURI);
    }

    public String getcolor()
    {
        return (String) this.getField("color");
    }

    public String getBgColor()
    {
        return (String) this.getField("bgColor");
    }

    public String getTextColor()
    {
        return (String) this.getField("textColor");
    }

    /**
     * Get the <i>text</i> field. This method returns the raw text string, before entity substitution. {@link
     * #getBalloonText(KMLAbstractFeature)} returns the text after substitutions.
     *
     * @return Raw text field.
     *
     * @see #getBalloonText(KMLAbstractFeature)
     */
    public String getText()
    {
        return (String) this.getField("text");
    }

    public String getDisplayMode()
    {
        return (String) this.getField("displayMode");
    }

    /**
     * Get the balloon text after entity substitutions (for example, $[name], $[description], etc) have been made. See
     * the KML specification for details on entity replacement.
     *
     * @param feature Feature to use as context for the entity replacements.
     *
     * @return String after replacements have been made. Returns null if {@link #getText()} returns null.
     *
     * @see #getText()
     */
    public BalloonText getBalloonText(KMLAbstractFeature feature)
    {
        if (feature == null)
        {
            String message = Logging.getMessage("nullValue.FeatureIsNull");
            Logging.logger().severe(message);
            throw new IllegalArgumentException(message);
        }

        String text = this.getText();
        if (text == null)
            return null;

        BalloonText balloonText = new BalloonText();

        Pattern p = Pattern.compile("\\$\\[(.*?)\\]");
        Matcher m = p.matcher(text);
        StringBuffer sb = new StringBuffer();
        while (m.find())
        {
            String r = this.resolveEntityReference(m.group(1), feature, balloonText);

            if (r != null)
                m.appendReplacement(sb, r);
        }
        m.appendTail(sb);

        balloonText.text = sb.toString();
        return balloonText;
    }

    /**
     * Resolve an entity reference. The pattern can be in one of these forms:
     * <pre>
     *  FeatureField
     *  DataField
     *  DataField/displayName
     *  SchemaName/SchemaField
     *  SchemaName/SchemaField/displayName
     *  </pre>
     * See the KML spec for details.
     *
     * @param pattern Pattern of entity to resolve.
     * @param feature Feature to use as context for resolving entity.
     * @param balloonText BalloonText object that will hold the entity replacement result. If the {@code pattern}
     *        cannot be resolved, but could refer to schema data, and if there are unresolved schema,
     *        balloonText.isUnresolved will be set to true and null will be returned.
     *
     * @return Return the replacement string for the entity, or null if no replacement can be found. This method may
     *         also modify {@link BalloonText#isUnresolved}.
     */
    protected String resolveEntityReference(String pattern, KMLAbstractFeature feature, BalloonText balloonText)
    {
        // First look for a field in the Feature
        String replacement = (String) feature.getField(pattern);
        if (replacement != null)
            return replacement;

        // Before searching data fields, split the pattern into name, field, and display name components
        String name;            // Name of data element or schema
        String field = "";      // Schema field (does not apply to data fields)
        boolean isDisplayName;  // Indicates that the replacement is the display name of the data, not the data itself

        isDisplayName = pattern.endsWith("/displayName");

        String[] parts = pattern.split("/");

        name = parts[0];  // Name is always the first field, or the whole pattern if there were no slashes

        if ((parts.length == 2 && !isDisplayName) || (parts.length > 2)) // name/field or name/field/displayName
        {
            field = parts[1];
        }

        // Next look for a data field in the Feature's extended data
        KMLExtendedData extendedData = feature.getExtendedData();
        if (extendedData != null)
        {
            // Search through untyped data first
            for (KMLData data : extendedData.getData())
            {
                if (name.equals(data.getName()))
                {
                    if (isDisplayName && !WWUtil.isEmpty(data.getDisplayName()))
                        return data.getDisplayName();
                    else
                        return data.getValue();
                }
            }

            // Search through typed schema data fields
            boolean schemaUnresolved = false;
            List<KMLSchemaData> schemaDataList = extendedData.getSchemaData();
            for (KMLSchemaData schemaData : schemaDataList)
            {
                // Try to resolve the schema. The schema may not be available immediately
                final String url = schemaData.getSchemaUrl();
                KMLSchema schema = (KMLSchema) feature.getRoot().resolveReference(url);

                if (schema != null && name.equals(schema.getName()))
                {
                    // We found the schema. Now we are looking for either the display name or the value of
                    // one of the fields.
                    if (isDisplayName)
                    {
                        // Search schema fields
                        for (KMLSimpleField simpleField : schema.getSimpleFields())
                        {
                            if (field.equals(simpleField.getName()))
                            {
                                return simpleField.getDisplayName();
                            }
                        }
                    }
                    else
                    {
                        // Search data fields
                        for (KMLSimpleData simpleData : schemaData.getSimpleData())
                        {
                            if (field.equals(simpleData.getName()))
                            {
                                return simpleData.getCharacters();
                            }
                        }
                    }
                }
                else if (schema == null)
                {
                    schemaUnresolved = true;
                }
            }

            // Set the balloon text to unresolved if we still haven't found a match, and there is at least one
            // unresolved schema, and the pattern could refer to a schema 
            if (schemaUnresolved && !WWUtil.isEmpty(name) && !WWUtil.isEmpty(field))
            {
                balloonText.isUnresolved = true;
            }
        }

        // TODO: implement geDirections. For now we will just remove it from the text.
        if ("geDirections".equals(pattern))
            return "";

        return null; // No match found
    }
}
