/*
Copyright (C) 2001, 2010 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/

package gov.nasa.worldwind.ogc.kml;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.util.xml.*;

import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;
import java.util.ArrayList;

/**
 * Parses KML <i>coordinates</i> elements.
 *
 * @author tag
 * @version $Id: KMLCoordinatesParser.java 13940 2010-10-06 07:53:59Z pabercrombie $
 */
public class KMLCoordinatesParser extends AbstractXMLEventParser
{
    @SuppressWarnings({"UnnecessaryContinue"})
    public Position.PositionList parse(XMLEventParserContext ctx, XMLEvent doubleEvent, Object... args)
        throws XMLStreamException
    {
        String s = ctx.getStringParser().parseString(ctx, doubleEvent);
        if (s == null || s.length() < 3) // "a,b" is the smallest possible coordinate string
            return null;

        String[] tuples = s.split("[\\s]");
        ArrayList<Position> positions = new ArrayList<Position>(tuples.length);

        for (String tuple : tuples)
        {
            try
            {
                String[] words = tuple.split(",");

                if (words.length > 2)
                    positions.add(Position.fromDegrees(Double.valueOf(words[1]), Double.valueOf(words[0]),
                        Double.valueOf(words[2])));
                else if (words.length == 2)
                    positions.add(Position.fromDegrees(Double.valueOf(words[1]), Double.valueOf(words[0])));
                // just silently continue if fewer than 2 words
            }
            catch (NumberFormatException e)
            {
                continue; // TODO: issue warning?
            }
            catch (NullPointerException e)
            {
                continue; // TODO: issue warning?
            }
            catch (Exception e)
            {
                continue; // TODO: issue warning
            }
        }

        return new Position.PositionList(positions);
    }
}
