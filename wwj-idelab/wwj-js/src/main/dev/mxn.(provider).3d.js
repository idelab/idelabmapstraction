mxn.register('{{api_id}}', {	

Mapstraction: {
	
	// 3D functionality
	
	getPitch: function() {
		var map = this.maps[this.api];
		var pitch;
		
		// TODO: Add provider code
		
		return pitch;
	},

	setPitch: function(pitch) {
		var map = this.maps[this.api];
		// TODO: Add provider code
	},
	
	getHeading: function() {
		var map = this.maps[this.api];
		var heading;
		
		// TODO: Add provider code
		
		return heading;
	},

	setHeading: function(heading) {
		var map = this.maps[this.api];
		// TODO: Add provider code
	},
	
	getFieldOfView: function() {
		var map = this.maps[this.api];
		var fieldOfView;
		
		// TODO: Add provider code
		
		return fieldOfView;
	},

	setFieldOfView: function(fieldOfView) {
		var map = this.maps[this.api];
		// TODO: Add provider code
	},
	
	setStereoMode: function(stereo) {
		var map = this.maps[this.api];
		// TODO: Add provider code
	},
    
	isStereoMode: function() {
		var map = this.maps[this.api];
		var stereo;
		
		// TODO: Add provider code
		
		return stereo;
	},
	
	getAltitude: function() {
		var map = this.maps[this.api];
		var altitude;
		
		// TODO: Add provider code
		
		return altitude;
	},

	setAltitude: function(altitude) {
		var map = this.maps[this.api];
		// TODO: Add provider code
	},
	
	setCenterAndAltitude: function(point, altitude) {
		var map = this.maps[this.api];
		// TODO: Add provider code
	},
	
	addSphere: function(sphere) {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
		
		return sphere;
	},

	removeSphere: function(sphere) {
		var map = this.maps[this.api];
		// TODO: Add provider code
	},
	
	addCylinder: function(cylinder) {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
		
		return cylinder;
	},

	removeCylinder: function(cylinder) {
		var map = this.maps[this.api];
		// TODO: Add provider code
	},
	
	addPolyline3D: function(polyline3D) {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
		
		return polyline3D;
	},

	removePolyline3D: function(polyline3D) {
		var map = this.maps[this.api];
		// TODO: Add provider code
	},
	
},

LatLonPoint3D: {
	
	toProprietary: function() {
		// TODO: Add provider code
	},

	fromProprietary: function(worldwindPoint) {
		// TODO: Add provider code
	}
	
},

Sphere: {

	toProprietary: function() {
		// TODO: Add provider code
	},
	
	show: function() {
		// TODO: Add provider code
	},

	hide: function() {
		// TODO: Add provider code
	}
	
},

Cylinder: {

	toProprietary: function() {
		// TODO: Add provider code
	},
	
	show: function() {
		// TODO: Add provider code
	},

	hide: function() {
		// TODO: Add provider code
	}
	
},

Polyline3D: {

	toProprietary: function() {
		// TODO: Add provider code
	},
	
	show: function() {
		// TODO: Add provider code
	},

	hide: function() {
		// TODO: Add provider code
	}
	
}

});
