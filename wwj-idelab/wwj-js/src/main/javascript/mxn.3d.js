(function(){

	function extend(subclass, superclass) {
	   	function Dummy() {}
	   	Dummy.prototype = superclass.prototype;
	   	subclass.prototype = new Dummy();
	   	subclass.prototype.constructor = subclass;
	   	subclass.superclass = superclass;
	   	subclass.superproto = superclass.prototype;
	}
	
	/**
	 * Mapstraction3D instantiates an interactive map with some API choice into the HTML element given
	 * @param {String} element The HTML element to replace with an interactive map
	 * @param {String} api The API to use, one of 'google', 'yahoo', 'microsoft', 'openstreetmap', 'multimap', 'map24', 'openlayers', 'mapquest'. If omitted, first loaded provider implementation is used.
	 * @param {Bool} debug optional parameter to turn on debug support - this uses alert panels for unsupported actions
	 * @extends mxn.Mapstraction
	 * @constructor
	 */
	mxn.Mapstraction3D = function (element, api,debug){
		mxn.Mapstraction3D.superclass.call(this, element,api,debug);
		mxn.addEvents(this, [
			'markerChanged',	// Marker is changed {marker: Marker}
			'polylineChanged',	// Polyline is changed {polyline: Polyline}
			'markerSelected',	// Marker is selected
			'polylineSelected',	// Polyline is selected
			'markerUnselected', // Marker is unselected
			'polylineUnselected'// Polyline is unselected
		]);

		var scripts = document.getElementsByTagName('script');
		for (var i = 0; i < scripts.length; i++) {
			var match = scripts[i].src.replace(/%20/g , '').match(/^(.*?)(mxn|idelabmapstraction)\.js/);
			if (match !== null) {
				this.src = match[1];
				break;
		   }
		}
		/**
		 * POINT FEATURE
		 */
		this.POINT = "point";
		/**
		 * LINESTRING FEATURE
		 */
		this.LINESTRING = "linestring";
		/**
		 *POLYGON FEATURE
		 */
		this.POLYGON = "polygon";
		/**
		 * ALLOWED FEATURES ARRAY
		 */
		this.ALLOWED_FEATURES = [this.POINT,this.LINESTRING,this.POLYGON];
	};
	var Mxn3D = mxn.Mapstraction3D; 
	extend(mxn.Mapstraction3D,mxn.Mapstraction);

	mxn.addProxyMethods(mxn.Mapstraction3D, [ 
                                   
    'getPitch',
    
    'setPitch',
    
    'getHeading',
    
    'setHeading',
    
    'getFieldOfView',
    
    'setFieldOfView',
    
    'getVerticalExaggeration',
    
    'setVerticalExaggeration',
    
    'isStereoMode',
    
    'setStereoMode',
    
    'getAltitude',
    
    'setAltitude',
    
    'setCenterAndAltitude',
    
    'addSphere',
    
    'removeSphere',
    
    'addCylinder',
    
    'removeCylinder',
    
    'addPolyline3D',
    
    'removePolyline3D',
    
    'addColladaModel',
    
    'removeColladaModel',
    
    'addMovable3DModel',
    
    'removeMovable3DModel'

]);
	
	
	//////////////////////////////
	//
	//   LatLonPoint3D
	//
	/////////////////////////////

	/**
	 * LatLonPoint3D is a point containing a latitude, longitude and an altitude with helper methods
	 * @name mxn.LatLonPoint3D
	 * @constructor
	 * @param {double} lat is the latitude
	 * @param {double} lon is the longitude
	 * @param {double} alt is the altitude
	 * @exports LatLonPoint3D as mxn.LatLonPoint3D
	 */
	var LatLonPoint3D = mxn.LatLonPoint3D = function(lat, lon, alt) {
		// TODO error if undefined?
		//  if (lat == undefined) alert('undefined lat');
		//  if (lon == undefined) alert('undefined lon');
		this.lat = lat;
		this.lon = lon;
		this.lng = lon; // lets be lon/lng agnostic
		this.alt = alt;
		
		this.invoker = new mxn.Invoker(this, 'LatLonPoint3D');		
	};

	mxn.addProxyMethods(LatLonPoint3D, [ 
		'fromProprietary', 'toProprietary'
	], true);

	/**
	 * toString returns a string represntation of a point
	 * @returns a string like '51.23, -0.123, 1000'
	 * @type String
	 */
	LatLonPoint3D.prototype.toString = function() {
		return this.lat + ', ' + this.lon + ', ' + this.alt;
	};

	/**
	 * distance returns the distance in kilometers between two points
	 * @param {LatLonPoint} otherPoint The other point to measure the distance from to this one
	 * @returns the distance between the points in kilometers
	 * @type double
	 */
	LatLonPoint3D.prototype.distance = function(otherPoint) {
		// Uses Haversine formula from http://www.movable-type.co.uk   // MODIFY!!!!!!!!!!!!!
		var rads = Math.PI / 180;
		var diffLat = (this.lat-otherPoint.lat) * rads;
		var diffLon = (this.lon-otherPoint.lon) * rads; 
		var a = Math.sin(diffLat / 2) * Math.sin(diffLat / 2) +
			Math.cos(this.lat*rads) * Math.cos(otherPoint.lat*rads) * 
			Math.sin(diffLon/2) * Math.sin(diffLon/2); 
		return 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)) * 6371; // Earth's mean radius in km
	};

	/**
	 * equals tests if this point is the same as some other one
	 * @param {LatLonPoint3D} otherPoint The other point to test with
	 * @returns true or false
	 * @type boolean
	 */
	LatLonPoint3D.prototype.equals = function(otherPoint) {
		return this.lat == otherPoint.lat && this.lon == otherPoint.lon && this.alt == otherPoint.alt;
	};

	/**
	 * Returns latitude conversion based on current projection
	 * @returns {Float} conversion
	 */
	LatLonPoint3D.prototype.latConv = function() {
		return this.distance(new LatLonPoint(this.lat + 0.1, this.lon))*10;
	};

	/**
	 * Returns longitude conversion based on current projection
	 * @returns {Float} conversion
	 */
	LatLonPoint3D.prototype.lonConv = function() {
		return this.distance(new LatLonPoint(this.lat, this.lon + 0.1))*10;
	};
	
	
	
	
	/////////////
	// Sphere ///
	/////////////

	/**
	 * Instantiates a new Sphere.
	 * @name mxn.Sphere
	 * @constructor
	 * @param {LatLonPoint3D} center of the sphere.
	 * @param {double} radius.
	 * @exports Sphere as mxn.Sphere
	 */
	var Sphere = mxn.Sphere = function(center,radius) {
		this.api = null;
		this.center = center;
		this.radius = radius;
		this.attributes = [];
		this.onmap = false;
		this.proprietary_sphere = false;
		this.pllID = "mspll-"+new Date().getTime()+'-'+(Math.floor(Math.random()*Math.pow(2,16)));
		this.invoker = new mxn.Invoker(this, 'Sphere', function(){return this.api;});
	};

	mxn.addProxyMethods(Sphere, [ 
		'fromProprietary', 
		'hide',
		'show',
		'toProprietary',
		'update'
	]);

	/**
	 * addData conviniently set a hash of options on a polyline
	 */
	Sphere.prototype.addData = function(options){
		for(var sOpt in options) {
			if(options.hasOwnProperty(sOpt)){
				switch(sOpt) {
					case 'color':
						this.setColor(options.color);
						break;
					case 'width':
						this.setWidth(options.width);
						break;
					case 'fillColor':
						this.setFillColor(options.fillColor);
						break;
					case 'opacity':
						this.setOpacity(options.opacity);
						break;
					default:
						this.setAttribute(sOpt, options[sOpt]);
						break;
				}
			}
		}
	};

	/**
	 * in the form: #RRGGBB
	 * Note map24 insists on upper case, so we convert it.
	 */
	mxn.Sphere.prototype.setColor = function(color){
		if(color !== null){
			this.color = (color.length==7 && color[0]=="#") ? color.toUpperCase() : color;
		}else {this.color = null;}
	};

	/**
	 * A float between 0.0 and 1.0
	 * @param {Float} opacity
	 */
	Sphere.prototype.setOpacity = function(opacity){
		this.opacity = opacity;
	};

	/**
	 * Fill color for the sphere as HTML color value e.g. #RRGGBB
	 * @param {String} sFillColor HTML color value #RRGGBB
	 */
	Sphere.prototype.setFillColor = function(sFillColor) {
		this.fillColor = sFillColor;
	};
	
	/**
	 * Stroke width of the sphere
	 * @param {Integer} width
	 */
	Sphere.prototype.setWidth = function(width){
		this.width = width;
	};

	/**
	 * setAttribute: set an arbitrary key/value pair on a Sphere
	 * @arg(String) key
	 * @arg value
	 */
	Sphere.prototype.setAttribute = function(key,value) {
		this.attributes[key] = value;
	};

	/**
	 * getAttribute: gets the value of "key"
	 * @arg(String) key
	 * @returns value
	 */
	Sphere.prototype.getAttribute = function(key) {
		return this.attributes[key];
	};
	
	
	
	///////////////
	// Cylinder ///
	///////////////

	/**
	 * Instantiates a new Cylinder.
	 * @name mxn.Sphere
	 * @constructor
	 * @param {LatLonPoint3D} center of the Cylinder.
	 * @param {double} radius.
	 * @exports Sphere as mxn.Cylinder
	 */
	var Cylinder = mxn.Cylinder = function(center,radius,height) {
		this.api = null;
		this.center = center;
		this.radius = radius;
		this.height = height;
		this.attributes = [];
		this.onmap = false;
		this.proprietary_cylinder = false;
		this.pllID = "mspll-"+new Date().getTime()+'-'+(Math.floor(Math.random()*Math.pow(2,16)));
		this.invoker = new mxn.Invoker(this, 'Cylinder', function(){return this.api;});
	};

	mxn.addProxyMethods(Cylinder, [ 
		'fromProprietary', 
		'hide',
		'show',
		'toProprietary',
		'update'
	]);

	/**
	 * addData conviniently set a hash of options on a Cylinder
	 */
	Cylinder.prototype.addData = function(options){
		for(var sOpt in options) {
			if(options.hasOwnProperty(sOpt)){
				switch(sOpt) {
					case 'color':
						this.setColor(options.color);
						break;
					case 'width':
						this.setWidth(options.width);
						break;
					case 'fillColor':
						this.setFillColor(options.fillColor);
						break;
					case 'opacity':
						this.setOpacity(options.opacity);
						break;
					default:
						this.setAttribute(sOpt, options[sOpt]);
						break;
				}
			}
		}
	};

	/**
	 * in the form: #RRGGBB
	 * Note map24 insists on upper case, so we convert it.
	 */
	mxn.Cylinder.prototype.setColor = function(color){
		if(color !== null){
			this.color = (color.length==7 && color[0]=="#") ? color.toUpperCase() : color;
		}else {this.color = null;}
	};

	/**
	 * A float between 0.0 and 1.0
	 * @param {Float} opacity
	 */
	Cylinder.prototype.setOpacity = function(opacity){
		this.opacity = opacity;
	};

	/**
	 * Fill color for the sphere as HTML color value e.g. #RRGGBB
	 * @param {String} sFillColor HTML color value #RRGGBB
	 */
	Cylinder.prototype.setFillColor = function(sFillColor) {
		this.fillColor = sFillColor;
	};
	
	/**
	 * Stroke width of the Cylinder
	 * @param {Integer} width
	 */
	Cylinder.prototype.setWidth = function(width){
		this.width = width;
	};

	/**
	 * setAttribute: set an arbitrary key/value pair on a Cylinder
	 * @arg(String) key
	 * @arg value
	 */
	Cylinder.prototype.setAttribute = function(key,value) {
		this.attributes[key] = value;
	};

	/**
	 * getAttribute: gets the value of "key"
	 * @arg(String) key
	 * @returns value
	 */
	Cylinder.prototype.getAttribute = function(key) {
		return this.attributes[key];
	};
	
	
	
	/**
	 * Instantiates a new three-dimensional Polygon.
	 * @name mxn.Polyline3D
	 * @constructor
	 * @param {Point[]} points Points that make up the Polygon.
	 * @exports Polyline3D as mxn.Polyline3D
	 */
	var Polyline3D = mxn.Polyline3D = function(points,min_altitude,max_altitude) {
		this.api = null;
		this.points = points;
		this.min_altitude = min_altitude;
		this.max_altitude = max_altitude;
		this.attributes = [];
		this.onmap = false;
		this.proprietary_polyline3D = false;
		this.pllID = "mspll-"+new Date().getTime()+'-'+(Math.floor(Math.random()*Math.pow(2,16)));
		this.invoker = new mxn.Invoker(this, 'Polyline3D', function(){return this.api;});
	};

	mxn.addProxyMethods(Polyline3D, [ 
		'fromProprietary', 
		'hide',
		'show',
		'toProprietary',
		'update'
	]);

	/**
	 * addData conviniently set a hash of options on a Polyline3D
	 */
	Polyline3D.prototype.addData = function(options){
		for(var sOpt in options) {
			if(options.hasOwnProperty(sOpt)){
				switch(sOpt) {
					case 'color':
						this.setColor(options.color);
						break;
					case 'width':
						this.setWidth(options.width);
						break;
					case 'opacity':
						this.setOpacity(options.opacity);
						break;
					case 'closed':
						this.setClosed(options.closed);
						break;
					case 'fillColor':
						this.setFillColor(options.fillColor);
						break;
					default:
						this.setAttribute(sOpt, options[sOpt]);
						break;
				}
			}
		}
	};

	/**
	 * in the form: #RRGGBB
	 * Note map24 insists on upper case, so we convert it.
	 */
	mxn.Polyline3D.prototype.setColor = function(color){
		if(color !== null){
			this.color = (color.length==7 && color[0]=="#") ? color.toUpperCase() : color;
		}else {this.color = null;}
	};

	/**
	 * Stroke width of the Polyline3D
	 * @param {Integer} width
	 */
	Polyline3D.prototype.setWidth = function(width){
		this.width = width;
	};

	/**
	 * Marks the Polyline3D as a closed polygon
	 * @param {Boolean} bClosed
	 */
	Polyline3D.prototype.setClosed = function(bClosed){
		this.closed = bClosed;
	};
	
	/**
	 * A float between 0.0 and 1.0
	 * @param {Float} opacity
	 */
	Polyline3D.prototype.setOpacity = function(opacity){
		this.opacity = opacity;
	};

	/**
	 * Fill color for a closed Polyline3D as HTML color value e.g. #RRGGBB
	 * @param {String} sFillColor HTML color value #RRGGBB
	 */
	Polyline3D.prototype.setFillColor = function(sFillColor) {
		this.fillColor = sFillColor;
	};


	/**
	 * setAttribute: set an arbitrary key/value pair on a Polyline3D
	 * @arg(String) key
	 * @arg value
	 */
	Polyline3D.prototype.setAttribute = function(key,value) {
		this.attributes[key] = value;
	};

	/**
	 * getAttribute: gets the value of "key"
	 * @arg(String) key
	 * @returns value
	 */
	Polyline3D.prototype.getAttribute = function(key) {
		return this.attributes[key];
	};		
	
})();