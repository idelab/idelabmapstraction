mxn.register('google', {	

Mapstraction: {
	
	// 3D functionality
	
	getPitch: function() {
		var map = this.maps[this.api];
		
		// Get the current view
		var camera = map.getView().copyAsCamera(map.ALTITUDE_RELATIVE_TO_GROUND);
		
		var pitch = camera.getRoll();
		return pitch;
	},

	setPitch: function(pitch) {
		var map = this.maps[this.api];
		
		// Get the current view
		var camera = map.getView().copyAsCamera(map.ALTITUDE_RELATIVE_TO_GROUND);
		
		// set pitch
		camera.setTilt(pitch);

		// Update the view in Google Earth
		map.getView().setAbstractView(camera);
	},
	
	getHeading: function() {
		var map = this.maps[this.api];
		var heading = map.getHeading();
		return heading;
	},

	setHeading: function(heading) {
		var map = this.maps[this.api];
		map.setHeading(heading);
	},
	
	getFieldOfView: function() {
		var map = this.maps[this.api];
		var fieldOfView = map.getFieldOfView();
		return fieldOfView;
	},

	setFieldOfView: function(fieldOfView) {
		var map = this.maps[this.api];
		map.setFieldOfView(fieldOfView);
	},
	
	setStereoMode: function(stereo) {
		var map = this.maps[this.api];
		map.setStereoMode(stereo);
	},
    
	isStereoMode: function() {
		var map = this.maps[this.api];
		var stereo = map.isStereoMode();
		return stereo;
	},
	
	getAltitude: function() {
		var map = this.maps[this.api];
		var altitude = map.getAltitude();
		return altitude;
	},

	setAltitude: function(altitude) {
		var map = this.maps[this.api];
		map.setAltitude(altitude);
	},
	
	setCenterAndAltitude: function(point, altitude) {
		var map = this.maps[this.api];
		map.setCenterAndAltitude(point.lat, point.lon, altitude);
	}
	
}

});