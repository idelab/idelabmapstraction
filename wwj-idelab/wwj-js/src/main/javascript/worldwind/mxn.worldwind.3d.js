mxn.register('worldwind', {	

Mapstraction: {
	
	// 3D functionality
	
	getPitch: function() {
		var map = this.maps[this.api];
		var pitch = map.getPitch();
		return pitch;
	},

	setPitch: function(pitch) {
		var map = this.maps[this.api];
		map.setPitch(pitch);
	},
	
	getHeading: function() {
		var map = this.maps[this.api];
		var heading = map.getHeading();
		return heading;
	},

	setHeading: function(heading) {
		var map = this.maps[this.api];
		map.setHeading(heading);
	},
	
	getFieldOfView: function() {
		var map = this.maps[this.api];
		var fieldOfView = map.getFieldOfView();
		return fieldOfView;
	},

	setFieldOfView: function(fieldOfView) {
		var map = this.maps[this.api];
		map.setFieldOfView(fieldOfView);
	},
	
	getVerticalExaggeration: function() {
		var map = this.maps[this.api];
		var ve = map.getVerticalExaggeration();
		return ve;
	},

	setVerticalExaggeration: function(ve) {
		var map = this.maps[this.api];
		map.setVerticalExaggeration(ve);
	},	
	
	setStereoMode: function(stereo) {
		var map = this.maps[this.api];
		map.setStereoMode(stereo);
	},
    
	isStereoMode: function() {
		var map = this.maps[this.api];
		var stereo = map.isStereoMode();
		return stereo;
	},
	
	getAltitude: function() {
		var map = this.maps[this.api];
		var altitude = map.getAltitude();
		return altitude;
	},

	setAltitude: function(altitude) {
		var map = this.maps[this.api];
		map.setAltitude(altitude);
	},
	
	setCenterAndAltitude: function(point, altitude) {
		var map = this.maps[this.api];
		map.setCenterAndAltitude(point.lat, point.lon, altitude);
	},
	
	addSphere: function(sphere) {
		var map = this.maps[this.api];
		map.addSphere(sphere);
		
		return sphere;
	},

	removeSphere: function(sphere) {
		var map = this.maps[this.api];
		
		map.removeRenderable(sphere);
	},
	
	addCylinder: function(cylinder) {
		var map = this.maps[this.api];
		map.addCylinder(cylinder);
		
		return cylinder;
	},

	removeCylinder: function(cylinder) {
		var map = this.maps[this.api];
		map.removeRenderable(cylinder);
	},
	
	addPolyline3D: function(polyline3D) {
		var map = this.maps[this.api];
		map.addPolyline3D(polyline3D);
		
		return cylinder;
	},

	removePolyline3D: function(polyline3D) {
		var map = this.maps[this.api];
		map.removeRenderable(polyline3D);
	},
	
	addColladaModel: function(url, point, autoCenterAndZoom) {
		var map = this.maps[this.api];
		map.addColladaModel(url, point, autoCenterAndZoom);
	},
	
	removeColladaModel: function(url) {
		var map = this.maps[this.api];
		map.removeLayerByName(url);
	},
	
	addMovable3DModel: function(url, point, size, autoCenterAndZoom) {
		var map = this.maps[this.api];
		map.addMovable3DModel(url, point, size, autoCenterAndZoom);
	},
	
	removeMovable3DModel: function(url) {
		var map = this.maps[this.api];
		map.removeLayerByName(url);
	}
	
},

LatLonPoint3D: {
	
	toProprietary: function() {
		// TODO: Add provider code
	},

	fromProprietary: function(worldwindPoint) {
		
		var lat_lon_alt = worldwindPoint.split(this.COORDS_SEP);
		var lat = lat_lon_alt[0];
		var lon = lat_lon_alt[1];
		var alt = lat_lon_alt[2];
		return new mxn.LatLonPoint3D(lat, lon, alt);
	}
	
},

Sphere: {

	toProprietary: function() {
		
		return this;
	},
	
	show: function() {
		var map = this.mapstraction.maps[this.api];
		var hashcode = this.getAttribute('hashcode');
		map.showSphere(hashcode);
	},

	hide: function() {
		var map = this.mapstraction.maps[this.api];
		var hashcode = this.getAttribute('hashcode');
		map.hideSphere(hashcode);
	}
	
},

Cylinder: {

	toProprietary: function() {
		
		return this;
	},
	
	show: function() {
		var map = this.mapstraction.maps[this.api];
		var hashcode = this.getAttribute('hashcode');
		map.showCylinder(hashcode);
	},

	hide: function() {
		var map = this.mapstraction.maps[this.api];
		var hashcode = this.getAttribute('hashcode');
		map.hideCylinder(hashcode);
	}
	
},

Polyline3D: {

	toProprietary: function() {
		
		return this;
	},
	
	show: function() {
		var map = this.mapstraction.maps[this.api];
		var hashcode = this.getAttribute('hashcode');
		map.showPolyline3D(hashcode);
	},

	hide: function() {
		var map = this.mapstraction.maps[this.api];
		var hashcode = this.getAttribute('hashcode');
		map.hidePolyline3D(hashcode);
	}
	
}

});
