mxn.register('worldwind', {	

Mapstraction: {
	
	init: function(element, api) {
		
		this.MAP_PARAMS_SEP = ";;;"; // key1===value1;;;key2===value2;...
		this.MAP_KEY_VALUE_SEP = "===";
		this.COORDS_SEP = ","; // i.e. lat;lon   minx;miny;maxx;maxy
		this.ARRAY_SEP = ";";
		
		var me = this;
				
//		element.innerHTML = '<applet id="wwj-mxn" name="wwj-mxn" mayscript code="org.jdesktop.applet.util.JNLPAppletLauncher" width=100% height=100%"'+
//					  'archive="http://download.java.net/media/applet-launcher/applet-launcher.jar, ../download/worldwind.jar, ../download/wwj-mxn.jar, http://worldwind.arc.nasa.gov/java/jogl/webstart/jogl.jar, http://worldwind.arc.nasa.gov/java/jogl/webstart/gluegen-rt.jar">'+
//				   //'<param name="jnlp_href" value="http://mvn.idelab.uva.es/wwj-idelab/download/wwj-mxn.jnlp">'+
//				   '<param name="jnlp_href" value="http://localhost/wwj-idelab/target/site/download/wwj-mxn.jnlp">'+
//				   
//				   '<param name="codebase_lookup" value="false">'+
//				   '<param name="subapplet.classname" value="gov.nasa.worldwind.mxn.applet.IdelabMxnWorldwindApplet">'+
//				   '<param name="subapplet.displayname" value="WWJ Applet">'+
//				   '<param name="noddraw.check" value="true">'+
//				   '<param name="progressbar" value="true">'+
//				   '<param name="jnlpNumExtensions" value="1">'+
//
//				   '<param name="jnlpExtension1" value="http://worldwind.arc.nasa.gov/java/jogl/webstart/jogl.jnlp">'+
//
//				   '<param name="InitialLatitude" value="40">'+
//				   '<param name="InitialLongitude" value="-3">'+
//				   '<param name="InitialAltitude" value="5113000">'+
//				   '<param name="InitialHeading" value="0">'+
//				   '<param name="InitialPitch" value="0">'+
//				'/applet>'		   
				
		element.innerHTML = '<applet id="wwj-mxn" name="wwj-mxn" mayscript code="org.jdesktop.applet.util.JNLPAppletLauncher" width=100% height=100%"'+
		  		   'archive="http://download.java.net/media/applet-launcher/applet-launcher.jar, ../download/worldwind.jar, ../download/wwj-collada.jar, ../download/wwj-mxn.jar, http://worldwind.arc.nasa.gov/java/jogl/webstart/jogl.jar, http://worldwind.arc.nasa.gov/java/jogl/webstart/gluegen-rt.jar">'+		   
				   //'archive="http://download.java.net/media/applet-launcher/applet-launcher.jar, http://mvn.idelab.uva.es/wwj-idelab/download/worldwind.jar, http://mvn.idelab.uva.es/wwj-idelab/download/wwj-mxn.jar, http://download.java.net/media/jogl/builds/archive/jsr-231-webstart-current/jogl.jar, http://download.java.net/media/gluegen/webstart/gluegen-rt.jar">'+
				   //'<param name="jnlp_href" value="http://mvn.idelab.uva.es/wwj-idelab-pfc/download/wwj-mxn.jnlp">'+
				   '<param name="jnlp_href" value="http://localhost/wwj-idelab/target/site/download/wwj-mxn.jnlp">'+
				   '<param name="codebase_lookup" value="false">'+
				   '<param name="subapplet.classname" value="gov.nasa.worldwind.mxn.applet.IdelabMxnWorldwindApplet">'+
				   '<param name="subapplet.displayname" value="WWJ Applet">'+
				   '<param name="noddraw.check" value="true">'+
				   '<param name="progressbar" value="true">'+
				   '<param name="jnlpNumExtensions" value="1">'+

				   '<param name="jnlpExtension1" value="http://download.java.net/media/jogl/builds/archive/jsr-231-webstart-current/jogl.jnlp">'+

				   '<param name="InitialLatitude" value="40">'+
				   '<param name="InitialLongitude" value="-3">'+
				   '<param name="InitialAltitude" value="5113000">'+
				   '<param name="InitialHeading" value="0">'+
				   '<param name="InitialPitch" value="0">'+
				'/applet>'
				   
		
		var theApplet = document.getElementById('wwj-mxn'); 
		try {
		  theApplet = theApplet.getSubApplet();
		} catch (e) {
		  // Using new-style applet -- ignore
		} 
		this.maps[this.api] = theApplet;
		
		
		theApplet.addActionListener({
		    actionPerformed: function(evt, args) {
		    	
		    	if(evt.toLowerCase() == "click") {
			        var lat_lon = args.split(",");
					var lat = lat_lon[0];
					var lon = lat_lon[1];
			        me.click.fire({'location': new mxn.LatLonPoint(lat, lon)});
		    	}
		    	
		    	if(evt.toLowerCase() == "moveend") {
		    		me.moveendHandler(me);
					me.endPan.fire();
		    	}
		  
		    }
		});
		
		this.loaded[api] = true;
		me.load.fire();
	},
	
//	//TODO: To interactive script
//	addFeature: function(feature,data){
//		var map = this.maps[this.api];
//		var me = this; 
//		switch(feature){
//		case "point":
//			map.activateMarkerEdition();
//			break;
//
//	    case "polygon":
//	    case "linestring":
//	    	map.activateLineEdition();
//			break;
//	    }
//	},
	
	applyOptions: function(){
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},

	resizeTo: function(width, height){	

		var map = this.maps[this.api];

		this.currentElement.style.width = width;
		this.currentElement.style.height = height;
	},

	addControls: function( args ) {
		var map = this.maps[this.api];
		
		map.addControls(args);
	},

	addSmallControls: function() {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},

	addLargeControls: function() {
		var map = this.maps[this.api];
		map.addLargeControls();
	},

	addMapTypeControls: function() {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},

	dragging: function(on) {
		
		var map = this.maps[this.api];
		map.dragging(on);
	},

	setCenterAndZoom: function(point, zoom) { 
		var map = this.maps[this.api];
		var pt = point.toProprietary(this.api);
		
		map.setCenterAndZoom(point.lat, point.lon, zoom);
	},
	
	addMarker: function(marker, old) {
		var map = this.maps[this.api];
		var pin = marker.toProprietary(this.api);
		
		map.addMarker(marker);

		return pin;
	},

	removeMarker: function(marker) {
		var map = this.maps[this.api];
		map.removeMarker(marker);
	},

	removeAllMarkers: function() {
		var map = this.maps[this.api];
		map.removeAllMarkers();
	},
	
	declutterMarkers: function(opts) {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},

	addPolyline: function(polyline, old) {
		var map = this.maps[this.api];
		var pl = polyline.toProprietary(this.api);
		
		map.addPolyline(polyline);
		
		return polyline;
	},

	removePolyline: function(polyline) {
		var map = this.maps[this.api];
		
		map.removePolyline(polyline);
	},
	
	getCenter: function() {
		var point;
		var map = this.maps[this.api];
		
		var center = map.getCenter(new mxn.LatLonPoint());
		return center;
	},

	setCenter: function(point, options) {
		var map = this.maps[this.api];
		var pt = point.toProprietary(this.api);
		
		map.setCenter(point, options);
	},

	setZoom: function(zoom) {
		var map = this.maps[this.api];
		map.setZoom(zoom);
	},
	
	getZoom: function() {
		var map = this.maps[this.api];
		var zoom = map.getZoom();
		return zoom;
	},

	getZoomLevelForBoundingBox: function( bbox ) {
		var map = this.maps[this.api];
		// NE and SW points from the bounding box.
		var ne = bbox.getNorthEast();
		var sw = bbox.getSouthWest();
		var zoom;
		
		// TODO: Add provider code
		
		return zoom;
	},

	setMapType: function(type) {
		var map = this.maps[this.api];
		switch(type) {
			case mxn.Mapstraction.ROAD:
				// TODO: Add provider code
				break;
			case mxn.Mapstraction.SATELLITE:
				// TODO: Add provider code
				break;
			case mxn.Mapstraction.HYBRID:
				// TODO: Add provider code
				break;
			default:
				// TODO: Add provider code
		}	 
	},

	getMapType: function() {
		var map = this.maps[this.api];
		
		// TODO: Add provider code

		//return mxn.Mapstraction.ROAD;
		//return mxn.Mapstraction.SATELLITE;
		//return mxn.Mapstraction.HYBRID;

	},

	getBounds: function () {
		var map = this.maps[this.api];
		
		var bbox = new mxn.BoundingBox();
		bbox = map.getMapBounds(bbox);
		return bbox;
	},

	setBounds: function(bounds){
		var map = this.maps[this.api];
		
		var sw = bounds.getSouthWest();
		var ne = bounds.getNorthEast();
		
		map.setBounds(sw.lon, sw.lat, ne.lon, ne.lat);
	},

	addImageOverlay: function(id, src, opacity, west, south, east, north, oContext) {
		var map = this.maps[this.api];
		
		map.addImageOverlay(id, src, opacity, west, south, east, north);
	},

	setImagePosition: function(id, oContext) {
		var map = this.maps[this.api];
		var topLeftPoint; var bottomRightPoint;

		// TODO: Add provider code

		//oContext.pixels.top = ...;
		//oContext.pixels.left = ...;
		//oContext.pixels.bottom = ...;
		//oContext.pixels.right = ...;
	},
	
	addOverlay: function(url, autoCenterAndZoom) {
		var map = this.maps[this.api];
		map.addOverlay(url, false);	
	},
	
	removeOverlay: function(url){
		var map = this.maps[this.api];
		map.removeOverlay(url);
	},

	addTileLayer: function(tile_url, opacity, copyright_text, min_zoom, max_zoom) {
		var map = this.maps[this.api];
		
		map.addTileLayer(tile_url);
	},

	toggleTileLayer: function(tile_url) {
		var map = this.maps[this.api];
		
		map.toggleTileLayer(tile_url);
	},

	getPixelRatio: function() {
		var map = this.maps[this.api];

		// TODO: Add provider code	
	},
	
	mousePosition: function(element) {
		var map = this.maps[this.api];

		// TODO: Add provider code	
	}
	
},

LatLonPoint: {
	
	toProprietary: function() {
		// TODO: Add provider code
	},

	fromProprietary: function(worldwindPoint) {
		
		var lat_lon = worldwindPoint.split(this.COORDS_SEP);
		var lat = lat_lon[0];
		var lon = lat_lon[1];
		return new mxn.LatLonPoint(lat, lon);
	}
	
},

Marker: {
	
	toProprietary: function() {
		
		return this;
	},

	openBubble: function() {		
		var map = this.mapstraction.maps[this.api];
		map.openMarkerBubble(this);
	},

	hide: function() {
		var map = this.mapstraction.maps[this.api];
		map.hideMarker(this);
	},

	show: function() {
		var map = this.mapstraction.maps[this.api];
		map.showMarker(this);
	},

	update: function() {
		var map = this.mapstraction.maps[this.api];
		map.updateMarker(this);
	}
	
},

Polyline: {

	toProprietary: function() {
		
		return this;
	},
	
	show: function() {
		var map = this.mapstraction.maps[this.api];
		var hashcode = this.getAttribute('hashcode');
		map.showPolyline(hashcode);
	},

	hide: function() {
		var map = this.mapstraction.maps[this.api];
		var hashcode = this.getAttribute('hashcode');
		map.hidePolyline(hashcode);
	}
	
}

});
