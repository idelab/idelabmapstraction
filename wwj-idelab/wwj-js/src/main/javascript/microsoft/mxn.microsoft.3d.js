mxn.register('microsoft3D', {	

Mapstraction: {
	
	// 3D functionality
	
	// see API reference: http://msdn.microsoft.com/en-us/library/bb412424.aspx
	
	getPitch: function() {
		var map = this.maps[this.api];
		var pitch = map.GetPitch();
		return pitch;
	},

	setPitch: function(pitch) {
		var map = this.maps[this.api];
		map.SetPitch(pitch);
	},
	
	getHeading: function() {
		var map = this.maps[this.api];
		var heading = map.GetHeading();
		return heading;
	},

	setHeading: function(heading) {
		var map = this.maps[this.api];
		map.SetHeading(heading);
	},
	
	getFieldOfView: function() {
		var map = this.maps[this.api];
		var fieldOfView = map.getFieldOfView();
		return fieldOfView;
	},

	setFieldOfView: function(fieldOfView) {
		var map = this.maps[this.api];
		map.setFieldOfView(fieldOfView);
	},
	
	setStereoMode: function(stereo) {
		var map = this.maps[this.api];
		map.setStereoMode(stereo);
	},
    
	isStereoMode: function() {
		var map = this.maps[this.api];
		var stereo = map.isStereoMode();
		return stereo;
	},
	
	getAltitude: function() {
		var map = this.maps[this.api];
		var altitude = map.GetAltitude();
		return altitude;
	},

	setAltitude: function(altitude) {
		var map = this.maps[this.api];
		map.SetAltitude(altitude);
	},
	
	setCenterAndAltitude: function(point, altitude) {
		var map = this.maps[this.api];
		map.setCenterAndAltitude(point.lat, point.lon, altitude);
	}
	
}

});