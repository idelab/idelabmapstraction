package gov.nasa.worldwind.mxn.wfs;

import java.util.List;

import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.Renderable;

public class WFSLayer extends RenderableLayer {

	public WFSLayer(String capsUrl, String featureName) {
		
		this.setName(capsUrl.toString()); // use url as default layer name
		
		List<Renderable> shapes = WFSParser.parseShapes(capsUrl, featureName);
		if (shapes != null) {
        	for (Renderable r : shapes)  {
                this.addRenderable(r);
        	}
        }
	}
}
