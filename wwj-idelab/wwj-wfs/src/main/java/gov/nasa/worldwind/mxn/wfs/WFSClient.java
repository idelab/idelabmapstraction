package gov.nasa.worldwind.mxn.wfs;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.geotools.data.DataStore;
import org.geotools.data.DataStoreFinder;
import org.geotools.data.FeatureSource;
import org.geotools.feature.FeatureCollection;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import com.vividsolutions.jts.geom.Geometry;

public class WFSClient {
	
	DataStore data = null;
	
	public WFSClient(String capsURL) throws IOException {
	
		if(!capsURL.contains("?")) {
			capsURL += "?";
		}
		if(!capsURL.toUpperCase().contains("SERVICE")) {
			capsURL += "service=WFS";
		}
		if(!capsURL.toUpperCase().contains("REQUEST")) {
			capsURL += "&request=GetCapabilities";
		}
		Map connectionParameters = new HashMap();
		connectionParameters.put("WFSDataStoreFactory:TIMEOUT", -1);
		connectionParameters.put("WFSDataStoreFactory:GET_CAPABILITIES_URL", capsURL );

		data = DataStoreFinder.getDataStore( connectionParameters );
	}
	
	public FeatureCollection<SimpleFeatureType, SimpleFeature> getFeatures(String featureName) throws IOException {
		
		FeatureSource<SimpleFeatureType, SimpleFeature> featureSource = data.getFeatureSource(featureName);
		FeatureCollection<SimpleFeatureType, SimpleFeature> features = featureSource.getFeatures();
		return features;
	}
	/**
	 * @param args
	 * @throws IOException 
	 */
	public static void main(String[] args) throws IOException {
		
		String getCapabilities = "http://geoserver.idelab.uva.es/geoserver/ows?service=WFS&request=GetCapabilities";
		
		String featureName = "topp:tasmania_roads";
		
		WFSClient wfsClient = new WFSClient(getCapabilities);		

		
		FeatureCollection<SimpleFeatureType, SimpleFeature> features = wfsClient.getFeatures(featureName);

		System.out.println(featureName.length()+" features returned");
		
		Iterator<SimpleFeature> iterator = features.iterator();
		try {
		    while( iterator.hasNext() ){
		    	SimpleFeature feature = (SimpleFeature) iterator.next();
		        Geometry defaultGeometry = (Geometry) feature.getDefaultGeometry();
		        System.out.println("> "+defaultGeometry.getGeometryType());
		    }
		}
		finally {
		    features.close( iterator );
		}

	}

}
