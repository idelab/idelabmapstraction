package gov.nasa.worldwind.mxn.wfs;

import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.Polygon;
import gov.nasa.worldwind.render.Polyline;
import gov.nasa.worldwind.render.Renderable;
import gov.nasa.worldwind.render.ShapeAttributes;
import gov.nasa.worldwind.util.Logging;

import java.awt.Color;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;

import org.geotools.feature.FeatureCollection;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;

import com.vividsolutions.jts.algorithm.ConvexHull;
import com.vividsolutions.jts.geom.Coordinate;
import com.vividsolutions.jts.geom.Geometry;
import com.vividsolutions.jts.geom.MultiPolygon;
import com.vividsolutions.jts.simplify.DouglasPeuckerLineSimplifier;

public class WFSParser {
	
	public static List<Renderable> parseShapes(String capsUrl, String featureName) {
		
		WFSClient wfsClient = null;
		try {
			wfsClient = new WFSClient(capsUrl);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		FeatureCollection<SimpleFeatureType, SimpleFeature> features = null;
		try {
			features = wfsClient.getFeatures(featureName);
		} catch (IOException e) {
			e.printStackTrace();
			return null;
		}
		
		return parseShapes(features);
	}
	
	public static List<Renderable> parseShapes(FeatureCollection<SimpleFeatureType, SimpleFeature> features) {
		
		List<Renderable> shapes = new ArrayList<Renderable>();
		
		if(features!=null) {
			
			Iterator<SimpleFeature> iterator = features.iterator();
			try {
			    while( iterator.hasNext() ){
			    	
			    	Renderable shape = null;
			    	
			    	SimpleFeature feature = (SimpleFeature) iterator.next();
			        Geometry geom = (Geometry) feature.getDefaultGeometry();
			        
			        // POINT
			        if(geom.getLength()==0) {
			        	shape = makePointShape(geom);
			        	shapes.add(shape);
			        }
			        
			        // LINE
			        else if(geom.getArea()==0) {
			        	shape = makeLineShape(geom, false);
			        	shapes.add(shape);
			        }
			        
			        // POLYGON
			        else {
			        	if(geom.getGeometryType().equalsIgnoreCase("MULTIPOLYGON")) {
				        	MultiPolygon multiPolygon = (MultiPolygon) geom;
				        	int numGeometries = multiPolygon.getNumGeometries();
				        	for (int i = 0; i < numGeometries; i++) {
								Geometry poly = multiPolygon.getGeometryN(i);
								shape = makePolygonShape(poly);
								shapes.add(shape);
							}
				        	ConvexHull convexHull = new ConvexHull(multiPolygon);
				        	shape = makePolygonShape(geom);
//				        	shape = makePolygonShape(convexHull.getConvexHull());
//				        	shape = makeLineShape(convexHull.getConvexHull(), false);
			        	} else {
			        		shape = makeLineShape(geom, true);
			        		shapes.add(shape);
			        	}
			        }
			        
			        
			    }
			}
			finally {
			    features.close( iterator );
			}
			
		}
		
		return shapes;
	}
	
	private static Renderable makePolygonShape(Geometry geom) {
		
		// Create and set an attribute bundle.
        ShapeAttributes normalAttributes = new BasicShapeAttributes();
        normalAttributes.setInteriorMaterial(Material.YELLOW);
        normalAttributes.setOutlineOpacity(0.5);
        normalAttributes.setInteriorOpacity(0.8);
        normalAttributes.setOutlineMaterial(Material.GREEN);
        normalAttributes.setOutlineWidth(5);
        normalAttributes.setDrawOutline(true);
        normalAttributes.setDrawInterior(true);
        normalAttributes.setEnableLighting(true);

        ShapeAttributes highlightAttributes = new BasicShapeAttributes(normalAttributes);
        highlightAttributes.setOutlineMaterial(Material.WHITE);
        highlightAttributes.setOutlineOpacity(1);
        
		Polygon polygon = null;    
		
        ArrayList<Position> positions = new ArrayList<Position>();
        
        Coordinate[] coords = geom.getCoordinates();
        
        for (int i = 0; i < coords.length; i++) {
			Coordinate c = coords[i];
			LatLon latlon = LatLon.fromDegrees(c.x, c.y);
			double elevation = 0d;
			if(!Double.isNaN(c.z))
				elevation = c.z;
			positions.add(new Position(latlon, 4e4));
			
        }    
        
        polygon = new Polygon(positions);
        
        //polygon.addInnerBoundary(positions);
        polygon.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
        polygon.setAttributes(normalAttributes);
        polygon.setHighlightAttributes(highlightAttributes);
		
		return polygon;
	}

	private static Renderable makeLineShape(Geometry geom, boolean closed) {

		Polyline polyline = null;
		
        ArrayList<LatLon> positions = new ArrayList<LatLon>();
        
        Coordinate[] coords = geom.getCoordinates();
        
        Coordinate[] coords_simpl = DouglasPeuckerLineSimplifier.simplify(coords, 0.1);
        
        double elevation = 0d;
        
        for (int i = 0; i < coords_simpl.length; i++) {
			Coordinate c = coords_simpl[i];
			positions.add(LatLon.fromDegrees(c.x, c.y));
			if(!Double.isNaN(c.z))
				elevation += c.z;
		}	

        elevation = elevation/coords_simpl.length;
        
        if (elevation != 0)
        {
            polyline = new Polyline(positions, elevation);
            polyline.setPathType(Polyline.GREAT_CIRCLE);
        }
        else
        {
        	polyline = new Polyline(positions, 0);
        }
        
        polyline.setFollowTerrain(true);
        polyline.setLineWidth(5);
        polyline.setColor(Color.BLUE);
        
        
        
		return polyline;
	}

	private static Renderable makePointShape(Geometry geom) {
		// TODO Auto-generated method stub
		return null;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
