/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class KMLMultiGeometry extends KMLGeometry {
    
    private ArrayList<KMLGeometry> geometries;
    
    public void addGeometry(KMLGeometry geometry) {
        this.geometries.add(geometry);
    }
    
    public List<KMLGeometry> getGeometries(){
        return geometries;
    }

    @Override
    protected String toKML() {
        throw new UnsupportedOperationException("Not supported yet.");
    }

}
