/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

import java.awt.Color;

/**
 *
 */
public class KMLListStyle extends KMLObject {

    protected enum KMLListItemType {
        CHECK, CHECK_OFF_ONLY, CHECK_HIDE_CHILDREN, RADIO_FOLDER
    }
    
    protected enum KMLItemIconMode {
        OPEN, CLOSED, ERROR, FETCHING0, FETCHING1, FETCHING2
    }
    
    protected Color bgColor;
    
    protected KMLListItemType listItemType = KMLListItemType.CHECK;
    
    protected KMLItemIconMode itemIconState = KMLItemIconMode.OPEN;
    
    protected String itemIconHref;

    public Color getBgColor() {
        return bgColor;
    }

    public void setBgColor(Color bgColor) {
        this.bgColor = bgColor;
    }

    public KMLListItemType getListItemType() {
        return listItemType;
    }

    public void setListItemType(KMLListItemType listItemType) {
        this.listItemType = listItemType;
    }

    public KMLItemIconMode getItemIconState() {
        return itemIconState;
    }

    public void setItemIconState(KMLItemIconMode itemIconState) {
        this.itemIconState = itemIconState;
    }

    public String getItemIconHref() {
        return itemIconHref;
    }

    public void setItemIconHref(String itemIconHref) {
        this.itemIconHref = itemIconHref;
    }
    
}
