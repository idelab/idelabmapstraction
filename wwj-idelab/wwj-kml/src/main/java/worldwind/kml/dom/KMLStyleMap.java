/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package worldwind.kml.dom;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 *
 */
public class KMLStyleMap extends KMLStyleSelector {

    protected ArrayList<HashMap<String, String>> pairs;
    
    public KMLStyleMap() {
        this.pairs = new ArrayList<HashMap<String, String>>();
    }
    
    public void addPair(HashMap<String, String> pair) {
        this.pairs.add(pair);
    }
    
    public List<HashMap<String, String>> getPairs() {
        return pairs;
    }

}
