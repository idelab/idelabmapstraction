/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class KMLDocument extends KMLContainer {
    
    protected ArrayList<KMLSchema> schemas;
    
    public KMLDocument(){
    }
    
    public void addSchema(KMLSchema schema) {
        this.schemas.add(schema);
    }
    
    public List<KMLSchema> getSchemas(){
        return schemas;
    }

}
