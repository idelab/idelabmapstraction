/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

/**
 *
 */
public class KMLStyle extends KMLStyleSelector {

    protected KMLIconStyle iconStyle;
    
    protected KMLLabelStyle labelStyle;
    
    protected KMLLineStyle lineStyle;
    
    protected KMLPolyStyle polyStyle;
    
    protected KMLBalloonStyle balloonStyle;
    
    protected KMLListStyle listStyle;

    public KMLIconStyle getIconStyle() {
        return iconStyle;
    }

    public void setIconStyle(KMLIconStyle iconStyle) {
        this.iconStyle = iconStyle;
    }

    public KMLLabelStyle getLabelStyle() {
        return labelStyle;
    }

    public void setLabelStyle(KMLLabelStyle labelStyle) {
        this.labelStyle = labelStyle;
    }

    public KMLLineStyle getLineStyle() {
        return lineStyle;
    }

    public void setLineStyle(KMLLineStyle lineStyle) {
        this.lineStyle = lineStyle;
    }

    public KMLPolyStyle getPolyStyle() {
        return polyStyle;
    }

    public void setPolyStyle(KMLPolyStyle polyStyle) {
        this.polyStyle = polyStyle;
    }

    public KMLBalloonStyle getBalloonStyle() {
        return balloonStyle;
    }

    public void setBalloonStyle(KMLBalloonStyle balloonStyle) {
        this.balloonStyle = balloonStyle;
    }

    public KMLListStyle getListStyle() {
        return listStyle;
    }

    public void setListStyle(KMLListStyle listStyle) {
        this.listStyle = listStyle;
    }
    
}
