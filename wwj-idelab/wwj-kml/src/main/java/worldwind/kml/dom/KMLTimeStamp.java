/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

/**
 *
 */
public class KMLTimeStamp extends KMLTimePrimitive{
    
    protected String when;
    
    public void setWhen(String when) {
        this.when = when;
    }
    
    public String getWhen() {
        return when;
    }

}
