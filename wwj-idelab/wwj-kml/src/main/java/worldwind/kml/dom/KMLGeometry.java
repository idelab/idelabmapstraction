/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

/**
 *
 */
public abstract class KMLGeometry extends KMLObject {
    
    protected enum KMLAltitudeMode {
        CLAMP_TO_GROUND, RELATIVE_TO_GROUND, ABSOLUTE
    }

    protected abstract String toKML();

}
