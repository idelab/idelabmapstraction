/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

import java.awt.Color;

/**
 *
 */
public class KMLBalloonStyle extends KMLObject {
    
    public static final int DISPLAY_MODE_DEFAULT = 1;
    
    public static final int DISPLAY_MODE_HIDE = 2;

    protected Color bgColor;
    
    protected Color textColor;
    
    protected String text;
    
    protected int displayMode = KMLBalloonStyle.DISPLAY_MODE_DEFAULT;

    public Color getBgColor() {
        return bgColor;
    }

    public void setBgColor(Color bgColor) {
        this.bgColor = bgColor;
    }

    public Color getTextColor() {
        return textColor;
    }

    public void setTextColor(Color textColor) {
        this.textColor = textColor;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public int getDisplayMode() {
        return displayMode;
    }

    public void setDisplayMode(int dieplayMode) {
        this.displayMode = dieplayMode;
    }
    
}
