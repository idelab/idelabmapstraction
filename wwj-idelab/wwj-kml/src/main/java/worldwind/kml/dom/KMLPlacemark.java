/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

/**
 *
 */
public class KMLPlacemark extends KMLFeature {
    
    protected KMLGeometry geometry;

    public KMLGeometry getGeometry() {
        return geometry;
    }

    public void setGeometry(KMLGeometry geometry) {
        this.geometry = geometry;
    }
    
    public String toKML() {
        String s = new String();
        s += "<Placemark>\n";
        s += "<name>" + getName() + "</name>\n";
        s += "<description>" + getDescription() + "</description>\n";
        s += geometry.toKML();
        s += "</Placemark>\n";
        return s;
    }

}
