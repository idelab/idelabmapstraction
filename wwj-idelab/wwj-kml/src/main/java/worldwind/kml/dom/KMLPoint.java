/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

/**
 *
 */
public class KMLPoint extends KMLGeometry{
    
    protected boolean extrude;
    
    protected KMLAltitudeMode altitudeMode = KMLAltitudeMode.CLAMP_TO_GROUND;
    
    protected String coordinates;

    public boolean isExtrude() {
        return extrude;
    }

    public void setExtrude(boolean extrude) {
        this.extrude = extrude;
    }

    public KMLAltitudeMode getAltitudeMode() {
        return altitudeMode;
    }

    public void setAltitudeMode(KMLAltitudeMode altitudeMode) {
        this.altitudeMode = altitudeMode;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    protected String toKML() {
        String s = new String();
        s += "<Point>\n";
        s += "<coordinates>" + coordinates + "</coordinates>\n";
        s += "</Point>\n";
        return s;
    }
    

}
