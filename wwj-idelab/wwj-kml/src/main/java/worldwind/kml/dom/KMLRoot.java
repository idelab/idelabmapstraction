/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package worldwind.kml.dom;

import java.util.List;

/**
 * This class represents the kml root element and has one kml features.
 */
public class KMLRoot {

    protected KMLFeature rootFeature;

    public KMLRoot() {}

    public void setRootFeature(KMLFeature feature) {
        this.rootFeature = feature;
    }

    public KMLFeature getFeatures() {
        return rootFeature;
    }

    void saveXML() {
        java.util.ArrayList<KMLFeature> f = new java.util.ArrayList<KMLFeature>();
        f.add(rootFeature);
        printFeatureList(f);
    }

    private void printFeatureList(List<KMLFeature> features) {
//        System.out.println("Size: " + features.size());
        for (KMLFeature f : features) {
            if (f instanceof KMLFolder) {
                KMLFolder folder = (KMLFolder) f;
                System.out.println("<Folder>");
                System.out.println("<name>" + folder.getName() + "</name>");
                printFeatureList(folder.getFeatures());
                System.out.println("</Folder>");
            }
            if (f instanceof KMLPlacemark) {
                System.out.print(((KMLPlacemark)f).toKML());
            }
            if (f instanceof KMLDocument) {
                KMLDocument d = (KMLDocument) f;
                System.out.println("<Document>");
                System.out.println("<name>" + d.getName() + "</name>");
                System.out.println("<visibility>" + d.isVisible() + "</visibility>");
                System.out.println("<open>" + d.isOpen() + "</open>");
                printFeatureList(d.getFeatures());
                System.out.println("</Document>");
            }
        }
    }

}
