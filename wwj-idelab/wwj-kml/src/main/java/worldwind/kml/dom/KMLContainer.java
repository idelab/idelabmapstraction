/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public abstract class KMLContainer extends KMLFeature{
    
    protected ArrayList<KMLFeature> features;
    
    public KMLContainer(){
        this.features = new ArrayList<KMLFeature>();
    }
    
    public void addFeature(KMLFeature feature) {
        this.features.add(feature);
    }
    
    public List<KMLFeature> getFeatures(){
        return features;
    }

}
