/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

/**
 *
 */
public class KMLLineString extends KMLGeometry {
    
    protected boolean extrude = false;
    
    protected boolean tessellate = false;
    
    protected KMLAltitudeMode altitudeMode = KMLAltitudeMode.CLAMP_TO_GROUND;
    
    protected String coordinates;

    public boolean isExtrude() {
        return extrude;
    }

    public void setExtrude(boolean extrude) {
        this.extrude = extrude;
    }

    public boolean isTessellate() {
        return tessellate;
    }

    public void setTessellate(boolean tessellate) {
        this.tessellate = tessellate;
    }

    public KMLAltitudeMode getAltitudeMode() {
        return altitudeMode;
    }

    public void setAltitudeMode(KMLAltitudeMode altitudeMode) {
        this.altitudeMode = altitudeMode;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(String coordinates) {
        this.coordinates = coordinates;
    }

    @Override
    protected String toKML() {
        String s = new String();
        s += "<LineString>\n";
        s += "<coordinates>" + coordinates + "</coordinates>\n";
        s += "</LineString>\n";
        return s;
    }

}
