/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

import java.awt.Color;

/**
 *
 */
public abstract class KMLColorStyle extends KMLObject {
    
    protected enum KMLColorMode {
        NORMAL, RANDOM
    }
    
    protected Color color = Color.WHITE;
    
    protected KMLColorMode colorMode = KMLColorMode.NORMAL;

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }

    public KMLColorMode getColorMode() {
        return colorMode;
    }

    public void setColorMode(KMLColorMode colorMode) {
        this.colorMode = colorMode;
    }

}
