/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

/**
 * The <hotSpot> element is not yet implemented!
 */
public class KMLIconStyle extends KMLColorStyle {
    
    protected float scale = 1;
    
    protected float heading = 0;
    
    protected String iconHref;

    public float getScale() {
        return scale;
    }

    public void setScale(float scale) {
        this.scale = scale;
    }

    public float getHeading() {
        return heading;
    }

    public void setHeading(float heading) {
        this.heading = heading;
    }

    public String getIconHref() {
        return iconHref;
    }

    public void setIconHref(String iconHref) {
        this.iconHref = iconHref;
    }
    

}
