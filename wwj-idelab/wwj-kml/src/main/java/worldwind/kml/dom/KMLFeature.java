/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public abstract class KMLFeature extends KMLObject {
    
    protected String name;
    
    protected boolean visibility;
    
    protected boolean open;
    
    protected String address;
    
    protected String phoneNumber;
    
    protected String snippet;
    
    protected String description;
    
    protected KMLAbstractView abstractView;
    
    protected KMLTimePrimitive timePrimitive;
    
    protected String styleUrl;
    
    /**
     * One or more Styles and StyleMaps can be defined to customize the appearance
     * of any element derived from Feature or of the Geometry in a Placemark.
     * (See <BalloonStyle>, <ListStyle>, <StyleSelector>, and the styles derived
     * from <ColorStyle>.) A style defined within a Feature is called an "inline style"
     * and applies only to the Feature that contains it. A style defined as the
     * child of a <Document> is called a "shared style." A shared style must have
     * an id defined for it. This id is referenced by one or more Features within
     * the <Document>. In cases where a style element is defined both in a shared
     * style and in an inline style for a Feature—that is, a Folder, GroundOverlay,
     * NetworkLink, Placemark, or ScreenOverlay—the value for the Feature's inline
     * style takes precedence over the value for the shared style.
     */
    protected ArrayList<KMLStyleSelector> styleSelectors;
    
    protected KMLRegion region;
    
    protected KMLExtendedData extendedData;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isVisible() {
        return visibility;
    }

    public void setVisibile(boolean visibility) {
        this.visibility = visibility;
    }

    public boolean isOpen() {
        return open;
    }

    public void setOpen(boolean open) {
        this.open = open;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getSnippet() {
        return snippet;
    }

    public void setSnippet(String snippet) {
        this.snippet = snippet;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public KMLAbstractView getAbstractView() {
        return abstractView;
    }

    public void setAbstractView(KMLAbstractView abstractView) {
        this.abstractView = abstractView;
    }

    public KMLTimePrimitive getTimePrimitive() {
        return timePrimitive;
    }

    public void setTimePrimitive(KMLTimePrimitive timePrimitive) {
        this.timePrimitive = timePrimitive;
    }

    public String getStyleUrl() {
        return styleUrl;
    }

    public void setStyleUrl(String styleUrl) {
        this.styleUrl = styleUrl;
    }

    public List<KMLStyleSelector> getStyleSelectors() {
        return styleSelectors;
    }

    public void addStyleSelector(KMLStyleSelector styleSelector) {
        this.styleSelectors.add(styleSelector);
    }

    public KMLRegion getRegion() {
        return region;
    }

    public void setRegion(KMLRegion region) {
        this.region = region;
    }

    public KMLExtendedData getExtendedData() {
        return extendedData;
    }

    public void setExtendedData(KMLExtendedData extendedData) {
        this.extendedData = extendedData;
    }

}
