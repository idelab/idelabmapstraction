/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package worldwind.kml.dom;

/**
 *
 */
public class KMLPolyStyle extends KMLColorStyle{
    
    protected boolean fill = true;
    
    protected boolean outline = true;

    public boolean isFill() {
        return fill;
    }

    public void setFill(boolean fill) {
        this.fill = fill;
    }

    public boolean isOutline() {
        return outline;
    }

    public void setOutline(boolean outline) {
        this.outline = outline;
    }

}
