/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package worldwind.kml.dom;

import java.util.ArrayList;
import java.util.List;

/**
 *
 */
public class KMLPolygon extends KMLGeometry {

    protected boolean extrude = false;
    protected boolean tessellate = false;
    protected KMLAltitudeMode altitudeMode = KMLAltitudeMode.CLAMP_TO_GROUND;
    protected KMLLinearRing outerBoundaryIs;
    protected ArrayList<KMLLinearRing> innnerBoundaryIs;

    public boolean isExtrude() {
        return extrude;
    }

    public void setExtrude(boolean extrude) {
        this.extrude = extrude;
    }

    public boolean isTessellate() {
        return tessellate;
    }

    public void setTessellate(boolean tessellate) {
        this.tessellate = tessellate;
    }

    public KMLAltitudeMode getAltitudeMode() {
        return altitudeMode;
    }

    public void setAltitudeMode(KMLAltitudeMode altitudeMode) {
        this.altitudeMode = KMLAltitudeMode.CLAMP_TO_GROUND;
    }

    public KMLLinearRing getOuterBoundaryIs() {
        return outerBoundaryIs;
    }

    public void setOuterBoundaryIs(KMLLinearRing outerBoundaryIs) {
        this.outerBoundaryIs = outerBoundaryIs;
    }

    public List<KMLLinearRing> getInnerBoundaryIs() {
        return innnerBoundaryIs;
    }

    public void addInnerBoundaryIs(KMLLinearRing innerBoundaryIs) {
        this.innnerBoundaryIs.add(innerBoundaryIs);
    }

    @Override
    protected String toKML() {
        String s = new String();
        s += "<Polygon>\n";
        s += "</Polygon>\n";
        return s;
    }
}
