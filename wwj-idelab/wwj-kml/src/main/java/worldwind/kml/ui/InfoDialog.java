package worldwind.kml.ui;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * A very simple popup to display HTML description elements for placemarks.
 *
 * Unfortunately, the built-in Java HTML viewer is pretty crappy.
 *
 */
public class InfoDialog extends JDialog {

    String htmlText = "<html><BODY>Hello World</BODY></html>";
    JEditorPane htmlView;

    public InfoDialog(Frame owner, String htmlText) throws HeadlessException {
        super(owner, true);

        this.setUndecorated(true);

        this.htmlText = htmlText;
        
        setLayout(new BorderLayout(10,10));


        htmlView = new JEditorPane("text/html", "");
        htmlView.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));
        htmlView.setEditable(false);
        //HTMLDocument doc = new HTMLDocument();


//        HTMLDocument.HTMLReader reader = new HTMLDocument.HTMLReader(0);
//        reader.
        

        //htmlView.setDocument(new HTMLDocument());
        htmlView.setText(htmlText);

        add(new JScrollPane(htmlView), BorderLayout.CENTER);


        Dimension size = htmlView.getPreferredSize();
        if (size.width < 400 && size.height < 400) {
            pack();
        } else {
            setSize(400, 400);
        }

        int width = this.getWidth();
        int height = this.getHeight();

        Rectangle rect = owner.getBounds();

        setLocation(rect.x + (rect.width - width)/2, rect.y + (rect.height - height)/2);

        htmlView.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                setVisible(false);
            }
        });

        setVisible(true);
    }



    public String getHtmlText() {
        return htmlText;
    }

    public void setHtmlText(String htmlText) {
        this.htmlText = htmlText;
    }
}
