package worldwind.kml;

/**
 * This interface defines KML files nodes names.
 */
public interface IKMLFormat {
	public static final String ALTITUDEMODE_NODE = "altitudeMode";
	public static final String COORDINATES_NODE = "coordinates";
	public static final String DESCRIPTION_NODE = "description";
	public static final String EXTRUDE_NODE = "extrude";
	public static final String FOLDER_NODE = "Folder";
	public static final String HREF_NODE = "href";
	public static final String LINESTRING_NODE = "LineString";
	public static final String LINK_NODE = "Link";
	public static final String LOCATION_NODE = "Location";
	/**
	 * Node for 3D external model definition.
	 */
	public static final String MODEL_NODE = "Model";
	public static final String MULTIGEOMETRY_NODE = "MultiGeometry";
	public static final String NAME_NODE = "name";
	public static final String PLACEMARK_NODE = "Placemark";
	public static final String POINT_NODE = "Point";
	public static final String POLYGON_NODE = "Polygon";
	public static final String POLYSTYLE_NODE = "PolyStyle";
	public static final String STYLE_NODE = "Style";
	public static final String STYLEURL_NODE = "styleUrl";
	public static final String TESSELLATE_NODE = "tessellate";
	public static final String VISIBILITY_NODE = "visibility";

}