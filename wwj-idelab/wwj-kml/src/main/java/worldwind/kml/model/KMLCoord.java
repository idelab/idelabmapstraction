package worldwind.kml.model;

import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;

/**
 * Created by IntelliJ IDEA. User: tgleason Date: Sep 1, 2008 Time: 9:05:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class KMLCoord {
	double lat;
	double lon;
	double height;

	public KMLCoord(double lon, double lat, double height) {
		this.lat = lat;
		this.lon = lon;
		this.height = height;
	}

	public double getLat() {
		return lat;
	}

	public void setLat(double lat) {
		this.lat = lat;
	}

	public double getLon() {
		return lon;
	}

	public void setLon(double lon) {
		this.lon = lon;
	}

	public double getHeight() {
		return height;
	}

	public void setHeight(double height) {
		this.height = height;
	}

	public double dist(KMLCoord kmlCoord) {
		return Math.sqrt((kmlCoord.lat - lat) * (kmlCoord.lat - lat)
				+ (kmlCoord.lon - lon) * (kmlCoord.lon - lon));
	}

	public Position toPosition() {
		return new Position(LatLon.fromDegrees(lat, lon), height);
	}
}
