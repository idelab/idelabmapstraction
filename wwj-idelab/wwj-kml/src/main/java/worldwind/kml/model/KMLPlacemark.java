package worldwind.kml.model;

/**
 * Created by IntelliJ IDEA. User: tgleason Date: Sep 1, 2008 Time: 8:29:52 PM
 * To change this template use File | Settings | File Templates.
 */
public class KMLPlacemark extends KMLObject {
	private KMLGraphic graphic;
	private String name;
	private String description;
	private KMLStyle style;
	private boolean visible = true;

	public KMLGraphic getGraphic() {
		return graphic;
	}

	public void setGraphic(KMLGraphic graphic) {
		this.graphic = graphic;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public KMLStyle getStyle() {
		return style;
	}

	public void setStyle(KMLStyle style) {
		this.style = style;
	}

	public boolean isVisible() {
		return visible;
	}

	public void setVisible(boolean visible) {
		this.visible = visible;
	}
}
