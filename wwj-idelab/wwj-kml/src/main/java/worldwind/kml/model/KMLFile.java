package worldwind.kml.model;

import gov.nasa.worldwind.geom.Sector;

import java.util.*;



/**
 * Created by IntelliJ IDEA.
 * User: tgleason
 * Date: Sep 1, 2008
 * Time: 9:04:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class KMLFile {
    KMLFolder rootFolder = new KMLFolder();
    Map<String, KMLStyle> styles = new HashMap<String, KMLStyle>();
    Map<String, Set<String>> schemaAliases = new HashMap<String, Set<String>>();
    Sector sector = null;

    public KMLFolder getRootFolder() {
        return rootFolder;
    }

    public void setRootFolder(KMLFolder rootFolder) {
        this.rootFolder = rootFolder;
    }

    public void addStyle (String name, KMLStyle style) {
        styles.put(name, style);
    }

    public KMLStyle getStyle (String name) {
        return styles.get(name);
    }

    public Sector getSector () {
        if (sector == null) {
            List<KMLCoord> coords = new ArrayList<KMLCoord>();

            collectCoords (rootFolder, coords);

            double minLon = 180;
            double minLat = 90;
            double maxLon = -180;
            double maxLat = -90;

            Iterator<KMLCoord> coordIter = coords.iterator();
            while (coordIter.hasNext()) {
                KMLCoord kmlCoord = coordIter.next();
                double lon = kmlCoord.getLon();
                double lat = kmlCoord.getLat();
                if (lon < minLon)
                    minLon = lon;
                if (lon > maxLon)
                    maxLon = lon;
                if (lat < minLat)
                    minLat = lat;
                if (lat > maxLat)
                    maxLat = lat;
            }

            sector = Sector.fromDegrees(minLat, maxLat, minLon, maxLon);
        }
        //System.out.println("KMLFIle: " + sector);
        return sector;
    }

    private void collectCoords(KMLFolder folder, List<KMLCoord> coords) {
        Iterator<KMLObject> objs = folder.getObjects().iterator();
        while (objs.hasNext()) {
            KMLObject kmlObject = objs.next();
            if (kmlObject instanceof KMLPlacemark) {
                KMLGraphic g = ((KMLPlacemark)kmlObject).getGraphic();
                if (g instanceof KMLPoint) {
                    coords.add(((KMLPoint)g).getCoord());
                } else if (g instanceof KMLLineString) {
                    coords.addAll(((KMLLineString)g).getCoords());
                } else if (g instanceof KMLPolygon) {
                    coords.addAll(((KMLPolygon)g).getOuter());
                } else if (g instanceof KMLMultiGeometry) {
                    coords.addAll(((KMLMultiGeometry)g).getCoords());
                }
            }
        }

        Iterator<KMLFolder> folders = folder.getChildFolders().iterator();
        while (folders.hasNext()) {
            KMLFolder kmlFolder = folders.next();
            collectCoords(kmlFolder, coords);
        }
    }

    public void addAlias (String parent, String name) {
        Set<String> aliases = schemaAliases.get(parent);
        if (aliases == null) {
            aliases = new HashSet<String>();
            schemaAliases.put(parent, aliases);
        }
        aliases.add(name);
    }

    public Set<String> getAliasesFor (String parent) {
        Set<String> aliases = schemaAliases.get(parent);
        if (aliases == null) {
            aliases = new HashSet<String>();
            schemaAliases.put(parent, aliases);
        }
        return aliases;
    }
}
