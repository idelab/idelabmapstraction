package worldwind.kml.model;

/**
 * Created by IntelliJ IDEA.
 * User: tgleason
 * Date: Sep 3, 2008
 * Time: 10:36:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class KMLColor {
    public float red, green, blue, alpha;

    public KMLColor() {
        red = 0.7f;
        green = 0.7f;
        blue = 0.7f;
        alpha = 1.0f;
    }

    public KMLColor(String asText) {
        asText = asText.trim();
        if (asText.length() < 8) {
            red = 0.7f;
            green = 0.7f;
            blue = 0.7f;
            alpha = 1.0f;
            System.out.println("Unable to parse color");
        } else {
            String alphaStr = asText.substring(0, 2);
            String blueStr = asText.substring(2, 4);
            String greenStr = asText.substring(4, 6);
            String redStr = asText.substring(6, 8);

            alpha = ((float)Integer.parseInt(alphaStr, 16))/255;
            green = ((float)Integer.parseInt(greenStr, 16))/255;
            blue = ((float)Integer.parseInt(blueStr, 16))/255;
            red = ((float)Integer.parseInt(redStr, 16))/255;

            //System.out.printf("Color: %f %f %f %f\n", red, green, blue, alpha);
        }
    }
}
