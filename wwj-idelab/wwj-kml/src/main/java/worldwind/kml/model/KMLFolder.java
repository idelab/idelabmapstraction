package worldwind.kml.model;

import gov.nasa.worldwind.geom.Sector;

import java.util.List;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: tgleason
 * Date: Sep 1, 2008
 * Time: 8:29:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class KMLFolder {
    String name;
    String description;
    boolean visible = true;
    List<KMLObject> objects = new ArrayList<KMLObject>();
    List<KMLFolder> childFolders = new ArrayList<KMLFolder>();

    Sector sector = null;

    public KMLFolder() {
    }

    public void addObject (KMLObject object) {
        objects.add(object);
    }

    public void addChildFolder (KMLFolder folder) {
        childFolders.add(folder);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<KMLObject> getObjects() {
        return objects;
    }

    public List<KMLFolder> getChildFolders() {
        return childFolders;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }
}
