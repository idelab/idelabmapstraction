package worldwind.kml.model;

import gov.nasa.worldwind.geom.Sector;

import java.util.List;
import java.util.Iterator;

/**
 * Created by IntelliJ IDEA.
 * User: tgleason
 * Date: Sep 1, 2008
 * Time: 9:02:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class KMLLineString extends KMLGraphic {
    List<KMLCoord> coords;
    Sector sector = null;
    boolean tessellate;
    boolean absolute = false;
    boolean extrude = false;

    public KMLLineString() {
    }

    public List<KMLCoord> getCoords() {
        return coords;
    }

    public void setCoords(List<KMLCoord> coords) {
        this.coords = coords;
    }

    public Sector getSector() {
        if (sector == null) {
            double minLon = 180;
            double minLat = 90;
            double maxLon = -180;
            double maxLat = -90;

            Iterator<KMLCoord> coordIter = coords.iterator();
            while (coordIter.hasNext()) {
                KMLCoord kmlCoord = coordIter.next();
                double lon = kmlCoord.getLon();
                double lat = kmlCoord.getLat();
                if (lon < minLon)
                    minLon = lon;
                if (lon > maxLon)
                    maxLon = lon;
                if (lat < minLat)
                    minLat = lat;
                if (lat > maxLat)
                    maxLat = lat;
            }

            sector = Sector.fromDegrees(minLat, maxLat, minLon, maxLon);
        }
        return sector;
    }

    public boolean isTessellate() {
        return tessellate;
    }

    public void setTessellate(boolean tessellate) {
        this.tessellate = tessellate;
    }

    public boolean isAbsolute() {
        return absolute;
    }

    public void setAbsolute(boolean absolute) {
        this.absolute = absolute;
    }

    public boolean isExtrude() {
        return extrude;
    }

    public void setExtrude(boolean extrude) {
        this.extrude = extrude;
    }
}
