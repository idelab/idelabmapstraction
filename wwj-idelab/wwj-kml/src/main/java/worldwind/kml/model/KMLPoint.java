package worldwind.kml.model;

import gov.nasa.worldwind.geom.Sector;

/**
 * Created by IntelliJ IDEA.
 * User: tgleason
 * Date: Sep 1, 2008
 * Time: 10:30:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class KMLPoint extends KMLGraphic {
    KMLCoord coord;
    Sector sector;
    int altitudeMode = CLAMP_TO_GROUND;

    public static final int CLAMP_TO_GROUND = 1;
    public static final int RELATIVE_TO_GROUND = 2;
    public static final int ABSOLUTE = 3;

    public KMLPoint() {
    }

    public KMLCoord getCoord() {
        return coord;
    }

    public void setCoord(KMLCoord coord) {
        this.coord = coord;
    }

    public Sector getSector() {
        if (sector == null) {
            sector = Sector.fromDegrees(coord.lat-.01, coord.lat+.01, coord.lon-.01, coord.lon+.01);
        }
        return sector;
    }

    public int getAltitudeMode() {
        return altitudeMode;
    }

    public void setAltitudeMode(int altitudeMode) {
        this.altitudeMode = altitudeMode;
    }
}
