package worldwind.kml.model;

import gov.nasa.worldwind.geom.Sector;

import java.security.InvalidParameterException;
import java.util.List;
import java.util.Iterator;

/**
 * Created by IntelliJ IDEA. User: tgleason Date: Sep 2, 2008 Time: 11:28:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class KMLPolygon extends KMLGraphic {
	List<KMLCoord> outer = null;
	List<KMLCoord> inner = null;
	Sector sector = null;
	private boolean extrude = false;
	private boolean tesselate = true;

	/**
	 * Specifies how altitude components in the <coordinates> element are
	 * interpreted.<br/>
	 * Default value is {@link AltitudeMode#ClampToGround}
	 */
	private AltitudeMode altitudeMode = AltitudeMode.ClampToGround;

	public KMLPolygon() {
	}

	public List<KMLCoord> getOuter() {
		return outer;
	}

	public void setOuter(List<KMLCoord> outer) {
		this.outer = outer;
	}

	public List<KMLCoord> getInner() {
		return inner;
	}

	public void setInner(List<KMLCoord> inner) {
		this.inner = inner;
	}

	public Sector getSector() {
		if (sector == null) {
			double minLon = 180;
			double minLat = 90;
			double maxLon = -180;
			double maxLat = -90;

			Iterator<KMLCoord> coordIter = outer.iterator();
			while (coordIter.hasNext()) {
				KMLCoord kmlCoord = coordIter.next();
				double lon = kmlCoord.getLon();
				double lat = kmlCoord.getLat();
				if (lon < minLon)
					minLon = lon;
				if (lon > maxLon)
					maxLon = lon;
				if (lat < minLat)
					minLat = lat;
				if (lat > maxLat)
					maxLat = lat;
			}

			sector = Sector.fromDegrees(minLat, maxLat, minLon, maxLon);
		}
		return sector;
	}

	/**
	 * @return the extrude
	 */
	public boolean isExtrude() {
		return extrude;
	}

	/**
	 * @param extrude
	 *            the extrude to set
	 */
	public void setExtrude(boolean extrude) {
		this.extrude = extrude;
	}

	/**
	 * @return the tesselate
	 */
	public boolean isTesselate() {
		return tesselate;
	}

	/**
	 * @param tesselate
	 *            the tesselate to set
	 */
	public void setTesselate(boolean tesselate) {
		this.tesselate = tesselate;
	}

	/**
	 * Specifies how altitude components in the <coordinates> element are
	 * interpreted.<br/>
	 * Default value is {@link AltitudeMode#ClampToGround}
	 */
	public AltitudeMode getAltitudeMode() {
		return altitudeMode;
	}

	/**
	 * Specifies how altitude components in the <coordinates> element are
	 * interpreted.<br/>
	 * Default value is {@link AltitudeMode#ClampToGround}
	 */
	public void setAltitudeMode(AltitudeMode mode) {
		if (mode == null)
			throw new InvalidParameterException("Altitude mode cannot be null");
		this.altitudeMode = mode;
	}
}
