package worldwind.kml.model;

import gov.nasa.worldwind.geom.Sector;

/**
 * Created by IntelliJ IDEA.
 * User: tgleason
 * Date: Sep 1, 2008
 * Time: 9:02:21 PM
 * To change this template use File | Settings | File Templates.
 */
public abstract class KMLGraphic {
    public abstract Sector getSector();
}
