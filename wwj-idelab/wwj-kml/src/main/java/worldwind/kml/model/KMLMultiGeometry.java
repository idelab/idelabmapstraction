package worldwind.kml.model;

import gov.nasa.worldwind.geom.Sector;

import java.util.List;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: tgleason
 * Date: Oct 18, 2008
 * Time: 8:03:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class KMLMultiGeometry extends KMLGraphic {
    List<KMLGraphic> geometries = new ArrayList<KMLGraphic>();

    public KMLMultiGeometry() {
    }

    public void addGeometry (KMLGraphic graphic) {
        geometries.add(graphic);
    }

    public List<KMLGraphic> getGraphics() {
        return geometries;
    }

    public List<KMLCoord> getCoords() {
        List<KMLCoord> coords = new ArrayList<KMLCoord>();

        for(KMLGraphic graphic : geometries) {
            if(graphic instanceof KMLPoint) {
                coords.add(((KMLPoint)graphic).getCoord());
            } else if(graphic instanceof KMLLineString) {
                coords.addAll(((KMLLineString)graphic).getCoords());
            } else if(graphic instanceof KMLPolygon) {
                coords.addAll(((KMLPolygon)graphic).getOuter());
            }
        }
        return coords;

    }

    public Sector getSector() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
