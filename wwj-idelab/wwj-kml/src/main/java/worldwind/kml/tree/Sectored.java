package worldwind.kml.tree;

import gov.nasa.worldwind.geom.Sector;

/**
 * Created by IntelliJ IDEA.
 * User: tgleason
 * Date: Sep 6, 2008
 * Time: 10:02:58 PM
 * To change this template use File | Settings | File Templates.
 */
public interface Sectored {
    public Sector getSector();
}
