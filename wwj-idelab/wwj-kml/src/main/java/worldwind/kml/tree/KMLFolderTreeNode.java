package worldwind.kml.tree;

import worldwind.kml.model.KMLFolder;
import worldwind.kml.model.KMLPlacemark;
import worldwind.kml.model.KMLObject;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: tgleason
 * Date: Sep 6, 2008
 * Time: 8:38:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class KMLFolderTreeNode extends DefaultMutableTreeNode {
    KMLFolder folder;

    public KMLFolderTreeNode(KMLFolder folder) {
        this.folder = folder;

        List<KMLFolder> folders = folder.getChildFolders();
        for (KMLFolder f : folders) {
            add(new KMLFolderTreeNode(f));
        }

        List<KMLObject> placemarks = folder.getObjects();
        for (KMLObject obj : placemarks) {
            if (obj instanceof KMLPlacemark) {
                add(new KMLPlacemarkTreeNode((KMLPlacemark) obj));
            }
        }
    }


    public KMLFolder getFolder() {
        return folder;
    }

    public String toString() {
//        String s = "<html>" + folder.getName();
//        if (folder.getDescription() != null) {
//            s+= "<br>" + folder.getDescription();
//        }
//        s += "</html>";

        return folder.getName();
    }
}
