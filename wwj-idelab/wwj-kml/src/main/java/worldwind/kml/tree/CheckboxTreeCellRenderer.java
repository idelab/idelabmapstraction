package worldwind.kml.tree;

import worldwind.kml.model.KMLFolder;
import worldwind.kml.model.KMLPlacemark;

import javax.swing.tree.DefaultTreeCellRenderer;
import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: tgleason
 * Date: Oct 18, 2008
 * Time: 3:57:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class CheckboxTreeCellRenderer extends DefaultTreeCellRenderer {

    JPanel panel;
    JCheckBox check;

    public CheckboxTreeCellRenderer() {
        check = new JCheckBox();
        check.setOpaque(false);
        panel = new JPanel(new BorderLayout());
        panel.setOpaque(false);
        panel.add(check, BorderLayout.WEST);
        panel.add(this, BorderLayout.CENTER);
    }

    public Component getTreeCellRendererComponent(JTree tree, Object value,
				   boolean selected, boolean expanded,
				   boolean leaf, int row, boolean hasFocus) {

        super.getTreeCellRendererComponent(tree, value, selected, expanded, leaf, row, hasFocus);
        
        check.setBackground(this.getBackground());
        panel.setBackground(this.getBackground());
//        this.setText(value.toString());

        boolean checked = false;

        //System.out.println("Class: " + value.getClass().getName());

        if (value instanceof KMLFolderTreeNode) {
            checked = ((KMLFolderTreeNode)value).getFolder().isVisible();
        } else if (value instanceof KMLPlacemarkTreeNode) {
            checked = ((KMLPlacemarkTreeNode)value).getPlacemark().isVisible();
        } else if (value instanceof KMLDocTreeNode) {
            checked = true;
        }

        check.setSelected(checked);

        panel.revalidate();

        return panel;
    }


}
