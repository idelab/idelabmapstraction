package worldwind.kml.tree;

import worldwind.kml.model.*;

import javax.swing.tree.DefaultMutableTreeNode;

import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.geom.Angle;

/**
 * Created by IntelliJ IDEA.
 * User: tgleason
 * Date: Sep 6, 2008
 * Time: 8:43:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class KMLPlacemarkTreeNode extends DefaultMutableTreeNode implements Sectored {
    KMLPlacemark placemark;

    public KMLPlacemarkTreeNode(KMLPlacemark placemark) {
        this.placemark = placemark;
    }


    public Sector getSector() {
        KMLGraphic graphic = placemark.getGraphic();
        if (graphic instanceof KMLPolygon) {
            KMLPolygon poly = (KMLPolygon)graphic;
            return poly.getSector();
        } else if (graphic instanceof KMLPoint) {
            KMLPoint point = (KMLPoint) graphic;
            KMLCoord coord = point.getCoord();
            return new Sector(
                    Angle.fromDegrees(coord.getLat() - 1),
                    Angle.fromDegrees(coord.getLat() + 1),
                    Angle.fromDegrees(coord.getLon() - 1),
                    Angle.fromDegrees(coord.getLon() + 1)
            );
        } else if (graphic instanceof KMLLineString) {
            KMLLineString lineString = (KMLLineString) graphic;
            return lineString.getSector();
        }
        return null;
    }

    public KMLPlacemark getPlacemark() {
        return placemark;
    }

    public String toString() {
//        String s = "<html>" + placemark.getName();
//        if (placemark.getDescription() != null) {
//            s+= "<br>" + placemark.getDescription();
//        }
//        s += "</html>";

        return placemark.getName();
    }

}
