package worldwind.kml.tree;

import worldwind.kml.model.KMLFile;
import worldwind.kml.model.KMLFolder;
import worldwind.kml.KMLLayer;

import javax.swing.tree.DefaultMutableTreeNode;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: tgleason
 * Date: Sep 6, 2008
 * Time: 8:31:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class KMLDocTreeNode extends DefaultMutableTreeNode {
    KMLFile kmlFile;
    KMLLayer layer;
    String name;

    public KMLDocTreeNode(KMLFile kmlFile, String name) {
        this.kmlFile = kmlFile;
        this.name = name;

        List<KMLFolder> folders = kmlFile.getRootFolder().getChildFolders();
        for (KMLFolder folder : folders) {
            add(new KMLFolderTreeNode(folder));
        }
    }

    public KMLDocTreeNode(KMLFile kmlFile, String name, KMLLayer layer) {
        this(kmlFile, name);
        this.layer = layer;
    }

    public String toString() {
        return name;
    }

    public KMLLayer getLayer() {
        return layer;
    }
}
