package gov.nasa.worldwind.geom;

import junit.framework.TestCase;
import gov.nasa.worldwind.globes.Earth;
import gov.nasa.worldwind.globes.Globe;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class EllipsoidalDistanceTests extends TestCase {

	private final static double TOLERANCE = 0.1;
    private Globe globe;

    @Before
    public void setUp()
    {
        this.globe = new Earth();
    }

    @After
    public void tearDown()
    {
        this.globe = null;
    }

    @Test
    public void testKnownDistanceA()
    {
        LatLon begin = LatLon.fromDegrees(30.608879, -102.118357);
        LatLon end = LatLon.fromDegrees(34.413929, -97.022765);
        double distance = LatLon.ellipsoidalDistance(begin, end, globe.getEquatorialRadius(),
            globe.getPolarRadius());
        assertEquals("Known ellipsoidal distance A", 638027.750, distance, TOLERANCE);
    }

    @Test
    public void testKnownDistanceB()
    {
        LatLon begin = LatLon.fromDegrees(9.2118, -79.5180);
        LatLon end = LatLon.fromDegrees(48.4216, -122.3352);
        double distance = LatLon.ellipsoidalDistance(begin, end, globe.getEquatorialRadius(),
            globe.getPolarRadius());
        assertEquals("Known ellipsoidal distance B", 5900926.896, distance, TOLERANCE);
    }

    @Test
    public void testKnownDistanceC()
    {
        LatLon begin = LatLon.fromDegrees(-31.9236, 116.1231);
        LatLon end = LatLon.fromDegrees(23.6937, 121.9831);
        double distance = LatLon.ellipsoidalDistance(begin, end, globe.getEquatorialRadius(),
            globe.getPolarRadius());
        assertEquals("Known ellipsoidal distance C", 6186281.864, distance, TOLERANCE);
    }

    @Test
    public void testKnownDistanceD()
    {
        LatLon begin = LatLon.fromDegrees(51.4898, 0.0539);
        LatLon end = LatLon.fromDegrees(42.3232, -71.0974);
        double distance = LatLon.ellipsoidalDistance(begin, end, globe.getEquatorialRadius(),
            globe.getPolarRadius());
        assertEquals("Known ellipsoidal distance D", 5296396.967, distance, TOLERANCE);
    }
}
