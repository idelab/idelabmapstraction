package gov.nasa.worldwind.geom;

import gov.nasa.worldwind.globes.Earth;
import gov.nasa.worldwind.globes.Globe;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import junit.framework.TestCase;

public class ForwardAzimuthTests extends TestCase {

	private final static double TOLERANCE = 0.1;
    private Globe globe;

    @Before
    public void setUp()
    {
        this.globe = new Earth();
    }

    @After
    public void tearDown()
    {
        this.globe = null;
    }

    @Test
    public void testKnownAzimuthA()
    {
        LatLon begin = LatLon.fromDegrees(30.000000, -102.000000);
        LatLon end = LatLon.fromDegrees(34.000000, -97.000000);
        Angle theta = LatLon.ellipsoidalForwardAzimuth(begin, end, globe.getEquatorialRadius(),
            globe.getPolarRadius());
        assertEquals("Known ellipsoidal Azimuth A", 45.50583, theta.degrees, TOLERANCE);
    }

    @Test
    public void testKnownAzimuthB()
    {
        LatLon begin = LatLon.fromDegrees(9.0000, -79.0000);
        LatLon end = LatLon.fromDegrees(48.0000, -122.0000);
        Angle theta = LatLon.ellipsoidalForwardAzimuth(begin, end, globe.getEquatorialRadius(),
            globe.getPolarRadius());
        assertEquals("Known ellipsoidal Azimuth B", Angle.normalizedLongitude(Angle.fromDegrees(325.10111)).degrees,
            theta.degrees, TOLERANCE);
    }

    @Test
    public void testKnownAzimuthC()
    {
        LatLon begin = LatLon.fromDegrees(-32.0000, 116.0000);
        LatLon end = LatLon.fromDegrees(23.0000, 122.0000);
        Angle theta = LatLon.ellipsoidalForwardAzimuth(begin, end, globe.getEquatorialRadius(),
            globe.getPolarRadius());
        assertEquals("Known ellipsoidal Azimuth C", 6.75777, theta.degrees, TOLERANCE);
    }

    @Test
    public void testKnownAzimuthD()
    {
        LatLon begin = LatLon.fromDegrees(51.5000, 0.0000);
        LatLon end = LatLon.fromDegrees(42.0000, -71.0000);
        Angle theta = LatLon.ellipsoidalForwardAzimuth(begin, end, globe.getEquatorialRadius(),
            globe.getPolarRadius());
        assertEquals("Known ellipsoidal Azimuth D", Angle.normalizedLongitude(Angle.fromDegrees(287.95372)).degrees,
            theta.degrees, TOLERANCE);
    }
}
