package gov.nasa.worldwind.render;

import gov.nasa.worldwind.util.RestorableSupport;
import junit.framework.TestCase;

public class ShapeAttributeTest extends TestCase
{
    protected static final ShapeAttributes exampleAttributes = new BasicShapeAttributes();

    static
    {
        exampleAttributes.setUnresolved(true);
        exampleAttributes.setDrawInterior(false);
        exampleAttributes.setDrawOutline(false);
        exampleAttributes.setEnableAntialiasing(false);
        exampleAttributes.setEnableLighting(false);
        exampleAttributes.setInteriorMaterial(Material.RED);
        exampleAttributes.setOutlineMaterial(Material.GREEN);
        exampleAttributes.setInteriorOpacity(0.5);
        exampleAttributes.setOutlineOpacity(0.75);
        exampleAttributes.setOutlineWidth(10.0);
        exampleAttributes.setOutlineStippleFactor(256);
        exampleAttributes.setOutlineStipplePattern((short) 0xABAB);
        exampleAttributes.setImageSource("images/pushpins/plain-black.png");
        exampleAttributes.setImageScale(2.0);
    }

    protected ShapeAttributes createDefaultAttributes()
    {
        return new BasicShapeAttributes();
    }

    protected ShapeAttributes createExampleAttributes()
    {
        return exampleAttributes.copy();
    }

    @org.junit.Test
    public void testBasicSaveRestore()
    {
        RestorableSupport rs = RestorableSupport.newRestorableSupport();

        ShapeAttributes expected = this.createExampleAttributes();
        expected.getRestorableState(rs, null);

        ShapeAttributes actual = this.createDefaultAttributes();
        actual.restoreState(rs, null);

        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testRestoreSameInstance()
    {
        RestorableSupport rs = RestorableSupport.newRestorableSupport();

        ShapeAttributes expected = this.createExampleAttributes();

        ShapeAttributes actual = this.createExampleAttributes();
        actual.getRestorableState(rs, null);
        actual.copy(this.createDefaultAttributes());
        actual.restoreState(rs, null);

        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testRestoreNullDocument()
    {
        try
        {
            ShapeAttributes attrs = this.createDefaultAttributes();
            attrs.restoreState(null, null);
            fail("Expected an IllegalArgumentException");
        }
        catch (Exception e)
        {
        }
    }

    @org.junit.Test
    public void testRestoreEmptyDocument()
    {
        ShapeAttributes expected = this.createExampleAttributes();

        // Restoring an empty state document should not change any attributes.
        ShapeAttributes actual = this.createExampleAttributes();
        String emptyStateInXml =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<emptyDocumentRoot/>";
        RestorableSupport rs = RestorableSupport.parse(emptyStateInXml);
        actual.restoreState(rs, null);

        assertEquals(expected, actual);
    }

    @org.junit.Test
    public void testRestoreOneAttribute()
    {
        ShapeAttributes expected = this.createExampleAttributes();
        expected.setOutlineWidth(11);

        ShapeAttributes actual = this.createExampleAttributes();
        String partialStateInXml =
            "<?xml version=\"1.0\" encoding=\"UTF-8\"?>" +
                "<restorableState>" +
                "<stateObject name=\"outlineWidth\">11</stateObject>" +
                "<unknownElement name=\"unknownName\">unknownValue</unknownElement>" +
                "</restorableState>";
        RestorableSupport rs = RestorableSupport.parse(partialStateInXml);
        actual.restoreState(rs, null);

        assertEquals(expected, actual);
    }
}
