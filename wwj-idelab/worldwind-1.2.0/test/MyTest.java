import java.io.File;

import junit.framework.TestCase;

public class MyTest extends TestCase
{
	public static final String STATE_BOUNDS_PATH = "testData/shapefiles/state_bounds.shp";
	
	@org.junit.Test
    public void testSize()
    {
		File file = new File(STATE_BOUNDS_PATH);
		assertTrue("File "+STATE_BOUNDS_PATH + "not found!" , file.exists());
    }
}