package gov.nasa.worldwind.mxn;

import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.AnnotationLayer;
import gov.nasa.worldwind.layers.IconLayer;
import gov.nasa.worldwind.render.GlobeAnnotation;
import gov.nasa.worldwind.render.UserFacingIcon;
import gov.nasa.worldwind.render.WWIcon;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.SocketPermission;
import java.net.URL;
import java.util.logging.Logger;

import javax.imageio.ImageIO;


public class IdelabMxnMarker extends UserFacingIcon {
	
	Logger logger = Logger.getLogger("IDELabMxnMarker");
	
	public static final String MARKER_LAYER_NAME = "MARKER_LAYER";
	
	public static final String BUBBLE_LAYER_NAME = "BUBBLE_LAYER";
	
	public static final String DEFAULT_IMAGE = "images/marker.png"; // http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png
	
	WorldWindow wwd = null;
	
	GlobeAnnotation bubble = null;
	
	private IconLayer iconLayer = null;
	
	private AnnotationLayer bubbleLayer = null;
	
	String groupName = null;

	boolean isBubbleInfoVisible = false;
	
	public boolean isBubbleInfoVisible() {
		return isBubbleInfoVisible;
	}

	public void setBubbleInfoVisible(boolean isBubbleInfoVisible) {
		this.isBubbleInfoVisible = isBubbleInfoVisible;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
		
	/**
	 * 
	 * @return Marker info bubble
	 */
//	public GlobeAnnotation getBubble() {
//		return bubble;
//	}

	/**
	 *  Set marker info bubble
	 *  
	 * @param bubble
	 */
//	public void setBubble(GlobeAnnotation bubble) {
//		this.bubble = bubble;
//	}
	
	/**
	 * 
	 * @param wwd
	 * @param position
	 * @throws IOException
	 */
	public IdelabMxnMarker(WorldWindow wwd, Position position) throws IOException {
		super(DEFAULT_IMAGE, position);
		init(wwd);
	}
	
	/**
	 * 
	 * @param wwd
	 * @param iconURL
	 * @param position
	 * @throws IOException
	 */
	public IdelabMxnMarker(WorldWindow wwd, String iconURL, Position position) throws IOException {
		
		super();
		
		URL url = null;
		
		try {
			url = new URL(iconURL);
		} catch (MalformedURLException e) {
			throw new MalformedURLException(iconURL+" is not a valid URL. "+e.getMessage());
		}
		BufferedImage bufferedImage = null;
		
		try {
			ImageIO.setUseCache(false); // cache stored in memory to avoid applet security issues
			bufferedImage = ImageIO.read(url);
		} catch (IOException e) {
			logger.warning(e.getMessage());
			throw new IOException("Image icon could not be read from url "+url, e);
		}
		setPosition(position);
		setImageSource(bufferedImage);
		
		init(wwd);
	}
	
	public void init(WorldWindow wwd) {
		
		this.wwd = wwd;
		
		iconLayer = (IconLayer) wwd.getModel().getLayers().getLayerByName(MARKER_LAYER_NAME);
		if(iconLayer==null) {
			iconLayer = new IconLayer();
			iconLayer.setName(MARKER_LAYER_NAME);
			wwd.getModel().getLayers().add(iconLayer);
		} 
		
		bubbleLayer = (AnnotationLayer) wwd.getModel().getLayers().getLayerByName(BUBBLE_LAYER_NAME);
		if(bubbleLayer==null) {
			bubbleLayer = new AnnotationLayer();
			bubbleLayer.setName(BUBBLE_LAYER_NAME);
			wwd.getModel().getLayers().add(bubbleLayer);
		}
		
		iconLayer.addIcon(this);
		bubble = new GlobeAnnotation("", getPosition());
	}

	
	/**
	 * 
	 * @param label Marker label
	 */
	public void setLabel(String label) {
		this.setToolTipText(label);
	}
	
	/**
	 * Show label
	 */
	public void showLabel() {
		this.setShowToolTip(true);
	}
	
	/**
	 * Hide label
	 */
	public void hideLabel() {
		this.setShowToolTip(false);
	}
	
	/**
	 * 
	 * @param bubbleInfo Textual info to show in the bubble (can be html code)
	 */
	public void setBubbleInfo(String bubbleInfo) {
		logger.info("BubbleInfo = "+bubbleInfo);
		this.bubble.setText(bubbleInfo);
	}
	
	/**
	 * Open bubble info
	 */
	public void openInfoBubble() {
        
		if(bubble.getText()!=null && !bubble.getText().isEmpty() && !isBubbleInfoVisible()) {
			bubbleLayer.addAnnotation(bubble);
			setBubbleInfoVisible(true);
		}
	}
	
	/**
	 * Close bubble info
	 */
	public void closeInfoBubble() {
        
		if(isBubbleInfoVisible()) {
			bubbleLayer.removeAnnotation(bubble);
			setBubbleInfoVisible(false);
			wwd.redrawNow();
		}
	}

	@Override
	public void setVisible(boolean visible) {
		
		if(visible) {
			if(!isVisible() && isBubbleInfoVisible) {
				openInfoBubble();
			}
		} else {
			closeInfoBubble();
			hideLabel();
		}
		super.setVisible(visible);
	}
	
	/**
	 * Remove marker (Removes icon from IconLayer and Bubble from AnnotationLayer)
	 */
	public void remove() {
		
		logger.info("Removing marker");
		
		if(iconLayer!=null) {
			iconLayer.removeIcon(this);
			bubbleLayer.removeAnnotation(bubble);
		}
	}
	
}
