package gov.nasa.worldwind.mxn;

import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.Polyline;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;

import java.io.IOException;
import java.net.URL;

public interface MxnCore {

	/**
	 * Move the current view position
	 * @param lat the target latitude in decimal degrees
	 * @param lon the target longitude in decimal degrees
	 */
	public abstract void gotoLatLon(double lat, double lon);

	/**
	 * Move the current view position, zoom, heading and pitch
	 * @param lat the target latitude in decimal degrees
	 * @param lon the target longitude in decimal degrees
	 * @param zoom the target eye distance in meters
	 * @param heading the target heading in decimal degrees
	 * @param pitch the target pitch in decimal degrees
	 */
	public abstract void gotoLatLon(double lat, double lon, double zoom,
			double heading, double pitch);

	/**
	 * Sets the central point of the map
	 * @param lat	Latitude in decimal degrees of the central point of the map to set
	 * @param lon	Longitude in decimal degrees of the central point of the map to set
	 */
	public abstract void setCenter(double lat, double lon);
	
	/**
	 * Pans the central point of the map to the specified point
	 * @param lat	Latitude in decimal degrees of the central point of the map to set
	 * @param lon	Longitude in decimal degrees of the central point of the map to set
	 */
	public abstract void panToCenter(double lat, double lon);

	/**
	 * Gets the central point of the map
	 * @return	String Point in the form lat;lon expressed in decimal degrees
	 */
	public abstract String getCenter();

	/**
	 * Set the current view heading and pitch
	 * @param heading the traget heading in decimal degrees
	 * @param pitch the target pitch in decimal degrees
	 */
	public abstract void setHeadingAndPitch(double heading, double pitch);
	
	/**
	 * Sets the zoom level for the map
	 * @param target_zoom	The level zoom the map to
	 */
	public abstract void setZoomLevel(double target_zoom);
	
	/**
	 * Returns the zoom level of the map
	 * @return zoom level of the map
	 */
	public abstract double getZoomLevel();
	
	/**
	 * Sets the BoundingBox of the map
	 * @param minLongitude	longitude of the down-left corner of the bbox to show
	 * @param minLatitude	latitude of the down-left corner of the bbox to show
	 * @param maxLongitude	longitude of the upper-right corner of the bbox to show
	 * @param maxLatitude	latitude of the upper-right corner of the bbox to show
	 */
	public abstract void setMapBounds(double minLongitude, double minLatitude,
			double maxLongitude, double maxLatitude);

	/**
	 * Gets the BoundingBox of the map
	 * @return Visible bounds in the format: minx,miny,maxx,maxy
	 */
	public abstract String getMapBounds();

	public abstract void addTileLayer(String configFile) throws IOException;
	
	public abstract void addTileLayer(String serverUrl, String layerName);

	public abstract void addTileLayer(URL url);

	public abstract void toggleTileLayer(String configFile);
	
	/**
	 * Adds a small map panning control and zoom buttons to the map
	 */
	public abstract void addSmallControlls();
	
	/**
	 * Removes a small map panning control and zoom buttons to the map
	 */
	public abstract void removeSmallControls();
	
	/**
	 * Adds a GeoRSS or KML overlay to the map
	 * @param kml_url URL of the KML resource to load
	 */
	public abstract void addKMLOverlay(String kml_url);
	
	/**
	 * Adds a marker pin to the map
	 * @param lat	Latitude of the marker
	 * @param lon	Longitude of the marker
	 */
	public abstract int addMarker(double lat, double lon);
	
	public abstract void hideMarker(int hashcode);

	public abstract void showMarker(int hashcode);
	
	public abstract void removeMarker(int hashcode);
	
	public abstract void removeAllMarkers();
	
	public abstract void activateLineEdition();

	public abstract void dactivateLineEdition();
	
	public abstract void insertBeforeLayerName(WorldWindow wwd, Layer layer, String targetName);

	public abstract void openBubble(int hashcode);

	public void setMarkerLabel(int hashcode, String label);

	public void setMarkerInfoBubble(int hashcode, String bubbleInfo);

	public int addMarker(double lat, double lon, String iconURL,
			String label, String bubbleInfo);
	
	public int addPolyline(String points, String color, int width, double opacity, boolean closed, String fillColor);

	public void hidePolyline(int hashcode);

	public void showPolyline(int hashcode);

	public void removePolyline(int hashcode);

	public void addLargeControls();

	public void removeLargeControlls();

	public abstract void removeOverlay(String url);

	public abstract void addOverlay(String url);

}