package gov.nasa.worldwind.mxn.geom;

import gov.nasa.worldwind.render.airspaces.CappedCylinder;

public class MxnCylinder extends CappedCylinder implements MxnFeature {

	private String fid;

	public String getFid() {
		return fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}
}
