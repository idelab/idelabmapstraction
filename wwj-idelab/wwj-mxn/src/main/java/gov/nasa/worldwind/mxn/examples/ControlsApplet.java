package gov.nasa.worldwind.mxn.examples;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;

import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.mxn.applet.IdelabMxnWorldwindApplet;

public class ControlsApplet extends IdelabMxnWorldwindApplet {

	public void init()
    {
		super.init();
		WorldWindowGLCanvas ww = getWW();
		
		ww.addMouseListener(new MouseListener() {
			
			boolean overview = false;
			
			public void mouseReleased(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			public void mousePressed(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			public void mouseExited(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			public void mouseEntered(MouseEvent e) {
				// TODO Auto-generated method stub
				
			}
			
			public void mouseClicked(MouseEvent e) {
				// TODO Auto-generated method stub
				System.out.println("CLICKED");
				HashMap<String, Object> controlsMap = new HashMap<String, Object>();
				if(overview) {
		            controlsMap.put("overview", "false");
		            overview = false;
				} else {
					controlsMap.put("overview", "true");
					overview = true;
				}
				addControls(controlsMap);
			}
		});
    }
}
