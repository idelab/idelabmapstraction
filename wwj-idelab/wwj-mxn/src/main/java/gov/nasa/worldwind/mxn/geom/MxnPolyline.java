package gov.nasa.worldwind.mxn.geom;

import gov.nasa.worldwind.render.Polyline;

public class MxnPolyline extends Polyline implements MxnFeature {

	private String fid;

	public String getFid() {
		return fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}
	
	public MxnPolyline(String fid) {
		
		setFid(fid);
	}
}
