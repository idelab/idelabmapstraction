package gov.nasa.worldwind.mxn.geom;

import gov.nasa.worldwind.render.airspaces.Curtain;
import gov.nasa.worldwind.render.airspaces.Polygon;

public class MxnPolyline3D extends Curtain implements MxnFeature {

	private String fid;

	public String getFid() {
		return fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}
}
