package gov.nasa.worldwind.mxn.geom;

import gov.nasa.worldwind.render.Renderable;

public interface MxnFeature extends Renderable{

	public abstract String getFid();

	public abstract void setFid(String fid);

}