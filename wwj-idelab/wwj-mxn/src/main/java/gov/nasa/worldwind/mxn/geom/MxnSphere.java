package gov.nasa.worldwind.mxn.geom;

import gov.nasa.worldwind.render.airspaces.SphereAirspace;

public class MxnSphere extends SphereAirspace implements MxnFeature {

	private String fid;

	public String getFid() {
		return fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}
}
