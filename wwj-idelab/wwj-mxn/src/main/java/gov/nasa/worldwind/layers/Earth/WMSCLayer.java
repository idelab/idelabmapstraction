package gov.nasa.worldwind.layers.Earth;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.util.*;
import gov.nasa.worldwind.wms.WMSTiledImageLayer;
import org.w3c.dom.Document;

public class WMSCLayer extends WMSTiledImageLayer
{
    protected static final String defaultDatasetName = "cartociudad";

    /**
     * Default OpenStreetMap hybrid layer - transparent, see-through.
     */
    public WMSCLayer()
    {
    	super(makeLevels(new URLBuilder()));
    }
    
    private static AVList makeLevels(URLBuilder urlBuilder)
    {
        AVList params = new AVListImpl();
        
	        params.setValue(AVKey.TILE_WIDTH, 256);
	        params.setValue(AVKey.TILE_HEIGHT, 256);
	        params.setValue(AVKey.DATA_CACHE_NAME, "Earth/Cartociudad");
	        params.setValue(AVKey.LAYER_NAME, "cartociudad");
	        params.setValue(AVKey.DISPLAY_NAME, "cartociudad");
	        //
	        // SERVICE
	        //
	        params.setValue(AVKey.SERVICE_NAME, "OGC:WMS");
	        params.setValue(AVKey.GET_CAPABILITIES_URL, "http://itastdevserver.tel.uva.es/wmscwrapper/wms");
	        params.setValue(AVKey.GET_MAP_URL, "http://itastdevserver.tel.uva.es/wmscwrapper/wms");
	        params.setValue(AVKey.SERVICE, "http://itastdevserver.tel.uva.es/wmscwrapper/wms");
	        params.setValue(AVKey.LAYER_NAMES, "cartociudad");
	        params.setValue(AVKey.RETRIEVE_PROPERTIES_FROM_SERVICE, false);
	        params.setValue(AVKey.IMAGE_FORMAT, "image/png");
	        params.setValue(AVKey.AVAILABLE_IMAGE_FORMATS, new String[]{"image/png"});
	        params.setValue(AVKey.FORMAT_SUFFIX, ".png");
	        
	        params.setValue(AVKey.NUM_LEVELS, 10);
	        params.setValue(AVKey.NUM_EMPTY_LEVELS, 0);
	        params.setValue(AVKey.TILE_ORIGIN, new LatLon(Angle.fromDegrees(-90), Angle.fromDegrees(-180)));
	        params.setValue(AVKey.LEVEL_ZERO_TILE_DELTA, new LatLon(Angle.fromDegrees(2.8125), Angle.fromDegrees(2.8125)));
	        //params.setValue(AVKey.SECTOR, Sector.FULL_SPHERE);
	        params.setValue(AVKey.SECTOR, new Sector(Angle.fromDegrees(33.75),Angle.fromDegrees(45),Angle.fromDegrees(-11.25),Angle.fromDegrees(0)));
	        
	        params.setValue(AVKey.FORCE_LEVEL_ZERO_LOADS, true);
	        params.setValue(AVKey.RETAIN_LEVEL_ZERO_TILES, true);
	        params.setValue(AVKey.USE_TRANSPARENT_TEXTURES, true);
	        params.setValue(AVKey.DATASET_NAME, "cartociudad");
	        params.setValue(AVKey.WMS_VERSION, "1.1");

	        params.setValue(AVKey.TILE_URL_BUILDER, urlBuilder);
        return params;
    }
    
    private static class URLBuilder implements TileUrlBuilder
	{
		public URL getURL(Tile tile, String imageFormat)
				throws MalformedURLException
		{
            StringBuffer sb = new StringBuffer(tile.getLevel().getService());
            if (sb.lastIndexOf("?") != sb.length() - 1)
                sb.append("?");
            sb.append("request=GetMap");
            sb.append("&layers=");
            sb.append(tile.getLevel().getDataset());
            sb.append("&srs=EPSG:4326");
            sb.append("&width=");
            sb.append(tile.getLevel().getTileWidth());
            sb.append("&height=");
            sb.append(tile.getLevel().getTileHeight());

            Sector s = tile.getSector();
            sb.append("&bbox=");
            sb.append(s.getMinLongitude().getDegrees());
            sb.append(",");
            sb.append(s.getMinLatitude().getDegrees());
            sb.append(",");
            sb.append(s.getMaxLongitude().getDegrees());
            sb.append(",");
            sb.append(s.getMaxLatitude().getDegrees());

            sb.append("&format=image/png");
            sb.append("&service=WMS");
            sb.append("&version=1.1.1");
            //sb.append("&bgcolor=0x000000");
            //sb.append("&transparent=true");

            //System.out.println("URL: " + sb);
            return new java.net.URL(sb.toString());
//			String quadkey = tileToQuadKey(tile.getColumn(), tile.getRow(),
//					tile.getLevelNumber() + 2);
//			return new URL(tile.getLevel().getService()
//					+ tile.getLevel().getDataset() + quadkey + ".jpeg?g=1");
		}
	}
    
    public WMSCLayer(URL url) throws IOException
    {
        super(createConfigurationDocument(url), null);
    }
    
    public WMSCLayer(String configurationXml) throws IOException
    {
        super(createConfigurationDocument(configurationXml), null);
    }

    protected static Document createConfigurationDocument(URL url) throws IOException
    {
        String configurationXml = "";

    	BufferedReader in = new BufferedReader(
    				new InputStreamReader(
    				url.openStream()));

    	String inputLine;

    	while ((inputLine = in.readLine()) != null)
    	    configurationXml += (inputLine+"\n");

    	in.close();

        java.io.InputStream inputStream = WWIO.getInputStreamFromString(configurationXml);
        return WWXML.openDocumentStream(inputStream);
    }
    
    protected static Document createConfigurationDocument(String configurationXml) throws IOException
    {
        java.io.InputStream inputStream = WWIO.getInputStreamFromString(configurationXml);
        return WWXML.openDocumentStream(inputStream);
    }
}

