/*
Copyright (C) 2001 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/
package gov.nasa.worldwind.mxn.examples;

import gov.nasa.worldwind.BasicModel;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.ogc.kml.KMLAbstractFeature;
import gov.nasa.worldwind.ogc.kml.KMLAbstractView;
import gov.nasa.worldwind.ogc.kml.KMLCamera;
import gov.nasa.worldwind.ogc.kml.KMLLookAt;
import gov.nasa.worldwind.ogc.kml.KMLRoot;
import gov.nasa.worldwind.ogc.kml.impl.KMLController;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.ExtrudedPolygon;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.ShapeAttributes;
import gov.nasa.worldwind.render.airspaces.Airspace;
import gov.nasa.worldwind.render.airspaces.CappedCylinder;
import gov.nasa.worldwind.render.airspaces.SphereAirspace;
import gov.nasa.worldwindx.examples.kml.KMLFlyViewController;
import gov.nasa.worldwindx.examples.kml.KMLViewController;

import java.awt.Color;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import javax.xml.stream.XMLStreamException;

/**
 * This is the most basic World Wind program.
 *
 * @version $Id: HelloWorldWind.java 4869 2008-03-31 15:56:36Z tgaskins $
 */
public class KMLExample
{
    // An inner class is used rather than directly subclassing JFrame in the main class so
    // that the main can configure system properties prior to invoking Swing. This is
    // necessary for instance on OS X (Macs) so that the application name can be specified.

    protected static class AppFrame extends javax.swing.JFrame
    {

		public AppFrame()
        {			
		
            WorldWindowGLCanvas wwd = new WorldWindowGLCanvas();
            wwd.setModel(new BasicModel());		
            wwd.setPreferredSize(new java.awt.Dimension(1000, 800));
            this.getContentPane().add(wwd, java.awt.BorderLayout.CENTER);
            this.pack();
            
            URL kml_url = null;
			try {
				kml_url = new URL("http://itastdevserver.tel.uva.es/wwj/kml/googlecampus.kml");
			} catch (MalformedURLException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
                        
            KMLRoot kmlRoot = null;
    		try {
    			kmlRoot = KMLRoot.createAndParse(kml_url);
    		} catch (IOException e) {
    			System.out.println(e.getMessage());
    		} catch (XMLStreamException e) {
    			System.out.println(e.getMessage());
    		}

            // Set the document's display name
            kmlRoot.setField(AVKey.DISPLAY_NAME, kml_url.toString());

            // Create a KMLController to adapt the KMLRoot to the World Wind renderable interface.
            KMLController kmlController = new KMLController(kmlRoot);

            // Adds a new layer containing the KMLRoot to the end of the WorldWindow's layer list. This
            // retrieves the layer name from the KMLRoot's DISPLAY_NAME field.
            RenderableLayer kmlLayer = new RenderableLayer();
            kmlLayer.setName((String) kmlRoot.getField(AVKey.DISPLAY_NAME));
            kmlLayer.addRenderable(kmlController);

            KMLViewController viewController = KMLViewController.create(wwd);
            
            if (kmlRoot.getNetworkLinkControl() != null
                && kmlRoot.getNetworkLinkControl().getView() != null)
            {
                if (viewController != null)
                	viewController.goTo(kmlRoot.getNetworkLinkControl().getView());
            }
            else if (kmlRoot.getFeature() != null
                && kmlRoot.getFeature().getView() != null)
            {
                if (viewController != null)
                	viewController.goTo(kmlRoot.getFeature().getView());
            }
            
            wwd.getModel().getLayers().add(kmlLayer);
            wwd.redrawNow();
            
            //////
        }
    }
  

    public static void main(String[] args)
    {
        if (Configuration.isMacOS())
        {
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Hello World Wind");
        }

        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                // Create an AppFrame and immediately make it visible. As per Swing convention, this
                // is done within an invokeLater call so that it executes on an AWT thread.
                new AppFrame().setVisible(true);
            }
        });
    }
}