package gov.nasa.worldwind.mxn;

import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;

import java.util.ArrayList;
import java.util.HashMap;

import netscape.javascript.JSObject;

/**
 * Interface which exposes Mapstraction Core functionallity
 * 
 * @author Jordana Torres Gonzalez (alepitheia@hotmail.com)
 * @author Ricardo Garc�a Mart�n (garcia.martin.ricardo@gmail.com)
 */
public interface MapstractionCore {
	
	/**
	 * Adds a large map panning control and zoom buttons to the map
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#addLargeControls">mxn.Mapstraction#addLargeControls</a>
	 */
	public void addLargeControls();
		
	/**
	 * Adds a map type control to the map (streets, aerial imagery etc)
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#addMapTypeControls">mxn.Mapstraction#addMapTypeControls</a>
	 */
	public void addMapTypeControls(); 
	
	/**
	 * Adds a GeoRSS or KML overlay to the map
	 *
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#addOverlay">mxn.Mapstraction#addOverlay</a>
	 * @param url GeoRSS or KML feed URL
	 * @param autoCenterAndZoom Set true to auto center and zoom after the feed is loaded
	 */
	public void addOverlay(String url, boolean autoCenterAndZoom); 
	
	/**
	 * Adds a small map panning control and zoom buttons to the map
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#addSmallControls"> mxn.Mapstraction#addSmallControls </a>
	 */
	public void addSmallControls();
	
	/**
	 * Applies the current option settings
	 * @param optionsMap Options map
	 */
	public void applyOptions(HashMap<String, String> optionsMap);
	
	/**
	 * Gets the BoundingBox of the map
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#getBounds"> mxn.Mapstraction#getBounds </a>
	 * @return The bounding box for the current map state
	 */
	public Sector getMapBounds(); 
	
	/**
	 * Gets the central point of the map
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#getCenter"> mxn.Mapstraction#getCenter </a>
	 * @return The center point of the map
	 */
	public LatLon getCenter(); 
	
	/**
	 * Gets the imagery type for the map.
	 * <pre>
	 * The type can be one of:
	 * 	Mapstraction.ROAD = 1;
	 * 	Mapstraction.SATELLITE = 2;
	 * 	Mapstraction.HYBRID = 3;
	 * </pre>
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#getMapType"> mxn.Mapstraction#getMapType </a>
	 * @return The imagery type for the map: 1 (ROAD), 2 (SATELLITE) or 3 (HYBRID)
	 */
	public int getMapType();

	/**
	 * Returns a ratio to turn distance into pixels based on current projection
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#getPixelRatio"> mxn.Mapstraction#getPixelRatio </a>
	 * @return ratio
	 */
	public double getPixelRatio(); 
	
	/**
	 * Returns the zoom level of the map
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#getZoom"> mxn.Mapstraction#getZoom </a>
	 * @return The zoom level of the map
	 */
	public int getZoom(); 
	
	/**
	 * Returns the best zoom level for bounds given
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#getZoomLevelForBoundingBox"> mxn.Mapstraction#getZoomLevelForBoundingBox </a>
	 * @param minLongitude longitude (degrees) of the lower-left corner of the bounding box to fit
	 * @param minLatitude latitude (degrees) of the lower-left corner of the bounding box to fit
	 * @param maxLongitude longitude (degrees) of the upper-right corner of the bounding box to fit
	 * @param maxLatitude latitude (degrees) of the upper-right corner of the bounding box to fit
	 * @return The closest zoom level that contains the bounding box
	 */
	public int getZoomLevelForBoundingBox(double minLongitude, double minLatitude, double maxLongitude, double maxLatitude); 
	
	/**
	 * Get the coordinates of the cursor
	 * @return The current mouse position point
	 */
	public LatLon getCurrentMousePosition();
	
	/**
	 * Removes a GeoRSS or KML overlay to the map
	 *  some flavors of GeoRSS and KML are not supported by some of the Map providers
	 * @param url GeoRSS or KML feed URL
	 */
	public void removeOverlay(String url);
	
	/**
	 * Resize the current map to the specified width and height
	 * (since it is actually on a child div of the mapElement passed
	 * as argument to the Mapstraction constructor, the resizing of this
	 * mapElement may have no effect on the size of the actual map)
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#resizeTo"> mxn.Mapstraction#resizeTo </a>
	 * @param width The width the map should be.
	 * @param height The width the map should be.
	 */
	public void resizeTo(int width, int height); 
	
	/**
	 * Sets the map to the appropriate location and zoom for a given BoundingBox
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#setBounds"> mxn.Mapstraction#setBounds /a>
	 * @param minLongitude longitude (degrees) of the lower-left corner of the bounding box you want the map to show
	 * @param minLatitude latitude (degrees) of the lower-left corner of the bounding box you want the map to show
	 * @param maxLongitude longitude (degrees) of the upper-right corner of the bounding box you want the map to show
	 * @param maxLatitude latitude (degrees) of the upper-right corner of the bounding box you want the map to show
	 */
	public void setBounds(double minLongitude, double minLatitude, double maxLongitude, double maxLatitude); 
		
	/**
	 * Sets the central point of the map
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#setCenter"> mxn.Mapstraction#setCenter </a>
	 * @param lat latitude (in degrees) of the point at which to center the map
	 * @param lon longitude (in degrees) of the point at which to center the map
	 */
	public void setCenter(double lat, double lon);

	/**
	 * Sets the central point of the map
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#setCenter"> mxn.Mapstraction#setCenter </a>
	 * @param lat latitude (in degrees) of the point at which to center the map
	 * @param lon longitude (in degrees) of the point at which to center the map
	 * @param pan weather to pan or not
	 */
	public void setCenter(double lat, double lon, boolean pan);

	/**
	 * Sets the central point of the map
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#setCenter"> mxn.Mapstraction#setCenter </a>
	 * @param lat latitude (in degrees) of the point at which to center the map
	 * @param lon longitude (in degrees) of the point at which to center the map
	 * @param zoom zoom level
	 * @param pan weather to pan or not
	 */
	public void setCenter(double lat, double lon, int zoom, boolean pan);
	
	/**
	 * Centers the map to some place and zoom level
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#setCenterAndZoom"> mxn.Mapstraction#setCenterAndZoom </a>
	 * @param lat latitude (in degrees) of the point at which to center the map
	 * @param lon longitude (in degrees) of the point at which to center the map
	 * @param zoom The zoom level where 0 is all the way out.
	 */
	public void setCenterAndZoom(double lat, double lon, int zoom); 
	
	/**
	 * Sets the imagery type for the map
	 * <pre>
	 * The type can be one of:
	 *  	Mapstraction.ROAD = 1;
	 *  	Mapstraction.SATELLITE = 2;
	 *  	Mapstraction.HYBRID = 3;
	 * </pre>
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#setMapType"> mxn.Mapstraction#setMapType </a>
	 * @param type The imagery type for the map: 1 (ROAD), 2(SATELLITE) or 3 (HYBRID) 
	 */
	public void setMapType(int type); 
	
	/**
	 * Sets the zoom level for the map
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#setZoom"> mxn.Mapstraction#setZoom </a>
	 * @param zoom The (native to the map) level zoom the map to.
	 */
	public void setZoom(int zoom);
	
	/**
	 * Turns a Tile Layer on or off
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#toggleTileLayer"> mxn.Mapstraction#toggleTileLayer </a>
	 * @param tile_url url of the tile layer that was created.
	 */
	public void toggleTileLayer(String tile_url);
	
	/**
	 * Adds controls to the map. You specify which controls to add in
	 * the associative array that is the only argument.
	 * addControls can be called multiple time, with different args, to dynamically change controls.
	 *
	 * args = {
	 *	 pan:		true,
	 *	 zoom:	 	'large' || 'small',
	 *	 overview: 	true,
	 *	 scale:		true,
	 *	 map_type: 	true,
	 * }
	 *
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#addControls"> Mapstraction.prototype.addControls </a> 
	 * @param controlsMap Which controls to switch on
	 */
	public void addControls(HashMap<String, String> controlsMap);
	
	/**
	 * Enable/disable dragging of the map
	 * 
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#dragging"> Mapstraction.prototype.dragging </a>
	 * @param drag Flag that enables/disables dragging of the map
	 */
	public void dragging(boolean drag);
	
	/**
	 * Sets the current options to those specified in oOpts and applies them
	 * 
	 * <pre>
	 * option defaults
	 * 	this.options = {
	 * 	enableScrollWheelZoom: false,
	 * 	enableDragging: true
	 * };
	 * </pre>
	 * 
	 * @param optionsMap map of options to set
	 */
	public void setOptions(HashMap<String, String> optionsMap);
	
	/**
	 * Adds a marker to the map
	 * 
	 * @param lat latitude (in degrees) of the point at which to center the map
	 * @param lon longitude (in degrees) of the point at which to center the map
	 * @param optionsMap Optional parameters: [options.pan Whether the map should move to the locations using a pan or just jump straight there]
	 * <pre>
	 * 	label 
	 *  infoBubble html/text content for a bubble popup for the marker
	 *  iconUrl The URL of the image you want to be the icon
	 *  iconSize The array size in pixels of the marker image (I.e. [27,31])
	 *  iconAnchor The array offset of the anchor point
	 *  draggable 
	 *  hover set to true if marker should display info on hover
	 *  hoverIcon The URL of the image you want to be the icon on hover
	 *  openBubble
	 *  groupName Markers are grouped up by this name. declutterGroup makes use of this.
	 * </pre>
	 * @return markerId Unique identifier of the marker
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#addMarker"> Mapstraction.prototype.addMarker </a>
	 */
	public JSObject addMarker(JSObject marker);
	
	
	/**
	 * Removes a Marker from the map
	 * 
	 * @param markerId Identifier of the marker to remove
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#removeMarker"> Mapstraction.prototype.removeMarker </a>
	 */
	public void removeMarker(JSObject marker);
	
	
	/**
	 * Hide the marker with the specified Id
	 * 
	 * @param markerId Unique identifier of the marker
	 * @see <a href="http://www.mapstraction.com/doc/Marker.html#hide"> http://www.mapstraction.com/doc/Marker.html#hide </a>
	 */
	public void hideMarker(JSObject marker);
	
	/**
	 * Show the marker with the specified Id
	 * 
	 * @param markerId Unique identifier of the marker
	 * @see <a href="http://www.mapstraction.com/doc/Marker.html#show"> http://www.mapstraction.com/doc/Marker.html#show </a>
	 */
	public void showMarker(JSObject marker);
	
	/**
	 * Open the info bubble of the marker with the specified Id
	 * 
	 * @param markerId Unique identifier of the marker
	 * @see <a href="http://www.mapstraction.com/doc/Marker.html#openBubble"> http://www.mapstraction.com/doc/Marker.html#openBubble </a>
	 */
	public void openMarkerBubble(JSObject marker);
	
	/**
	 * Removes all the Markers on the map
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#removeAllMarkers">http://www.mapstraction.com/doc/Mapstraction.html#removeAllMarkers</a>
	 */
	public void removeAllMarkers();
	
	/**
	 * Declutter the markers on the map, group together overlapping markers.
	 * @param optionsMap Declutter options map
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#declutterMarkers">http://www.mapstraction.com/doc/Mapstraction.html#declutterMarkers</a>
	 */
	public void declutterMarkers(HashMap<String, String> optionsMap);
	
	/**
	 * Add a polyline to the map
	 * @param pointsArray array of points
	 * @param optionsMap options
	 * <pre>
	 * Available options are:
	 * 	color The color of the polyline in the form: #RRGGBB
	 * 	width The width of the polyline
	 * 	opacity A float between 0.0 and 1.0
	 * 	closed (boolean) Marks the polyline as a closed polygon
	 * 	fillColor Fill color for a closed polyline as HTML color value e.g. #RRGGBB
	 * </pre>
	 * @return polylineId Unique identifier of the polyline
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#addPolyline">http://www.mapstraction.com/doc/Mapstraction.html#addPolyline</a>
	 */
	public int addPolyline(ArrayList<Position> pointsArray, HashMap<String, String> optionsMap);
	
	/**
	 * Removes a polyline from the map
	 * 
	 * @param polylineId Identifier of the polyline to remove
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#removePolyline"> http://www.mapstraction.com/doc/Mapstraction.html#removePolyline </a>
	 */
	public void removePolyline(int polylineId);
	
	/**
	 * Hide the polyline with the specified Id
	 * 
	 * @param polylineId Unique identifier of the polyline
	 * @see <a href="http://www.mapstraction.com/doc/Polyline.html#hide"> http://www.mapstraction.com/doc/Polyline.html#hide </a>
	 */
	public void hidePolyline(int polylineId);
	
	/**
	 * Show the polyline with the specified Id
	 * 
	 * @param polylineId Unique identifier of the polyline
	 * @see <a href="http://www.mapstraction.com/doc/Polyline.html#show"> http://www.mapstraction.com/doc/Polyline.html#show </a>
	 */
	public void showPolyline(int polylineId);
	
	/**
	 * Layers a georeferenced image over the map
	 * @param id unique DOM identifier
	 * @param src url of image
	 * @param opacity opacity 0-100
	 * @param west west boundary
	 * @param south south boundary
	 * @param east east boundary
	 * @param north north boundary
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#addImageOverlay"> http://www.mapstraction.com/doc/Mapstraction.html#addImageOverlay </a>
	 */
	public void addImageOverlay(String id, String src, double opacity, double west, double south, double east, double north);
	
	/**
	 * Adds a Tile Layer to the map
	 *
	 * <pre>
	 * Requires providing a parameterized tile url. Use {Z}, {X}, and {Y} to specify where the parameters
	 *  should go in the URL.
	 *
	 * For example, the OpenStreetMap tiles are:
	 *  m.addTileLayer("http://tile.openstreetmap.org/{Z}/{X}/{Y}.png", 1.0, "OSM", 1, 19, true);
	 * </pre>
	 *
	 * @param tile_url template url of the tiles.
	 * @param opacity opacity of the tile layer - 0 is transparent, 1 is opaque. (default=0.6)
	 * @param copyright_text copyright text to use for the tile layer. (default=Mapstraction)
	 * @param min_zoom Minimum (furtherest out) zoom level that tiles are available (default=1)
	 * @param max_zoom Maximum (closest) zoom level that the tiles are available (default=18)
	 * @param map_type Should the tile layer be a selectable map type in the layers palette (default=false)
	 * @see <a href="http://www.mapstraction.com/doc/Mapstraction.html#addTileLayer"> http://www.mapstraction.com/doc/Mapstraction.html#addTileLayer </a>
	 */
	public void addTileLayer(String tile_url, double opacity, String copyright_text, int min_zoom, int max_zoom, int map_type);
	
}
