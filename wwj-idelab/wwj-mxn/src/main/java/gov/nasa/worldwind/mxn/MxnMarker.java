package gov.nasa.worldwind.mxn;

import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.AnnotationLayer;
import gov.nasa.worldwind.layers.IconLayer;
import gov.nasa.worldwind.render.GlobeAnnotation;
import gov.nasa.worldwind.render.UserFacingIcon;
import gov.nasa.worldwind.render.WWIcon;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.imageio.ImageIO;

import netscape.javascript.JSException;
import netscape.javascript.JSObject;


public class MxnMarker extends UserFacingIcon {

	Logger logger = Logger.getLogger("MxnMarker");
	
	private static final String DEFAULT_MARKER_URL = "http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png";
	
	private JSObject jsMarker = null;
	
	private String infoBubble = null;
	private String iconUrl = null;
	private String iconSize = null;
	private String iconAnchor = null;
	private boolean draggable = false;
	private boolean hover = false;
	private String hoverIcon = null;
	private boolean openBubble = false;
	private String groupName = null;
	
	boolean isBubbleInfoVisible = false;
	
	GlobeAnnotation bubble = null;
	
	private IconLayer iconLayer = null;
	private AnnotationLayer bubbleLayer = null;

	
	public MxnMarker(JSObject jsmarker, IconLayer iconLayer, AnnotationLayer bubbleLayer) {
		
		this.jsMarker = jsmarker;
		this.iconLayer = iconLayer;
		this.bubbleLayer = bubbleLayer;
		
		JSObject jsLocation = (JSObject) jsmarker.getMember("location"); 
		
		Object latObj = jsLocation.getMember("lat");
		Double lat = new Double(latObj.toString());
		
		Object lonObj = jsLocation.getMember("lon");
		Double lon = new Double(lonObj.toString());
		
		this.setPosition(Position.fromDegrees(lat, lon, 0));
		
		try {
			String label = (String) jsmarker.getMember("labelText");
			setToolTipText(label);
		} catch (JSException e2) {
		
		}
		
		String infoBubble = "";
		try {
			infoBubble = (String) jsmarker.getMember("infoBubble");
		} catch (JSException e2) {
	
		}
		
		setInfoBubble(infoBubble);
		bubble = new GlobeAnnotation(getInfoBubble(), getPosition());
		
		String iconUrl = null;
		try {
			iconUrl = (String) jsmarker.getMember("iconUrl");
		} catch (JSException e) {
			if(logger.isLoggable(Level.INFO)) {
				logger.info("Marker has no iconUrl. Using default marker image: "+DEFAULT_MARKER_URL);
				iconUrl = DEFAULT_MARKER_URL;
			}
		}
		setIconUrl(iconUrl);
		
		URL url = null;
		try {
			url = new URL(iconUrl);
		} catch (MalformedURLException e) {
			logger.warning(iconUrl+" is not a valid URL. "+e.getMessage());
			logger.info("Using default marker image: "+DEFAULT_MARKER_URL);
			try {
				url = new URL(DEFAULT_MARKER_URL);
			} catch (MalformedURLException e1) {
				logger.severe("Default marker url {"+DEFAULT_MARKER_URL+"} is not a valid URL. "+e1.getLocalizedMessage());
				return;
			}
		}
		BufferedImage bufferedImage = null;
		try {
			ImageIO.setUseCache(false); // cache stored in memory to avoid applet security issues
			bufferedImage = ImageIO.read(url);
		} catch (IOException e) {
			logger.warning(e.getMessage());
			logger.severe("Image icon could not be read from url "+url+". "+ e.getMessage());
			return;
		}
		setImageSource(bufferedImage);
		
		String iconSize = null;
		try {
			iconSize = (String) jsmarker.getMember("iconSize");
			setIconSize(iconSize);
		} catch (JSException e) {
			
		}		
		
		String iconAnchor = null;
		try {
			iconAnchor = (String) jsmarker.getMember("iconAnchor");
			setIconAnchor(iconAnchor);
		} catch (JSException e) {

		}
		
		Boolean draggable = null;
		try {
			draggable = (Boolean) jsmarker.getMember("draggable");
		} catch (JSException e) {
	
		}
		setDraggable( draggable!=null ? draggable.booleanValue() : false );
		
		Boolean hover = null;
		try {
			hover = (Boolean) jsmarker.getMember("hover");
		} catch (JSException e) {

		}
		setHover( hover!=null ? hover.booleanValue() : false );
		
		try {
			String hoverIcon = (String) jsmarker.getMember("hoverIcon");
			setHoverIcon(hoverIcon);
		} catch (JSException e) {

		}
		
		
//		Boolean openBubble = (Boolean) jsmarker.eval("openInfoBubble");
//		setHover( openBubble!=null ? openBubble.booleanValue() : false );
		
		try {
			String groupName = (String) jsmarker.getMember("groupName");
			setGroupName(groupName);
		} catch (JSException e) {

		}
		
	}
	
	public String getInfoBubble() {
		return infoBubble;
	}

	public void setInfoBubble(String infoBubble) {
		this.infoBubble = infoBubble;
	}

	public String getIconUrl() {
		return iconUrl;
	}

	public void setIconUrl(String iconUrl) {
		this.iconUrl = iconUrl;
	}

	public String getIconSize() {
		return iconSize;
	}

	public void setIconSize(String iconSize) {
		this.iconSize = iconSize;
	}

	public String getIconAnchor() {
		return iconAnchor;
	}

	public void setIconAnchor(String iconAnchor) {
		this.iconAnchor = iconAnchor;
	}

	public boolean isDraggable() {
		return draggable;
	}

	public void setDraggable(boolean draggable) {
		this.draggable = draggable;
	}

	public boolean isHover() {
		return hover;
	}

	public void setHover(boolean hover) {
		this.hover = hover;
	}

	public String getHoverIcon() {
		return hoverIcon;
	}

	public void setHoverIcon(String hoverIcon) {
		this.hoverIcon = hoverIcon;
	}

	public boolean isOpenBubble() {
		return openBubble;
	}

	public void setOpenBubble(boolean openBubble) {
		this.openBubble = openBubble;
	}

	public String getGroupName() {
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}
	
	public JSObject getJsMarker() {
		return jsMarker;
	}

	public void setJsMarker(JSObject jsMarker) {
		this.jsMarker = jsMarker;
	}
	
	
	
	public boolean isBubbleInfoVisible() {
		return isBubbleInfoVisible;
	}

	public void setBubbleInfoVisible(boolean isBubbleInfoVisible) {
		this.isBubbleInfoVisible = isBubbleInfoVisible;
	}

	public void showLabel() {
		this.setShowToolTip(true);
	}
	
	public void hideLabel() {
		this.setShowToolTip(false);
	}
	
	public void openInfoBubble() {
        
		if(bubble.getText()!=null && !bubble.getText().isEmpty() && !isBubbleInfoVisible()) {
			bubbleLayer.addAnnotation(bubble);
			setBubbleInfoVisible(true);
		}
	}
	
	/**
	 * Close bubble info
	 */
	public void closeInfoBubble() {
        
		if(isBubbleInfoVisible()) {
			bubbleLayer.removeAnnotation(bubble);
			setBubbleInfoVisible(false);
//			wwd.redrawNow();
		}
	}

	@Override
	public void setVisible(boolean visible) {
		
		if(visible) {
			if(!isVisible() && isBubbleInfoVisible) {
				openInfoBubble();
			}
		} else {
			closeInfoBubble();
			hideLabel();
		}
		super.setVisible(visible);
	}
	
	/**
	 * Remove marker (Removes icon from IconLayer and Bubble from AnnotationLayer)
	 */
	public void remove() {
		
		logger.info("Removing marker");
		
		if(iconLayer!=null) {
			Iterator<WWIcon> iconIt = iconLayer.getIcons().iterator();
			while (iconIt.hasNext()) {
				WWIcon wwIcon = (WWIcon) iconIt.next();
				logger.info("before> icon at "+wwIcon.getPosition());
			}
			iconLayer.removeIcon(this);
			bubbleLayer.removeAnnotation(bubble);
			iconIt = iconLayer.getIcons().iterator();
			while (iconIt.hasNext()) {
				WWIcon wwIcon = (WWIcon) iconIt.next();
				logger.info("after> icon at "+wwIcon.getPosition());
			}
		}
	}

	@Override
	public String toString() {
		
		StringBuffer s = new StringBuffer();
		s.append("Marker {"+getJsMarker().getMember("pllID")+":\n");
		s.append("location: "+getPosition()+"\n");
		s.append("label: "+getToolTipText()+"\n");
		s.append("infoBubble: "+infoBubble+"\n");
		s.append("iconUrl: "+iconUrl+"\n");
		s.append("iconSize: "+iconSize+"\n");
		s.append("iconAnchor: "+iconAnchor+"\n");
		s.append("draggable: "+draggable+"\n");
		s.append("hover: "+hover+"\n");
		s.append("hoverIcon: "+hoverIcon+"\n");
		s.append("openBubble: "+openBubble+"\n");
		s.append("groupName: "+groupName+"\n");
		
		return s.toString();
	}

	@Override
	public boolean equals(Object obj) {
		
		if(!(obj instanceof MxnMarker)) return false;
		
		String id = (String) ((MxnMarker)obj).getJsMarker().getMember("pllID");
		String myId = (String) this.getJsMarker().getMember("pllID");
		
		boolean eq = myId.equals(id);
		return eq;
	}
}
