/*
Copyright (C) 2001 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/
package gov.nasa.worldwind.mxn.examples;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;

import sun.plugin.services.WPlatformService;

import gov.nasa.worldwind.BasicModel;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.event.SelectListener;
//import gov.nasa.worldwind.mxn.IDELabMxn;

/**
 * This is the most basic World Wind program.
 *
 * @version $Id: HelloWorldWind.java 4869 2008-03-31 15:56:36Z tgaskins $
 */
public class ControlsExample
{
    // An inner class is used rather than directly subclassing JFrame in the main class so
    // that the main can configure system properties prior to invoking Swing. This is
    // necessary for instance on OS X (Macs) so that the application name can be specified.

    protected static class AppFrame extends javax.swing.JFrame
    {

		public AppFrame()
        {			
		
            WorldWindowGLCanvas wwd = new WorldWindowGLCanvas();
            wwd.setModel(new BasicModel());		
            wwd.setPreferredSize(new java.awt.Dimension(1000, 800));
            this.getContentPane().add(wwd, java.awt.BorderLayout.CENTER);
            this.pack();
            
//            final IDELabMxn mxn = new IDELabMxn(wwd);
//            
//            mxn.addControls(new HashMap<String, String>());
            
            final boolean overview = true;
            
//            HashMap<String, String> controlsMap = new HashMap<String, String>();
//            controlsMap.put("pan", "false");
//            controlsMap.put("zoom", "false");
//            controlsMap.put("overview", "true");
//            controlsMap.put("compass", "true");
//            controlsMap.put("scale", "true");
//            controlsMap.put("look", "false");
//            controlsMap.put("heading", "false");
//            controlsMap.put("pitch", "false");
//            controlsMap.put("fov", "false");
//            controlsMap.put("ve", "false");
//            controlsMap.put("layers", "true");
//            controlsMap.put("terrain", "true");
//			mxn.addControls(controlsMap );
			
//			WorldWindowGLCanvas worldWindow = (WorldWindowGLCanvas) mxn.getWorldWindow();
//			
//			worldWindow.addMouseListener(new MouseListener() {
//				
//				boolean overview = false;
//				
//				public void mouseReleased(MouseEvent e) {
//					// TODO Auto-generated method stub
//					
//				}
//				
//				public void mousePressed(MouseEvent e) {
//					// TODO Auto-generated method stub
//					
//				}
//				
//				public void mouseExited(MouseEvent e) {
//					// TODO Auto-generated method stub
//					
//				}
//				
//				public void mouseEntered(MouseEvent e) {
//					// TODO Auto-generated method stub
//					
//				}
//				
//				public void mouseClicked(MouseEvent e) {
//					// TODO Auto-generated method stub
//					System.out.println("CLICKED");
//					HashMap<String, String> controlsMap = new HashMap<String, String>();
//					if(overview) {
//			            controlsMap.put("overview", "false");
//			            overview = false;
//					} else {
//						controlsMap.put("overview", "true");
//					}
////					mxn.addControls(controlsMap);
//				}
//			});
			
        }
    }

    public static void main(String[] args)
    {
        if (Configuration.isMacOS())
        {
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Hello World Wind");
        }

        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                // Create an AppFrame and immediately make it visible. As per Swing convention, this
                // is done within an invokeLater call so that it executes on an AWT thread.
                new AppFrame().setVisible(true);
            }
        });
    }
}