/*
Copyright (C) 2001 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/
package gov.nasa.worldwind.mxn.examples;

import java.awt.Color;
import java.util.ArrayList;
import java.util.HashMap;

import gov.nasa.worldwind.BasicModel;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sphere;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.BasicShapeAttributes;
import gov.nasa.worldwind.render.ExtrudedPolygon;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.Polyline;
import gov.nasa.worldwind.render.ShapeAttributes;
import gov.nasa.worldwind.render.airspaces.Airspace;
import gov.nasa.worldwind.render.airspaces.Box;
import gov.nasa.worldwind.render.airspaces.CappedCylinder;
import gov.nasa.worldwind.render.airspaces.Polygon;
import gov.nasa.worldwind.render.airspaces.SphereAirspace;
import gov.nasa.worldwind.render.airspaces.TrackAirspace;

/**
 * This is the most basic World Wind program.
 *
 * @version $Id: HelloWorldWind.java 4869 2008-03-31 15:56:36Z tgaskins $
 */
public class SphereExample
{
    // An inner class is used rather than directly subclassing JFrame in the main class so
    // that the main can configure system properties prior to invoking Swing. This is
    // necessary for instance on OS X (Macs) so that the application name can be specified.

    protected static class AppFrame extends javax.swing.JFrame
    {

		public AppFrame()
        {			
		
            WorldWindowGLCanvas wwd = new WorldWindowGLCanvas();
            wwd.setModel(new BasicModel());		
            wwd.setPreferredSize(new java.awt.Dimension(1000, 800));
            this.getContentPane().add(wwd, java.awt.BorderLayout.CENTER);
            this.pack();
                        
            //////
            
            
    		RenderableLayer featureLayer = new RenderableLayer();
    				           
    		// Sphere
            SphereAirspace sphere = new SphereAirspace();
            sphere.setLocation(LatLon.fromDegrees(0.0, -180.0));
            sphere.setAltitude(1000000.0);
            sphere.setTerrainConforming(false);
            sphere.setRadius(1000000.0);
            setupDefaultMaterial(sphere, Color.RED);
            featureLayer.addRenderable(sphere);
            
            // Cylinder.
            CappedCylinder cyl = new CappedCylinder();
            cyl.setCenter(LatLon.fromDegrees(0.0, 0.0));
            cyl.setRadius(3000000.0);
            cyl.setAltitudes(100000.0, 500000.0);
            cyl.setTerrainConforming(false, false);
            //cyl.setValue(DESCRIPTION, "3,000,000m Cylinder.");
            setupDefaultMaterial(cyl, Color.RED);
            featureLayer.addRenderable(cyl);
            
            RenderableLayer layer = new RenderableLayer();
            
            // EXTRUDEN POLYGON
            // Create and set an attribute bundle.
            ShapeAttributes sideAttributes = new BasicShapeAttributes();
            sideAttributes.setInteriorMaterial(Material.MAGENTA);
            sideAttributes.setOutlineOpacity(0.5);
            sideAttributes.setInteriorOpacity(0.5);
            sideAttributes.setOutlineMaterial(Material.GREEN);
            sideAttributes.setOutlineWidth(2);
            sideAttributes.setDrawOutline(true);
            sideAttributes.setDrawInterior(true);
            sideAttributes.setEnableLighting(true);

            ShapeAttributes sideHighlightAttributes = new BasicShapeAttributes(sideAttributes);
            sideHighlightAttributes.setOutlineMaterial(Material.WHITE);
            sideHighlightAttributes.setOutlineOpacity(1);

            ShapeAttributes capAttributes = new BasicShapeAttributes(sideAttributes);
            capAttributes.setInteriorMaterial(Material.YELLOW);
            capAttributes.setInteriorOpacity(0.8);
            capAttributes.setDrawInterior(true);
            capAttributes.setEnableLighting(true);

            // Create a path, set some of its properties and set its attributes.
            ArrayList<Position> pathPositions = new ArrayList<Position>();
            pathPositions.add(Position.fromDegrees(28, -106, 3e4));
            pathPositions.add(Position.fromDegrees(35, -104, 3e4));
            pathPositions.add(Position.fromDegrees(35, -107, 9e4));
            pathPositions.add(Position.fromDegrees(28, -107, 9e4));
            pathPositions.add(Position.fromDegrees(28, -106, 3e4));
            ExtrudedPolygon pgon = new ExtrudedPolygon(pathPositions);

            pathPositions.clear();
            pathPositions.add(Position.fromDegrees(29, -106.4, 4e4));
            pathPositions.add(Position.fromDegrees(30, -106.4, 4e4));
            pathPositions.add(Position.fromDegrees(29, -106.8, 7e4));
            pathPositions.add(Position.fromDegrees(29, -106.4, 4e4));
            pgon.addInnerBoundary(pathPositions);
            pgon.setAltitudeMode(WorldWind.RELATIVE_TO_GROUND);
            pgon.setSideAttributes(sideAttributes);
            pgon.setSideHighlightAttributes(sideHighlightAttributes);
            pgon.setCapAttributes(capAttributes);
            
            featureLayer.addRenderable(pgon);

//            ArrayList<LatLon> pathLocations = new ArrayList<LatLon>();
//            pathLocations.add(LatLon.fromDegrees(28, -110));
//            pathLocations.add(LatLon.fromDegrees(35, -108));
//            pathLocations.add(LatLon.fromDegrees(35, -111));
//            pathLocations.add(LatLon.fromDegrees(28, -111));
//            pathLocations.add(LatLon.fromDegrees(28, -110));
//            pgon = new ExtrudedPolygon(pathLocations, 6e4);
//            pgon.setSideAttributes(sideAttributes);
//            pgon.setSideHighlightAttributes(sideHighlightAttributes);
//            pgon.setCapAttributes(capAttributes);
//            layer.addRenderable(pgon);
            
            wwd.getModel().getLayers().add(featureLayer);
            wwd.redrawNow();
            
            //////
        }
    }
    
    protected static void setupDefaultMaterial(Airspace a, Color color)
    {
        Color outlineColor = makeBrighter(color);

        a.getAttributes().setDrawOutline(true);
        a.getAttributes().setMaterial(new Material(color));
        a.getAttributes().setOutlineMaterial(new Material(outlineColor));
        a.getAttributes().setOpacity(0.8);
        a.getAttributes().setOutlineOpacity(0.9);
        a.getAttributes().setOutlineWidth(3.0);
    }
    
    protected static Color makeBrighter(Color color)
    {
        float[] hsbComponents = new float[3];
        Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), hsbComponents);
        float hue = hsbComponents[0];
        float saturation = hsbComponents[1];
        float brightness = hsbComponents[2];

        saturation /= 3f;
        brightness *= 3f;

        if (saturation < 0f)
            saturation = 0f;

        if (brightness > 1f)
            brightness = 1f;

        int rgbInt = Color.HSBtoRGB(hue, saturation, brightness);

        return new Color(rgbInt);
    }

    public static void main(String[] args)
    {
        if (Configuration.isMacOS())
        {
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Hello World Wind");
        }

        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                // Create an AppFrame and immediately make it visible. As per Swing convention, this
                // is done within an invokeLater call so that it executes on an AWT thread.
                new AppFrame().setVisible(true);
            }
        });
    }
}