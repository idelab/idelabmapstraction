/*
Copyright (C) 2001 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/
package gov.nasa.worldwind.mxn.examples;

import gov.nasa.worldwind.BasicModel;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.mxn.wfs.WFSLayer;

/**
 * This is the most basic World Wind program.
 *
 * @version $Id: HelloWorldWind.java 4869 2008-03-31 15:56:36Z tgaskins $
 */
public class WFSOverlay
{
    // An inner class is used rather than directly subclassing JFrame in the main class so
    // that the main can configure system properties prior to invoking Swing. This is
    // necessary for instance on OS X (Macs) so that the application name can be specified.

    protected static class AppFrame extends javax.swing.JFrame
    {

		public AppFrame()
        {			
		
            WorldWindowGLCanvas wwd = new WorldWindowGLCanvas();
            wwd.setModel(new BasicModel());		
            wwd.setPreferredSize(new java.awt.Dimension(1000, 800));
            this.getContentPane().add(wwd, java.awt.BorderLayout.CENTER);
            this.pack();
            
            String getCapabilities = "http://geoserver.idelab.uva.es/geoserver/ows?service=WFS&request=GetCapabilities";
            
//            WFSLayer tasmaniaRoadsLayer = new WFSLayer(getCapabilities, "topp:tasmania_roads");
//            wwd.getModel().getLayers().add(tasmaniaRoadsLayer);
            
	          WFSLayer tasmaniaRoadsLayer = new WFSLayer(getCapabilities, "topp:states");
	          wwd.getModel().getLayers().add(tasmaniaRoadsLayer);
            
//            WFSLayer tasmaniaStatesLayer = new WFSLayer(getCapabilities, "topp:tasmania_state_boundaries");
//            wwd.getModel().getLayers().add(tasmaniaStatesLayer);
            
//            WFSLayer segmentosRio = new WFSLayer(getCapabilities, "idelab:Segmentos_Rio");
//            wwd.getModel().getLayers().add(segmentosRio);
        	
        }
    }

    public static void main(String[] args)
    {
        if (Configuration.isMacOS())
        {
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Hello World Wind");
        }

        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                // Create an AppFrame and immediately make it visible. As per Swing convention, this
                // is done within an invokeLater call so that it executes on an AWT thread.
                new AppFrame().setVisible(true);
            }
        });
    }
}