package gov.nasa.worldwind.mxn.applet;

//import gov.nasa.worldwind.AnaglyphSceneController;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.Model;
import gov.nasa.worldwind.StereoOptionSceneController;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.awt.ViewInputAttributes;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.custom.render.Movable3DModel;
//import gov.nasa.worldwind.custom.render.Person;
import gov.nasa.worldwind.custom.render.WWJArdor3DModel;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.event.SelectListener;
import gov.nasa.worldwind.formats.gpx.GpxReader;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.LatLon;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.AnnotationLayer;
import gov.nasa.worldwind.layers.CompassLayer;
import gov.nasa.worldwind.layers.IconLayer;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.LayerList;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.layers.ScalebarLayer;
import gov.nasa.worldwind.layers.TerrainProfileLayer;
import gov.nasa.worldwind.layers.ViewControlsLayer;
import gov.nasa.worldwind.layers.ViewControlsSelectListener;
import gov.nasa.worldwind.layers.WorldMapLayer;
import gov.nasa.worldwind.layers.placename.PlaceNameLayer;
import gov.nasa.worldwind.mxn.GeoRSSLayer;
import gov.nasa.worldwind.mxn.IdelabMxnMarker;
import gov.nasa.worldwind.mxn.MxnMarker;
import gov.nasa.worldwind.mxn.geom.MxnCylinder;
import gov.nasa.worldwind.mxn.geom.MxnFeature;
import gov.nasa.worldwind.mxn.geom.MxnPolygon3D;
import gov.nasa.worldwind.mxn.geom.MxnPolyline;
import gov.nasa.worldwind.mxn.geom.MxnPolyline3D;
import gov.nasa.worldwind.mxn.geom.MxnSphere;
import gov.nasa.worldwind.ogc.custom.kml.model.CustomKMLRoot;
import gov.nasa.worldwind.ogc.kml.KMLRoot;
import gov.nasa.worldwind.ogc.kml.impl.KMLController;
import gov.nasa.worldwind.ogc.wms.WMSCapabilities;
import gov.nasa.worldwind.ogc.wms.WMSLayerCapabilities;
import gov.nasa.worldwind.pick.PickedObject;
import gov.nasa.worldwind.pick.PickedObjectList;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.render.FrameFactory;
import gov.nasa.worldwind.render.GlobeAnnotation;
import gov.nasa.worldwind.render.Material;
import gov.nasa.worldwind.render.Polyline;
import gov.nasa.worldwind.render.Renderable;
import gov.nasa.worldwind.render.ScreenAnnotation;
import gov.nasa.worldwind.render.SurfaceImage;
import gov.nasa.worldwind.render.UserFacingIcon;
import gov.nasa.worldwind.render.WWIcon;
import gov.nasa.worldwind.render.airspaces.AbstractAirspace;
import gov.nasa.worldwind.util.BasicDragger;
import gov.nasa.worldwind.util.BrowserOpener;
import gov.nasa.worldwind.util.Logging;
import gov.nasa.worldwind.util.StatusBar;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.layertree.LayerTree;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;
import gov.nasa.worldwind.view.orbit.OrbitView;
import gov.nasa.worldwind.wms.WMSTiledImageLayer;
import gov.nasa.worldwindx.examples.kml.KMLViewController;
import gov.nasa.worldwindx.examples.util.HotSpotController;
import gov.nasa.worldwindx.examples.util.OpenStreetMapShapefileLoader;
import gov.nasa.worldwindx.examples.util.ShapefileLoader;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Point;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;
import java.net.SocketPermission;
import java.net.URI;
import java.net.URL;
import java.net.URLConnection;
import java.security.CodeSource;
import java.security.PermissionCollection;
import java.security.Permissions;
import java.security.Policy;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.PropertyPermission;
import java.util.Set;
import java.util.logging.Logger;

import javax.imageio.ImageIO;
import javax.swing.JApplet;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.stream.XMLStreamException;

import netscape.javascript.JSException;
import netscape.javascript.JSObject;

import org.xml.sax.SAXException;

/**
 * World Wind Java Applet for embedding in a web page. The applet
 * exposes to interact from Mapstraction JavaScript library.
 * Started from WWJApplet template by Patrick Murris {@link}
 *
 * @author Jordana Torres Gonzalez (alepitheia@hotmail.com)
 * @author Ricardo Garc�a Mart�n (garcia.martin.ricardo@gmail.com)
 */

public class IdelabMxnWorldwindApplet extends JApplet
{
	
	/**
	 * Logger for this class (logs can be monitored in java console)
	 */
	Logger logger = Logger.getLogger("IdelabMxnWorldwindApplet");
	
    private WorldWindowGLCanvas wwd;
    private RenderableLayer labelsLayer;
    
    //
    // JavaScript listener
    //
    JSObject jsl;
    
    //
    // String separators
    //
    
	public static final String MAP_PARAMS_SEP = ";;;"; // key1=value1;key2=value2;...
	public static final String MAP_KEY_VALUE_SEP = "==="; // key1=value1;key2=value2;...
	public static final String COORDS_SEP = ","; // i.e. lat;lon   minx;miny;maxx;maxy
	public static final String ARRAY_SEP = ";";
	
	//
    // Layers
    //
	
	private IconLayer markerLayer = null;
	private AnnotationLayer bubbleLayer = null;
	
	private ViewControlsLayer viewControlsLayer;
	private RenderableLayer _featureLayer = null; // this layer is intended to store geometries (point, line, polygon)
	
	//
	// Layer Manager (new)
	//
	
	protected LayerTree layerTree;
    protected RenderableLayer hiddenLayer;
    protected HotSpotController controller;
    
    //
    // Layer names
    //
    
	public static final String MARKER_LAYER_NAME = "MARKER_LAYER";
	public static final String BUBBLE_LAYER_NAME = "BUBBLE_LAYER";
    public static final String VIEW_CONTROLS_LAYER_NAME = "VIEW_CONTROLS_LAYER";
    public static final String FEATURE_LAYER_NAME = "FEATURE_LAYER";
    public static final String LAYER_MANAGER_LAYER_NAME = "LAYER_MANAGER_LAYER";
    
    //
    // File Extensions
    //
    
    public static final String KML_EXT = ".kml";
    public static final String KMZ_EXT = ".kmz"; 
    public static final String SHP_EXT = ".shp";
    public static final String GPX_EXT = ".gpx";
    public static final String DAE_EXT = ".dae";
    public static final String EXT_3DS = ".3ds";
    public static final String OBJ_EXT = ".obj";
    
    private ArrayList<MxnMarker> markers = new ArrayList<MxnMarker>();

    public IdelabMxnWorldwindApplet()
    {
    }

    public void init()
    {
    	
		Policy.setPolicy(new Policy() {
			public void refresh() {
			}

			public PermissionCollection getPermissions(CodeSource arg0) {
				Permissions perms = new Permissions();
//				perms.add(new AllPermission());
				perms.add(new SocketPermission("*:*", "connect,resolve"));
				perms.add(new PropertyPermission("gov.nasa.worldwind.stereo.mode", "read,write"));
				perms.add(new PropertyPermission("ardor3d.stats", "read"));
				perms.add(new PropertyPermission("ardor3d.trackDirect", "read"));
				perms.add(new PropertyPermission("ardor3d.useMultipleContexts", "read"));
				perms.add(new PropertyPermission("ardor3d.*", "read"));
				//perms.add(new java.security.AllPermission());
				
				return (perms);
			}
		});

        try
        {
            // Check for initial configuration values
            String value = getParameter("InitialLatitude");
            if (value != null)
                    Configuration.setValue(AVKey.INITIAL_LATITUDE, Double.parseDouble(value));
            value = getParameter("InitialLongitude");
            if (value != null)
                    Configuration.setValue(AVKey.INITIAL_LONGITUDE, Double.parseDouble(value));
            value = getParameter("InitialAltitude");
            if (value != null)
                    Configuration.setValue(AVKey.INITIAL_ALTITUDE, Double.parseDouble(value));
            value = getParameter("InitialHeading");
            if (value != null)
                    Configuration.setValue(AVKey.INITIAL_HEADING, Double.parseDouble(value));
            value = getParameter("InitialPitch");
            if (value != null)
                    Configuration.setValue(AVKey.INITIAL_PITCH, Double.parseDouble(value));
                        
            
            // Set AnaglyphSceneController for enabling stereo visualization
        	////Configuration.setValue(AVKey.SCENE_CONTROLLER_CLASS_NAME, AnaglyphSceneController.class.getName());
                    	
            // Create World Window GL Canvas
            this.wwd = new WorldWindowGLCanvas();
            this.getContentPane().add(this.wwd, BorderLayout.CENTER);
            
            // Set default scene controller to mono mode
            ////((AnaglyphSceneController)wwd.getSceneController()).setDisplayMode(AnaglyphSceneController.DISPLAY_MODE_MONO);
            System.setProperty("gov.nasa.worldwind.stereo.mode", "");
            
            // Create the default model as described in the current worldwind properties.
            Model m = (Model) WorldWind.createConfigurationComponent(AVKey.MODEL_CLASS_NAME);
            this.wwd.setModel(m);

            // Add the status bar
            StatusBar statusBar = new StatusBar();
            this.getContentPane().add(statusBar, BorderLayout.PAGE_END);

            // Forward events to the status bar to provide the cursor position info.
            statusBar.setEventSource(this.wwd);

            // Add a BMNG base layer to the model layer list, before the Blue Marble
            insertBeforeLayerName(this.wwd, makeBMNGImageLayer(), "Blue Marble");
            
            //
        	// Initialize layers
        	//
        	_featureLayer = getFeatureLayer();
        }
        catch (Throwable e)
        {
            e.printStackTrace();
        }
        
        //
        // Disable Click And Go Feature
        //
        ViewInputAttributes attrs = wwd.getView().getViewInputHandler().getAttributes();         
        attrs.getActionMap(ViewInputAttributes.DEVICE_MOUSE)
            .getActionAttributes(ViewInputAttributes.VIEW_MOVE_TO)
            .setMouseActionListener(null);
            
        
        wwd.addMouseListener(new MouseListener() {
			
        	Vec4 previousLocation = null;
        	
			public void mouseReleased(MouseEvent e) {
				
				Vec4 currentLocation = wwd.getView().getCenterPoint();
				if(currentLocation!=previousLocation) {
					System.out.println("Move end");
					jsl.call("actionPerformed", new String[] { "moveend", null });
				}		
			}
			
			public void mousePressed(MouseEvent e) {
				
				previousLocation = wwd.getView().getCenterPoint();
			}
			
			public void mouseExited(MouseEvent e) {}
			
			public void mouseEntered(MouseEvent e) {}
			
			public void mouseClicked(MouseEvent e) {
				
				Position loc = wwd.getCurrentPosition();
				PickedObjectList objectsAtCurrentPosition = getWW().getObjectsAtCurrentPosition();
				boolean clickOverTerrain = true;
				for (Iterator iterator = objectsAtCurrentPosition.iterator(); iterator.hasNext();) {
					PickedObject pickedObject = (PickedObject) iterator.next();
					logger.info("PICKED OBJECT: "+pickedObject.getObject().getClass().getSimpleName());
					// ignore events over the following objects
					if(	pickedObject.getObject() instanceof MxnMarker || 
						pickedObject.getObject() instanceof ScreenAnnotation || 
						pickedObject.getObject().getClass().getSimpleName().equals("LayerTreeNode")
					  ) {
						clickOverTerrain = false;
					}
				}
				
				if(clickOverTerrain && loc!=null) {
					logger.info("Mouse clicked at - "+loc);
					jsl.call("actionPerformed", new String[] { "click", getLatLonPointAsString(loc) });
				}
			}
		});
        
        
        wwd.addSelectListener(new SelectListener() {
			
			public void selected(SelectEvent event) {
								
				if (event.hasObjects()) {
					
					PickedObject topPickedObject = event.getTopPickedObject();
					
					if(topPickedObject!=null && event.getEventAction().equals(SelectEvent.LEFT_DOUBLE_CLICK)) {
						
						if (topPickedObject.isTerrain()) 
						{
							logger.info("Left click on terrain");
						} 
						
						else if (topPickedObject.getObject() instanceof MxnMarker) 
						{
							
							MxnMarker marker = (MxnMarker) topPickedObject.getObject();
							if (marker.isBubbleInfoVisible()) 
							{
								marker.closeInfoBubble();
								wwd.redrawNow();
							} else 
							{
								marker.openInfoBubble();
								wwd.redrawNow();
							}
						}
					}
					
					if(topPickedObject!=null && topPickedObject.getObject() instanceof MxnMarker && event.getEventAction().equals(SelectEvent.LEFT_CLICK)) {
						
						logger.info("Marker clicked");
						MxnMarker marker = (MxnMarker) topPickedObject.getObject();
						JSObject jsMarker = marker.getJsMarker();
						JSObject click = (JSObject) jsMarker.getMember("click");
						click.call("fire", null);
					}
					
					if(topPickedObject!=null && topPickedObject.getObject() instanceof MxnMarker && event.getEventAction().equals(SelectEvent.HOVER)) {
						
						MxnMarker marker = (MxnMarker) topPickedObject.getObject();
						marker.setHighlighted(true);
						marker.showLabel();
						wwd.redrawNow();
						try {
							Thread.sleep(3000);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						marker.hideLabel();
						wwd.redrawNow();
					}
					
					if(topPickedObject!=null && topPickedObject.getObject() instanceof MxnMarker && event.getEventAction().equals(SelectEvent.ROLLOVER)) {
						
						MxnMarker marker = (MxnMarker) topPickedObject.getObject();
						marker.setHighlighted(false);
					}
					
					if (event.getTopObject() instanceof GlobeAnnotation)
                    {
						GlobeAnnotation annotation = (GlobeAnnotation) event.getTopObject();
						if (event.getEventAction().equals(SelectEvent.LEFT_CLICK))
	                    {
	                        
	                        // Check for text or url
	                        PickedObject po = event.getTopPickedObject();
	                        if (po.getValue(AVKey.TEXT) != null)
	                        {
	                            logger.info("Text: \"" + po.getValue(AVKey.TEXT) + "\" Hyperlink: "
	                                + po.getValue(AVKey.URL));
	                            if (po.getValue(AVKey.URL) != null)
	                            {
	                                // Try to launch a browser with the clicked URL
	                                try
	                                {
	                                    BrowserOpener.browse(new URL((String) po.getValue(AVKey.URL)));
	                                }
	                                catch (Exception ignore)
	                                {
	                                }
	                            }
	                        }
	                    }
                    }
					
                    if (event.getEventAction().equals(SelectEvent.LEFT_CLICK) && event.getTopObject() instanceof WorldMapLayer)
                    {
                        logger.info("Left click on World Map : iterate view to target position");
                    }
				}				
			}
		});    
        
        
        // initialize layer manager
        this.layerTree = new LayerTree();

        // Set up a layer to display the on-screen layer tree in the WorldWindow.
		if(hiddenLayer==null)
		{
			hiddenLayer = new RenderableLayer();
			hiddenLayer.setName(LAYER_MANAGER_LAYER_NAME);
			this.hiddenLayer.addRenderable(this.layerTree);
	        
	        // Mark the layer as hidden to prevent it being included in the layer tree's model. Including the layer in
	        // the tree would enable the user to hide the layer tree display with no way of bringing it back.
	        this.hiddenLayer.setValue(AVKey.HIDDEN, true);
	        

	        // Refresh the tree model with the WorldWindow's current layer list.
	        this.layerTree.getModel().refresh(wwd.getModel().getLayers());

	        // Add a controller to handle input events on the layer tree.
	        this.controller = new HotSpotController(wwd);
		}
            
    }

    public void start()
    {
        // Call javascript appletStart()
        try
        {
            JSObject win = JSObject.getWindow(this);
            win.call("appletStart", null);
        }
        catch(Exception ignore) {}
    }

    public void stop()
    {
        // Call javascript appletSop()
        try
        {
            JSObject win = JSObject.getWindow(this);
            win.call("appletStop", null);
        }
        catch(Exception ignore) {}
        
        // Shut down World Wind
        WorldWind.shutDown();
    }
    
    //
    // LAYER UTILS
    //

    /**
     * Adds a layer to WW current layerlist, before a named layer.
     * Target name can be a part of the layer name
     * @param wwd the <code>WorldWindow</code> reference.
     * @param layer the layer to be added.
     * @param targetName the partial layer name to be matched - case sensitive.
     */
    public static void insertBeforeLayerName(WorldWindow wwd, Layer layer, String targetName)
    {
        // Insert the layer into the layer list just before the target layer.
        LayerList layers = wwd.getModel().getLayers();
        int targetPosition = layers.size() - 1;
        for (Layer l : layers)
        {
            if (l.getName().indexOf(targetName) != -1)
            {
                targetPosition = layers.indexOf(l);
                break;
            }
        }
        layers.add(targetPosition, layer);
    }    
    
	private List<Layer> getLayerByClass(Class c) {
		return wwd.getModel().getLayers().getLayersByClass(c);
	}
    
    public void removeLayerByName(String layerName) {
   
    	LayerList layers = wwd.getModel().getLayers();
    	Layer layerToRemove = null;
    	if((layerToRemove = layers.getLayerByName(layerName))!=null) {
    		layers.remove(layerToRemove);
    	}
    }
    
    private void removeLayer(Layer layer) {
    	   
    	LayerList layers = wwd.getModel().getLayers();
		layers.remove(layer);
    }
    
    static void insertOnTop(WorldWindow wwd, Layer layer)
    {
        LayerList layers = wwd.getModel().getLayers();
        layers.add(layer);
    }
    
    public static void insertBeforeCompass(WorldWindow wwd, Layer layer)
    {
        // Insert the layer into the layer list just before the compass.
        int compassPosition = 0;
        LayerList layers = wwd.getModel().getLayers();
        for (Layer l : layers)
        {
            if (l instanceof CompassLayer)
                compassPosition = layers.indexOf(l);
        }
        layers.add(compassPosition, layer);
    }

    public static void insertBeforePlacenames(WorldWindow wwd, Layer layer)
    {
        // Insert the layer into the layer list just before the placenames.
        int compassPosition = 0;
        LayerList layers = wwd.getModel().getLayers();
        for (Layer l : layers)
        {
            if (l instanceof PlaceNameLayer)
                compassPosition = layers.indexOf(l);
        }
        layers.add(compassPosition, layer);
    }

    public static void insertAfterPlacenames(WorldWindow wwd, Layer layer)
    {
        // Insert the layer into the layer list just after the placenames.
        int compassPosition = 0;
        LayerList layers = wwd.getModel().getLayers();
        for (Layer l : layers)
        {
            if (l instanceof PlaceNameLayer)
                compassPosition = layers.indexOf(l);
        }
        layers.add(compassPosition + 1, layer);
    }
    
    /**
     * Makes a BMNG base image layer using a SurfaceImage
     * @return BMNG base image layer
     */
    public RenderableLayer makeBMNGImageLayer()
    {
    	RenderableLayer layer = new RenderableLayer();
    	layer.setName("BMNG SurfaceImage");
    	layer.setPickEnabled(false);
    	layer.addRenderable(new SurfaceImage("images/BMNG_world.topo.bathy.200405.3.2048x1024.jpg",
    		Sector.FULL_SPHERE));
    	return layer;
    }

    // ============== Public API - Javascript ======================= //

    /**
     * Move the current view position
     * @param lat the target latitude in decimal degrees
     * @param lon the target longitude in decimal degrees
     */
    public void gotoLatLon(double lat, double lon)
    {
        this.gotoLatLon(lat, lon, Double.NaN, 0, 0);
    }

    /**
     * Move the current view position, zoom, heading and pitch
     * @param lat the target latitude in decimal degrees
     * @param lon the target longitude in decimal degrees
     * @param zoom the target eye distance in meters
     * @param heading the target heading in decimal degrees
     * @param pitch the target pitch in decimal degrees
     */
    public void gotoLatLon(double lat, double lon, double zoom, double heading, double pitch)
    {
        BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
        if(!Double.isNaN(lat) || !Double.isNaN(lon) || !Double.isNaN(zoom))
        {
            lat = Double.isNaN(lat) ? view.getCenterPosition().getLatitude().degrees : lat;
            lon = Double.isNaN(lon) ? view.getCenterPosition().getLongitude().degrees : lon;
            zoom = Double.isNaN(zoom) ? view.getZoom() : zoom;
            heading = Double.isNaN(heading) ? view.getHeading().degrees : heading;
            pitch = Double.isNaN(pitch) ? view.getPitch().degrees : pitch;
            view.addPanToAnimator(Position.fromDegrees(lat, lon, 0),
                    Angle.fromDegrees(heading), Angle.fromDegrees(pitch), zoom, true);
        }
    }


    /**
     * Set the current view zoom
     * @param zoom the target eye distance in meters
     */
    public void setZoom(double zoom)
    {
        BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
        if(!Double.isNaN(zoom))
        {
            view.addZoomAnimator(view.getZoom(), zoom);
        }
    }

    /**
     * Get the WorldWindowGLCanvas
     * @return the current WorldWindowGLCanvas
     */
    public WorldWindowGLCanvas getWW()
    {
        return this.wwd;
    }

    /**
     * Get the current OrbitView
     * @return the current OrbitView
     */
    public OrbitView getOrbitView()
    {
        if(this.wwd.getView() instanceof OrbitView)
            return (OrbitView)this.wwd.getView();
        return null;
    }

    /**
     * Get a reference to a layer with part of its name
     * @param layerName part of the layer name to match.
     * @return the corresponding layer or null if not found.
     */
    public Layer getLayerByName(String layerName)
    {
        for (Layer layer : wwd.getModel().getLayers())
            if (layer.getName().indexOf(layerName) != -1)
                return layer;
        return null;
    }

    /**
     * Add a text label at a position on the globe.
     * @param text the text to be displayed.
     * @param lat the latitude in decimal degrees.
     * @param lon the longitude in decimal degrees.
     * @param font a string describing the font to be used.
     * @param color the color to be used as an hexadecimal coded string.
     */
    public void addLabel(String text, double lat, double lon, String font, String color)
    {
        GlobeAnnotation ga = new GlobeAnnotation(text, Position.fromDegrees(lat, lon, 0),
            Font.decode(font), Color.decode(color));
        ga.getAttributes().setBackgroundColor(Color.BLACK);
        ga.getAttributes().setDrawOffset(new Point(0, 0));
        ga.getAttributes().setFrameShape(FrameFactory.SHAPE_NONE);
        ga.getAttributes().setEffect(AVKey.TEXT_EFFECT_OUTLINE);
        ga.getAttributes().setTextAlign(AVKey.CENTER);
        this.labelsLayer.addRenderable(ga);
    }
    
    public void addActionListener(final JSObject jsl) {
		
    	logger.info("Adding action listener");
    	
		this.jsl = jsl;
				
		try {
			jsl.call("actionPerformed", new String[] { "load", null });
		} catch (Exception e) {
			logger.warning("Error while notificating mapstraction about map loading event. "+e.getMessage());
		}
	}
    
	/**
	 * Convert LatLon position to string representation for passing to javascript
	 * @param pos position to convert
	 * @return string representation of the specified LatLon point 
	 */
	private String getLatLonPointAsString(LatLon pos) {
		
		return pos.getLatitude().degrees + COORDS_SEP + pos.getLongitude().degrees;
	}
	
	/**
	 * Gets string representation of bounding box 
	 * @param sector The bounding box
	 * @return String representation of bounding box
	 */
	private String getBoundsAsString(Sector sector) {
		
		String bounds = null;
    	double min_lat = 0, max_lat = 0, min_lon = 0, max_lon = 0;
    	
   		if(sector!=null) {
			min_lat = sector.getMinLatitude().getDegrees();
			max_lat = sector.getMaxLatitude().getDegrees();
			min_lon = sector.getMinLongitude().getDegrees();
			max_lon = sector.getMaxLongitude().getDegrees();
		}
   		bounds = min_lon+COORDS_SEP+min_lat+COORDS_SEP+max_lon+COORDS_SEP+max_lat;
   		return bounds;
	}
	
	public ArrayList<Position> getPositionArrayFromString(String points) {
		
		ArrayList<Position> pointArray = new ArrayList<Position>();
        
        String[] pointStrArray = points.trim().split(ARRAY_SEP); // lat1,lon1|lat2,lon2|lat3,lon3|...
        String[] coords = null;
        for (int i = 0; i < pointStrArray.length; i++) {
        	coords = pointStrArray[i].split(COORDS_SEP);
        	double lat = Double.valueOf(coords[0].trim()).doubleValue();
        	double lon = Double.valueOf(coords[1].trim()).doubleValue();
        	pointArray.add(Position.fromDegrees(lat, lon));
		}
        
        return pointArray;
	}
	
	
	/**
	 * Convert string map representation to java map
	 * key1===value1;;;key2===value2;...
	 * @return Map of parameters
	 */
	private HashMap<String, String> buildMapFromString(String mapStr) {
		
		HashMap<String, String> map = new HashMap<String, String>();
		
		if(mapStr!=null && mapStr.contains(MAP_PARAMS_SEP))
		{
			String[] params = mapStr.split(MAP_PARAMS_SEP);
			
			for (int i = 0; i < params.length; i++) {
				String param = params[i]; // keyX===valueX
				String[] key_value = param.split(MAP_KEY_VALUE_SEP);
				logger.info("> "+key_value[0]+"->"+key_value[1]);
				map.put(key_value[0], key_value[1]);
			}
		}
				
		return map;
	}
	
	
	public LatLon fromJSLatLonPoint(JSObject jsLatLonPoint) {
		
		Double lat = Double.valueOf(jsLatLonPoint.getMember("lat").toString()).doubleValue();
		Double lon = Double.valueOf(jsLatLonPoint.getMember("lon").toString()).doubleValue();
		
		LatLon latLon = LatLon.fromDegrees(lat, lon);
		
        return latLon;
	}
	
	public JSObject toJSLatLonPoint(LatLon latLon, JSObject jsLatLonPoint) {
		
		jsLatLonPoint.setMember("lat", latLon.latitude.degrees);
		jsLatLonPoint.setMember("lon", latLon.longitude.degrees);
		Double lat = (Double) jsLatLonPoint.getMember("lat");
		Double lon = (Double) jsLatLonPoint.getMember("lon");
			
        return jsLatLonPoint;
	}
		
	
    private Sector getVisibleSector() {
    	
    	Sector visibleSector = null;
        DrawContext drawContext = wwd.getSceneController().getDrawContext();
    	if(drawContext!=null) {
    		visibleSector = drawContext.getVisibleSector();
    	}
    	return visibleSector;
    }
    
    /**
     * Compute scale size in real world
     * @return meters per pixel
     */
    private double getPixelSize() {
    	
    	DrawContext dc = wwd.getSceneController().getDrawContext();
        Position referencePosition = dc.getViewportCenterPosition();
        double pixelSize = 0;
        if(referencePosition != null)
        {
            Vec4 groundTarget = dc.getGlobe().computePointFromPosition(referencePosition);
            Double distance = dc.getView().getEyePoint().distanceTo3(groundTarget);
            pixelSize = dc.getView().computePixelSizeAtDistance(distance);
        }
        return pixelSize;
    }
    
    private double getPixelSizeAtZoomLevel(double zoomLevel) {
    	
    	double pixelSize = getPixelSizeAtLevel0()/Math.pow(2, zoomLevel);
    	
        return pixelSize;   
    }
    
    private double getArcLength(double degrees) {
    	
    	double radius = this.wwd.getModel().getGlobe().getRadius();
    	Angle angle = Angle.fromDegrees(degrees);
    	return radius * angle.radians;
    }
    
    private double getPixelSizeAtLevel0() {
    	
    	return Math.PI * wwd.getModel().getGlobe().getRadius() /256; 
    }

    
    // ------------------------------------------------------------------------------- //

//    public void addControls(String controlsMap) {
//		
//		logger.info("controls: "+controlsMap);
//		HashMap<String, String> controls = buildMapFromString(controlsMap);
//		addControls(controls);
//	}
    
    public void addControls(JSObject jsControls) {
    	
    	logger.info("adding controls from jsobject");
    	
    	HashMap<String, Object> controlsMap = new HashMap<String, Object>();
	
		String controls[] = {"pan","zoom","look","heading","pitch","fov","ve","overview","scale","compass","layers","terrain"};
		
		for (int i = 0; i < controls.length; i++) {
			String c = controls[i];
			try {
				controlsMap.put(controls[i], jsControls.getMember(controls[i]));
			} catch (JSException e) {

			}
		}
		
		addControls(controlsMap);
    }
    
    public void addControls(HashMap<String, Object> controlsMap) {
		
		Iterator<String> it = controlsMap.keySet().iterator();
		while (it.hasNext()) {
			String key = it.next();
			Object val = controlsMap.get(key);
			logger.info("["+key+","+val+"]");
		}
		
		Boolean pan = false;
		Boolean zoom = false;
		Boolean look = false;
		Boolean heading = false;
		Boolean pitch = false;
		Boolean fov = false;
		Boolean ve = false;
		Boolean overview = false;
		Boolean scale = false;
		Boolean compass = false;
		Boolean layers = false;
		Boolean terrain = false;
		
		// Add view controls layer
		viewControlsLayer = (ViewControlsLayer) getLayerByName(VIEW_CONTROLS_LAYER_NAME);
		if(viewControlsLayer==null) {
			viewControlsLayer = new ViewControlsLayer();
			viewControlsLayer.setName(VIEW_CONTROLS_LAYER_NAME);
			insertOnTop(wwd, viewControlsLayer);
			this.wwd.addSelectListener(new ViewControlsSelectListener(this.wwd, viewControlsLayer));
		}
		if(controlsMap.containsKey("pan")) {
			logger.info("pan");
			try {
				pan = (Boolean) controlsMap.get("pan");
			} catch (ClassCastException e) {
				logger.warning(e.getMessage());
			}
		}
		if(controlsMap.containsKey("zoom")) {
			logger.info("zoom");
			try {
				zoom = (Boolean) controlsMap.get("zoom");
			} catch (ClassCastException e) {
				logger.warning(e.getMessage()+", zoom=["+controlsMap.get("zoom")+"]");
			}
		}
		if(controlsMap.containsKey("look")) {
			look = (Boolean) controlsMap.get("look");
		}
		if(controlsMap.containsKey("heading")) {
			heading = (Boolean) controlsMap.get("heading");
		}
		if(controlsMap.containsKey("pitch")) {
			pitch = (Boolean) controlsMap.get("pitch");
		}
		if(controlsMap.containsKey("fov")) {
			fov = (Boolean) controlsMap.get("fov");
		}
		if(controlsMap.containsKey("ve")) {
			ve = (Boolean) controlsMap.get("ve");
		}
		if(controlsMap.containsKey("overview")) {
			overview = (Boolean) controlsMap.get("overview");
		}
		if(controlsMap.containsKey("scale")) {
			scale = (Boolean) controlsMap.get("scale");
		}
		if(controlsMap.containsKey("compass")) {
			compass = (Boolean) controlsMap.get("compass");
		}
		if(controlsMap.containsKey("layers")) {
			layers = (Boolean) controlsMap.get("layers");
		}
		if(controlsMap.containsKey("terrain")) {
			terrain = (Boolean) controlsMap.get("terrain");
		}
		
		viewControlsLayer.setShowPanControls(pan);
		viewControlsLayer.setShowZoomControls(zoom);
		viewControlsLayer.setShowLookControls(look);
		viewControlsLayer.setShowHeadingControls(heading);
		viewControlsLayer.setShowPitchControls(pitch);
		viewControlsLayer.setShowFovControls(fov);
		viewControlsLayer.setShowVeControls(ve);
		
		if(overview) {
			showOverviewMap();
		}
		else 
		{
			hideOverviewMap();
		}
		if(scale) 
		{
			showBar();
		}
		else 
		{
			hideBar();
		}
		if(compass) 
		{
			showCompass();
		}
		else 
		{
			hideCompass();
		}
		if(layers) 
		{
			showLayerManager();
		}
		else 
		{
			hideLayerManager();
		}
		if(terrain) 
		{
			showTerrainProfiler();
		}
		else 
		{
			hideTerrainProfiler();
		}
		wwd.redrawNow();
	}
	
//	public void addControls(HashMap<String, String> controlsMap) {
//		
//		Iterator<String> it = controlsMap.keySet().iterator();
//		while (it.hasNext()) {
//			String key = it.next();
//			Object val = controlsMap.get(key);
//			logger.info(key+" , "+val);
//		}
//		
//		Boolean pan = false;
//		Boolean zoom = false;
//		Boolean look = false;
//		Boolean heading = false;
//		Boolean pitch = false;
//		Boolean fov = false;
//		Boolean ve = false;
//		Boolean overview = false;
//		Boolean scale = false;
//		Boolean compass = false;
//		Boolean layers = false;
//		Boolean terrain = false;
//		
//		// Add view controls layer
//		viewControlsLayer = (ViewControlsLayer) getLayerByName(VIEW_CONTROLS_LAYER_NAME);
//		if(viewControlsLayer==null) {
//			viewControlsLayer = new ViewControlsLayer();
//			viewControlsLayer.setName(VIEW_CONTROLS_LAYER_NAME);
//			insertOnTop(wwd, viewControlsLayer);
//			this.wwd.addSelectListener(new ViewControlsSelectListener(this.wwd, viewControlsLayer));
//		}
//		if(controlsMap.containsKey("pan")) {
//			logger.info("pan");
//			try {
//				pan = Boolean.valueOf(controlsMap.get("pan")).booleanValue();
//			} catch (ClassCastException e) {
//				logger.warning(e.getMessage());
//			}
//		}
//		if(controlsMap.containsKey("zoom")) {
//			logger.info("zoom");
//			try {
//				zoom = Boolean.valueOf(controlsMap.get("zoom")).booleanValue();
//			} catch (ClassCastException e) {
//				logger.warning(e.getMessage()+", zoom=["+controlsMap.get("zoom")+"]");
//			}
//		}
//		if(controlsMap.containsKey("look")) {
//			look = Boolean.valueOf(controlsMap.get("look")).booleanValue();
//		}
//		if(controlsMap.containsKey("heading")) {
//			heading = Boolean.valueOf(controlsMap.get("heading")).booleanValue();
//		}
//		if(controlsMap.containsKey("pitch")) {
//			pitch = Boolean.valueOf(controlsMap.get("pitch")).booleanValue();
//		}
//		if(controlsMap.containsKey("fov")) {
//			fov = Boolean.valueOf(controlsMap.get("fov")).booleanValue();
//		}
//		if(controlsMap.containsKey("ve")) {
//			ve = Boolean.valueOf(controlsMap.get("ve")).booleanValue();
//		}
//		if(controlsMap.containsKey("overview")) {
//			overview = Boolean.valueOf(controlsMap.get("overview")).booleanValue();
//		}
//		if(controlsMap.containsKey("scale")) {
//			scale = Boolean.valueOf(controlsMap.get("scale")).booleanValue();
//		}
//		if(controlsMap.containsKey("compass")) {
//			compass = Boolean.valueOf(controlsMap.get("compass")).booleanValue();
//		}
//		if(controlsMap.containsKey("layers")) {
//			layers = Boolean.valueOf(controlsMap.get("layers")).booleanValue();
//		}
//		if(controlsMap.containsKey("terrain")) {
//			terrain = Boolean.valueOf(controlsMap.get("terrain")).booleanValue();
//		}
//		
//		viewControlsLayer.setShowPanControls(pan);
//		viewControlsLayer.setShowZoomControls(zoom);
//		viewControlsLayer.setShowLookControls(look);
//		viewControlsLayer.setShowHeadingControls(heading);
//		viewControlsLayer.setShowPitchControls(pitch);
//		viewControlsLayer.setShowFovControls(fov);
//		viewControlsLayer.setShowVeControls(ve);
//		if(overview) {
//			showOverviewMap();
//		}
//		else 
//		{
//			hideOverviewMap();
//		}
//		if(scale) 
//		{
//			showBar();
//		}
//		else 
//		{
//			hideBar();
//		}
//		if(compass) 
//		{
//			showCompass();
//		}
//		else 
//		{
//			hideCompass();
//		}
//		if(layers) 
//		{
//			showLayerManager();
//		}
//		else 
//		{
//			hideLayerManager();
//		}
//		if(terrain) 
//		{
//			showTerrainProfiler();
//		}
//		else 
//		{
//			hideTerrainProfiler();
//		}
//		wwd.redrawNow();
//	}
	
	
	private void showTerrainProfiler() {
		 
		List<Layer> layers = getLayerByClass(TerrainProfileLayer.class);
		if(layers==null || layers.isEmpty()) {
			TerrainProfileLayer tpl = new TerrainProfileLayer();
			tpl.setEventSource(wwd);
			insertOnTop(wwd, tpl);
		}
	}
	
	private void hideTerrainProfiler() {
		
		List<Layer> layers = getLayerByClass(TerrainProfileLayer.class);
		if(layers!=null && !layers.isEmpty()) {
			TerrainProfileLayer tpl = (TerrainProfileLayer) layers.get(0);
			removeLayer(tpl);
		}
	}
	
	private void showLayerManager() {
				
		if(wwd.getModel().getLayers().getLayerByName(LAYER_MANAGER_LAYER_NAME)==null)
				wwd.getModel().getLayers().add(this.hiddenLayer);
        
//        // Size the World Window to take up the space typically used by the layer panel. This illustrates the
//        // screen space gained by using the on-screen layer tree.
//        Dimension size = new Dimension(1000, 600);
//        this.setPreferredSize(size);
//        this.pack();
        
//		LayerList layers = wwd.getModel().getLayers();
//				
//		RenderableLayer layerManager = (RenderableLayer) layers.getLayerByName(LAYER_MANAGER_LAYER_NAME);
//		
//		if(layerManager==null) {
//			layerManager = new RenderableLayer();
//			layerManager.setName(LAYER_MANAGER_LAYER_NAME);
//			
//			LayerList layerList = new LayerList();
//			
//			for (Iterator iterator = layers.iterator(); iterator.hasNext();) {
//				Layer layer = (Layer) iterator.next();
//				//if(layer instanceof WMSTiledImageLayer || layer instanceof BasicTiledImageLayer) {
//				if(layer instanceof WMSTiledImageLayer || layer instanceof WorldMapLayer) {
//					layerList.add(layer);
//				}
//			}
//			
//	        LayerSelector selector = new LayerSelector(layerList);
//	        
//	        layerManager.addRenderable(selector);
//	        BasicTreeController controller = new BasicTreeController(wwd, selector);
//	        insertOnTop(wwd, layerManager);
//		}
	}
	
	private void hideLayerManager() {
		
		logger.info("Hide Layer Manager");
		RenderableLayer layerManager = (RenderableLayer) getLayerByName(LAYER_MANAGER_LAYER_NAME);
		
		if(layerManager!=null) {
			removeLayer(layerManager);
		}
	}	
	
	private void showBar() {
		
		List<Layer> scaleBarLayers = getLayerByClass(ScalebarLayer.class);
		if(scaleBarLayers==null || scaleBarLayers.isEmpty()) {
			ScalebarLayer scaleBarLayer = new ScalebarLayer();
			insertOnTop(wwd, scaleBarLayer);
		}
	}
	
	private void hideBar() {
		
		List<Layer> scaleBarLayers = getLayerByClass(ScalebarLayer.class);
		if(scaleBarLayers!=null && !scaleBarLayers.isEmpty()) {
			ScalebarLayer scaleBarLayer = (ScalebarLayer) scaleBarLayers.get(0);
			removeLayer(scaleBarLayer);
		}
	}
	
	private void showOverviewMap() {
		
		logger.info("show overview");
		//logLayers();
		removeLayerByName("World Map");
		List<Layer> worldMapLayers = getLayerByClass(WorldMapLayer.class);
		if(worldMapLayers==null || worldMapLayers.isEmpty()) {
			logger.info("Entra");
			WorldMapLayer worldMapLayer = new WorldMapLayer();
			insertBeforePlacenames(wwd, worldMapLayer);
		} else {
			logger.info("NOOOOO Entra");
		}
		//logLayers();
	}
	
	private void logLayers() {
		LayerList layers = wwd.getModel().getLayers();
		logger.info("*****************************************************");
		for (Iterator iterator = layers.iterator(); iterator.hasNext();) {
			Layer layer = (Layer) iterator.next();
			logger.info(layer.getName()+", "+layer.getClass() +", "+layer.isEnabled()+", "+layer.getOpacity());
		}
	}
	
	private void hideOverviewMap() {
		
		logger.info("hide overview");
		List<Layer> worldMapLayers = getLayerByClass(WorldMapLayer.class);
		if(worldMapLayers!=null && !worldMapLayers.isEmpty()) {
			WorldMapLayer worldMapLayer = (WorldMapLayer) worldMapLayers.get(0);
			removeLayer(worldMapLayer);
		}
	}
	
	private void showCompass() {
		
		logger.info("show compass");
		LayerList layers = wwd.getModel().getLayers();
		for (Layer layer : layers) {
			logger.info("> "+layer.getName()+", "+layer.getClass().getName());
		}
		Layer compassLayer = getLayerByName("Compass");
		if(compassLayer==null) {
			compassLayer = new CompassLayer();
			logger.info("compass on top");
			wwd.getModel().getLayers().add(compassLayer);
			//insertOnTop(wwd, compassLayer);
		} else {
			logger.info("No entra");
		}
	}
	
	private void hideCompass() {
		
		logger.info("hide compass");
		Layer compassLayer = getLayerByName("Compass");
		if(compassLayer!=null) {
			removeLayer(compassLayer);
		}
	}

	public void addImageOverlay(String divId, String src, double opacity,
			double west, double south, double east, double north) {
		
		logger.info("adding image overlay");
		
		URL imgUrl = WWIO.makeURL(src);
		
		BufferedImage bufferedImage = null;
	
		try {
			bufferedImage = ImageIO.read(imgUrl);
		} catch (IOException e) {
			logger.severe("Image icon could not be read from url "+imgUrl+". " + e);
			return;
		}
		
		Sector sector = new Sector(Angle.fromDegrees(south), Angle.fromDegrees(north), Angle.fromDegrees(west), Angle.fromDegrees(east));
		
        SurfaceImage si1 = new SurfaceImage(bufferedImage, sector);
            
        Polyline boundary = new Polyline(si1.getCorners(), 0);
        boundary.setFollowTerrain(true);
        boundary.setClosed(true);
        boundary.setPathType(Polyline.RHUMB_LINE);
        boundary.setColor(new Color(0, 255, 0));

        RenderableLayer layer = new RenderableLayer();
        layer.setName(divId);
 
        layer.setPickEnabled(false);
        layer.addRenderable(si1);
        layer.addRenderable(boundary);

        insertOnTop(wwd, layer);
		
	}
	
	public void removeImageOverlay(String divId) {
		
		removeLayerByName(divId);
	}

//	public void addLargeControls() {
//		
//		HashMap<String, String> controlsMap = new HashMap<String, String>();
//		controlsMap.put("pan", "true");
//		controlsMap.put("zoom", "true");
//		controlsMap.put("look", "true");
//		controlsMap.put("heading", "true");
//		controlsMap.put("pitch", "true");
//		controlsMap.put("fov", "true");
//		controlsMap.put("ve", "true");
//		controlsMap.put("overview", "true");
//		controlsMap.put("scale", "true");
//		controlsMap.put("compass", "true");
//
//		addControls(controlsMap);
//	}
	
	public void addLargeControls() {
		
		HashMap<String, Object> controlsMap = new HashMap<String, Object>();
		controlsMap.put("pan", true);
		controlsMap.put("zoom", true);
		controlsMap.put("look", true);
		controlsMap.put("heading", true);
		controlsMap.put("pitch", true);
		controlsMap.put("fov", true);
		controlsMap.put("ve", true);
		controlsMap.put("overview", true);
		controlsMap.put("scale", true);
		controlsMap.put("compass", true);

		addControls(controlsMap);
	}

	public void addMapTypeControls() {
		// TODO Auto-generated method stub
		
	}
	
	public JSObject addMarker(JSObject marker) {
		
		logger.info("adding marker from jsobject");
		try {
			MxnMarker mxnMarker = new MxnMarker(marker, getMarkerLayer(), getBubbleLayer());
			markers.add(mxnMarker);
			getMarkerLayer().addIcon(mxnMarker);
			wwd.redrawNow();
			logger.info(mxnMarker.toString());
		} catch(Exception e) {
			logger.warning(e.getMessage());
		}

		return marker;
	}

	public int addMarker(double lat, double lon,
			HashMap<String, String> optionsMap) {
		
		String label = (String) optionsMap.get("label");
		String infoBubble = (String) optionsMap.get("infoBubble");
		String iconUrl = (String) optionsMap.get("iconUrl");
    	
    	IdelabMxnMarker marker = null;
    	
    	Position position = Position.fromDegrees(lat, lon, 0);
    	
    	if(iconUrl==null || "null".equalsIgnoreCase("iconURL")) { 
    		// use default marker icon
    		try {
				marker = new IdelabMxnMarker(wwd, position);
			} catch (IOException e) {
				e.printStackTrace();
			}	
    	} else {
    		// use custom marker icon from url
    		try {
				marker = new IdelabMxnMarker(wwd, iconUrl, position);
			} catch (IOException e) {
				e.printStackTrace();
			}
    	}
    	
    	if(label!=null && !label.equalsIgnoreCase("null")) {
    		marker.setLabel(label);
    	}
    	
    	if(infoBubble!=null && !infoBubble.equalsIgnoreCase("null")) {
    		marker.setBubbleInfo(infoBubble);
    	}
    	wwd.redrawNow();
    	    	    	
    	return marker.hashCode();
	}

	public void addOverlay(String url, boolean autoCenterAndZoom) {
		
		logger.info("adding overlay from url: "+url);
		
		Layer layer = null;
		URL ovlURL = WWIO.makeURL(url);
		if(ovlURL!=null) 
		{
			if(url.endsWith(KML_EXT) || url.endsWith(KMZ_EXT)) 
			{
	    		layer = makeKMLLayer(ovlURL);
	    	} 
			else if(url.endsWith(SHP_EXT))
			{
				layer = this.makeShapefileLayer(ovlURL);
			}
			else if(url.endsWith(GPX_EXT))
			{
				layer = this.makeGpxLayer(ovlURL);
			}
			else {
				layer = makeGeoRssLayer(ovlURL);
	    	}
			if(layer!=null) {
				layer.setName(url);
				BasicDragger dragger = new BasicDragger(wwd);
				wwd.addSelectListener(dragger);
				layer.setPickEnabled(true);
				insertOnTop(wwd, layer);
			}
		}
	}
	
	//
	// Shape Overlay
	//
	
	protected Layer makeShapefileLayer(Object source)
    {
		if (OpenStreetMapShapefileLoader.isOSMPlacesSource(source))
        {
            return OpenStreetMapShapefileLoader.makeLayerFromOSMPlacesSource(source);
        }
        else
        {
            ShapefileLoader loader = new ShapefileLoader();
            return loader.createLayerFromSource(source);
        }
    }
		
	//
	// GPX Overlay
	//
	
	protected Layer makeGpxLayer(URL url)
    {
        try
        {
        	URLConnection urlConn = url.openConnection();
        	InputStream inputStream = urlConn.getInputStream();
        	
            GpxReader reader = new GpxReader();
            reader.readStream(inputStream);
            Iterator<Position> positions = reader.getTrackPositionIterator();
            
            UserFacingIcon icon = null;
            
            IconLayer gpxLayer = new IconLayer();

            while (positions.hasNext())
            {
            	icon = new UserFacingIcon("images/marker.png", positions.next());
            	gpxLayer.addIcon(icon);
            }

            return gpxLayer;
        }
        catch (ParserConfigurationException e)
        {
            e.printStackTrace();
        }
        catch (SAXException e)
        {
            e.printStackTrace();
        }
        catch (IOException e)
        {
            e.printStackTrace();
        }

        return null;
    }
	
    //
    // KML Overlay
    //
    
    public RenderableLayer makeKMLLayer(URL kml_url) {
    	
    	//
    	//
    	
    	KMLRoot kmlRoot = null;
		try {
			kmlRoot = CustomKMLRoot.create(kml_url);
		} catch (IOException e) {
			logger.warning(e.getMessage());
		}
        if (kmlRoot == null)
        {
            String message = Logging.getMessage("generic.UnrecognizedSourceTypeOrUnavailableSource", kml_url);
            throw new IllegalArgumentException(message);
        }

        try {
			kmlRoot.parse();
		} catch (XMLStreamException e) {
			logger.warning(e.getMessage());
		}
    	
    	//
    	//
    	
//    	KMLRoot kmlRoot = null;
//		try {
//			kmlRoot = KMLRoot.createAndParse(kml_url);
//		} catch (IOException e) {
//			logger.warning(e.getMessage());
//		} catch (XMLStreamException e) {
//			logger.warning(e.getMessage());
//		}

        // Set the document's display name
        kmlRoot.setField(AVKey.DISPLAY_NAME, kml_url.toString());

        // Create a KMLController to adapt the KMLRoot to the World Wind renderable interface.
        KMLController kmlController = new KMLController(kmlRoot);

        // Adds a new layer containing the KMLRoot to the end of the WorldWindow's layer list. This
        // retrieves the layer name from the KMLRoot's DISPLAY_NAME field.
        RenderableLayer kmlLayer = new RenderableLayer();
        kmlLayer.setName((String) kmlRoot.getField(AVKey.DISPLAY_NAME));
        kmlLayer.addRenderable(kmlController);

        KMLViewController viewController = KMLViewController.create(this.wwd);
        
        if (kmlRoot.getNetworkLinkControl() != null
            && kmlRoot.getNetworkLinkControl().getView() != null)
        {
            if (viewController != null)
            	viewController.goTo(kmlRoot.getNetworkLinkControl().getView());
        }
        else if (kmlRoot.getFeature() != null
            && kmlRoot.getFeature().getView() != null)
        {
            if (viewController != null)
            	viewController.goTo(kmlRoot.getFeature().getView());
        }
    	
        return kmlLayer;
    }
    
    //
    // GeoRSS Overlay
    //
    
    private GeoRSSLayer makeGeoRssLayer(URL geoRssURL) {
		
		GeoRSSLayer geoRssLayer = null;
		try {
			geoRssLayer = new GeoRSSLayer(geoRssURL);
		} catch (IOException e) {
			logger.severe(e.getMessage());
		}
		return geoRssLayer;
	}
    
	//
	// Collada Overlay
	//
	
	protected Layer makeColladaLayer(URL url, Position pos)
    {
		
		logger.info("Adding collada layer from url: "+url.toString());
		
		RenderableLayer layer = new RenderableLayer();		
		
		try {
//			Person person = new Person();
//			person.setName("Pepe");
//			logger.info(person.getName());
			WWJArdor3DModel model = new WWJArdor3DModel(url.toString(), pos);
			logger.info("model created");
			layer.addRenderable(model);
			logger.info("model added to layer");
		} catch (Exception e) {
			logger.warning(e.getMessage());
		}

        return layer;
    }
	
	//
	// 3Ds Overlay
	//
	
	protected Layer makeMovable3DLayer(String url, Position pos, double size)
    {
		logger.info("Making movable 3d layer from url: "+url);
		RenderableLayer layer = new RenderableLayer();		
		layer.addRenderable(new Movable3DModel(url, pos, size));
        return layer;
    }
    
	public void addColladaModel(String url, JSObject jsPoint3D, boolean autoCenterAndZoom) {
		
		logger.info("adding collada model from url: "+url);
								
		Position pos = getPositionFromJS(jsPoint3D);
		
		Layer layer = null;
		URL colladaURL = WWIO.makeURL(url);
		if(colladaURL!=null) 
		{
			if(url.endsWith(DAE_EXT))
			{
				layer = this.makeColladaLayer(colladaURL, pos);
			}
			if(layer!=null) {
				layer.setName(url);
				BasicDragger dragger = new BasicDragger(wwd);
				wwd.addSelectListener(dragger);
				layer.setPickEnabled(true);
				insertOnTop(wwd, layer);
			}
		}
		
		if(autoCenterAndZoom) {
			setCenterAndAltitude(pos.getLatitude().degrees, pos.getLongitude().degrees, pos.getAltitude()+500);
		}
	}
	
	public void addMovable3DModel(String url, JSObject jsPoint3D, double size, boolean autoCenterAndZoom) {
		
		logger.info("adding Movable3D Model from url: "+url);
								
		Position pos = getPositionFromJS(jsPoint3D);
		
		Layer layer = null;
		
		if(url.endsWith(EXT_3DS) || url.endsWith(OBJ_EXT))
		{
			layer = this.makeMovable3DLayer(url, pos, size);
		}
		if(layer!=null) {
			layer.setName(url);
			BasicDragger dragger = new BasicDragger(wwd);
			wwd.addSelectListener(dragger);
			layer.setPickEnabled(true);
			insertOnTop(wwd, layer);
		}
		
		if(autoCenterAndZoom) {
			setCenterAndAltitude(pos.getLatitude().degrees, pos.getLongitude().degrees, pos.getAltitude()+500);
		}
	}
	
	private Position getPositionFromJS(JSObject jsPoint) {
		
		Object latObj = null;
		Object lonObj = null; 
		Object altObj = null;
		
		Double lat = 0.0;
		Double lon = 0.0;
		Double alt = 0.0;
		
		try {
			latObj = jsPoint.getMember("lat");
			lat = new Double(latObj.toString());
		} catch (JSException e) {
			e.printStackTrace();
		}
		
		
		try {
			lonObj = jsPoint.getMember("lon");
			lon = new Double(lonObj.toString());
		} catch (JSException e) {
			e.printStackTrace();
		}
		
		try {
			altObj = jsPoint.getMember("alt");
			alt = new Double(altObj.toString());
		} catch (JSException e) {
			e.printStackTrace();
		}
		
		return Position.fromDegrees(lat, lon, alt);
	}
    
    public JSObject addPolyline(JSObject jsPolyline) {
		
    	logger.info("adding polyline");
    	
    	Boolean closed = false;
    	
    	String fid = (String) jsPolyline.getMember("pllID");
		JSObject jsPoints = (JSObject) jsPolyline.getMember("points");
		try {
			closed = (Boolean) jsPolyline.getMember("closed");
		} catch (JSException e) {
			e.printStackTrace();
		}
		Integer length = (Integer) jsPoints.getMember("length");
		logger.info("length: "+length);
		
		ArrayList<Position> points = new ArrayList<Position>();
		
		for (int i = 0; i < length; i++) {
			
			JSObject jsPoint = (JSObject) jsPoints.getSlot(i);
			
			Object latObj = jsPoint.getMember("lat");
			Double lat = new Double(latObj.toString());
			
			Object lonObj = jsPoint.getMember("lon");
			Double lon = new Double(lonObj.toString());
			
			points.add(Position.fromDegrees(lat, lon, 0));
		}
		
//		for (Position position : points) {
//			logger.info("> point: "+position);
//		}
		
		MxnPolyline polyline = new MxnPolyline(fid);
		polyline.setFollowTerrain(true);
		
        polyline.setPositions(points);
        polyline.setClosed(closed);
        
        polyline.setColor(Color.ORANGE);
        polyline.setLineWidth(3);
          
        RenderableLayer featureLayer = getFeatureLayer();
        featureLayer.addRenderable(polyline);
		wwd.redrawNow();
		
		return jsPolyline;
	}
    
    public JSObject addSphere(JSObject jsSphere) {
		
    	logger.info("addSphere");
    	
    	String fid = (String) jsSphere.getMember("pllID");
		JSObject jsCenter = (JSObject) jsSphere.getMember("center");
		
		Double radius = new Double(jsSphere.getMember("radius").toString());
						
		Object latObj = jsCenter.getMember("lat");
		Double lat = new Double(latObj.toString());
		
		Object lonObj = jsCenter.getMember("lon");
		Double lon = new Double(lonObj.toString());
		
		Object altObj = jsCenter.getMember("alt");
		Double alt = new Double(altObj.toString());
				
		Color color = Color.RED;
		try {
			String colorString = jsSphere.getMember("color").toString();
			if(colorString!=null) {
				color = fromRGB(colorString);
			}
		} catch (JSException e) {
			logger.info(e.getMessage());
		}
			
		Double opacity = 1.0;		
		try {
			opacity = new Double(jsSphere.getMember("opacity").toString());
		} catch (JSException e) {
			logger.info(e.getMessage());
		}
		
		Double width = 1.0;		
		try {
			width = new Double(jsSphere.getMember("width").toString());
		} catch (JSException e) {
			logger.info(e.getMessage());
		}
					
		logger.info("lat: "+lat);
		logger.info("lon: "+lon);
		logger.info("alt: "+alt);
			
			
		// Sphere
        MxnSphere sphere = new MxnSphere();
        sphere.setFid(fid);
        sphere.setLocation(Position.fromDegrees(lat, lon, alt));
        sphere.setAltitude(alt);
        sphere.setTerrainConforming(false);
        sphere.setRadius(radius);
        
        Color outlineColor = makeBrighter(color);

        sphere.getAttributes().setDrawOutline(true);
        sphere.getAttributes().setMaterial(new Material(color));
        sphere.getAttributes().setOutlineMaterial(new Material(outlineColor));
        sphere.getAttributes().setOpacity(opacity);
        sphere.getAttributes().setOutlineOpacity(opacity);
        sphere.getAttributes().setOutlineWidth(width);
        
        sphere.getAttributes().setOutlineMaterial(new Material(color));
        sphere.getAttributes().setOutlineOpacity(opacity);
        sphere.getAttributes().setOpacity(opacity);
        
        RenderableLayer featureLayer = getFeatureLayer();
        featureLayer.addRenderable(sphere);
	
		wwd.redrawNow();
		
		return jsSphere;
	}
    
    public JSObject addCylinder(JSObject jsCylinder) {
		
    	logger.info("addCylinder");
    	
    	String fid = (String) jsCylinder.getMember("pllID");
		JSObject jsCenter = (JSObject) jsCylinder.getMember("center");
		
		Double radius = new Double(jsCylinder.getMember("radius").toString());
		Double height = new Double(jsCylinder.getMember("height").toString());
						
		Object latObj = jsCenter.getMember("lat");
		Double lat = new Double(latObj.toString());
		
		Object lonObj = jsCenter.getMember("lon");
		Double lon = new Double(lonObj.toString());
		
		Object altObj = jsCenter.getMember("alt");
		Double alt = new Double(altObj.toString());
				
		Color color = Color.RED;
		try {
			String colorString = jsCylinder.getMember("color").toString();
			if(colorString!=null) {
				color = fromRGB(colorString);
			}
		} catch (JSException e) {
			logger.info(e.getMessage());
		}
			
		Double opacity = 1.0;		
		try {
			opacity = new Double(jsCylinder.getMember("opacity").toString());
		} catch (JSException e) {
			logger.info(e.getMessage());
		}
		
		Double width = 1.0;		
		try {
			width = new Double(jsCylinder.getMember("width").toString());
		} catch (JSException e) {
			logger.info(e.getMessage());
		}
					
		logger.info("lat: "+lat);
		logger.info("lon: "+lon);
		logger.info("alt: "+alt);
			
			
		// Sphere
        MxnCylinder cylinder = new MxnCylinder();
        cylinder.setFid(fid);
        cylinder.setCenter(Position.fromDegrees(lat, lon, alt));
        cylinder.setAltitude(alt);
        cylinder.setAltitudes(alt, alt+height);
        cylinder.setTerrainConforming(false);
        cylinder.setRadius(radius);
        
        Color outlineColor = makeBrighter(color);

        cylinder.getAttributes().setDrawOutline(true);
        cylinder.getAttributes().setMaterial(new Material(color));
        cylinder.getAttributes().setOutlineMaterial(new Material(outlineColor));
        cylinder.getAttributes().setOpacity(opacity);
        cylinder.getAttributes().setOutlineOpacity(opacity);
        cylinder.getAttributes().setOutlineWidth(width);
        
        cylinder.getAttributes().setOutlineMaterial(new Material(color));
        cylinder.getAttributes().setOutlineOpacity(opacity);
        cylinder.getAttributes().setOpacity(opacity);
        
        RenderableLayer featureLayer = getFeatureLayer();
        featureLayer.addRenderable(cylinder);
	
		wwd.redrawNow();
		
		return jsCylinder;
	}
    
    public JSObject addPolyline3D(JSObject jsPolyline3D) {
		
    	logger.info("addPolygon3D");
    	
    	String fid = (String) jsPolyline3D.getMember("pllID");
		
    	JSObject jsPoints = (JSObject) jsPolyline3D.getMember("points");
		
		Integer length = (Integer) jsPoints.getMember("length");
		logger.info("length: "+length);
		
		ArrayList<Position> points = new ArrayList<Position>();
		
		for (int i = 0; i < length; i++) {
			
			JSObject jsPoint = (JSObject) jsPoints.getSlot(i);
			
			Object latObj = jsPoint.getMember("lat");
			Double lat = new Double(latObj.toString());
			
			Object lonObj = jsPoint.getMember("lon");
			Double lon = new Double(lonObj.toString());
			
			points.add(Position.fromDegrees(lat, lon, 0));
		}
		
		Double min_altitude = new Double(jsPolyline3D.getMember("min_altitude").toString());
		Double max_altitude = new Double(jsPolyline3D.getMember("max_altitude").toString());
		
		boolean closed = false;
		try {
			closed = new Boolean(jsPolyline3D.getMember("closed").toString());
		} catch (JSException e) {
			logger.info(e.getMessage());
		}
							
		Color color = Color.RED;
		try {
			String colorString = jsPolyline3D.getMember("color").toString();
			if(colorString!=null) {
				color = fromRGB(colorString);
			}
		} catch (JSException e) {
			logger.info(e.getMessage());
		}
			
		Double opacity = 1.0;		
		try {
			opacity = new Double(jsPolyline3D.getMember("opacity").toString());
		} catch (JSException e) {
			logger.info(e.getMessage());
		}
		
		Double width = 1.0;		
		try {
			width = new Double(jsPolyline3D.getMember("width").toString());
		} catch (JSException e) {
			logger.info(e.getMessage());
		}
		
		RenderableLayer featureLayer = getFeatureLayer();
		
		AbstractAirspace poly = null;
		if(closed) {
			// Polygon3D
			poly = new MxnPolygon3D();
			((MxnPolygon3D)poly).setLocations(points);
			
		} else {
			// Polyline3D
			poly = new MxnPolyline3D();
			((MxnPolyline3D)poly).setLocations(points);
		}
		
		((MxnFeature)poly).setFid(fid);
		
		poly.setAltitudes(min_altitude,max_altitude);
		poly.setTerrainConforming(false);
        
        Color outlineColor = makeBrighter(color);

        poly.getAttributes().setDrawOutline(true);
        poly.getAttributes().setMaterial(new Material(color));
        poly.getAttributes().setOutlineMaterial(new Material(outlineColor));
        poly.getAttributes().setOpacity(opacity);
        poly.getAttributes().setOutlineOpacity(opacity);
        poly.getAttributes().setOutlineWidth(width);
        
        poly.getAttributes().setOutlineMaterial(new Material(color));
        poly.getAttributes().setOutlineOpacity(opacity);
        poly.getAttributes().setOpacity(opacity);
        
        featureLayer.addRenderable(poly);
	
		wwd.redrawNow();
		
		return jsPolyline3D;
	}

	private RenderableLayer getFeatureLayer() {
		if(_featureLayer==null) {
			_featureLayer = (RenderableLayer) wwd.getModel().getLayers().getLayerByName(FEATURE_LAYER_NAME);
			if(_featureLayer==null) {
				_featureLayer = new RenderableLayer();
				_featureLayer.setName(FEATURE_LAYER_NAME);
				insertOnTop(wwd, _featureLayer);
			}
		}
		return _featureLayer;
	}
    
    protected static Color makeBrighter(Color color)
    {
        float[] hsbComponents = new float[3];
        Color.RGBtoHSB(color.getRed(), color.getGreen(), color.getBlue(), hsbComponents);
        float hue = hsbComponents[0];
        float saturation = hsbComponents[1];
        float brightness = hsbComponents[2];

        saturation /= 3f;
        brightness *= 3f;

        if (saturation < 0f)
            saturation = 0f;

        if (brightness > 1f)
            brightness = 1f;

        int rgbInt = Color.HSBtoRGB(hue, saturation, brightness);

        return new Color(rgbInt);
    }
    
    public void removeSphere(JSObject jsSphere) {
		
    	logger.info("remove sphere object.");
    	Renderable sphere = getFeature(jsSphere);
    	if(sphere!=null) {
    		getFeatureLayer().removeRenderable(sphere);
		}
    	wwd.redrawNow();
	}
    
    public void removeRenderable(JSObject jsObject) {
		
    	logger.info("remove renderable");
    	Renderable feature = getFeature(jsObject);
    	if(feature!=null) {
    		getFeatureLayer().removeRenderable(feature);
		}
    	wwd.redrawNow();
	}
    
	public void removeRenderable(int renderableId) {
				
		logger.info("remove renderable "+renderableId);
		Renderable renderable = getFeature(renderableId);
		if(renderable!=null) {
			logger.info("renderable is not null");
			getFeatureLayer().removeRenderable(renderable);
		}
		
		wwd.redrawNow();
	}

	public int addPolyline(ArrayList<Position> pointsArray,
			HashMap<String, String> optionsMap) {
		
		getFeatureLayer();
	
		Polyline polyline = new Polyline();
		polyline.setFollowTerrain(true);
		
        polyline.setPositions(pointsArray);
        
        if(optionsMap.containsKey("closed")) {
        	boolean closed = Boolean.valueOf(optionsMap.get("closed")).booleanValue();
        	polyline.setClosed(closed);
        }
        
        int width = 4; // default width
        if(optionsMap.containsKey("width")) {
        	width = Integer.valueOf(optionsMap.get("width")).intValue();
        }
        polyline.setLineWidth(width);
        
        if(optionsMap.containsKey("opacity")) {
        	double opacity = Double.valueOf(optionsMap.get("opacity")).doubleValue();
        	logger.info("Ignoring opacity value");
        }
        
        Color c = Color.BLUE; // default color
        if(optionsMap.containsKey("color")) { // #RRGGBB
        	String color = (String) optionsMap.get("color");
        	color = color.substring(1); // removes initial #
        	int colorInt = Integer.parseInt( color, 16);
        	c = new Color( colorInt );
            //Color c_inv = new Color( colorInt ^ 0x00FFFFFF );
        	polyline.setColor(c);
        }
                
        getFeatureLayer().addRenderable(polyline);
        wwd.redrawNow();
		return polyline.hashCode();
	}

//	public void addSmallControls() {
//		
//		HashMap<String, String> controlsMap = new HashMap<String, String>();
//		controlsMap.put("pan", "true");
//		controlsMap.put("zoom", "true");
//		controlsMap.put("look", "true");
//		controlsMap.put("heading", "true");
//		controlsMap.put("pitch", "true");
//		controlsMap.put("fov", "true");
//		controlsMap.put("ve", "true");
//		controlsMap.put("overview", "true");
//		controlsMap.put("scale", "true");
//		controlsMap.put("compass", "true");
//
//		addControls(controlsMap);
//	}
	
	public void addSmallControls() {
		
		HashMap<String, Object> controlsMap = new HashMap<String, Object>();
		controlsMap.put("pan", true);
		controlsMap.put("zoom", true);
		controlsMap.put("look", true);
		controlsMap.put("heading", true);
		controlsMap.put("pitch", true);
		controlsMap.put("fov", true);
		controlsMap.put("ve", true);
		controlsMap.put("overview", true);
		controlsMap.put("scale", true);
		controlsMap.put("compass", true);

		addControls(controlsMap);
	}

	public void addTileLayer(String tileUrl, double opacity,
			String copyrightText, int minZoom, int maxZoom, int mapType) {
		// TODO Auto-generated method stub
		
	}

	public void applyOptions(String optionsMap) {
		
		HashMap<String, String> options = buildMapFromString(optionsMap); 
		applyOptions(options);
	}

	public void applyOptions(HashMap<String, String> optionsMap) {
		// TODO Auto-generated method stub
		
	}

	public void declutterMarkers(HashMap<String, String> optionsMap) {
		// TODO Auto-generated method stub
		
	}

	public void dragging(boolean drag) {
		
		List<Layer> layers = getLayerByClass(BasicDragger.class);
		
		if(layers!=null && !layers.isEmpty()) {
			if(!drag) {
				for (Layer layer : layers) {
					removeLayer(layer);
				}
			}
		} else {
			if(drag) {
				BasicDragger dragger = new BasicDragger(wwd);
				wwd.addSelectListener(dragger);
			}
		}
	}

	public LatLon getCenter() {
		
        BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
        Position center = view.getCenterPosition();
		return center;
	}
	
	public JSObject getCenter(JSObject jsLatLonPoint) {
		
		LatLon center = getCenter();
        return toJSLatLonPoint(center, jsLatLonPoint);
	}

    /**
     * Returns the current latitude, longitude and altitude of the current cursor position, or <code>null</code> if the
     * cursor is not on the globe.
     *
     * @return The current position of the cursor, or <code>null</code> if the cursor is not positioned on the globe.
     */
	public LatLon getCurrentMousePosition() {
		
		return wwd.getCurrentPosition();
	}

//	public Sector getMapBounds() {
//		
//    	return getVisibleSector();
//	}
	
	public JSObject getMapBounds(JSObject jsBBox) {
				
		Sector s = getVisibleSector();
		JSObject sw = (JSObject) jsBBox.getMember("sw");
		JSObject ne = (JSObject) jsBBox.getMember("ne");
		
		sw.setMember("lat", s.getMinLatitude().degrees);
		sw.setMember("lon", s.getMinLongitude().degrees);
		ne.setMember("lat", s.getMaxLatitude().degrees);
		ne.setMember("lon", s.getMaxLongitude().degrees);
		
    	return jsBBox;
	}

	public int getMapType() {
		// TODO Auto-generated method stub
		return 0;
	}

	public double getPixelRatio() {
		// TODO Auto-generated method stub
		return 0;
	}

	public int getZoom() {
		
		return Math.round(Math.round(getZoomLevel(this.getPixelSize())));
	}

	public int getZoomLevelForBoundingBox(double minLongitude,
			double minLatitude, double maxLongitude, double maxLatitude) {
		// TODO Auto-generated method stub
		return 0;
	}



	
	public void hideMarker(JSObject jsMarker) {
		
		MxnMarker marker = getMarker(jsMarker);
		if(marker!=null) {
			marker.setVisible(false);
			wwd.redrawNow(); // force redraw
		}
	}
	
	public MxnMarker getMarker(JSObject jsMarker) {
		
		String markerID = null;
		try {
			markerID = (String) jsMarker.getMember("pllID");
		} catch (JSException e) {
			logger.warning("Marker has no identifier {pllID}. "+e.getMessage());
			return null;
		}
		if(markerLayer!=null) {
			for (Iterator<MxnMarker> iterator = markers.iterator(); iterator.hasNext();) {
				MxnMarker marker = (MxnMarker) iterator.next();
				JSObject jsm = marker.getJsMarker();
				String jsmid = (String) jsm.getMember("pllID");
				logger.info("> "+jsmid);
				if(jsmid.equals(markerID)) {
					return marker;
				}
			}
		}
		return null;
	}

	public void hidePolyline(int polylineId) {
		// TODO Auto-generated method stub
		
	}

	public void openMarkerBubble(JSObject jsMarker) {
		
		MxnMarker marker = getMarker(jsMarker);
		if(marker!=null) {
			marker.openInfoBubble();
			wwd.redrawNow(); // force redraw
		}
	}

	public void removeAllMarkers() {
		
		IconLayer markerLayer = (IconLayer) wwd.getModel().getLayers().getLayerByName(IdelabMxnMarker.MARKER_LAYER_NAME);
		if (markerLayer != null) {
			markerLayer.dispose();
		}
	}

	public void removeMarker(JSObject jsMarker) {
		
		logger.info("Removing mxn marker {"+jsMarker.getMember("pllID")+"}");
		MxnMarker marker = getMarker(jsMarker);
		if(marker!=null) {
			marker.remove();
			wwd.redrawNow(); // force redraw
			logger.info("MARKER REMOVED");
		} else {
			logger.info("MARKER NOT REMOVED - "+(String)jsMarker.getMember("pllID"));
		}
	}
	
	/**
	 * 
	 * @param markerId
	 * @return The marker with the specified hashcode or null if not found
	 */
	private IdelabMxnMarker getMarker(int markerId) {
		
		IconLayer markerLayer = (IconLayer) wwd.getModel().getLayers().getLayerByName(IdelabMxnMarker.MARKER_LAYER_NAME);
		if (markerLayer != null) {
			Iterable<WWIcon> markers = markerLayer.getIcons();
			for (WWIcon marker : markers) {
				if(marker.hashCode()==markerId) {
					return (IdelabMxnMarker) marker;
				}
			}
		}
		return null;
	}

	public void removeOverlay(String url) {
		
		removeLayerByName(url);
	}
	
    public void removePolyline(JSObject jsPolyline) {
		
		Renderable polyline = getFeature(jsPolyline);
		if(polyline!=null) {
			getFeatureLayer().removeRenderable(polyline);
		}
    	wwd.redrawNow();
	}

	public void removePolyline(int polylineId) {
		
//		if(featureLayer!=null) {
			Polyline polyline = (Polyline) getFeature(polylineId);
			if(polyline!=null) {
				getFeatureLayer().removeRenderable(polyline);
			}
//		}
		wwd.redrawNow();
	}
	
	Renderable getFeature(JSObject jsFeature) {
		
		String featureID = null;
		try {
			featureID = (String) jsFeature.getMember("pllID");
		} catch (JSException e) {
			logger.warning("Feature has no identifier {pllID}. "+e.getMessage());
			return null;
		}
		
		RenderableLayer featureLayer = getFeatureLayer();
		if (featureLayer != null) {
			logger.info("featureId: "+featureID);
			Iterable<Renderable> features = featureLayer.getRenderables();
			for (Renderable feature : features) {
				logger.info("> " + ((MxnFeature)feature).getFid());
				if(((MxnFeature)feature).getFid().equals(featureID)) {
					logger.info("feature found");
					return feature;
				}
			}
		}
		return null;
	}
	
	/**
	 * Get feature from feature layer
	 * @param hashcode
	 * @return
	 */
	Renderable getFeature(int hashcode) {
		
		RenderableLayer featureLayer = (RenderableLayer) wwd.getModel().getLayers().getLayerByName(FEATURE_LAYER_NAME);
		if (featureLayer != null) {
			Iterable<Renderable> features = featureLayer.getRenderables();
			for (Renderable feature : features) {
				logger.info("featureId: "+feature.hashCode());
				if(feature.hashCode()==hashcode) {
					return feature;
				}
			}
		}
		return null;
	}

	public void resizeTo(int width, int height) {
		// TODO Auto-generated method stub
		
	}

	public void setBounds(double minLongitude, double minLatitude,
			double maxLongitude, double maxLatitude) {
		
    	double deltaLat = maxLatitude - minLatitude;
    	double deltaLon = maxLongitude - minLongitude;
    	double delta = Math.max(deltaLat, deltaLon);
    	double viewportWidth = wwd.getView().getViewport().getWidth();
    	double pixelSize = getArcLength(delta)/viewportWidth;
    	double zoomLevel = this.getZoomLevel(pixelSize);
    	double zoom = getDistanceAtZoomLevel(zoomLevel);
    	double lat = (minLatitude+maxLatitude)/2; 
    	double lon = (minLongitude+maxLongitude)/2;
    	setCenter(lat, lon, zoom, true);
	}
	
	public void setCenter(double lat, double lon) {
		
		BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
    	double zoom = view.getZoom();
    	setCenter(lat, lon, zoom, false);
	}
	
	public void setCenterAndZoom(double lat, double lon, int zoom) {
    	
		double distance = getDistanceAtZoomLevel(zoom);
    	setCenter(lat, lon, distance, false);
    }
	
	public void setCenterAndAltitude(double lat, double lon, double altitude) {
		
		logger.info("setCenterAndAltitude");
		BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
		try {
			view.addPanToAnimator(Position.fromDegrees(lat, lon, 0),view.getHeading(), view.getPitch(), altitude, false);
		} catch(Exception e) {
			logger.warning(e.getMessage());
			view.setCenterPosition(Position.fromDegrees(lat, lon, 0));
			view.setZoom(altitude);
		}
	}
	
	public void setCenter(double lat, double lon, boolean pan) {
    	
		logger.info("panToCenter - "+pan);
		BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
    	double zoom = view.getZoom();
    	setCenter(lat, lon, zoom, pan);
    }
	
	public void setCenter(double lat, double lon, int zoom, boolean pan) {
		
		double distance = getDistanceAtZoomLevel(zoom);
		setCenter(lat, lon, distance, pan);
	}
	
//	public void setCenter(double lat, double lon, String optionsMap) {
//		
//		logger.info("setCenter - "+optionsMap);
//		HashMap<String, String> options = buildMapFromString(optionsMap); 
//		setCenter(lat, lon, options);
//	}
	
	public void setCenter(JSObject jsLatLonPoint, JSObject jsOptions) {

		LatLon center = fromJSLatLonPoint(jsLatLonPoint);

		
		HashMap<String, Object> optionsMap = new HashMap<String, Object>();
		
		String options[] = {"pan","zoom"};
		
		if(jsOptions!=null) {
			for (int i = 0; i < options.length; i++) {
				String c = options[i];
				try {
					optionsMap.put(options[i], jsOptions.getMember(options[i]));
				} catch (JSException e) {
	
				}
			}
		}
		
		setCenter(center.latitude.degrees, center.longitude.degrees, optionsMap);
	}
	
	public void setCenter(double lat, double lon, HashMap<String, Object> options) {
		
		logger.info("setCenter");
		
		Double zoom = null;
		Boolean pan = null;
		
		if(options!=null) {
			Set<String> keySet = options.keySet();
			for (String key : keySet) {
				logger.info(key+"->"+options.get(key));
			}
			
			if(options.containsKey("zoom")) {
				zoom = (Double) options.get("zoom");
			}
			if(options.containsKey("pan")) {
				pan = (Boolean) options.get("pan");
			}
		}
		if(zoom==null && pan==null) {
			setCenter(lat, lon);
		} else if(zoom!=null && pan==null) {
			setCenterAndZoom(lat, lon, Math.round(Math.round(zoom)));
		} else if(zoom==null && pan!=null) {
			setCenter(lat, lon, pan);
		} else if(zoom!=null && pan!=null) {
			setCenter(lat, lon, zoom, pan);
		}
	}
	
	public void setCenter(double lat, double lon, double zoom, boolean pan) {
		
		BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
        if(!Double.isNaN(lat) || !Double.isNaN(lon))
        {        	
            lat = Double.isNaN(lat) ? view.getCenterPosition().getLatitude().degrees : lat;
            lon = Double.isNaN(lon) ? view.getCenterPosition().getLongitude().degrees : lon;
            zoom = Double.isNaN(zoom) ? view.getZoom() : zoom;
            Position center = new Position(Angle.fromDegrees(lat), Angle.fromDegrees(lon), zoom);
            if(pan) {
            	view.addPanToAnimator(Position.fromDegrees(lat, lon, 0),view.getHeading(), view.getPitch(), zoom, true);
            } else {
            	view.setCenterPosition(center);
            	wwd.redrawNow(); // force redraw
            }
        }
	}
	
	private void setZoomLevel(double target_zoom)
    {
    	Angle fieldOfView = wwd.getView().getFieldOfView();
    	double viewportWidth = this.getOrbitView().getViewport().getWidth();
    	double pixelSizeScale = 2 * fieldOfView.tanHalfAngle() / (viewportWidth <= 0 ? 1d : viewportWidth);
    	double targetPixelSize = this.getPixelSizeAtZoomLevel(target_zoom);
        double distance = Math.abs(targetPixelSize/pixelSizeScale);
        this.setAltitude(distance);
    }
    
	private double getDistanceAtZoomLevel(double zoomLevel)
    {
    	Angle fieldOfView = wwd.getView().getFieldOfView();
    	double viewportWidth = this.getOrbitView().getViewport().getWidth();
    	double pixelSizeScale = 2 * fieldOfView.tanHalfAngle() / (viewportWidth <= 0 ? 1d : viewportWidth);
    	double targetPixelSize = this.getPixelSizeAtZoomLevel(zoomLevel);
        double distance = Math.abs(targetPixelSize/pixelSizeScale);
        return distance;
    }
    
    private double getZoomLevel() {
    	
    	return getZoomLevel(this.getPixelSize());
    }
    
    private double getZoomLevel(double pixelSize) {
    	
    	double scale = pixelSize>getPixelSizeAtLevel0() ? 0 : Math.round(Math.round(Math.log(getPixelSizeAtLevel0()/pixelSize)/Math.log(2)));
    	return scale;
    }

	public void setMapType(int type) {
		// TODO Auto-generated method stub
		
	}

	public void setOptions(String optionsMap) {
		
		HashMap<String, String> options = buildMapFromString(optionsMap); 
		setOptions(options);
	}

	public void setOptions(HashMap<String, String> optionsMap) {
		// TODO Auto-generated method stub
		
	}

	public void setZoom(int zoom) {
		
		Angle fieldOfView = wwd.getView().getFieldOfView();
    	double viewportWidth = this.getOrbitView().getViewport().getWidth();
    	double pixelSizeScale = 2 * fieldOfView.tanHalfAngle() / (viewportWidth <= 0 ? 1d : viewportWidth);
    	double targetPixelSize = this.getPixelSizeAtZoomLevel(zoom);
        double distance = Math.abs(targetPixelSize/pixelSizeScale);
        this.setAltitude(distance);
	}

	public void showMarker(JSObject jsMarker) {
		
		MxnMarker marker = getMarker(jsMarker);
		if(marker!=null) {
			marker.setVisible(true);
			wwd.redrawNow(); // force redraw
		}
	}

	public void showPolyline(int polylineId) {
		// TODO Auto-generated method stub
		
	}

	public void toggleTileLayer(String tileUrl) {
		
		LayerList layers = wwd.getModel().getLayers();
    	for (Layer l : layers)
        {
    		logger.info("> "+l.getName());
            if (l.getName().equals(tileUrl)){
            	layers.remove(l);
            }
        }
	}

    //
    // 3D Orientation
    //
    
    public double getPitch()
    {
        BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
        double pitch = view.getPitch().getDegrees();
		return pitch;
    }
    
    public void setPitch(double pitch)
    {
    	System.out.println("setPitch() - "+pitch+", "+Angle.fromDegrees(pitch));
        BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
        if(!Double.isNaN(pitch)) {
        	view.addPitchAnimator(view.getPitch(), Angle.fromDegrees(pitch));
        }
    }
    
    public double getHeading()
    {
        BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
        double heading = view.getHeading().getDegrees();
        return heading;
    }
    
    public void setHeading(double heading)
    {
        BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
        if(!Double.isNaN(heading)) {
        	view.addHeadingAnimator(view.getHeading(), Angle.fromDegrees(heading));
        }
    }
    
    public double getFieldOfView()
    {
        BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
        double fov = view.getFieldOfView().getDegrees();
        return fov;
    }
    
    public void setFieldOfView(double fieldOfView)
    {
        BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
        if(!Double.isNaN(fieldOfView)) {
        	view.setFieldOfView(Angle.fromDegrees(fieldOfView));
        	wwd.redrawNow(); // force redraw
        }
    }
    
    public double getVerticalExaggeration()
    {
    	double ve = this.wwd.getSceneController().getVerticalExaggeration();
        return ve;
    }
    
    public void setVerticalExaggeration(double ve)
    {
        if(!Double.isNaN(ve)) {
        	this.wwd.getSceneController().setVerticalExaggeration(ve);
        	wwd.redrawNow(); // force redraw
        }
    }

    public void setHeadingAndPitch(double heading, double pitch)
    {
        BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
        if(!Double.isNaN(heading) || !Double.isNaN(pitch))
        {
            heading = Double.isNaN(heading) ? view.getHeading().degrees : heading;
            pitch = Double.isNaN(pitch) ? view.getPitch().degrees : pitch;

            view.addHeadingPitchAnimator(
                view.getHeading(), Angle.fromDegrees(heading), view.getPitch(), Angle.fromDegrees(pitch));
        }
    }
    
	public void setAltitude(double altitude) {
		
		BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
		
        if(!Double.isNaN(altitude)) {
        	view.addZoomAnimator(view.getZoom(), altitude);
//        	view.setZoom(altitude);
        }
	}
	
	public double getAltitude() {
		
		BasicOrbitView view = (BasicOrbitView)this.wwd.getView();
		double altitude = view.getZoom();
        return altitude;
	}

	public void setStereoMode(boolean stereo) {
		
		logger.info("stereo: "+stereo);
		
        // Set the stereo.mode property to request stereo. Request red-blue anaglyph in this case. Can also request
        // "device" if the display device supports stereo directly. To prevent stereo, leave the property unset or set
        // it to an empty string.
		
		//AnaglyphSceneController anaglyphSceneController = ((AnaglyphSceneController)wwd.getSceneController());
		StereoOptionSceneController stereoOptionSceneController = ((StereoOptionSceneController)wwd.getSceneController());
		
		if(stereo) {
			//anaglyphSceneController.setDisplayMode(AnaglyphSceneController.DISPLAY_MODE_STEREO);
			//System.setProperty("gov.nasa.worldwind.stereo.mode", "redblue");
			stereoOptionSceneController.setStereoMode(AVKey.STEREO_MODE_RED_BLUE);
		} else {
			//anaglyphSceneController.setDisplayMode(AnaglyphSceneController.DISPLAY_MODE_MONO);
			//System.setProperty("gov.nasa.worldwind.stereo.mode", "");
			stereoOptionSceneController.setStereoMode(AVKey.STEREO_MODE_NONE);
		}
		wwd.redraw();
	}

	public boolean isStereoMode() {
		
//		String displayMode = ((AnaglyphSceneController)wwd.getSceneController()).getDisplayMode();
//		if(displayMode.equals(AnaglyphSceneController.DISPLAY_MODE_STEREO)) {
//			return true;
//		}
//		return false;
		
//		String displayMode = System.getProperty("gov.nasa.worldwind.stereo.mode");
//		if(displayMode.equals("redblue")||displayMode.equals("device")) {
//			return true;
//		}
//		return false;
		
		StereoOptionSceneController stereoOptionSceneController = ((StereoOptionSceneController)wwd.getSceneController());
		return stereoOptionSceneController.isInStereo();
	}
	
	private IconLayer getMarkerLayer() {
		
		if(markerLayer==null) {
			markerLayer = new IconLayer();
			markerLayer.setName(MARKER_LAYER_NAME);
			insertOnTop(wwd, markerLayer);
		}
		return markerLayer;
	}
	
	private AnnotationLayer getBubbleLayer() {
		
		if(bubbleLayer==null) {
			bubbleLayer = new AnnotationLayer();
			bubbleLayer.setName(BUBBLE_LAYER_NAME);
			insertOnTop(wwd, bubbleLayer);
		}
		return bubbleLayer;
	}

	public void fireEvent(Object[] args) {
    	
    	logger.info("event fired");
    	
    	if(jsl!=null) {
			jsl.call("actionPerformed", args);
    	}
    }

	public void addWMSLayer(JSObject jsWMSLayer) {
		
		logger.info("Adding wms layer");
		String name = (String) jsWMSLayer.getMember("name");
		String wmsUrl = (String) jsWMSLayer.getMember("URL");
		String layerName = (String) jsWMSLayer.getMember("layerName");
		
		if(getLayerByName(wmsUrl)!=null) return;
		
		Double opacity = null;
		
		Object opacityObj = null;
		try {
			opacityObj = jsWMSLayer.getMember("opacity");
		} catch (JSException e1) {
			;
		}
		
		if(opacityObj!=null)
		try {
			opacity = new Double(opacityObj.toString());
		} catch (Exception e) {
			logger.info("opacity: "+opacityObj.toString());
		}
		
		URI uri = WWIO.makeURI(wmsUrl);
        
        WMSCapabilities caps = null;

        try
        {
            caps = WMSCapabilities.retrieve(uri);
            caps.parse();
        }
        catch (Exception e)
        {
            e.printStackTrace();
            return;
        }    
        
        WMSLayerCapabilities wmsLayerCapabilities = caps.getLayerByName("Foundation.GTOPO30");
        
        final List<WMSLayerCapabilities> namedLayerCaps = caps.getNamedLayers();
        if (namedLayerCaps == null) {
        	
        	logger.severe("not existent layers in capabilities");
        	return;
        }
            
        
        if(wmsLayerCapabilities==null) {
        	
        	logger.warning("wms layer "+layerName+" is not supported.");
        	logger.info("Supported layers are:");
        	try
            {
                for (WMSLayerCapabilities lc : namedLayerCaps)
                {
                	logger.info("> "+lc.getName());
                }
            }
            catch (Exception e)
            {
                e.printStackTrace();
                //return;
            }
            //return;
        }

        AVList params = new AVListImpl();
        params.setValue(AVKey.LAYER_NAMES, layerName);
        params.setValue(AVKey.URL_CONNECT_TIMEOUT, 30000);
        params.setValue(AVKey.URL_READ_TIMEOUT, 30000);
        params.setValue(AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT, 60000);
        
		if (opacity != null) {
			params.setValue(AVKey.OPACITY, opacity);
		}
        
        WMSTiledImageLayer wmsLayer = new WMSTiledImageLayer(caps, params);
        
        wmsLayer.setName(wmsUrl);
        
        logger.info("url: "+wmsUrl);
        logger.info("name: "+wmsLayer.getName());
        logger.info("url: "+wmsLayer.getOpacity());
        insertBeforePlacenames(wwd, wmsLayer);
        updateLayerManager();
	}

	private void updateLayerManager() {
		RenderableLayer layerManager = (RenderableLayer) getLayerByName(LAYER_MANAGER_LAYER_NAME);
        if(layerManager!=null) {
        	hideLayerManager();
        	showLayerManager();
        }
	}

	public void removeWMSLayer(JSObject jsWMSLayer) {
		
		String wmsUrl = (String) jsWMSLayer.getMember("URL");
		removeLayerByName(wmsUrl);
		updateLayerManager();
	}

	public void addTileLayer(JSObject jsTileLayer) {
		
		
	}

	public void removeTileLayer(JSObject jsTileLayer) {
		
		
	}
	
	private static Color fromRGB(String rgb) {
		
		rgb = rgb.substring(1); // removes initial #
    	int colorInt = Integer.parseInt( rgb, 16);
    	Color c = new Color( colorInt );
    	return c;
	}
	
}

