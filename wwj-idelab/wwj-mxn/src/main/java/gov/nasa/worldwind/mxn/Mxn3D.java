package gov.nasa.worldwind.mxn;

public interface Mxn3D {
	
	/**
	 * 
	 * @return the view's current pitch.
	 */
	public abstract double getPitch();
	
	/**
	 * 
	 * @param pitch the view's current pitch.
	 */
	public abstract void setPitch(double pitch);
    
	/**
	 * 
	 * @return the view's current heading.
	 */
    public abstract double getHeading();
    
    /**
     * 
     * @param heading the view's current heading.
     */
    public abstract void setHeading(double heading);
    
    /**
     * 
     * @return the horizontal field-of-view angle (the angle of visibility), or null if the implementation does not support a field-of-view.
     */
    public abstract double getFieldOfView();
    
    /**
     * 
     * @param fieldOfView the horizontal field-of-view angle (the angle of visibility), or null if the implementation does not support a field-of-view.
     */
    public abstract void setFieldOfView(double fieldOfView);
    
    /**
     * 
     * @return The distance between the eye position and the center position.
     */
    public abstract double getAltitude();
    
    /**
     * 
     * @param altitude Distance between the eye position and the center position.
     */
    public abstract void setAltitude(double altitude);
    
    /**
     * Set the scene controller to stereo/mono
     * 
     * @param stereo true: stereo (anagliph), false: mono  
     */
    public abstract void setStereoMode(boolean stereo);
    
    
    /**
     * Whether ths scene controller is in stereo mode or not
     * 
     * @return true: stereo (anagliph), false: mono
     */
    public boolean isStereoMode();
}
