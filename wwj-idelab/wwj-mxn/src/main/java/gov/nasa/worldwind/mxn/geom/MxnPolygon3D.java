package gov.nasa.worldwind.mxn.geom;

import gov.nasa.worldwind.render.airspaces.Polygon;

public class MxnPolygon3D extends Polygon implements MxnFeature {

	private String fid;

	public String getFid() {
		return fid;
	}

	public void setFid(String fid) {
		this.fid = fid;
	}
}
