package gov.nasa.worldwind.mxn;

import gov.nasa.worldwind.formats.georss.GeoRSSParser;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.render.Renderable;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.logging.Logger;

public class GeoRSSLayer extends RenderableLayer {

	Logger logger = Logger.getLogger("GeoRSSLayer");
	
	public GeoRSSLayer(URL url) throws IOException {
		
		super();
		
		this.setName(url.toString()); // use url as default layer name
		
        InputStream in=url.openStream ();
        BufferedReader dis =
          new BufferedReader (new InputStreamReader (in));
        StringBuffer fBuf = new StringBuffer  () ;

        String line;
        while ( (line = dis.readLine ()) != null) {
          fBuf.append (line + "\n");
        }

        in.close ();
       
        String georssDoc = fBuf.toString();
		
		logger.info("Geo RSS document: "+georssDoc);
		buildGeoRSS(georssDoc);
	}
	
	public GeoRSSLayer(String docString) {
	
		super();
		buildGeoRSS(docString);
	}

	private void buildGeoRSS(String docString) {
		java.util.List<Renderable> shapes;

        shapes = GeoRSSParser.parseShapes(docString);
        if (shapes != null) {
        	for (Renderable r : shapes)
                this.addRenderable(r);
        }
	}
	
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		
		URL url = new URL("http://itastdevserver.tel.uva.es/sample.xml");
		GeoRSSLayer georss = new GeoRSSLayer(url);
	}

}
