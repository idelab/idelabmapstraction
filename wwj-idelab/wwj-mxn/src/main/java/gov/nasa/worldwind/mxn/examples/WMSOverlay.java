/*
Copyright (C) 2001 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/
package gov.nasa.worldwind.mxn.examples;

import java.net.URI;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import gov.nasa.worldwind.BasicModel;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.Factory;
import gov.nasa.worldwind.WorldWind;
import gov.nasa.worldwind.avlist.AVKey;
import gov.nasa.worldwind.avlist.AVList;
import gov.nasa.worldwind.avlist.AVListImpl;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
//import gov.nasa.worldwind.examples.WMSLayersPanel;
//import gov.nasa.worldwind.examples.util.LayerSelector;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.ogc.wms.WMSCapabilities;
import gov.nasa.worldwind.ogc.wms.WMSLayerCapabilities;
import gov.nasa.worldwind.ogc.wms.WMSLayerStyle;
import gov.nasa.worldwind.util.WWIO;
import gov.nasa.worldwind.util.WWUtil;
//import gov.nasa.worldwind.util.tree.BasicTreeController;
import gov.nasa.worldwind.wms.WMSTiledImageLayer;

/**
 * This is the most basic World Wind program.
 *
 * @version $Id: HelloWorldWind.java 4869 2008-03-31 15:56:36Z tgaskins $
 */
public class WMSOverlay
{
    // An inner class is used rather than directly subclassing JFrame in the main class so
    // that the main can configure system properties prior to invoking Swing. This is
    // necessary for instance on OS X (Macs) so that the application name can be specified.

    protected static class AppFrame extends javax.swing.JFrame
    {

		public AppFrame()
        {			
		
            WorldWindowGLCanvas wwd = new WorldWindowGLCanvas();
            wwd.setModel(new BasicModel());		
            wwd.setPreferredSize(new java.awt.Dimension(1000, 800));
            this.getContentPane().add(wwd, java.awt.BorderLayout.CENTER);
            this.pack();

            //String wmsURL = "http://www.idee.es/wms/PNOA/PNOA";
            //String layerName = "pnoa";
            
            //String wmsURL1 = "http://www.mirame.chduero.es/duerowfd";
            //String layerName1 = "Segmentos_Rio";
            
            String wmsURL2 = "http://demo.cubewerx.com/demo/cubeserv/cubeserv.cgi";
            String layerName2 = "Foundation.GTOPO30";
            
            //WMSTiledImageLayer wmsLayer = getWMSLayer(wmsURL, layerName);
            //WMSTiledImageLayer wmsLayer1 = getWMSLayer(wmsURL1, layerName1);
            WMSTiledImageLayer wmsLayer2 = getWMSLayer(wmsURL2, layerName2);
            
            //wwd.getModel().getLayers().add(wmsLayer);
            //wwd.getModel().getLayers().add(wmsLayer1);
            wwd.getModel().getLayers().add(wmsLayer2);
            
//            LayerSelector selector = new LayerSelector(wwd.getModel().getLayers());
            
	        
//            RenderableLayer layerManager = new RenderableLayer();
//	        layerManager.addRenderable(selector);
//	        BasicTreeController controller = new BasicTreeController(wwd, selector);
//	        wwd.getModel().getLayers().add(layerManager);
            
        }

		private WMSTiledImageLayer getWMSLayer(String wmsURL, String layerName) {
			URI uri = WWIO.makeURI(wmsURL);
            
            WMSCapabilities caps = null;

            try
            {
                caps = WMSCapabilities.retrieve(uri);
                System.out.println(caps.toString());
                caps.parse();
            }
            catch (Exception e)
            {
                e.printStackTrace();
             
            }    
            
            WMSLayerCapabilities wmsLayerCapabilities = caps.getLayerByName("Foundation.GTOPO30");
            
            final List<WMSLayerCapabilities> namedLayerCaps = caps.getNamedLayers();
            if (namedLayerCaps == null) {
            	
            	System.out.println("not existent layers in capabilities");
            
            }
                
            
            if(wmsLayerCapabilities==null) {
            	
            	System.out.println("wms layer "+layerName+" is not supported.");
            	System.out.println("Supported layers are:");
            	try
                {
                    for (WMSLayerCapabilities lc : namedLayerCaps)
                    {
                    	System.out.println("> "+lc.getName());
                    }
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                    
                }
                //return;
            }

            AVList params = new AVListImpl();
            params.setValue(AVKey.LAYER_NAMES, layerName);
            params.setValue(AVKey.URL_CONNECT_TIMEOUT, 30000);
            params.setValue(AVKey.URL_READ_TIMEOUT, 30000);
            params.setValue(AVKey.RETRIEVAL_QUEUE_STALE_REQUEST_LIMIT, 60000);
            params.setValue(AVKey.OPACITY, 0.5);
            
    		
            
            WMSTiledImageLayer wmsLayer = new WMSTiledImageLayer(caps, params);
            if(layerName!=null)
            	wmsLayer.setName(layerName);
			return wmsLayer;
		}
    }

    public static void main(String[] args)
    {
        if (Configuration.isMacOS())
        {
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Hello World Wind");
        }

        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                // Create an AppFrame and immediately make it visible. As per Swing convention, this
                // is done within an invokeLater call so that it executes on an AWT thread.
                new AppFrame().setVisible(true);
            }
        });
    }
}