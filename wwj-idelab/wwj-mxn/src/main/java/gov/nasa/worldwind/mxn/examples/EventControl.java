/*
Copyright (C) 2001 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/
package gov.nasa.worldwind.mxn.examples;

import gov.nasa.worldwind.BasicModel;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.View;
import gov.nasa.worldwind.WorldWindow;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.event.PositionEvent;
import gov.nasa.worldwind.event.PositionListener;
import gov.nasa.worldwind.event.SelectEvent;
import gov.nasa.worldwind.event.SelectListener;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Vec4;
import gov.nasa.worldwind.layers.Layer;
import gov.nasa.worldwind.layers.LayerList;
import gov.nasa.worldwind.view.orbit.OrbitView;

import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Iterator;

/**
 * This is the most basic World Wind program.
 *
 * @version $Id: HelloWorldWind.java 4869 2008-03-31 15:56:36Z tgaskins $
 */
public class EventControl
{
    // An inner class is used rather than directly subclassing JFrame in the main class so
    // that the main can configure system properties prior to invoking Swing. This is
    // necessary for instance on OS X (Macs) so that the application name can be specified.

    protected static class AppFrame extends javax.swing.JFrame
    {

		public AppFrame()
        {			
		
            final WorldWindowGLCanvas wwd = new WorldWindowGLCanvas();
            wwd.setModel(new BasicModel());		
            wwd.setPreferredSize(new java.awt.Dimension(800, 600));
            this.getContentPane().add(wwd, java.awt.BorderLayout.CENTER);
            this.pack();
            
            LayerList layers = wwd.getModel().getLayers();
            for (Iterator iterator = layers.iterator(); iterator.hasNext();) {
				Layer layer = (Layer) iterator.next();
				System.out.println(layer.getName());
			}
            
            WorldWindow worldwindow = wwd;
            
            wwd.addMouseListener(new MouseListener() {
				
            	Vec4 previousLocation = null;
            	
				public void mouseReleased(MouseEvent e) {
					
					Vec4 currentLocation = wwd.getView().getCenterPoint();
					if(currentLocation!=previousLocation) {
						System.out.println("Move end");
					}		
				}
				
				public void mousePressed(MouseEvent e) {
					
					previousLocation = wwd.getView().getCenterPoint();
				}
				
				public void mouseExited(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				public void mouseEntered(MouseEvent e) {
					// TODO Auto-generated method stub
					
				}
				
				public void mouseClicked(MouseEvent e) {
					
					System.out.println("click");
				}
			});
            
         
            
//            MotionListener motionListener = new MotionListener(wwd);
//            
//            motionListener.start();
            
//            wwd.addSelectListener(new SelectListener() {
//				
//				public void selected(SelectEvent event) {
//					
//					if (event.getTopObject() == null || event.getTopPickedObject().isTerrain())
//		            {
//		                System.out.println("click over terrain");
//		            }
//				}
//			});
//            
//            
//            
//            wwd.addPositionListener(new PositionListener() {
//            	
//            	OrbitView orbitView = (OrbitView) wwd.getView();
//				Position previousPosition = orbitView.getCenterPosition();
//            	long lastUpdate = System.currentTimeMillis();
//            	boolean animating = false;
//            	
//    			public void moved(PositionEvent event) {
//    				
//    				Position currentPosition = orbitView.getCenterPosition();
//                	
//    				if( (System.currentTimeMillis()-lastUpdate)>100 ) {
//	    				
//	    				if(currentPosition!=previousPosition && !animating) {
//	    					animating = true;
//	    				} else if(currentPosition==previousPosition && animating) {
//	    					System.out.println("Move end!");
//	    					animating = false;
//	    				}
//    				}
//    				previousPosition = currentPosition;
//    			}
//    		});
            
            
        }
    }

    public static void main(String[] args)
    {
        if (Configuration.isMacOS())
        {
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Hello World Wind");
        }

        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                // Create an AppFrame and immediately make it visible. As per Swing convention, this
                // is done within an invokeLater call so that it executes on an AWT thread.
                new AppFrame().setVisible(true);
            }
        });
    }
}