/*
Copyright (C) 2001 United States Government
as represented by the Administrator of the
National Aeronautics and Space Administration.
All Rights Reserved.
*/
package gov.nasa.worldwind.mxn.examples;

import java.awt.BorderLayout;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.HashMap;

import gov.nasa.worldwind.BasicModel;
import gov.nasa.worldwind.Configuration;
import gov.nasa.worldwind.awt.WorldWindowGLCanvas;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.geom.Sector;
import gov.nasa.worldwind.mxn.IdelabMxnMarker;
import gov.nasa.worldwind.render.DrawContext;
import gov.nasa.worldwind.util.StatusBar;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;

/**
 * This is the most basic World Wind program.
 *
 * @version $Id: HelloWorldWind.java 4869 2008-03-31 15:56:36Z tgaskins $
 */
public class BoundsExample
{
    // An inner class is used rather than directly subclassing JFrame in the main class so
    // that the main can configure system properties prior to invoking Swing. This is
    // necessary for instance on OS X (Macs) so that the application name can be specified.

    protected static class AppFrame extends javax.swing.JFrame
    {

		public AppFrame()
        {			
		
            final WorldWindowGLCanvas wwd = new WorldWindowGLCanvas();
            wwd.setModel(new BasicModel());		
            wwd.setPreferredSize(new java.awt.Dimension(600, 400));
            this.getContentPane().add(wwd, java.awt.BorderLayout.CENTER);
            StatusBar statusBar = new StatusBar();
            this.getContentPane().add(statusBar, BorderLayout.PAGE_END);
            // Forward events to the status bar to provide the cursor position info.
            statusBar.setEventSource(wwd);
            this.pack();
            
               	
        	wwd.addMouseListener(new MouseListener() {
				
				public void mouseReleased(MouseEvent e) {
					
				}
				
				public void mousePressed(MouseEvent e) {
					
				}
				
				public void mouseExited(MouseEvent e) {
					
				}
				
				public void mouseEntered(MouseEvent e) {
					
				}
				
				public void mouseClicked(MouseEvent e) {
					
					Sector visibleSector = null;
			        DrawContext drawContext = wwd.getSceneController().getDrawContext();
			    	if(drawContext!=null) {
			    		visibleSector = drawContext.getVisibleSector();
			    	}
			    	String boundsAsString = getBoundsAsString(visibleSector);
			    	System.out.println(boundsAsString);
				}
			});
        	
        	BasicOrbitView view = (BasicOrbitView)wwd.getView();
        	view.setZoom(1000000);
        	view.setCenterPosition(Position.fromDegrees(-41.8, 146.7));
        	wwd.redrawNow();
        }
		
		String getBoundsAsString(Sector sector) {
			
			String bounds = null;
	    	double min_lat = 0, max_lat = 0, min_lon = 0, max_lon = 0;
	    	
	   		if(sector!=null) {
				min_lat = sector.getMinLatitude().getDegrees();
				max_lat = sector.getMaxLatitude().getDegrees();
				min_lon = sector.getMinLongitude().getDegrees();
				max_lon = sector.getMaxLongitude().getDegrees();
			}
	   		bounds = min_lon+","+min_lat+","+max_lon+","+max_lat;
	   		return bounds;
		}
    }

    public static void main(String[] args)
    {
        if (Configuration.isMacOS())
        {
            System.setProperty("com.apple.mrj.application.apple.menu.about.name", "Hello World Wind");
        }

        java.awt.EventQueue.invokeLater(new Runnable()
        {
            public void run()
            {
                // Create an AppFrame and immediately make it visible. As per Swing convention, this
                // is done within an invokeLater call so that it executes on an AWT thread.
                new AppFrame().setVisible(true);
            }
        });
    }
}