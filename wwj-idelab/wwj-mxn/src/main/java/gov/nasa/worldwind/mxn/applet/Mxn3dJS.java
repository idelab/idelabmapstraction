package gov.nasa.worldwind.mxn.applet;

/**
 * Interface between Mapstraction Javascript code and World Wind Java API
 * exposing 3D functionality
 * 
 * @author Jordana Torres Gonzalez (alepitheia@hotmail.com)
 * @author Ricardo Garc�a Mart�n (garcia.martin.ricardo@gmail.com)
 */
public interface Mxn3dJS {
	
	/**
	 * Gets the pitch of the current view
	 * 
	 * @return the view's current pitch in degrees.
	 */
	public abstract double getPitch();
	
	/**
	 * Sets the pitch of the current view
	 * 
	 * @param pitch the view's current pitch in degrees.
	 */
	public abstract void setPitch(double pitch);
    
	/**
	 * Gets the heading of the current view
	 * 
	 * @return the view's current heading in degrees.
	 */
    public abstract double getHeading();
    
    /**
     * Sets the heading of the current view
     * 
     * @param heading the view's current heading in degrees.
     */
    public abstract void setHeading(double heading);
    
    /**
     * Gets the field of view of the current view
     * 
     * @return the horizontal field-of-view angle (the angle of visibility) in degrees, or null if the implementation does not support a field-of-view.
     */
    public abstract double getFieldOfView();
    
    /**
     * Sets the field of view of the current view
     * 
     * @param fieldOfView the horizontal field-of-view angle (the angle of visibility) in degrees, or null if the implementation does not support a field-of-view.
     */
    public abstract void setFieldOfView(double fieldOfView);
    
    /**
     * Get the altitude of the current view
     * 
     * @return The distance in meters between the eye position and the center position.
     */
    public abstract double getAltitude();
    
    /**
     * Set the altitude of the current view
     * 
     * @param altitude Distance in meters between the eye position and the center position.
     */
    public abstract void setAltitude(double altitude);
    
    
    /**
     * Set the scene controller to stereo/mono
     * 
     * @param stereo true: stereo (anagliph), false: mono  
     */
    public abstract void setStereoMode(boolean stereo);
    
    
    /**
     * Whether ths scene controller is in stereo mode or not
     * 
     * @return true: stereo (anagliph), false: mono
     */
    public boolean isStereoMode();
}
