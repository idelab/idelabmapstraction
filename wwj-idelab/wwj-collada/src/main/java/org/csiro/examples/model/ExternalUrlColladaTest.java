package org.csiro.examples.model;

import gov.nasa.worldwind.custom.render.Ardor3DModel;
import gov.nasa.worldwind.custom.render.WWJArdor3DModel;
import gov.nasa.worldwind.geom.Angle;
import gov.nasa.worldwind.geom.Position;
import gov.nasa.worldwind.layers.RenderableLayer;
import gov.nasa.worldwind.view.orbit.BasicOrbitView;
import gov.nasa.worldwindx.examples.ApplicationTemplate;

public class ExternalUrlColladaTest extends ApplicationTemplate {

	public static class AppFrame extends ApplicationTemplate.AppFrame {

		public AppFrame() {
			super(true, true, false);
			try {
				RenderableLayer layer = new RenderableLayer();
				layer.setName("movable 3D object");
			
				//String resource = "http://itastdevserver.tel.uva.es/wwj/collada/testmodels/models/ship.dae";
				//String resource = "http://itastdevserver.tel.uva.es/wwj/collada/testmodels/models/superdome.dae";
				//String resource = "./testmodels/models/model.dae";
				//String resource = "http://itastdevserver.tel.uva.es/wwj/collada/TajMahal/models/model.dae";
				String resource = "http://itastdevserver.tel.uva.es/wwj/campusDelibes/Teleko/models/untitled.dae";
				
				Position telekoPos = new Position(Angle
						.fromDegrees(41.66260158803), Angle
						.fromDegrees(-4.705316476939), 0);
				
				Position pos = new Position(Angle
						.fromDegrees(-34.940679766700), Angle
						.fromDegrees(138.623116628920), 1000);
				
				WWJArdor3DModel airap = new WWJArdor3DModel(resource, telekoPos);

				
				
				layer.addRenderable(airap);

				this.getWwd().addSelectListener(
						new GenericDraggerAdjuster(this.getWwd()));
				insertBeforeCompass(this.getWwd(), layer);
				this.getLayerPanel().update(this.getWwd());
				
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
	}

	public static void main(String[] args) {
		ApplicationTemplate
				.start("Movable 3D Model Layer Test", AppFrame.class);
	}
}
