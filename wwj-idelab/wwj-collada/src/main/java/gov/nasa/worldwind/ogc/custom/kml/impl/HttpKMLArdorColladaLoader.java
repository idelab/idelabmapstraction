package gov.nasa.worldwind.ogc.custom.kml.impl;

import gov.nasa.worldwind.formats.models.loader.HttpResourceLocator;
import gov.nasa.worldwind.ogc.kml.KMLRoot;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import com.ardor3d.extension.model.collada.jdom.ColladaImporter;
import com.ardor3d.extension.model.collada.jdom.data.ColladaStorage;
import com.ardor3d.scenegraph.Node;
import com.ardor3d.util.resource.ResourceLocator;
import com.ardor3d.util.resource.ResourceLocatorTool;
import com.ardor3d.util.resource.ResourceSource;
import com.ardor3d.util.resource.SimpleResourceLocator;
import com.ardor3d.util.resource.URLResourceSource;

public class HttpKMLArdorColladaLoader {
	
	static String baseURL = null;
	
	/**
	 * Logger for this class (logs can be monitored in java console)
	 */
	static Logger logger = Logger.getLogger("HttpKMLArdorColladaLoader");
	
	public static String getBaseURL() {
		return baseURL;
	}

	public static void setBaseURL(String baseURL) {
		HttpKMLArdorColladaLoader.baseURL = baseURL;
	}

	public static Node loadColladaModel(String modelURL, final KMLRoot kmlRoot) throws Exception {
    	
		baseURL = modelURL.substring(0,modelURL.lastIndexOf("/")+1);
		logger.info("baseURL: "+baseURL);
		
        final Node root = new Node( "rootNode" );

        ColladaImporter importer = new ColladaImporter();

        HttpResourceLocator modelLocator = new HttpResourceLocator(baseURL);
        
        importer.setModelLocator(modelLocator);
        importer.setTextureLocator(new HttpResourceLocator(baseURL));

        ResourceLocatorTool.addResourceLocator(ResourceLocatorTool.TYPE_MODEL, modelLocator);
        
        ColladaStorage storage =  importer.load(modelURL);
        root.attachChild(storage.getScene());

        root.updateGeometricState(0);
        return root;
    }
}
