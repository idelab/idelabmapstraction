package gov.nasa.worldwind.formats.models.loader;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Logger;

import com.ardor3d.util.resource.ResourceLocator;
import com.ardor3d.util.resource.ResourceSource;
import com.ardor3d.util.resource.URLResourceSource;

public class HttpResourceLocator implements ResourceLocator {
	
	String baseURL = null;
	
	/**
	 * Logger for this class (logs can be monitored in java console)
	 */
	Logger logger = Logger.getLogger("HttpResourceLocator");
	
	public HttpResourceLocator(String baseURL) {
		this.baseURL = baseURL;
	}
	
	public ResourceSource locateResource(String resourceName) {
		
		String url = "";
		//logger.info("initial URL: "+resourceName);
		if(!resourceName.startsWith("http://")) {
			//logger.info("does not start by http. Appending base URL");
			url = baseURL+resourceName;
		} else {
			url = resourceName;
		}
		
		//logger.info("url: "+url);
		
		URL resourceURL = null;
		try {
			resourceURL = new URL(url);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		}
		
		ResourceSource resourceSource = new URLResourceSource(resourceURL) ;
		
		return resourceSource;
	}

}
