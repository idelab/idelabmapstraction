<?php
// Ejemplo de Proxy PHP para manejar peticiones POST y GET
// Autor: Jason Levitt
// Necesita activar las librerías curl en php

//Comandos de Debug para observar peticiones con FirePHP
//require('lib/FirePHPCore/fb.php');
//ob_start();

define ('HOSTNAME', 'http://www.cartociudad.es/');

// Obtenemos la ruta de la peticion AJAX
$path = ($_POST['c_path']) ? $_POST['c_path'] : $_GET['c_path'];
$url = HOSTNAME.$path;

// Iniciamos una sesion curl
$session = curl_init($url);

// Para manejar los parametros de la peticion POST (WPS)
if ($_POST['request']) {
  
  $postvars = "request=".$_POST['request'];
  $postvars = stripslashes($postvars);

  curl_setopt ($session, CURLOPT_POST, true);
  curl_setopt ($session, CURLOPT_POSTFIELDS, $postvars);
}

// No devolver cabeceras HTTP
curl_setopt($session, CURLOPT_HEADER, false);
curl_setopt($session, CURLOPT_RETURNTRANSFER, true);

// Lanzamos la peticion
$xml = curl_exec($session);

// Fijamos el Content-Type adecuado (necesario para IE)
header("Content-Type: text/xml; charset=UTF-8");

//Necesario para las peticiones POST porque el servidor WPS devuelve espacios antes del xml
if ($_POST['request']) {
  $xmlDoc = substr($xml,9); //eliminar caracteres
  $xmlDoc = stripslashes($xmlDoc);
  echo $xmlDoc;
} else{
  echo $xml;
}
curl_close($session);

?>
