mxn.register('google', {	

Mapstraction: {
	
	activateEdition: function(){
		var me = this;
		this.selectedFeature = null;
		if(typeof(this.handler) === "undefined"){
			this.handler = [];
			this.handler.activate = function(){
				if(me.selectedFeature !== null){
					if(me.selectedFeature.mapstraction_marker){
						me.selectedFeature.setImage(me.selectedFeature.mapstraction_marker.oldIcon);
						me.selectedFeature.mapstraction_marker.iconUrl = me.selectedFeature.mapstraction_marker.oldIcon;
						me.markerUnselected.fire({'marker': me.selectedFeature.mapstraction_marker});
					}else{
						me.selectedFeature.setStrokeStyle({"color":me.selectedFeature.mapstraction_polyline.oldColor});
						me.selectedFeature.mapstraction_polyline.color = me.selectedFeature.mapstraction_polyline.oldColor;
						me.polylineUnselected.fire({'polyline': me.selectedFeature.mapstraction_polyline});				
					}
		
				}
				me.selectedFeature = null;
			};
		}
		this.click.addHandler(this.handler.activate);
		for (var i in this.markers){
			this.invoker.go('activateMarker',[this.markers[i]]);
  		}
  		for (var j in this.polylines) {
  			this.invoker.go('activatePolyline',[this.polylines[j]]);
  		}
		
		document.onkeydown=function(e){
			if (!e) e = window.event;
			vKeyCode=e.keyCode;
			if((vKeyCode==63272)||vKeyCode==46){
				if(me.selectedFeature !== null){
		  			if(me.selectedFeature.mapstraction_marker){
		  				me.removeMarker(me.selectedFeature.mapstraction_marker);
		  			}else{
		  				me.removePolyline(me.selectedFeature.mapstraction_polyline);
		  			}
	  			}
	  			me.selectedFeature = null;
			}
		};
	},
	deactivateEdition:function(){
		this.click.fire();
		this.click.removeHandler(this.handler.activate);
		for (var i = 0;i < this.markers.length; i++){
			this.markers[i].proprietary_marker.disableDragging();
			this.markers[i].click.removeHandler(this.markers[i].handler.activate);
			GEvent.clearListeners(this.markers[i].proprietary_marker, "dragend"); 
  		}
  		for (var j in this.polylines) {
  		    GEvent.clearListeners(this.polylines[j].proprietary_polyline, "mouseover"); 
	        GEvent.clearListeners(this.polylines[j].proprietary_polyline, "mouseout");
	        GEvent.clearListeners(this.polylines[j].proprietary_polyline, "lineupdated");
	        GEvent.clearListeners(this.polylines[j].proprietary_polyline, "endline");
	        GEvent.clearListeners(this.polylines[j].proprietary_polyline, "click");
  		}
	},
	addFeature:function(feature,data){
		var map = this.maps[this.api];
		var me = this; 
		switch(feature){
		case "point":
  			var listener = GEvent.addListener(map, "click", function(overlay, latlng) {	
	    		if (latlng) {
	    			GEvent.removeListener(listener);
			    	var marker = new mxn.Marker(new mxn.LatLonPoint(latlng.lat(),latlng.lng()));
	      			me.addMarkerWithData(marker,data);
	      			me.addingFeature = false;
	      			if(me.editionActive){
	      				me.invoker.go('activateMarker',[marker]);
	      			}
		    	}
  			});
  		break;

	    case "polygon":
	    case "linestring":
	   		var gpoly = new GPolyline([]);
	    	var poly = new mxn.Polyline([]);
	    	if(feature == "polygon"){
	    		poly.setClosed(true);
	    	}
	    	map.addOverlay(gpoly);
	    	gpoly.enableDrawing({});
	    	
  			GEvent.addListener(gpoly,"endline",function(){
  				var points = [];
  				me.addingFeature = false;
  				for (var i = 0;i < gpoly.getVertexCount() ; i ++){
  					var point = gpoly.getVertex(i);
  					points.push(new mxn.LatLonPoint(point.lat(),point.lng()));
  				}
  				poly.points = points;
  				me.addPolylineWithData(poly,data);
  				map.removeOverlay(gpoly);
  				me.addingFeature = false;
  				if(me.editionActive){
	  				me.invoker.go('activatePolyline',[poly]);
  				}
  			});
  		break;
	    }
	},
	activateMarker:function(marker){
		var me = this;
		marker.proprietary_marker.enableDragging();
		GEvent.addListener(marker.proprietary_marker,"dragend",function(newPoint){
			marker.location = new mxn.LatLonPoint(newPoint.lat(),newPoint.lng());
			me.markerChanged.fire({'marker': this.mapstraction_marker});
		});
		if(typeof(marker.handler === "undefined")){
			marker.handler =[];
			marker.handler.activate = function(event,marker){
				if(me.selectedFeature !== null){
					if(me.selectedFeature.mapstraction_marker){
						me.selectedFeature.setImage(me.selectedFeature.mapstraction_marker.oldIcon);
						me.selectedFeature.mapstraction_marker.iconUrl = me.selectedFeature.mapstraction_marker.oldIcon;
						me.markerUnselected.fire({'marker': me.selectedFeature.mapstraction_marker});
					}else{
						me.selectedFeature.setStrokeStyle({"color":me.selectedFeature.mapstraction_polyline.oldColor});
						me.selectedFeature.mapstraction_polyline.color = me.selectedFeature.mapstraction_polyline.oldColor;
						me.polylineUnselected.fire({'polyline': me.selectedFeature.mapstraction_polyline});
					}
	
				}
				me.selectedFeature = marker.proprietary_marker;
				me.markerSelected.fire({'marker': me.selectedFeature.mapstraction_marker});
				if(typeof(marker.iconUrl) === "undefined"){
					marker.oldIcon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png";
				}else{
					marker.oldIcon = marker.iconUrl;
				}
				marker.proprietary_marker.setImage("http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png");
				marker.iconUrl = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
			};
		}
		marker.click.addHandler(marker.handler.activate);
	},
	activatePolyline:function(poly){
		var me = this;
		GEvent.addListener(poly.proprietary_polyline,"mouseover",function(){this.enableEditing();});
		GEvent.addListener(poly.proprietary_polyline,"mouseout",function(){this.disableEditing();});
		GEvent.addListener(poly.proprietary_polyline,"lineupdated",function(){
			var points = [];
			var equals = true;
			for (var i = 0;i < this.getVertexCount() ; i ++){
				var point = this.getVertex(i);
				var mapstractionPoint = new mxn.LatLonPoint(point.lat(),point.lng());
				points.push(mapstractionPoint);
				if(typeof(this.mapstraction_polyline.points[i]) === "undefined" ||!this.mapstraction_polyline.points[i].equals(mapstractionPoint)){equals = false;}
			}
			if(!equals){
				this.mapstraction_polyline.points = points;
				me.polylineChanged.fire({'polyline': this.mapstraction_polyline});
			}
		});
		GEvent.addListener(poly.proprietary_polyline,"click",function(){
			
			if(me.selectedFeature !== null){
				if(me.selectedFeature.mapstraction_marker){
					me.selectedFeature.setImage(me.selectedFeature.mapstraction_marker.oldIcon);
					me.selectedFeature.mapstraction_marker.iconUrl = me.selectedFeature.mapstraction_marker.oldIcon;
					me.markerUnselected.fire({'marker': me.selectedFeature.mapstraction_marker});
				}else{
					me.selectedFeature.setStrokeStyle({"color":me.selectedFeature.mapstraction_polyline.oldColor});
					me.selectedFeature.mapstraction_polyline.color = me.selectedFeature.mapstraction_polyline.oldColor;
					me.polylineUnselected.fire({'polyline': me.selectedFeature.mapstraction_polyline});
				}

			}
			me.selectedFeature = this;
			if(typeof(me.selectedFeature.mapstraction_polyline.color) === "undefined"){
				me.selectedFeature.mapstraction_polyline.oldColor = "#0000FF";
			}else{
				me.selectedFeature.mapstraction_polyline.oldColor = me.selectedFeature.mapstraction_polyline.color;
			}
			me.selectedFeature.mapstraction_polyline.color = "#00FF00";
			me.selectedFeature.setStrokeStyle({"color":"#00FF00"});
			me.polylineSelected.fire({'polyline': me.selectedFeature.mapstraction_polyline});
		});
	}
}
});

mxn.register('google', {	

	WFSLayer: {
		addFeatures: function(features){
			var encontrado = false;
			for (var i in features){
				encontrado = false;
				for (var j in this.removedFeatures){
					if (features[i].getAttribute('feature').fid == this.removedFeatures[j].fid) {
						encontrado = true; 
						break;
					}
				}
				for (var j in this.modifiedFeatures){
					if (features[i].getAttribute('feature').fid == this.modifiedFeatures[j].getAttribute('feature').fid) {
						features[i] = this.modifiedFeatures[j];
						break;
					}
				}
				if(!encontrado){
				if(features[i] instanceof mxn.Marker) this.addMarker(features[i],true,true);
				else this.addPolyline(features[i],true,true);
				}
			}
		},
		addMarker: function(marker,old){
			var map = this.map.maps[this.api];
			var gpin = marker.toProprietary(this.map.api);
			map.addOverlay(gpin);
			
			GEvent.addListener(gpin, 'infowindowopen', function() {
				marker.openInfoBubble.fire();
			});
			GEvent.addListener(gpin, 'infowindowclose', function() {
				marker.closeInfoBubble.fire();
			});
			return gpin;
		},
		removeMarker: function(marker){
			var map = this.map.maps[this.map.api];
			map.removeOverlay(marker.proprietary_marker);
		},
		removePolyline: function(polyline) {
			var map = this.map.maps[this.api];
			map.removeOverlay(polyline.proprietary_polyline);
		},
		addPolyline: function(polyline, old) {
			var map = this.map.maps[this.map.api];
			gpolyline = polyline.toProprietary(this.api);
			map.addOverlay(gpolyline);
			return gpolyline;
		},
		addFeature:function(feature,data){
			var map = this.map.maps[this.api];
			var me = this; 
			switch(feature){
			case "point":
	  			var listener = GEvent.addListener(map, "click", function(overlay, latlng) {	
		    		if (latlng) {
		    			GEvent.removeListener(listener);
				    	var marker = new mxn.Marker(new mxn.LatLonPoint(latlng.lat(),latlng.lng()));
		      			me.addMarkerWithData(marker,data);
		      			me.addingFeature = false;
		      			if(me.editionActive){
		      				me.invoker.go('activateMarker',[marker]);
		      			}
			    	}
	  			});
	  		break;
	
		    case "polygon":
		    case "linestring":
		   		var gpoly = new GPolyline([]);
		    	var poly = new mxn.Polyline([]);
		    	if(feature == "polygon"){
		    		poly.setClosed(true);
		    	}
		    	map.addOverlay(gpoly);
		    	gpoly.enableDrawing({});
		    	
	  			GEvent.addListener(gpoly,"endline",function(){
	  				var points = [];
	  				me.addingFeature = false;
	  				for (var i = 0;i < gpoly.getVertexCount() ; i ++){
	  					var point = gpoly.getVertex(i);
	  					points.push(new mxn.LatLonPoint(point.lat(),point.lng()));
	  				}
	  				poly.points = points;
	  				me.addPolylineWithData(poly,data);
	  				map.removeOverlay(gpoly);
	  				me.addingFeature = false;
	  			});
	  		break;
		    }
	  	},
	  	activateEdition: function(){
			var me = this;
			this.selectedFeature = null;
			if(typeof(this.handler) === "undefined"){
				this.handler = [];
				this.handler.activate = function(){
					if(me.selectedFeature !== null){
						if(me.selectedFeature.mapstraction_marker){
							me.selectedFeature.setImage(me.selectedFeature.mapstraction_marker.oldIcon);
							me.selectedFeature.mapstraction_marker.iconUrl = me.selectedFeature.mapstraction_marker.oldIcon;
							me.featureselected.fire(me.selectedFeature.mapstraction_marker);
						}else{
							me.selectedFeature.setStrokeStyle({"color":me.selectedFeature.mapstraction_polyline.oldColor});
							me.selectedFeature.mapstraction_polyline.color = me.selectedFeature.mapstraction_polyline.oldColor;
							me.featureunselected.fire(me.selectedFeature.mapstraction_polyline);				
						}
			
					}
					me.selectedFeature = null;
				};
			}
			me.map.click.addHandler(this.handler.activate);
			for (var i in this.features){
				if(this.features[i] instanceof mxn.Marker)
					this.invoker.go('activateMarker',[this.features[i]]);
				else
					this.invoker.go('activatePolyline',[this.features[i]]);
	  		}
	  		for (var i in this.myFeatures){
				if(this.myFeatures[i] instanceof mxn.Marker){
					this.invoker.go('activateMarker',[this.myFeatures[i]]);
				
				}else
					this.invoker.go('activatePolyline',[this.myFeatures[i]]);
	  		}
			
			document.onkeydown=function(e){
				if (!e) e = window.event;
				vKeyCode=e.keyCode;
				if((vKeyCode==63272)||vKeyCode==46){
					if(me.selectedFeature !== null){
			  			if(me.selectedFeature.mapstraction_marker){
			  				me.removeMarker(me.selectedFeature.mapstraction_marker,true);
			  			}else{
			  				me.removePolyline(me.selectedFeature.mapstraction_polyline,true);
			  			}
		  			}
		  			me.selectedFeature = null;
				}
			};
		},
		activateMarker:function(marker){
			var me = this;
			marker.proprietary_marker.enableDragging();
			GEvent.addListener(marker.proprietary_marker,"dragend",function(newPoint){
				marker.location = new mxn.LatLonPoint(newPoint.lat(),newPoint.lng());
				me.featuremodified.fire(this.mapstraction_marker);
			});
			if(typeof(marker.handler === "undefined")){
				marker.handler =[];
				marker.handler.activate = function(event,marker){
					if(me.selectedFeature !== null){
						if(me.selectedFeature.mapstraction_marker){
							me.selectedFeature.setImage(me.selectedFeature.mapstraction_marker.oldIcon);
							me.selectedFeature.mapstraction_marker.iconUrl = me.selectedFeature.mapstraction_marker.oldIcon;
							me.featureunselected.fire(me.selectedFeature.mapstraction_marker);
						}else{
							me.selectedFeature.setStrokeStyle({"color":me.selectedFeature.mapstraction_polyline.oldColor});
							me.selectedFeature.mapstraction_polyline.color = me.selectedFeature.mapstraction_polyline.oldColor;
							me.featureunselected.fire(me.selectedFeature.mapstraction_polyline);
						}
		
					}
					me.selectedFeature = marker.proprietary_marker;
					me.featureselected.fire(me.selectedFeature.mapstraction_marker);
					if(typeof(marker.iconUrl) === "undefined"){
						marker.oldIcon = "http://www.google.com/intl/en_us/mapfiles/ms/micons/red-dot.png";
					}else{
						marker.oldIcon = marker.iconUrl;
					}
					marker.proprietary_marker.setImage("http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png");
					marker.iconUrl = "http://www.google.com/intl/en_us/mapfiles/ms/micons/green-dot.png";
				};
			}
			marker.click.addHandler(marker.handler.activate);
		},
		activatePolyline:function(poly){
			var me = this;
			GEvent.addListener(poly.proprietary_polyline,"mouseover",function(){this.enableEditing();});
			GEvent.addListener(poly.proprietary_polyline,"mouseout",function(){this.disableEditing();});
			GEvent.addListener(poly.proprietary_polyline,"lineupdated",function(){
				var points = [];
				var equals = true;
				for (var i = 0;i < this.getVertexCount() ; i ++){
					var point = this.getVertex(i);
					var mapstractionPoint = new mxn.LatLonPoint(point.lat(),point.lng());
					points.push(mapstractionPoint);
					if(typeof(this.mapstraction_polyline.points[i]) === "undefined" ||!this.mapstraction_polyline.points[i].equals(mapstractionPoint)){equals = false;}
				}
				if(!equals){
					this.mapstraction_polyline.points = points;
					me.featuremodified.fire(this.mapstraction_polyline);
				}
			});
			GEvent.addListener(poly.proprietary_polyline,"click",function(){
				if(me.selectedFeature !== null){
					if(me.selectedFeature.mapstraction_marker){
						me.selectedFeature.setImage(me.selectedFeature.mapstraction_marker.oldIcon);
						me.selectedFeature.mapstraction_marker.iconUrl = me.selectedFeature.mapstraction_marker.oldIcon;
						me.featureunselected.fire(me.selectedFeature.mapstraction_marker);
					}else{
						me.selectedFeature.setStrokeStyle({"color":me.selectedFeature.mapstraction_polyline.oldColor});
						me.selectedFeature.mapstraction_polyline.color = me.selectedFeature.mapstraction_polyline.oldColor;
						me.featureunselected.fire(me.selectedFeature.mapstraction_polyline);
					}
	
				}
				me.selectedFeature = this;
				if(typeof(me.selectedFeature.mapstraction_polyline.color) === "undefined"){
					me.selectedFeature.mapstraction_polyline.oldColor = "#0000FF";
				}else{
					me.selectedFeature.mapstraction_polyline.oldColor = me.selectedFeature.mapstraction_polyline.color;
				}
				me.selectedFeature.mapstraction_polyline.color = "#00FF00";
				me.selectedFeature.setStrokeStyle({"color":"#00FF00"});
				me.featureselected.fire(me.selectedFeature.mapstraction_polyline);
			});
		},
		deactivateEdition:function(){
			this.map.click.fire();
			this.map.click.removeHandler(this.handler.activate);
			for (var i in this.features){
				if(this.features[i] instanceof mxn.Marker){
					this.features[i].proprietary_marker.disableDragging();
					this.features[i].click.removeHandler(this.features[i].handler.activate);
					GEvent.clearListeners(this.features[i].proprietary_marker, "dragend"); 
				}else{
					GEvent.clearListeners(this.features[i].proprietary_polyline, "mouseover"); 
			        GEvent.clearListeners(this.features[i].proprietary_polyline, "mouseout");
			        GEvent.clearListeners(this.features[i].proprietary_polyline, "lineupdated");
			        GEvent.clearListeners(this.features[i].proprietary_polyline, "endline");
			        GEvent.clearListeners(this.features[i].proprietary_polyline, "click");
				}
			}
			
			for (var i in this.myFeatures){
				if(this.myFeatures[i] instanceof mxn.Marker){
					this.myFeatures[i].proprietary_marker.disableDragging();
					this.myFeatures[i].click.removeHandler(this.myFeatures[i].handler.activate);
					GEvent.clearListeners(this.myFeatures[i].proprietary_marker, "dragend"); 
				}else{
					GEvent.clearListeners(this.myFeatures[i].proprietary_polyline, "mouseover"); 
			        GEvent.clearListeners(this.myFeatures[i].proprietary_polyline, "mouseout");
			        GEvent.clearListeners(this.myFeatures[i].proprietary_polyline, "lineupdated");
			        GEvent.clearListeners(this.myFeatures[i].proprietary_polyline, "endline");
			        GEvent.clearListeners(this.myFeatures[i].proprietary_polyline, "click");
				}
			}
	
		},
		commit: function(added,removed,modified){
			if (added.length)this.forceUpdate = true;
			var aux = [];
			for (var i in added){
			  aux.push(added[i]);
			}
			
			for (var i in removed){
				removed[i].state = OpenLayers.State.DELETE;
				aux.push(removed[i]);
				
			}
			//this.layer.strategies[1].save(auxRemoved);
			//var auxModified = [];
			for (var i in modified){
				var auxFeature = modified[i].getAttribute('feature');
				var newGeom = modified[i].toOpenLayers().geometry;
				var result;
				function recorrer(geom,attribute){
					for (var i in geom){
						if(geom[i].id == attribute.id){
							result = {geometry:geom[i],index:i};
						}else{
							recorrer(geom[i].components,attribute);
						}
					}
				}
				recorrer([auxFeature.geometry],modified[i].getAttribute('oGeometry'));
				newGeom.transform(new OpenLayers.Projection("EPSG:900913"),new OpenLayers.Projection("EPSG:4326"));
				var parentGeom = result.geometry.parent;
				parentGeom.components[result.index] = newGeom;
				modified[i].setAttribute('oGeometry',newGeom);
				parentGeom.components[result.index].parent = parentGeom;
				auxFeature.state = OpenLayers.State.UPDATE;
				aux.push(auxFeature);
			}
			this.layer.strategies[1].save(aux);
		} 	
	},
	Mapstraction:{
		removeFeatures:function(layer){
			for (var i in layer.features){
				if(layer.features[i] instanceof mxn.Marker) layer.invoker.go('removeMarker',[layer.features[i]]);
				else  layer.invoker.go('removePolyline',[layer.features[i]]);
			}
			for (var i in layer.myFeatures){
				if(layer.myFeatures[i] instanceof mxn.Marker) layer.invoker.go('removeMarker',[layer.myFeatures[i]]);
				else layer.invoker.go('removePolyline',[layer.myFeatures[i]]);
			}
		
		}
	}
});

mxn.register('google',{
	TileLayer:{
		init: function(){
			var me = this;
			var tileCopyright = new GCopyright(1, new GLatLngBounds(new GLatLng(-90,-180), new GLatLng(90,180)), 0, "copyleft");
			var copyrightCollection = new GCopyrightCollection(this.copyright);
			copyrightCollection.addCopyright(tileCopyright);
	
			var layer = new GTileLayer(copyrightCollection, 1, 18);
			layer.isPng = function() {
				return true;
			};
			layer.getOpacity = function() {
				return me.opacity;
			};
			layer.getTileUrl = function (a, b) {
				var url = me.tileURL;
				url = url.replace(/\{Z\}/,b);
				url = url.replace(/\{X\}/,a.x);
				url = url.replace(/\{Y\}/,a.y);
				return url;
			};
			tileLayerOverlay = new GTileLayerOverlay(layer);
	
			me.map.maps[me.api].addOverlay(tileLayerOverlay);
			this.layer = tileLayerOverlay;
		},
		toggle:function(){
			if(this.shown) {
				this.map.maps[this.api].removeOverlay(this.layer);
				this.shown = false;
			}
			else {
				this.map.maps[this.api].addOverlay(this.layer);
				this.shown = true;
			}
		}
	}
});

mxn.register('google',{
	Mapstraction:{
		addWMSLayer:function(wmsLayer){
			//this.maps[this.api].addOverlay(wmsLayer.layer);
		},
		removeWMSLayer:function(wmsLayer){
			for (var i in wmsLayer.layer.images)
			this.maps[this.api].removeOverlay(wmsLayer.layer.images[i]);
			GEvent.removeListener(wmsLayer.layer.event);
		}
	},
	WMSLayer:{
		init: function(){
			var groundOverlay = [];
			var map = this.map;
			this.layer = {};
		      function updateOverlay(){
		        
		        if(groundOverlay.length != 0){
		          for (var i in groundOverlay){
		            map.maps[map.api].removeOverlay(groundOverlay[i]);
		          }
		          groundOverlay = [];
		        }
		        
		        var div = map.currentElement;
				var size = {w:div.clientWidth,h:div.clientHeight};
				
				if (size.w == 0 && size.h == 0 || isNaN(size.w) && isNaN(size.h)) {
					size.w = div.offsetWidth;
					size.h = div.offsetHeight;
				}
				if (size.w == 0 && size.h == 0 || isNaN(size.w) && isNaN(size.h)) {
					size.w = parseInt(div.style.width);
					size.h = parseInt(div.style.height);
				}
				
		        var gbox = map.maps[map.api].getBounds();
	  			sw = gbox.getSouthWest();
	  			ne = gbox.getNorthEast();
	        
	            if(sw.lng()>ne.lng()){
	              var aux = ne;
	              ne = new GLatLng(aux.lat(),180);
	              var sw2 = new GLatLng(sw.lat(),-180);
	              var ne2 = new GLatLng(aux.lat(),aux.lng());
	              groundOverlay.push(new GGroundOverlay("http://demo.cubewerx.com/demo/cubeserv/cubeserv.cgi?LAYERS=Foundation.GTOPO30&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&FORMAT=image%2Fjpeg&SRS=EPSG%3A4326&BBOX="+sw2.lng()+","+sw2.lat()+","+ne2.lng()+","+ne2.lat()+"&WIDTH="+size.w+"&HEIGHT="+size.h, new GLatLngBounds(sw2, ne2))) ;
	            }
					
		        groundOverlay.push(new GGroundOverlay("http://demo.cubewerx.com/demo/cubeserv/cubeserv.cgi?LAYERS=Foundation.GTOPO30&SERVICE=WMS&VERSION=1.1.1&REQUEST=GetMap&STYLES=&EXCEPTIONS=application%2Fvnd.ogc.se_inimage&FORMAT=image%2Fjpeg&SRS=EPSG%3A4326&BBOX="+sw.lng()+","+sw.lat()+","+ne.lng()+","+ne.lat()+"&WIDTH="+size.w+"&HEIGHT="+size.h, new GLatLngBounds(sw, ne))) ;
		
		        for (var i in groundOverlay){
		          map.maps[map.api].addOverlay(groundOverlay[i]);
		        }
		      }
		      updateOverlay();
		      this.layer.event = GEvent.addListener(map.maps[map.api], 'moveend', function() {
		    		updateOverlay();
		      });
		   
		   this.layer.images = groundOverlay;
		}
	}
});

mxn.register('google',{
	XMLLayer:{
		init: function(){
			var geoXML = new GGeoXml(this.URL);
			this.layer = geoXML;
			this.map.maps[this.api].addOverlay(geoXML);
		}, 
		toggle:function(){
			if(this.shown) {
				this.map.maps[this.api].removeOverlay(this.layer);
				this.shown = false;
			}
			else {
				this.map.maps[this.api].addOverlay(this.layer);
				this.shown = true;
			}
		}
	}
});