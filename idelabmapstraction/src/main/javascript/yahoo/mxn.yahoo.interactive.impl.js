var yahoo = new GenericInteractivity();
yahoo.Mapstraction.iconURL = function(state){
	return this.src+"icons/yahoo/yahoo-"+state+".png";
};
mxn.register('yahoo',yahoo);

mxn.register('yahoo', {	

	WFSLayer: {
		addFeatures: function(features){
			var encontrado = false;
			for (var i in features){
				encontrado = false;
				for (var j in this.removedFeatures){
					if (features[i].getAttribute('feature').fid == this.removedFeatures[j].fid) {
						encontrado = true; 
						break;
					}
				}
				for (var j in this.modifiedFeatures){
					if (features[i].getAttribute('feature').fid == this.modifiedFeatures[j].getAttribute('feature').fid) {
						features[i] = this.modifiedFeatures[j];
						break;
					}
				}
				if(!encontrado){
				if(features[i] instanceof mxn.Marker) this.addMarker(features[i],true);
				else this.addPolyline(features[i],true);
				}
			}
		},
		addMarker: function(marker){
			var map = this.map.maps[this.api];
			var pin = marker.toProprietary(this.api);
			map.addOverlay(pin);
			YEvent.Capture(pin, EventsList.MouseClick, function() {
				marker.click.fire();
			});
			YEvent.Capture(pin, EventsList.openSmartWindow, function() {
				marker.openInfoBubble.fire();
			});
			YEvent.Capture(pin, EventsList.closeSmartWindow, function() {
				marker.closeInfoBubble.fire();
			});
			return pin;
		},
		removeMarker: function(marker) {
			var map = this.map.maps[this.api];
			map.removeOverlay(marker.proprietary_marker);
		},
		addPolyline: function(polyline, old) {
			var map = this.map.maps[this.api];
			var pl = polyline.toProprietary(this.api);
			map.addOverlay(pl);
			return pl;
		},
		removePolyline: function(polyline) {
			var map = this.map.maps[this.api];
			map.removeOverlay(polyline.proprietary_polyline);
		},
		addFeature: function(feature,data){
			var me = this;
			switch(feature){
			case "point":
				var addMarker = function (event,map,point){
						var mymarker = new mxn.Marker(point.location);
						me.addMarkerWithData(mymarker,data);
						map.click.removeHandler(addMarker);
						me.addingFeature = false;
						if(me.editionActive){
							me.invoker.go('activateMarker',[mymarker]);
						}
					};
					me.map.click.addHandler(addMarker);
			break;
			case "linestring":
			case "polygon":
			var points = [];
			var markers = [];
			var poly = null;
			
			
			var addPoint = function(event,map,point){
				var marker = new mxn.Marker(point.location);
				markers.push(marker);
				marker.setIcon(me.iconURL("create"));
				marker.click.addHandler(function(event,map){
					for (var i in markers){
						me.invoker.go('removeMarker',[markers[i]]);
					}
					me.map.click.removeHandler(addPoint);
					me.addingFeature = false;
					
					if(me.editionActive){
						if(me.handler){me.map.click.addHandler(me.handler.activate);}
						me.invoker.go('activatePolyline',[poly]);
					}
					me.featureadded.fire(poly);
				});
				me.addMarker(marker,true,true,true);
				
				
				points.push(point.location);
	
		        if(poly === null &&points.length > 1){ 
		        	poly = new mxn.Polyline(points);
		        	poly.addData(data);
		        	me.addPolyline(poly,false,true,true);
		        }
		        if(feature =="polygon" && points.length > 2){poly.setClosed(true);}
		        if(poly !== null){
		        	poly.points = points;
		        	poly.updateProprietary();
		        }
			};

			if(this.handler){
				this.click.removeHandler(this.handler.activate);
				if(this.selectedMarker!== null){
				if(me.selectedMarker.attributes.polyline){
						me.selectedMarker.setIcon(me.selectedMarker.oldIcon);
						me.featureunselected.fire(me.selectedMarker.attributes.polyline);
		  			}else{ 
						me.selectedMarker.setIcon(me.selectedMarker.oldIcon);
						me.featureunselected.fire(me.selectedMarker);
		  			}
		  			me.selectedMarker.updateProprietary();
					me.selectedMarker = null;
				}
			}
			this.map.click.addHandler(addPoint);
			
			break;
			}
		},iconURL: function(state){
			return "http://localhost/idelabmapstraction/resources/"+"icons/default/default-"+state+".png";
			/*switch(state){
			case "edit-vertex":
			return 'http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png'
			case "create":
			return 'http://maps.google.com/mapfiles/kml/pushpin/blue-pushpin.png'
			case "select":	
			return 'http://maps.google.com/mapfiles/kml/pushpin/red-pushpin.png'
			case "add-vertex":	
			return 'http://maps.google.com/mapfiles/kml/pushpin/ylw-pushpin.png'
			}
			//return 'http://maps.google.com/mapfiles/kml/pushpin/red-pushpin.png'
			return '/default-edit.png'*/
		},
		activateEdition: function(){
			var map = this.map.maps[this.api];
			var me = this;
			
			me.selectedMarker = null;
			if(typeof(this.handler) === "undefined"){
				this.handler = [];
				this.handler.activate = function(event,map,point){
					if(me.selectedMarker === null) {return;}
					me.selectedMarker.location = point.location;
			
					if (me.selectedMarker.attributes.polyline){
						me.selectedMarker.attributes.polyline.points.splice(me.selectedMarker.attributes.point,me.selectedMarker.attributes.splice,point.location);
						me.invoker.go('removePolyline',[me.selectedMarker.attributes.polyline]);
						me.addPolyline(me.selectedMarker.attributes.polyline,true,true);
						//me.invoker.go('activatePolyline',[me.selectedMarker.attributes.polyline]);
						me.featuremodified.fire(me.selectedMarker.attributes.polyline);
						//me.polylineChanged.fire({'polyline': me.selectedMarker.attributes.polyline});
						me.featureunselected.fire(me.selectedMarker.attributes.polyline);
					}else{
						me.selectedMarker.setIcon(me.selectedMarker.oldIcon);
						me.selectedMarker.updateProprietary();
						me.featuremodified.fire(me.selectedMarker);
						//me.markerChanged.fire({'marker': me.selectedMarker});
						me.featureunselected.fire(me.selectedMarker);
					}
					
					me.selectedMarker = null;
				};
			}
			me.map.click.addHandler(this.handler.activate);
			for (var i in this.features){
				if (this.features[i] instanceof mxn.Marker)
					this.invoker.go('activateMarker',[this.features[i]]);
				else
					this.invoker.go('activatePolyline',[this.features[i]]);
	  		}
	  		
	  		for (var i in this.myFeatures){
				if (this.myFeatures[i] instanceof mxn.Marker)
					this.invoker.go('activateMarker',[this.myFeatures[i]]);
				else
					this.invoker.go('activatePolyline',[this.myFeatures[i]]);
	  		}

			document.onkeydown=function(e){
				if (!e) e = window.event;
				vKeyCode=e.keyCode;
				if((vKeyCode==63272)||vKeyCode==46){
					if(me.selectedMarker !== null){
		  				if(me.selectedMarker.attributes.polyline){
		  					for (var i in me.selectedMarker.attributes.polyline.markers){
		  						me.invoker.go('removeMarker',[me.selectedMarker.attributes.polyline.markers[i]]	);
		  					}
		  					me.removePolyline(me.selectedMarker.attributes.polyline,true);
		  					
		  				}else{
		  					me.removeMarker(me.selectedMarker,true);
		  				}
		  				me.selectedMarker = null;
		  				if (me.map.onShape != null)	me.map.onShape = null;
		  			}
				}
				if (vKeyCode == 27 && me.selectedMarker !== null){
		  			if(me.selectedMarker.attributes.polyline){
						me.selectedMarker.setIcon(me.selectedMarker.oldIcon);
						me.featureunselected.fire(me.selectedMarker.attributes.polyline);
		  			}else{ 
						me.selectedMarker.setIcon(me.selectedMarker.oldIcon);
						me.featureunselected.fire(me.selectedMarker);
		  			}
		  			me.selectedMarker.updateProprietary();
					me.selectedMarker = null;
				}
			};
		
		},
		activatePolyline:function (polyline){
				var me = this;
				var lat, lon;
				for (var i in polyline.markers){
					me.invoker.go('removeMarker',[polyline.markers[i]]);
				}
				polyline.markers = [];
				for (var j in polyline.points){
					var mark = new mxn.Marker(new mxn.LatLonPoint(polyline.points[j].lat,polyline.points[j].lon));
					mark.attributes.polyline = polyline;
					mark.attributes.point = j;
					mark.attributes.splice = 1;
					mark.setIcon(this.iconURL("edit-vertex"));
					mark.click.addHandler(function(event,marker){
						if(me.selectedMarker !== null){
							if(me.selectedMarker.attributes.polyline){
								me.selectedMarker.setIcon(me.iconURL("edit-vertex"));
							}else{ 
								me.selectedMarker.setIcon(null);
							}						
							me.invoker.go('removeMarker',[me.selectedMarker]);
							me.addMarker(me.selectedMarker,true,true,true);
							me.selectedMarker = null;
						} 
						marker.oldIcon = marker.iconUrl;
						marker.setIcon(me.iconURL("select"));
						me.invoker.go('removeMarker',[marker]);
						me.addMarker(marker,true,true,true);
						me.selectedMarker = marker;
					});
					me.addMarker(mark,true,true,true);
					polyline.markers.push(mark);
					if(j>0){
						lat = (polyline.points[j].lat + polyline.points[j-1].lat)/2;
						lon = (polyline.points[j].lon + polyline.points[j-1].lon)/2;
						middleMark = new mxn.Marker(new mxn.LatLonPoint(lat,lon));
						middleMark.attributes.polyline = polyline;
						middleMark.attributes.point = j;
						middleMark.attributes.splice = 0;
						middleMark.setIcon(this.iconURL("add-vertex"));
						middleMark.click.addHandler(function(event,marker){
							if(me.selectedMarker !== null){
								if(me.selectedMarker.attributes.polyline){
									me.selectedMarker.setIcon(me.iconURL("edit-vertex"));
									me.featureunselected.fire(me.selectedMarker.attributes.polyline);
								}else{ 
									me.selectedMarker.setIcon(null);
									me.featureunselected.fire(me.selectedMarker);
								}
								me.invoker.go('removeMarker',[me.selectedMarker]);
							me.addMarker(me.selectedMarker,true,true,true);
								me.selectedMarker = null;
							}
							marker.oldIcon = marker.iconUrl;
							marker.setIcon(me.iconURL("select"));
							me.invoker.go('removeMarker',[marker]);
							me.addMarker(marker,true,true,true);
							me.selectedMarker = marker;
							me.featureselected.fire(me.selectedMarker.attributes.polyline);
						});
						me.addMarker(middleMark,true,true,true);
						polyline.markers.push(middleMark);
					}
					if (j == polyline.points.length - 1 &&polyline.closed  === true){
						lat = (polyline.points[j].lat + polyline.points[0].lat)/2;
						lon = (polyline.points[j].lon + polyline.points[0].lon)/2;
						middleMark = new mxn.Marker(new mxn.LatLonPoint(lat,lon));
						middleMark.attributes.polyline = polyline;
						middleMark.attributes.point = j+1;
						middleMark.attributes.splice = 0;
						middleMark.setIcon(this.iconURL("add-vertex"));
						middleMark.click.addHandler(function(event,marker){
							if(me.selectedMarker !== null){
								if(me.selectedMarker.attributes.polyline){
									me.selectedMarker.setIcon(me.iconURL("edit-vertex"));
									me.featureunselected.fire(me.selectedMarker.attributes.polyline);
								}else{ 
									me.selectedMarker.setIcon(me.selectedMarker.oldIcon);
									me.featureunselected.fire(me.selectedMarker);
								}
								me.invoker.go('removeMarker',[marker]);
								me.addMarker(marker,true,true,true);
								me.selectedMarker = null;
							} 
							marker.oldIcon = marker.iconUrl;
							marker.setIcon(me.iconURL("select"));
							me.invoker.go('removeMarker',[marker]);
							me.addMarker(marker,true,true,true);

							me.selectedMarker = marker;
							me.featureselected.fire(me.selectedMarker.attributes.polyline);
						});
						me.addMarker(middleMark,true,true,true);
						polyline.markers.push(middleMark);
					}
					
				}
			}
			
		,activateMarker: function(activeMarker){
			if(typeof(activeMarker.handler) === "undefined"){
				activeMarker.handler = [];
				activeMarker.handler.activate = function(event,marker){
					var map = marker.WFSLayer;
					if(map.selectedMarker !== null){
						if(map.selectedMarker.attributes.polyline){
							map.selectedMarker.setIcon(map.iconURL("edit-vertex"));
							map.featureunselected.fire(map.selectedMarker.attributes.polyline);
						}else{ 
							map.selectedMarker.setIcon(map.selectedMarker.oldIcon);
							map.featureunselected.fire(map.selectedMarker);
						}
						map.selectedMarker.updateProprietary();
						map.selectedMarker = null;
					} 
					marker.oldIcon = marker.iconUrl;
					marker.setIcon(map.iconURL("select"));
					me.invoker.go('removeMarker',[marker]);
					me.addMarker(marker,true,true,true);
					map.selectedMarker = marker;
					map.featureselected.fire(map.selectedMarker);
				};
			}
			activeMarker.click.addHandler(activeMarker.handler.activate);
		},
		deactivateEdition: function(){
			this.map.click.removeHandler(this.handler.activate);
			if(this.selectedMarker!== null &&this.selectedMarker.attributes.polyline === null){
				this.selectedMarker.setIcon(null);
				this.selectedMarker.updateProprietary();
			}
			for (var i in this.features){
				if(this.features[i] instanceof mxn.Polyline){
					for (var j in this.features[i].markers){
						this.invoker.go('removeMarker',[this.features[i].markers[j]]);
					}
				}else{
					this.features[i].click.removeHandler(this.features[i].handler.activate);
				}
			}
			
			for (var i in this.myFeatures){
				if(this.myFeatures[i] instanceof mxn.Polyline){
					for (var j in this.myFeatures[i].markers){
						this.invoker.go('removeMarker',[this.myFeatures[i].markers[j]]);
					}
				}else{
					this.myFeatures[i].click.removeHandler(this.myFeatures[i].handler.activate);
				}
			}

		},
		commit: function(added,removed,modified){
			for (var i in added){
				
			}
			var aux = [];
			for (var i in removed){
				removed[i].state = OpenLayers.State.DELETE;
				auxRemoved.push(removed[i]);
				
			}
			//this.layer.strategies[1].save(auxRemoved);
			//var auxModified = [];
			for (var i in modified){
				var auxFeature = modified[i].getAttribute('feature');
				var newGeom = modified[i].toOpenLayers().geometry;
				var result;
				function recorrer(geom,attribute){
					for (var i in geom){
						if(geom[i].id == attribute.id){
							result = {geometry:geom[i],index:i};
						}else{
							recorrer(geom[i].components,attribute);
						}
					}
				}
				recorrer([auxFeature.geometry],modified[i].getAttribute('oGeometry'));
				newGeom.transform(new OpenLayers.Projection("EPSG:900913"),new OpenLayers.Projection("EPSG:4326"));
				var parentGeom = result.geometry.parent;
				parentGeom.components[result.index] = newGeom;
				modified[i].setAttribute('oGeometry',newGeom);
				parentGeom.components[result.index].parent = parentGeom;
				auxFeature.state = OpenLayers.State.UPDATE;
				aux.push(auxFeature);
			}
			this.layer.strategies[1].save(aux);
		}
	},
	Mapstraction:{
		removeFeatures:function(layer){
			for (var i in layer.features){
				if(layer.features[i] instanceof mxn.Marker) layer.invoker.go('removeMarker',[layer.features[i]]);
				else  layer.invoker.go('removePolyline',[layer.features[i]]);
			}
			for (var i in layer.myFeatures){
				if(layer.myFeatures[i] instanceof mxn.Marker) layer.invoker.go('removeMarker',[layer.myFeatures[i]]);
				else layer.invoker.go('removePolyline',[layer.myFeatures[i]]);
			}
		
		}
	}
});