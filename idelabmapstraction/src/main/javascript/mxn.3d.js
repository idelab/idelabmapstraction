(function(){

	function extend(subclass, superclass) {
	   	function Dummy() {}
	   	Dummy.prototype = superclass.prototype;
	   	subclass.prototype = new Dummy();
	   	subclass.prototype.constructor = subclass;
	   	subclass.superclass = superclass;
	   	subclass.superproto = superclass.prototype;
	}
	
	/**
	 * Mapstraction3D instantiates an interactive map with some API choice into the HTML element given
	 * @param {String} element The HTML element to replace with an interactive map
	 * @param {String} api The API to use, one of 'google', 'yahoo', 'microsoft', 'openstreetmap', 'multimap', 'map24', 'openlayers', 'mapquest'. If omitted, first loaded provider implementation is used.
	 * @param {Bool} debug optional parameter to turn on debug support - this uses alert panels for unsupported actions
	 * @extends mxn.Mapstraction
	 * @constructor
	 */
	mxn.Mapstraction3D = function (element, api,debug){
		mxn.Mapstraction3D.superclass.call(this, element,api,debug);
		mxn.addEvents(this, [
			'markerChanged',	// Marker is changed {marker: Marker}
			'polylineChanged',	// Polyline is changed {polyline: Polyline}
			'markerSelected',	// Marker is selected
			'polylineSelected',	// Polyline is selected
			'markerUnselected', // Marker is unselected
			'polylineUnselected'// Polyline is unselected
		]);

		var scripts = document.getElementsByTagName('script');
		for (var i = 0; i < scripts.length; i++) {
			var match = scripts[i].src.replace(/%20/g , '').match(/^(.*?)(mxn|idelabmapstraction)\.js/);
			if (match !== null) {
				this.src = match[1];
				break;
		   }
		}
		/**
		 * POINT FEATURE
		 */
		this.POINT = "point";
		/**
		 * LINESTRING FEATURE
		 */
		this.LINESTRING = "linestring";
		/**
		 *POLYGON FEATURE
		 */
		this.POLYGON = "polygon";
		/**
		 * ALLOWED FEATURES ARRAY
		 */
		this.ALLOWED_FEATURES = [this.POINT,this.LINESTRING,this.POLYGON];
	};
	var Mxn3D = mxn.Mapstraction3D; 
	extend(mxn.Mapstraction3D,mxn.Mapstraction);

	mxn.addProxyMethods(mxn.Mapstraction3D, [ 
                                   
    'getPitch',
    
    'setPitch',
    
    'getHeading',
    
    'setHeading',
    
    'getFieldOfView',
    
    'setFieldOfView'   

]);


})();