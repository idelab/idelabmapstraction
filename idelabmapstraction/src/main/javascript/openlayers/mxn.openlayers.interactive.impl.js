var OpenLayersInteractive = function(){
	
	this.Mapstraction = {
		activateEdition:function(){
			var map = this.maps[this.api];
			var me = this;
			
			this.controls.modify.activate();
			function feature_modified(event_triggered){
				var geom = event_triggered.feature.geometry.clone();
				if(this.map.getProjection()!="EPSG:4326"){
					geom.transform(new OpenLayers.Projection("EPSG:900913"),new OpenLayers.Projection("EPSG:4326"));
				}
				switch(event_triggered.feature.geometry.CLASS_NAME.split(".")[2].toLowerCase()){
				case "point":
					if(event_triggered.feature.popup){
						me.controls.select.unselect(event_triggered.feature);
					}
					event_triggered.feature.mapstraction_marker.location = new mxn.LatLonPoint(geom.y,geom.x);
					me.markerChanged.fire({'marker': event_triggered.feature.mapstraction_marker});
				break;
				case "linestring":
					var points = [];
					for(var i in geom.components){
						points[i] = new mxn.LatLonPoint(geom.components[i].y,geom.components[i].x);
					}
					event_triggered.feature.mapstraction_polyline.points = points;
					me.polylineChanged.fire({'polyline': event_triggered.feature.mapstraction_polyline});
				break;
				case "polygon":
					var points = [];
					for(var i in geom.components[0].components){
						points[i] = new mxn.LatLonPoint(geom.components[0].components[i].y,geom.components[0].components[i].x);
					}
					event_triggered.feature.mapstraction_polyline.points = points;
					me.polylineChanged.fire({'polyline': event_triggered.feature.mapstraction_polyline});
				break;
				}
			}
			if(this.olayers.features.events.listeners.featuremodified.length === 0){
				this.olayers.features.events.register("featuremodified",this.olayers.feature,feature_modified);	
			}
			if(this.olayers.features.events.listeners.featureselected.length === 0){
				this.olayers.features.events.register("featureselected",this.olayers.feature,function(event_triggered){
					switch(event_triggered.feature.geometry.CLASS_NAME.split(".")[2].toLowerCase()){
					case "point":
						me.markerSelected.fire({'marker': event_triggered.feature.mapstraction_marker}); 
					break;
					case "linestring":
					case "polygon":
						me.polylineSelected.fire({'polyline': event_triggered.feature.mapstraction_polyline});
					break;
					}
				});	
				this.olayers.features.events.register("featureunselected",this.olayers.feature,function(event_triggered){
					switch(event_triggered.feature.geometry.CLASS_NAME.split(".")[2].toLowerCase()){
					case "point":
						me.markerUnselected.fire({'marker': event_triggered.feature.mapstraction_marker}); 
					break;
					case "linestring":
					case "polygon":
						me.polylineUnselected.fire({'polyline': event_triggered.feature.mapstraction_polyline});
					break;
					}
				});
			}
			document.onkeydown=function(e){
				if (!e) e = window.event;
				vKeyCode=e.keyCode;
				if((vKeyCode==63272)||vKeyCode==46){
					var vector_to_remove = me.olayers.features.selectedFeatures[0];
			    	if(typeof(vector_to_remove) != "undefined"){
				    	if(vector_to_remove.mapstraction_marker){
				    		me.removeMarker(vector_to_remove.mapstraction_marker);
				    	}else{
				    		me.removePolyline(vector_to_remove.mapstraction_polyline);
				    	}
			    	}
			    	me.controls.modify.deactivate();
			    	me.controls.modify.activate();
				}
			};
		},
		deactivateEdition:function(){
			
			this.controls.modify.deactivate();
			this.olayers.features.events.remove("featureselected");
			this.olayers.features.events.remove("featureunselected");
			
		},
		addFeature:function(feature,data){
			var map = this.maps[this.api];
			var me = this;
			var poly;
			
			this.controls['draw_'+feature].activate();
			function feature_added(event_triggered){
				var geom = event_triggered.feature.geometry.clone();
				if(this.map.getProjection()!= "EPSG:4326"){
					geom.transform(new OpenLayers.Projection("EPSG:900913"),new OpenLayers.Projection("EPSG:4326"));
				}
				switch(feature){
				case "point":
				 	var marker = new mxn.Marker(new mxn.LatLonPoint(geom.y,geom.x));
					me.addMarkerWithData(marker,data);
					
				break;
				case "polygon":
					var points = [];
					for(var i in geom.components[0].components){
						points[i] = new mxn.LatLonPoint(geom.components[0].components[i].y,geom.components[0].components[i].x);
					}
					poly = new mxn.Polyline(points);
					poly.setClosed(true);
					me.addPolylineWithData(poly,data);
					break;
				case "linestring":
					var points = [];
					for(var i in geom.components){
						points[i] = new mxn.LatLonPoint(geom.components[i].y,geom.components[i].x);
					}
					poly = new mxn.Polyline(points);
					me.addPolylineWithData(poly,data);
				break;
				} 
				event_triggered.feature.destroy();
				me.controls['draw_'+feature].deactivate();
				me.addingFeature = false;
			}
	
			if(this.controls['draw_'+feature].events.listeners.featureadded.length === 0){
				this.controls['draw_'+feature].events.register('featureadded',this.controls['draw_'+feature],feature_added);
			}
		}
	};
};

var openlayers = new OpenLayersInteractive();

mxn.register('openlayers',openlayers);

mxn.register('openlayers',{
	WFSLayer:{
		addFeatures: function(features){
			var aux = [];
			for (var i in features){
				aux[i]=features[i];
			}
			var encontrado;

			for (var i in aux){
				encontrado = false;
				for (var j in this.removedFeatures){
					if (aux[i].fid == this.removedFeatures[j].fid) {
						this.layer.removeFeatures([aux[i]]);
						encontrado = true;
						break;
					}
				}
				if (!encontrado){
					for (var j in this.modifiedFeatures){
						if (aux[i].fid == this.modifiedFeatures[j].fid) {
							this.layer.removeFeatures([aux[i]]);
							this.layer.addFeatures(this.modifiedFeatures[j]); 
						}
					}
				}
			}
			for (var i in this.myFeatures){
				if(this.myFeatures[i] instanceof mxn.Marker) this.addMarker(this.myFeatures[i],true,false,true);
				else this.addPolyline(this.myFeatures[i],true,false,true);
			}
		},
		addMarker: function(marker, old) {
			var map = this.map.maps[this.map.api];
			var pin = marker.toProprietary(this.map.api);
			this.layer.addFeatures(pin);
							     	
			return pin;
		},
		removeMarker: function(marker) {
			var map = this.map.maps[this.api];
	
			this.layer.removeFeatures([marker.proprietary_marker]);
		},
		addPolyline: function(polyline, old) {
			var map = this.map.maps[this.api];
			var pl = polyline.toProprietary(this.api);
			this.layer.addFeatures(pl);
			return pl;
		},removePolyline: function(polyline) {
			var map = this.map.maps[this.api];
			
			this.layer.removeFeatures([polyline.proprietary_polyline]);
		},
		activateEdition:function(){
			var map = this.map.maps[this.api];
			var me = this;
			
			this.controls.modify.activate();
			function feature_modified(event_triggered){
				var geom = event_triggered.feature.geometry.clone();
				var feature = event_triggered.feature.clone();
				if(this.map.getProjection()!="EPSG:4326"){
					geom.transform(new OpenLayers.Projection("EPSG:900913"),new OpenLayers.Projection("EPSG:4326"));
				}
				switch(event_triggered.feature.geometry.CLASS_NAME.split(".")[2].toLowerCase()){
				case "point":
					if(event_triggered.feature.popup){
						me.controls.select.unselect(event_triggered.feature);
					}
					//event_triggered.feature.mapstraction_marker.location = new mxn.LatLonPoint(geom.y,geom.x);
					me.featuremodified.fire(event_triggered.feature.mapstraction_marker);
				break;
				case "linestring":
					var points = [];
					for(var i in geom.components){
						points[i] = new mxn.LatLonPoint(geom.components[i].y,geom.components[i].x);
					}
					event_triggered.feature.mapstraction_polyline.points = points;
					me.featuremodified.fire(event_triggered.feature.mapstraction_polyline);
				break;
				case "polygon":
				case 'multilinestring':
					var points = [];
					for(var i in geom.components[0].components){
						points[i] = new mxn.LatLonPoint(geom.components[0].components[i].y,geom.components[0].components[i].x);
					}
					event_triggered.feature.mapstraction_polyline.points = points;
					me.featuremodified.fire(event_triggered.feature.mapstraction_polyline);
				break;
				}
				/*
				//If the feature comes from the WFS server
				if(event_triggered.feature.mapstraction_polyline == null && event_triggered.feature.mapstraction_marker == null){
					for (var i in features){
						if (event_triggered.feature.id == me.features[i].getAttribute('feature').id){
							///REVISAR ESTO
						}
					}
				}*/
				
				
			}
			if(this.layer.events.listeners.featuremodified.length === 0){
				this.layer.events.register("featuremodified",this.layer,feature_modified);	
			}
			if(this.layer.events.listeners.featureselected.length === 0){
				this.layer.events.register("featureselected",this.layer,function(event_triggered){
					switch(event_triggered.feature.geometry.CLASS_NAME.split(".")[2].toLowerCase()){
					case "point":
						me.featureselected.fire(event_triggered.feature.mapstraction_marker); 
					break;
					case "linestring":
					case "polygon":
					case "multilinestring":
						me.featureselected.fire(event_triggered.feature.mapstraction_polyline);
					break;
					}
				});	
				this.layer.events.register("featureunselected",this.layer,function(event_triggered){
					switch(event_triggered.feature.geometry.CLASS_NAME.split(".")[2].toLowerCase()){
					case "point":
						me.featureunselected.fire(event_triggered.feature.mapstraction_marker); 
					break;
					case "linestring":
					case "polygon":
					case "multilinestring":
						me.featureunselected.fire(event_triggered.feature.mapstraction_polyline);
					break;
					}
				});
			}
			document.onkeydown=function(e){
				if (!e) e = window.event;
				vKeyCode=e.keyCode;
				if((vKeyCode==63272)||vKeyCode==46){
					var vector_to_remove = me.layer.selectedFeatures[0];
			    	if(typeof(vector_to_remove) != "undefined"){
				    	if(vector_to_remove.mapstraction_marker){
				    		me.removeMarker(vector_to_remove.mapstraction_marker,true);
				    	}else if(vector_to_remove.mapstraction_polyline){
				    		me.removePolyline(vector_to_remove.mapstraction_polyline,true);
				    	}/*else{
				    		me.layer.removeFeatures(vector_to_remove);
				    		me.removedFeatures.push(vector_to_remove);
				    	}*/
			    	}
			    	me.controls.modify.deactivate();
			    	me.controls.modify.activate();
				}
			};
		},
		deactivateEdition:function(){
			
			this.controls.modify.deactivate();
			this.layer.events.remove("featureselected");
			this.layer.events.remove("featureunselected");
			
		},
		addFeature:function(feature,data){
			var map = this.map.maps[this.api];
			var me = this;
			var poly;
			
			this.controls['draw_'+feature].activate();
			function feature_added(event_triggered){
				var geom = event_triggered.feature.geometry.clone();
				if(this.map.getProjection()!= "EPSG:4326"){
					geom.transform(new OpenLayers.Projection("EPSG:900913"),new OpenLayers.Projection("EPSG:4326"));
				}
				switch(feature){
				case "point":
				 	var marker = new mxn.Marker(new mxn.LatLonPoint(geom.y,geom.x));
				 	marker.addData(data);
					me.addMarker(marker,false,false,true);
					
				break;
				case "polygon":
					var points = [];
					for(var i in geom.components[0].components){
						points[i] = new mxn.LatLonPoint(geom.components[0].components[i].y,geom.components[0].components[i].x);
					}
					poly = new mxn.Polyline(points);
					poly.setClosed(true);
					me.addPolyline(poly, false,false,true);
					//me.addPolylineWithData(poly,data);
					break;
				case "linestring":
					var points = [];
					for(var i in geom.components){
						points[i] = new mxn.LatLonPoint(geom.components[i].y,geom.components[i].x);
					}
					poly = new mxn.Polyline(points);
					//me.addPolylineWithData(poly,data);
					me.addPolyline(poly, false,false,true);
				break;
				} 
				event_triggered.feature.destroy();
				me.controls['draw_'+feature].deactivate();
				me.addingFeature = false;
			}
	
			if(this.controls['draw_'+feature].events.listeners.featureadded.length === 0){
				this.controls['draw_'+feature].events.register('featureadded',this.controls['draw_'+feature],feature_added);
			}
		},
		commit: function(added, removed, modified){

			for (var i in removed){
				removed[i].state = OpenLayers.State.DELETE;
			}
			this.layer.strategies[1].save(removed);
			/*for (var i in added){
				added[i].state = OpenLayers.State.INSERT;
			}
			this.layer.strategies[1].save(added);*/
			var auxModified = [];
			for (var i in modified){
				auxModified.push(modified[i].getAttribute('feature'));
			}
			this.layer.strategies[1].save(auxModified.concat(removed));
		}
	},
	Mapstraction:{
		removeFeatures:function(layer){
			this.map[this.api].removeLayer(layer.layer);
		
		}
	}
});

mxn.register('openlayers',{
	TileLayer:{
		init:function(){
		  var layer = new OpenLayers.Layer.WMS(
	          this.name, this.tileURL,
	          {
	              tiled: 'true'
	          },{
	            buffer: 0,
	            displayOutsideMaxExtent: true
	        } 
	
	      );
	      this.layer = layer;
	      this.map.maps[this.api].addLayer(layer);
	      this.map.maps[this.api].setBaseLayer(layer); 
		},
		toggle: function(){
			if (this.layer.visibility){
				this.map.maps[this.api].setBaseLayer(this.map.olayers.osm2);
			}else{
				this.map.maps[this.api].setBaseLayer(this.layer);
			}
		}
	}
	
});

mxn.register('openlayers',{
	Mapstraction:{
		addWMSLayer:function(wmsLayer){
			this.maps[this.api].addLayer(wmsLayer.layer);
		},
		removeWMSLayer:function(wmsLayer){
			this.maps[this.api].removeLayer(wmsLayer.layer);
		}
	},
	WMSLayer:{
		init:function(){
			var me = this;
			var layer = new OpenLayers.Layer.WMS(
                me.name,
                me.URL,
                {layers: me.layerName},
                {singleTile: true,isBaseLayer:false}
            );
			this.layer = layer;
		}
	}
});

mxn.register('openlayers',{
	XMLLayer:{
		init: function(){
			if(this.URL.substring(this.URL.length-3,this.URL.length) == "kml"){
			  var layer = new OpenLayers.Layer.GML("KML Layer", this.URL,{
                format: OpenLayers.Format.KML, 
                formatOptions: {
                  extractStyles: true, 
                  extractAttributes: true,
                  maxDepth: 2
                }
               });
			}else{
			  var layer = new OpenLayers.Layer.GeoRSS("GeoRSS Layer", this.URL);
			}
			this.layer = layer;
			this.map.maps[this.api].addLayer(layer);
		},
		toggle:function(){
			if(this.shown) {
				this.map.maps[this.api].removeLayer(this.layer);
				this.shown = false;
			}
			else {
				this.map.maps[this.api].addLayer(this.layer);
				this.shown = true;
			}
		}
	}
});