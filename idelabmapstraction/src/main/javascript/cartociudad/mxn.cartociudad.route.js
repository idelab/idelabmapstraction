mxn.register('cartociudad',{
	MapstractionRouter:{
		init:function(callback, api){
			this.geocoders[api] = new metodosCartociudad();
			this.routers[api] = new metodosCartociudad();
		},
		route: function(addresses){
			var error = 0;
			for (var i = 0; i < addresses.length; i++) {
				addresses[i].error=0;
				this.geocoders[this.api].queryNomenclator(addresses[i]);
				
				if(addresses[i].error==0){
					this.geocoders[this.api].addressToMapstraction(addresses[i]);
				}
				else{
					var direccion = addresses[i];
					error = addresses[i].error;
					break;
				}
			}
			if (error == 0) {
				var ruta = this.routers[this.api].queryRoute(addresses);
				
				if (ruta.error !== 0) {//error de routing
					this.error_callback(ruta.error);
				}
				else {
					this.callback(addresses, ruta);
				}
			}
			else{//error de geocoding
				this.geocoders[this.api].addressToMapstraction(direccion);
				this.error_callback(error,direccion);
			}
		},
		routePoints:function(points){
			var ruta = this.routers[this.api].queryRoute(points);
			var waypoints = ruta.points;

			var paramsRuta = new Object();
			paramsRuta.bounds = ruta.bounds;
																											
			this.callback(waypoints, paramsRuta);
		},
		route_callback: function(){
			
		}	
	}
});