var cartociudad = new OpenLayersInteractive();

mxn.register('cartociudad',cartociudad);

mxn.register('cartociudad',{
	XMLLayer:{
		init: function(){
			if(this.URL.substring(this.URL.length-3,this.URL.length) == "kml"){
			  var layer = new OpenLayers.Layer.GML("KML Layer", this.URL,{
                format: OpenLayers.Format.KML, 
                formatOptions: {
                  extractStyles: true, 
                  extractAttributes: true,
                  maxDepth: 2
                }
               });
			}else{
			  var layer = new OpenLayers.Layer.GeoRSS("GeoRSS Layer", this.URL);
			}
			this.layer = layer;
			this.map.maps[this.api].addLayer(layer);
		},
		toggle:function(){
			if(this.shown) {
				this.map.maps[this.api].removeLayer(this.layer);
				this.shown = false;
			}
			else {
				this.map.maps[this.api].addLayer(this.layer);
				this.shown = true;
			}
		}
	}
});