(function(){
var OpenLayersAdapter = function (map){
	if (map.maps[map.api] instanceof OpenLayers.Map){
		this.map = map.maps[map.api];
		this.init = function(layer){
			this.map.addLayer(layer.layer);
			layer.controls = [];
        	layer.controls.draw_point = new OpenLayers.Control.DrawFeature(layer.layer, OpenLayers.Handler.Point);
        	layer.map.maps[layer.api].addControl(layer.controls.draw_point);
        	layer.controls.draw_point.deactivate();
        	layer.controls.draw_linestring = new OpenLayers.Control.DrawFeature(layer.layer, OpenLayers.Handler.Path);
        	layer.map.maps[layer.api].addControl(layer.controls.draw_linestring);
        	layer.controls.draw_linestring.deactivate();
        	layer.controls.draw_polygon = new OpenLayers.Control.DrawFeature(layer.layer, OpenLayers.Handler.Polygon);
        	layer.map.maps[layer.api].addControl(layer.controls.draw_polygon);
        	layer.controls.draw_polygon.deactivate();
        	layer.controls.modify = new OpenLayers.Control.ModifyFeature(layer.layer);
        	layer.map.maps[layer.api].addControl(layer.controls.modify);
        	layer.controls.modify.deactivate();
        	layer.layer.events.register('loadend',layer.layer,function(e){
		  	function recorreGeom(geometries,feature){
		  		for (var i in geometries){
		  			if(geometries[i] instanceof OpenLayers.Geometry.Point ||
		  				geometries[i] instanceof OpenLayers.Geometry.LineString ||
		  				geometries[i] instanceof OpenLayers.Geometry.Polygon){
		  				switch(geometries[i].toString().substr(0,geometries[i].toString().indexOf('(')).toLowerCase()){
							case "point":	
						  	  var point = new mxn.LatLonPoint(geometries[i].y,geometries[i].x);
						  	  if(map.api == 'openlayers')point.fromProprietary(map.api);
						  	  var marker = new mxn.Marker(point);
						  	  marker.setAttribute('oGeometry',geometries[i]);
						  	  marker.setAttribute('feature',feature);
						  	  layer.features.push(marker);
						  	  marker.setChild(feature);
							break;
							case "linestring":
							  var points = [];
							  for (var j in geometries[i].components){
							  	var point = new mxn.LatLonPoint(geometries[i].components[j].y,geometries[i].components[j].x);
							  	if(map.api == 'openlayers')point.fromProprietary(map.api);
							  	points.push(point);
							  }
						  	  var linestring = new mxn.Polyline(points);
						  	  linestring.setAttribute('oGeometry',geometries[i]);
						  	  linestring.setAttribute('feature',feature);
						  	  layer.features.push(linestring);
						  	  linestring.setChild(feature);
							break;
							case "polygon":
							  var points = [];
							  for (var j in geometries[i].components[0].components){
							  	var point = new mxn.LatLonPoint(geometries[i].components[0].components[j].y,geometries[i].components[0].components[j].x);
							  	if(map.api == 'openlayers')point.fromProprietary(map.api);
							  	points.push(point);
							  }
						  	  var polygon = new mxn.Polyline(points);
						  	  polygon.setClosed(true);
						  	  polygon.setAttribute('oGeometry',geometries[i]);
						  	  polygon.setAttribute('feature',feature);
						  	  layer.features.push(polygon);
						  	  polygon.setChild(feature);
							break;
							}
		  			}else{
		  				recorreGeom(geometries[i].components,feature/*,tree*/);
		  			}
		  		}
		  	}
		  	for (var i =layer.features.length-1;i>=0;i--){
		  		if(layer.features[i] instanceof mxn.Marker){
		  			layer.removeMarker(layer.features[i],true);
		  		}else{
		  			if (layer.features[i].markers){
		  				for (var j in layer.features[i].markers)
		  					layer.invoker.go('removeMarker',[layer.features[i].markers[j]]);
		  			}
		  			layer.removePolyline(layer.features[i],false,true);
		  		}
		  	}
		  	layer.features = [];
		  	for (var i in e.object.features)  	recorreGeom([e.object.features[i].geometry],e.object.features[i]);
		  	if(e.object.features.length >0){
		  	  layer.loadend.fire(layer.features);
		  	  layer.invoker.go('addFeatures',[layer.features]);
		  	}
		  });
		  layer.strategies[0].update();
		};
	}else{
		this.map = map;
		this.maxExtent  = null;
		this.projection = new OpenLayers.Projection("EPSG:4326"); 
		this.units = "degrees";
		this.getTileSize = function(){return new OpenLayers.Size(256,256);};
		this.div = map.currentElement;
		this.getProjectionObject = function(){
			return this.projection;
		};
		this.getSize = function() {
			var div = this.div;
			var size = new OpenLayers.Size(div.clientWidth,
			div.clientHeight);
			
			if (size.w == 0 && size.h == 0 || isNaN(size.w) && isNaN(size.h)) {
				size.w = div.offsetWidth;
				size.h = div.offsetHeight;
			}
			if (size.w == 0 && size.h == 0 || isNaN(size.w) && isNaN(size.h)) {
				size.w = parseInt(div.style.width);
				size.h = parseInt(div.style.height);
			}
			return size;
		};
		this.getExtent = function(){
			var bbox = map.getBounds();
			return new OpenLayers.Bounds(bbox.getSouthWest().lon,bbox.getSouthWest().lat,bbox.getNorthEast().lon,bbox.getNorthEast().lat);
		};
		this.getResolution = function(){
			return 0.021;
		};
		
		this.init = function(layer){
		  this.layer = layer;
	      var updateLayer = function(type,map){
			map.layers[layer.name].layer.strategies[0].update();
		  };
		  this.map.endPan.addHandler(updateLayer);
		  layer.layer.setMap(this);
		  layer.layer.strategies[0].update();
		  layer.layer.strategies[1].events.register('success',layer.layer.strategies[1],function(e){
		  	layer.success.fire(e.response);
		  	if(layer.forceUpdate){
		  		e.object.layer.strategies[0].update({force:true});
		  		layer.forceUpdate = false;
		  	}
		  });
		  layer.layer.events.register('loadend',layer.layer,function(e){
		  	function recorreGeom(geometries,feature){
		  		for (var i in geometries){
		  			if(geometries[i] instanceof OpenLayers.Geometry.Point ||
		  				geometries[i] instanceof OpenLayers.Geometry.LineString ||
		  				geometries[i] instanceof OpenLayers.Geometry.Polygon){
		  				switch(geometries[i].toString().substr(0,geometries[i].toString().indexOf('(')).toLowerCase()){
							case "point":	
						  	  var point = new mxn.LatLonPoint(geometries[i].y,geometries[i].x);
						  	  if(map.api == 'openlayers')point.fromProprietary(map.api);
						  	  var marker = new mxn.Marker(point);
						  	  marker.setAttribute('oGeometry',geometries[i]);
						  	  marker.setAttribute('feature',feature);
						  	  layer.features.push(marker);
							break;
							case "linestring":
							  var points = [];
							  for (var j in geometries[i].components){
							  	var point = new mxn.LatLonPoint(geometries[i].components[j].y,geometries[i].components[j].x);
							  	if(map.api == 'openlayers')point.fromProprietary(map.api);
							  	points.push(point);
							  }
						  	  var linestring = new mxn.Polyline(points);
						  	  linestring.setAttribute('oGeometry',geometries[i]);
						  	  linestring.setAttribute('feature',feature);
						  	  layer.features.push(linestring);
							break;
							case "polygon":
							  var points = [];
							  for (var j in geometries[i].components[0].components){
							  	var point = new mxn.LatLonPoint(geometries[i].components[0].components[j].y,geometries[i].components[0].components[j].x);
							  	if(map.api == 'openlayers')point.fromProprietary(map.api);
							  	points.push(point);
							  }
						  	  var polygon = new mxn.Polyline(points);
						  	  polygon.setClosed(true);
						  	  polygon.setAttribute('oGeometry',geometries[i]);
						  	  polygon.setAttribute('feature',feature);
						  	  layer.features.push(polygon);
							break;
							}
		  			}else{
		  				recorreGeom(geometries[i].components,feature/*,tree*/);
		  			}
		  		}
		  	}
		  	for (var i =layer.features.length-1;i>=0;i--){
		  		if(layer.features[i] instanceof mxn.Marker){
		  			layer.removeMarker(layer.features[i],true);
		  		}else{
		  			if (layer.features[i].markers){
		  				for (var j in layer.features[i].markers)
		  					layer.invoker.go('removeMarker',[layer.features[i].markers[j]]);
		  			}
		  			layer.removePolyline(layer.features[i],false,true);
		  		}
		  	}
		  	layer.features = [];
		  	for (var i in e.object.features)  	recorreGeom([e.object.features[i].geometry],e.object.features[i]);
		  	if(e.object.features.length >0){
		  	  layer.loadend.fire(layer.features);
		  	  layer.invoker.go('addFeatures',[layer.features]);
		  	}
		  });
		};
	}
};

var WFSLayer = mxn.WFSLayer = function(name,strategies, protocol){
  this.api = null;
  this.name = name;
  this.strategies = strategies;
  this.protocol = protocol;
  this.adapter = null;
  this.features = [];
  this.myFeatures = [];
  this.removedFeatures = [];
  this.modifiedFeatures = [];
  this.map = null;
  this.forceUpdate = false;
  var me = this;
  this.layer = new OpenLayers.Layer.Vector(name, {
      strategies: strategies,
      protocol: new OpenLayers.Protocol.WFS(protocol)
  });
  this.invoker = new mxn.Invoker(this, 'WFSLayer', function(){return this.api;});
  mxn.addEvents(this, [ 
  	'fail',
	'featureadded',	
	'featureremoved', 	
	'featureselected',
	'featureunselected',
	'featuremodified',
	'loadend',
	'success'
					
  ]);

  this.featuremodified.addHandler(function(event,layer,feature){
  	if(typeof feature.getAttribute('feature') != "undefined"){
  		me.modifiedFeatures[feature.pllID] = feature;
  	}
  });
  this.strategies[1].events.register('success',this.strategies[1],function(e){
  	me.success.fire(e.response);
  });
  this.strategies[1].events.register('fail',this.strategies[1],function(e){
  	me.fail.fire(e.response);
  });

  
    /**
	 * POINT FEATURE
	 */
	this.POINT = "point";
	/**
	 * LINESTRING FEATURE
	 */
	this.LINESTRING = "linestring";
	/**
	 *POLYGON FEATURE
	 */
	this.POLYGON = "polygon";
	/**
	 * ALLOWED FEATURES ARRAY
	 */
	this.ALLOWED_FEATURES = [this.POINT,this.LINESTRING,this.POLYGON];
};

mxn.addProxyMethods(WFSLayer, ['commit','addFeature','activateEdition','deactivateEdition','activatePolyline','activateMarker','iconURL']);

/*WFSLayer.prototype.autoSaveMode = function(mode){
	this.autosave = mode;
} */

WFSLayer.prototype.getFeatures = function(){
  var text = "";
  for (var i in this.layer.features){
	text += "\n" + this.layer.features[i].geometry.toString();
  };
};

WFSLayer.prototype.initialyze = function(map){
  this.adapter = new OpenLayersAdapter(map);
  this.adapter.init(this);
  this.map = map;
  this.api = map.api;
  var features  = this.features;
  var layer = this;
};

WFSLayer.prototype.addMarker = function(marker,old,update,active){
	marker.mapstraction = this;
	marker.api = this.map.api;
	marker.location.api = this.map.api;
	marker.map = this.map.maps[this.map.api]; 
	marker.layer = this.layer;
	marker.WFSLayer = this;
	var propMarker = this.invoker.go('addMarker', arguments);
	marker.setChild(propMarker);
	if (!old) {
		this.myFeatures.push(marker);
	}
	if(!update) {this.featureadded.fire(marker);}
	if(this.editionActive && !active){
		this.invoker.go('activateMarker',[marker]);
	}
};

WFSLayer.prototype.removeMarker = function(marker,update){
	var current_marker;
	for(var i = 0; i < this.myFeatures.length; i++){
		current_marker = this.myFeatures[i];
		if(marker == current_marker) {
			this.invoker.go('removeMarker', arguments);
			marker.onmap = false;
			if (typeof marker.getAttribute('feature') != "undefined"){
				this.removedFeatures.push(marker.getAttribute('feature'));
			}
			this.myFeatures.splice(i, 1);
			if(!update){this.featureremoved.fire(marker);}
			break;
		}
	}
	
	for(var i = 0; i < this.features.length; i++){
		current_marker = this.features[i];
		if(marker == current_marker) {
			this.invoker.go('removeMarker', arguments);
			marker.onmap = false;
			if (typeof marker.getAttribute('feature') != "undefined"){
				this.removedFeatures.push(marker.getAttribute('feature'));
			}
			this.features.splice(i, 1);
			if(!update){this.featureremoved.fire(marker);}
			break;
		}
	}
};

WFSLayer.prototype.addPolyline = function(polyline, old, update,active) {
	polyline.mapstraction = this;
	polyline.api = this.map.api;
	polyline.map = this.map.maps[this.map.api];
	polyline.layer = this.map.layer;
	polyline.WFSLayer = this;
	var propPoly = this.invoker.go('addPolyline', arguments);
	polyline.setChild(propPoly);
	if(!old) {
		this.myFeatures.push(polyline);
	}
	if(!update) {this.featureadded.fire(polyline);}
	if(this.editionActive&& !active){
		this.invoker.go('activatePolyline',[polyline]);
	}
};

/**
 * Removes a Polyline from the map
 * @param {Polyline} polyline The polyline to remove.
 * @param {Boolean} update If true, doesn't fire the polylineRemoved event. Used by "updateProprietary" method
 */

WFSLayer.prototype.removePolyline = function(polyline,old,update) {
	var current_polyline;
	for(var i = 0; i < this.myFeatures.length; i++){
		current_polyline = this.myFeatures[i];
		if(polyline == current_polyline) {
			this.myFeatures.splice(i, 1);
			this.invoker.go('removePolyline', arguments);
			polyline.onmap = false;
			if(!update) {this.featureremoved.fire(polyline);}
			break;
		}
	}
	for(var i = 0; i < this.features.length; i++){
		current_polyline = this.features[i];
		if(polyline == current_polyline) {
			this.features.splice(i, 1);
			this.invoker.go('removePolyline', arguments);
			polyline.onmap = false;
			
			if (old && typeof polyline.getAttribute('feature') != "undefined"){
				this.removedFeatures.push(polyline.getAttribute('feature'));
			}
			if(!update) {this.featureremoved.fire(polyline);}
			break;
		}
	}
};

/**
 * addMarkerWithData will addData to the marker, then add it to the map
 * @param {Marker} marker The marker to add
 * @param {Object} data A data has to add
 */
WFSLayer.prototype.addMarkerWithData = function(marker, data) {
	marker.addData(data);
	this.addMarker(marker);
};

/**
 * addPolylineWithData will addData to the polyline, then add it to the map
 * @param {Polyline} polyline The polyline to add
 * @param {Object} data A data has to add
 */
WFSLayer.prototype.addPolylineWithData = function(polyline, data) {
	polyline.addData(data);
	this.addPolyline(polyline);
};
	
/**
 * Add a feature to map
 * @param feature Feature type to add. It must be in the ALLOWED_FEATURES Array
 */
WFSLayer.prototype.addFeature = function(feature,data){
	var allowed = false;
	for (var i in this.ALLOWED_FEATURES){
		if(this.ALLOWED_FEATURES[i] === feature){
			allowed = true;
			break;
		} 
	}
	
	if(!allowed){
		alert('This Feature is not Allowed');
		return;
	}
	
	if(this.addingFeature) {return alert("You can't add 2 features at same time!!!");}
	this.addingFeature = true;

	this.invoker.go('addFeature',arguments);
};

/**
 * Activates the edition mode
 */
WFSLayer.prototype.activateEdition = function(){
	if(this.editionActive === true) {return;}
	this.editionActive = true;
	this.invoker.go('activateEdition',arguments);
	
};

/**
 * Deactivates the edition mode
 */
WFSLayer.prototype.deactivateEdition = function(){
	if(this.editionActive === false) {return;}
	this.editionActive = false;
	this.invoker.go('deactivateEdition',arguments);
};

/**
 * Property that stores the layers added to the map in an array
 */
mxn.Mapstraction.prototype.layers = [];

/**
 * addVectorlayer add a Vector Laer to the map
 * @param {VectoLayer} layer that accept features as vector
 */
mxn.Mapstraction.prototype.addVectorLayer = function (layer){
	this.layers[layer.name] = layer;
	layer.api = this.api;
	layer.map = this;
	layer.initialyze(this);
};

mxn.Mapstraction.prototype.removeVectorLayer = function(layer){
	for (var i in this.layers){
		if (this.layers[i] == layer){
			this.layers.splice(i, 1);
			break;
		}
	}
	this.invoker.go('removeFeatures',arguments);
};


var init = function() {
	this.invoker.go('init', [this]);
};

//////////////////
////Tile Layer////
//////////////////
var TileLayer = mxn.TileLayer = function(name,tileURL,copyright,opacity,minZoom,maxZoom){
  this.api = null;
  this.name = name;
  this.tileURL = tileURL;
  this.copyright = copyright;
  this.opacity = opacity;
  this.minZoom = minZoom;
  this.maxZoom = maxZoom;
  this.layer = null;
  this.map = null;
  this.shown = null;
  this.invoker = new mxn.Invoker(this, 'TileLayer', function(){return this.api;});
};

mxn.addProxyMethods(TileLayer, ['toggle']);

mxn.Mapstraction.prototype.addTileLayer = function(tileLayer){
	this.layers.push(tileLayer);
	tileLayer.api = this.api;
	tileLayer.map = this;
	init.apply(tileLayer); 
	tileLayer.shown = true;
};

//////////////////
////WMS Layer/////
//////////////////

var WMSLayer = mxn.WMSLayer = function(name,URL,layerName,opacity){
  this.api = null;
  this.name = name;
  this.URL = URL;
  this.opacity = opacity?opacity:1.0;
  this.layerName = layerName;
  this.layer = null;
  this.map = null;
  this.shown = null;
  
  this.invoker = new mxn.Invoker(this, 'WMSLayer', function(){return this.api;});
};

mxn.addProxyMethods(WMSLayer, ['toggle']);

mxn.Mapstraction.prototype.addWMSLayer = function(wmsLayer){
	this.layers.push(wmsLayer);
	wmsLayer.api = this.api;
	wmsLayer.map = this;
	init.apply(wmsLayer); 
	this.invoker.go('addWMSLayer',arguments);
	wmsLayer.shown = true;
};

mxn.Mapstraction.prototype.removeWMSLayer = function(wmsLayer){
	for (var i in this.layers){
		if(wmsLayer == this.layers[i]){
			this.layers.splice(i,1);
			wmsLayer.shown = false;
			this.invoker.go('removeWMSLayer',arguments);
			break;
		}
	}
};

//////////////////
////XML Layer/////
//////////////////

var XMLLayer = mxn.XMLLayer = function(name, URL){
	this.api = null;
	this.map = null;
	this.name = name;
	this.URL = URL;
	this.layer = null;
	this.shown = null;
	this.invoker = new mxn.Invoker(this, 'XMLLayer', function(){return this.api;});
};

mxn.addProxyMethods(XMLLayer, ['toggle']);

mxn.Mapstraction.prototype.addXMLLayer = function(XMLLayer){
	this.layers.push(XMLLayer);
	XMLLayer.api = this.api;
	XMLLayer.map = this;
	init.apply(XMLLayer); 
	XMLLayer.shown = true;
};
})();






