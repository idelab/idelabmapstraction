mxn.register('worldwind', {	

Mapstraction: {
	
	// 3D functionality
	
	getPitch: function() {
		var map = this.maps[this.api];
		var pitch = map.getPitch();
		return pitch;
	},

	setPitch: function(pitch) {
		var map = this.maps[this.api];
		map.setPitch(pitch);
	},
	
	getHeading: function() {
		var map = this.maps[this.api];
		var heading = map.getHeading();
		return heading;
	},

	setHeading: function(heading) {
		var map = this.maps[this.api];
		map.setHeading(heading);
	},
	
	getFieldOfView: function() {
		var map = this.maps[this.api];
		var fieldOfView = map.getFieldOfView();
		return fieldOfView;
	},

	setFieldOfView: function(fieldOfView) {
		var map = this.maps[this.api];
		map.setFieldOfView(fieldOfView);
	}
	
}

});
