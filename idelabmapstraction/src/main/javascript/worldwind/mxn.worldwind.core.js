mxn.register('worldwind', {	

Mapstraction: {
	
	init: function(element, api) {		
		var me = this;
		
		element.innerHTML = '<applet id="wwj-mxn" name="wwj-mxn" mayscript code="org.jdesktop.applet.util.JNLPAppletLauncher" width=100% height=100%"'+
					  'archive="http://download.java.net/media/applet-launcher/applet-launcher.jar, http://mvn.idelab.uva.es/wwj-idelab/download/worldwind.jar, http://mvn.idelab.uva.es/wwj-idelab/download/wwj-mxn.jar, http://download.java.net/media/jogl/builds/archive/jsr-231-webstart-current/jogl.jar, http://download.java.net/media/gluegen/webstart/gluegen-rt.jar">'+
				   '<param name="jnlp_href" value="http://mvn.idelab.uva.es/wwj-idelab/download/wwj-mxn.jnlp">'+
				   
				   '<param name="codebase_lookup" value="false">'+
				   '<param name="subapplet.classname" value="gov.nasa.worldwind.mxn.IdelabMxnWWJApplet">'+
				   '<param name="subapplet.displayname" value="WWJ Applet">'+
				   '<param name="noddraw.check" value="true">'+
				   '<param name="progressbar" value="true">'+
				   '<param name="jnlpNumExtensions" value="1">'+

				   '<param name="jnlpExtension1" value="http://download.java.net/media/jogl/builds/archive/jsr-231-webstart-current/jogl.jnlp">'+

				   '<param name="InitialLatitude" value="40">'+
				   '<param name="InitialLongitude" value="-3">'+
				   '<param name="InitialAltitude" value="5113000">'+
				   '<param name="InitialHeading" value="0">'+
				   '<param name="InitialPitch" value="0">'+
//				   '<param name="WMSLayer" value="CartociudadWMSCLayer.xml">'+
				'/applet>'
		var theApplet = document.getElementById('wwj-mxn'); 
		try {
		  theApplet = theApplet.getSubApplet();
		} catch (e) {
		  // Using new-style applet -- ignore
		} 
		this.maps[this.api] = theApplet;
	},
	
	//TODO: To interactive script
	addFeature: function(feature,data){
		var map = this.maps[this.api];
		var me = this; 
		switch(feature){
		case "point":
			map.activateMarkerEdition();
			break;

	    case "polygon":
	    case "linestring":
	    	map.activateLineEdition();
			break;
	    }
	},
	
	applyOptions: function(){
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},

	resizeTo: function(width, height){	
		// TODO: Add provider code
		var map = this.maps[this.api];
//		width = width.substring(0,width.indexOf('px'));
//		height = height.substring(0,height.indexOf('px'));
		this.currentElement.style.width = width;
		this.currentElement.style.height = height;
		//map.setSize(width, height);
	},

	addControls: function( args ) {
		var map = this.maps[this.api];
	
		if (args.zoom) {
			map.addSmallControlls();
		}
		else {
			map.removeSmallControls();
		}
		
	},

	addSmallControls: function() {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},

	addLargeControls: function() {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},

	addMapTypeControls: function() {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},

	dragging: function(on) {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},

	setCenterAndZoom: function(point, zoom) { 
		var map = this.maps[this.api];
		var pt = point.toProprietary(this.api);
		
		// TODO: Add provider code
	},
	
	addMarker: function(marker, old) {
		var map = this.maps[this.api];
		var pin = marker.toProprietary(this.api);
		
		// TODO: Add provider code
		
		return pin;
	},

	removeMarker: function(marker) {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},

	removeAllMarkers: function() {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},
	
	declutterMarkers: function(opts) {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},

	addPolyline: function(polyline, old) {
		var map = this.maps[this.api];
		var pl = polyline.toProprietary(this.api);
		
		// TODO: Add provider code
		
		return pl;
	},

	removePolyline: function(polyline) {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},
	
	getCenter: function() {
		var point;
		var map = this.maps[this.api];
		
		var center = map.getCenter();
		var lat_lon = center.split(";");
		var lat = lat_lon[0];
		var lon = lat_lon[1];
		return new mxn.LatLonPoint(lat, lon);
	},

	setCenter: function(point, options) {
		var map = this.maps[this.api];
		var pt = point.toProprietary(this.api);
		if(options && options['pan']) { 
			map.panToCenter(point.lat,point.lon);
		}
		else { 
			map.setCenter(point.lat,point.lon);
		}
	},

	setZoom: function(zoom) {
		var map = this.maps[this.api];
		map.setZoomLevel(zoom);
	},
	
	getZoom: function() {
		var map = this.maps[this.api];
		var zoom = map.getZoomLevel();
		return zoom;
	},

	getZoomLevelForBoundingBox: function( bbox ) {
		var map = this.maps[this.api];
		// NE and SW points from the bounding box.
		var ne = bbox.getNorthEast();
		var sw = bbox.getSouthWest();
		var zoom;
		
		// TODO: Add provider code
		
		return zoom;
	},

	setMapType: function(type) {
		var map = this.maps[this.api];
		switch(type) {
			case mxn.Mapstraction.ROAD:
				// TODO: Add provider code
				break;
			case mxn.Mapstraction.SATELLITE:
				// TODO: Add provider code
				break;
			case mxn.Mapstraction.HYBRID:
				// TODO: Add provider code
				break;
			default:
				// TODO: Add provider code
		}	 
	},

	getMapType: function() {
		var map = this.maps[this.api];
		
		// TODO: Add provider code

		//return mxn.Mapstraction.ROAD;
		//return mxn.Mapstraction.SATELLITE;
		//return mxn.Mapstraction.HYBRID;

	},

	getBounds: function () {
		var map = this.maps[this.api];
		
		var bounds_str = map.getMapBounds().split(";");
		var minx = bounds_str[0];
		var miny = bounds_str[1];
		var maxx = bounds_str[2];
		var maxy = bounds_str[3];
		return new mxn.BoundingBox( minx , miny , maxx , maxy );
	},

	setBounds: function(bounds){
		var map = this.maps[this.api];
		var sw = bounds.getSouthWest();
		var ne = bounds.getNorthEast();
		var minLongitude = sw.lon;
		var minLatitude = sw.lat;
		var maxLongitude = ne.lon;
		var maxLatitude = ne.lat;
		// TODO: Add provider code
		map.setMapBounds(minLongitude, minLatitude, maxLongitude, maxLatitude);
	},

	addImageOverlay: function(id, src, opacity, west, south, east, north, oContext) {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
	},

	setImagePosition: function(id, oContext) {
		var map = this.maps[this.api];
		var topLeftPoint; var bottomRightPoint;

		// TODO: Add provider code

		//oContext.pixels.top = ...;
		//oContext.pixels.left = ...;
		//oContext.pixels.bottom = ...;
		//oContext.pixels.right = ...;
	},
	
	addOverlay: function(url, autoCenterAndZoom) {
		var map = this.maps[this.api];
		
		// TODO: Add provider code
		
	},

	addTileLayer: function(tile_url, opacity, copyright_text, min_zoom, max_zoom) {
		var map = this.maps[this.api];
		
		map.addTileLayer(tile_url);
	},

	toggleTileLayer: function(tile_url) {
		var map = this.maps[this.api];
		
		map.toggleTileLayer(tile_url);
	},

	getPixelRatio: function() {
		var map = this.maps[this.api];

		// TODO: Add provider code	
	},
	
	mousePosition: function(element) {
		var map = this.maps[this.api];

		// TODO: Add provider code	
	}
},

LatLonPoint: {
	
	toProprietary: function() {
		// TODO: Add provider code
	},

	fromProprietary: function(googlePoint) {
		// TODO: Add provider code
	}
	
},

Marker: {
	
	toProprietary: function() {
		// TODO: Add provider code
	},

	openBubble: function() {		
		// TODO: Add provider code
	},

	hide: function() {
		// TODO: Add provider code
	},

	show: function() {
		// TODO: Add provider code
	},

	update: function() {
		// TODO: Add provider code
	}
	
},

Polyline: {

	toProprietary: function() {
		// TODO: Add provider code
	},
	
	show: function() {
		// TODO: Add provider code
	},

	hide: function() {
		// TODO: Add provider code
	}
	
}

});
