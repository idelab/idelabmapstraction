Antes de "mvn package", "mvn install" o "mvn deploy" se debe hacer "mvn clean" (ya que el plugin de javascript da problemas al crear el .jar por segunda vez)

Antes de "mvn site" hacer "mvn install" (ya que TODOS los recursos los coge del repositorio local en lugar de la carpeta target)

Antes de subir una nueva versi�n estable de la librer�a, descomentar las l�neas correspondientes para que se suban correctamente los ficheros a la ra�z de la carpeta downloads.

Para subir nueva version: mvn clean install site site-deploy